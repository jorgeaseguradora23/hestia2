'use strict';
$(document).ready(function () {


	//Alert confirm
	document.querySelector('.alert-confirm').onclick = function(){
		swal({
				title: "¿Estas seguro?",
				text: "Una vez registrada la unidad no podrá eliminarse!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Si, de acuerdo!",
				closeOnConfirm: false
			},
			function(){
				$("#btnTerminar").val("1");
				$("#login-form").submit();

			});
	};









	$('#openBtn').on('click',function () {
		$('#myModal').modal({
			show: true
		})
	});

	$(document).on('show.bs.modal', '.modal', function (event) {
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() {
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});
});
