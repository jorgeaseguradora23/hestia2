<?php
/* @var $this ClienteController */
/* @var $dataProvider CActiveDataProvider */
?>

<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-ui-chat bg-danger"></i>

                    <div class="d-inline">
                        <h4>Quejas y Sugerencias</h4>
                        <span>Seguimiento a clientes en quejas y sugerencias.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 right">

                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Quejas y Sugerencias</a>
                        </li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">
        <div class="row">
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-pie-chart bg-c-yellow card1-icon"></i>
                        <span class="text-warning f-w-600">Quejas Pendientes</span>
                        <h4>6 seguimiento</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-blue f-16 icofont icofont-warning m-r-10"></i>Get more space
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-ui-home bg-c-pink card1-icon"></i>
                        <span class="text-c-pink f-w-600">Quejas tono negativo</span>
                        <h4>255 Activos</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-pink f-16 icofont icofont-calendar m-r-10"></i>Last 24 hours
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-warning-alt bg-c-green card1-icon"></i>
                        <span class="text-c-green f-w-600">Quejas o Sugerencias Positivas</span>
                        <h4>15 Clientes</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-green f-16 icofont icofont-tag m-r-10"></i>Tracked from microsoft
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-social-twitter bg-c-yellow card1-icon"></i>
                        <span class="text-c-yellow f-w-600">Quejas Cerradas</span>
                        <h4>63 Activos</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-yellow f-16 icofont icofont-refresh m-r-10"></i>Just update
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->




        </div>
        <!-- Config. table start -->
        <div class="card">
            <div class="card-header">
                <h5>Resultados de la Consulta</h5>
                <span>Top 10 Clientes alto nivel</span>
                <div class="card-header-right" style="margin-right: 10px;">
                    <a href="<?=CController::createUrl('quejasugerencia/create') ?>"><button class="btn btn-sm btn-info">NUEVA QUEJA O SUGERENCIA</button></a>
                    <button class="btn btn-sm btn-primary waves-effect"  data-toggle="modal" data-target="#default-Modal">FILTRAR</button>
                    <button class="btn btn-sm btn-success waves-effect" >CSV</button>
                    <button class="btn btn-sm btn-danger waves-effect"  >PDF</button>


                </div>
            </div>
            <div class="card-block">
                <div class="">
                    <div class="dt-responsive table-responsive" >
                        <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="row" >
                                <div class="col-xs-12 col-sm-12 col-lg-12" >
                                    <table width="100%" id="res-config" class="table compact table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 1544px; font-size: 12px; ">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 89px;" aria-label="Salary: activate to sort column ascending">Alta</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >Cliente</th>

                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 117px;" aria-label="Start date: activate to sort column ascending">Empleado</th>

                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px; display: none;" aria-label="">Tono</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;" >Estatus</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 115px;" >Celula</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px; display: none;" aria-label="">Método Contacto</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px; display: none;" aria-label="E-mail: activate to sort column ascending">Acciones</th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($model as $queja){
                                            $color="";
                                            if($queja->estatus=="P"){
                                                $color="warning";
                                            }elseif($queja->estatus=="A"){
                                                $color="success";
                                            }elseif($queja->estatus=="C"){
                                                $color="danger";
                                            }
                                            ?>
                                            <tr role="row" class="odd" style="font-size: 12px;">
                                                <td><?=$queja->fechaAlta ?> | <?=$queja->usuario ?></td>
                                                <td tabindex="0" class="sorting_1"><?=getTxtCte($queja->idCliente) ?> - <?=$queja->nombre ?></td>
                                                <td>E<?=getTxtCte($queja->idEmpleado) ?> - <?=$queja->empleado ?></td>

                                                <td style=""><label class="label label-md bg-<?=$queja->color ?>" style="min-width: 40px; text-align: center; "><?=$queja->tono ?></label></td>
                                                <td style=""><label class="label label-md bg-<?=$color ?>" style="min-width: 65px; text-align: center; "><?=$queja->txtEstatus ?></label></td>
                                                <td><?=$queja->celula ?></td>
                                                <td><?=$queja->metodoContacto ?></td>
                                                <td >

                                                    <a class="btn btn-mini btn-info" id="btnAgregar"
                                                            href="<?=CController::createUrl('quejasugerencia/seguimiento') ?>?idQS=<?=$queja->id ?>"
                                                            name="btnAgregar" value="1" onclick=""> SEGUIMIENTO</a>
                                                    <button class="btn btn-mini btn-primary"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=ucfirst(strtolower($queja->descripcion)) ?>"
                                                            type="button" id="btnAgregar" title="Detalle" name="btnAgregar" value="1" onclick="" style="max-width: 10px;"><i class="icofont icofont-file-text"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>


                                        </tbody>
                                    </table>
                                    </br>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Config. table end -->

    </div>
</div>
<!-- MODAL FILTROS -->
<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-header bg-info" >
                <h4 class="modal-title">Filtros de Búsqueda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <p>Selecciona uno o varios campos de búsqueda, a continuación presiona 'Aplicar' para ejecutar tu petición.</p>
                <div class="container">
                    <div class="ccol-lg-12 col-md-12 m-b-15">
                        <h4 class="sub-title text-muted"><strong>Opciones de búsqueda</strong></h4>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            $tiposCte = CHtml::listData(Cattipocliente::model()->findAll(), 'id', 'descripcion');
                        //    echo $form->dropDownList($ClienteBusqueda, 'idCatTipoCliente', $tiposCte, array('class' => "form-control", 'placeholder' => "Tipo ...", 'empty' => "Tipo ..."));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                         //   echo $form->dropDownList($ClienteBusqueda, 'categoria', array("P" => "PROSPECTO","C" => "CLIENTE"), array('class' => "form-control", 'placeholder' => "Categoria ...", 'empty' => "Categoria ..."));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            $sectores = CHtml::listData(Catsector::model()->findAll(), 'id', 'descripcion');
                        //    echo $form->dropDownList($ClienteBusqueda, 'idCatSector', $sectores, array('class' => "form-control", 'placeholder' => "Sector ...", 'empty' => "Sector ..."));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            $ponderaciones = CHtml::listData(Catponderacion::model()->findAll(), 'id', 'descripcion');
                        //    echo $form->dropDownList($ClienteBusqueda, 'idCatPonderacion', $ponderaciones, array('class' => "form-control", 'placeholder' => "Ponderación ...", 'empty' => "Ponderación ..."));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            $sectores = CHtml::listData(Catfuentecontratacion::model()->findAll(), 'id', 'descripcion');
                        //    echo $form->dropDownList($ClienteBusqueda, 'idCatFuenteContratacion', $sectores, array('class' => "form-control", 'placeholder' => "Fuente Contratación ...", 'empty' => "Fuente Contratación ..."));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                        //    echo $form->dropDownList($ClienteBusqueda, 'estatus', array("A"=>"ACTIVO","B" => "BAJA"), array('class' => "form-control", 'placeholder' => "Estatus ...", 'empty' => "Estatus ..."));
                            ?>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>
                <a href="<?=CController::createUrl('unidad/index') ?>"> <button type="button" class="btn btn-sm btn-warning waves-effect waves-light ">LIMPIAR</button></a>
                <button type="submit" class="btn btn-sm btn-info waves-effect waves-light ">APLICAR</button>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
