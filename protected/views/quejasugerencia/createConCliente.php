
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-exclamation-tringle bg-danger"></i>

                    <div class="d-inline">
                        <h4>Nueva Queja / Sugerencia</h4>
                        <span>Ingrese todos los campos solictados, toda la información es obligatoria.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 right">

                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Queja o Sugerencia</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Nueva queja o sugerencia</a>
                        </li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'nuevaQs-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>
                    <div class="card">

                        <div class="card-header bg-success">
                            <h5 class="text-white">INFORMACIÓN PARA EL REGISTRO</h5>
                            <div class="card-header-right" style="margin-right: 10px;">
                                <a href="<?=CController::createUrl('quejasugerencia/create') ?>">
                                    <button class="btn btn-sm btn-success btn-out-dashed" type="button">SALIR
                                    </button>
                                </a>

                            </div>
                        </div>
                            <?php
                            if(!empty($NuevaQueja->errors)){
                                ?>
                        <div class="col-12">
                            <br>

                            <div class="alert alert-danger icons-alert" style="font-size: 12px;">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="icofont icofont-close-line-circled"></i>
                                    </button>
                                    <p><strong>Atención!</strong> <?=$form->errorSummary($NuevaQueja) ?></p>
                                </div>
                        </div>


                                <?php
                            }
                            ?>
                        <div class="card-body">
                            <div style="padding-left: 30px; padding-right: 30px;padding-top: 20px;padding-bottom: 10px;">
                                <div class="row">
                                    <div class="col-lg-3 col-md-12 m-b-15">
                                        <label class="text-muted" style="font-size: 13px;">Tipo de evento</label>
                                        <?php

                                        echo $form->dropDownList($NuevaQueja, 'tipo', array("Q" => "QUEJA","S" => "SUGERENCIA"), array('class' => "form-control",
                                            'placeholder' => "Empleado ...",
                                            'onchange' => "setTono()",
                                            'empty' => "-- Selecciona --"));
                                        ?>                                </div>
                                    <div class="col-lg-3 col-md-12 m-b-15">
                                        <label class="text-muted" style="font-size: 13px;">Indica el Tono del evento</label>
                                        <?php
                                        $atrDis="";
                                        $valDis="";
                                        if($NuevaQueja->tipo=="Q"){
                                            $atrDis="disabled";
                                            $valDis="disabled";
                                        }else{

                                        }
                                        $tonos = CHtml::listData(Cattono::model()->findAll(), 'id', 'descripcion');
                                        echo $form->dropDownList($NuevaQueja, 'idCatTono', $tonos, array('class' => "form-control", $atrDis =>  $valDis,'placeholder' => "Tono ...", 'empty' => "-- Selecciona --"));
                                        ?>
                                    </div>

                                    <div class="col-lg-3 col-md-12 m-b-15">
                                        <label class="text-muted" style="font-size: 13px;">Celúla a la que pertenece</label>
                                        <?php
                                        $celulas = CHtml::listData(Catcelula::model()->findAll(), 'id', 'descripcion');
                                        echo $form->dropDownList($NuevaQueja, 'idCatCelula', $celulas, array('class' => "form-control", 'placeholder' => "Sector ...", 'empty' => "-- Selecciona --"));
                                        ?>
                                    </div>
                                    <div class="col-lg-3 col-md-12 m-b-15">
                                        <label class="text-muted" style="font-size: 13px;">Método contacto queja o sugerencia</label>
                                        <?php
                                        $metodos = CHtml::listData(Catmetodocontacto::model()->findAll(), 'id', 'descripcion');
                                        echo $form->dropDownList($NuevaQueja, 'idCatMetodoContacto', $metodos, array('class' => "form-control", 'placeholder' => "Método Contacto ...", 'empty' => "-- Selecciona --"));
                                        ?>
                                    </div>

                                    <div class="col-lg-12 col-md-12 m-b-15">
                                        <label class="text-muted" style="font-size: 13px;">Descripción queja o suegrencia</label>
                                        <?php echo $form->textArea($NuevaQueja, 'descripcion', array('class' => "form-control" ,"rows"=> "5",'placeholder' => "Descripción de la Queja o Sugerencia",'title' => "idCliente",  'value' => $NuevaQueja->descripcion)); ?>
                                        <?php echo $form->hiddenField($NuevaQueja, 'idCliente', array('class' => "form-control" ,'placeholder' => "idCliente",'title' => "idCliente",  'value' => $_GET["id"])); ?>

                                    </div>

                                    <div class="col-lg-8">
                                        <label class="text-muted" style="font-size: 13px;">Responsable de la queja o sugerencia</label>
                                        <?php
                                        $empleados = CHtml::listData(Viewempleado::model()->findAll(), 'id', 'nombre');
                                        echo $form->dropDownList($NuevaQueja, 'idEmpleado', $empleados, array('class' => "form-control", 'placeholder' => "Empleado ...", 'empty' => "-- Selecciona --"));
                                        ?>
                                    </div>

                                    <div class="col-lg-4">
                                        <label class="text-muted" style="font-size: 13px;">Tipo de incidencia</label>
                                        <?php
                                        $indicencias = CHtml::listData(ViewIncidencia::model()->findAll(), 'id', 'descripcion');
                                        echo $form->dropDownList($NuevaQueja, 'idCatTipoIncidencia', $indicencias, array('class' => "form-control", 'placeholder' => "Empleado ...", 'empty' => "-- Selecciona --"));
                                        ?>
                                    </div>

                                </div>
                                <div class="row float-right mt-4">
                                    <div class="col-lg-12 col-md-12 m-b-15">
                                        <button class="btn btn-sm btn-success sendForm " onclick="enviaForm();" id="agregarProducto"><i class="fas fa-save mr-2"></i> GUARDAR QUEJA O SUGERENCIA</button>
                                    </div>

                                </div>






                            </div>



                        </div>
                    </div>






                <?php $this->endWidget(); ?>             <!-- tab content end -->
            </div>
        </div>


        <!-- Config. table start -->

        <!-- Config. table end -->

    </div>
</div>

<!-- MODAL REGISTRO -->
<div class="modal fade" id="mdNuevaQS" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header bg-info" >
                <h4 class="modal-title">Nueva Queja o Sugerencia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <p>Ingresa todos los campos solicitados para realizar una queja o sugerencia del cliente <strong><label id="lblCte" name="lblCte" ></label></strong>.</p>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>
                <a href="<?=CController::createUrl('quejasugerencia/create') ?>"> <button type="button" class="btn btn-sm btn-warning waves-effect waves-light ">LIMPIAR</button></a>
                <button type="button" onclick="" class="btn btn-sm btn-info waves-effect waves-light " id="btnGuardar" name="btnGuardar">GUARDAR</button>
            </div>

        </div>
    </div>
</div>
<script>
    function setQuejaCliente(idCliente,txtCte){
        $("#Quejasugerencia_idCliente").val(idCliente);
        $("#lblCte").html(txtCte);
    }
    
    function setTono() {
        if($("#Quejasugerencia_tipo").val()!=""){
            if($("#Quejasugerencia_tipo").val()=="Q"){
                $("#Quejasugerencia_idCatTono").prop("disabled",true);
                $("#Quejasugerencia_idCatTono").val("2");
            }else{
                $("#Quejasugerencia_idCatTono").prop("disabled",false);
                $("#Quejasugerencia_idCatTono").val("");
            }
        }

    }

    function enviaForm() {

        if($("#Quejasugerencia_tipo").val()!=""){
            if($("#Quejasugerencia_tipo").val()=="Q"){
                $("#Quejasugerencia_idCatTono").prop("disabled",false);
                $("#Quejasugerencia_idCatTono").val("2");
            }
        }
        $("#nuevaQs-form").submit();

    }





</script>