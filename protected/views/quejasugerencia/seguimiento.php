<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="span-19">
            <div id="content">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <i class="icofont icofont-user-alt-3 bg-success"></i>

                                    <div class="d-inline">

                                        <h4><?=getTxtCte($queja->idCliente) ?> - <?=$queja->nombre ?> </h4>
                                        <span>Folio: <?=$queja->id ?> | <?=$queja->fechaAlta ?> | <?=$queja->usuario ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="index.html">
                                                <i class="icofont icofont-home"></i>
                                            </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Quejas y Seguimiento</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Seguimiento</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->


                    <div class="row">

                        <!-- user card start -->
                        <div class="col-sm-4">
                            <?php
                            $color="";
                            if($queja->estatus=="P"){
                                $color="warning";
                            }elseif($queja->estatus=="A"){
                                $color="success";
                            }elseif($queja->estatus=="C"){
                                $color="danger";
                            }
                            ?>
                            <div class="card bg-<?=$color ?> text-white widget-visitor-card">
                                <div class="card-block-small text-center">
                                    <h2><?=ucwords(mb_strtolower($queja->txtEstatus)) ?></h2>
                                    <h6><?=ucwords(mb_strtolower($queja->textTipo)) ?></h6>
                                    <i class="ti-user"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card bg-<?=(($queja->color)) ?> text-white widget-visitor-card">
                                <div class="card-block-small text-center">
                                    <h2><?=ucwords(mb_strtolower($queja->tono)) ?></h2>
                                    <h6>Tono <?=ucwords(mb_strtolower($queja->textTipo)) ?></h6>
                                    <i class="icofont icofont-paper"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card bg-c-lite-green text-white widget-visitor-card">
                                <div class="card-block-small text-center">
                                    <h2><?=ucwords(mb_strtolower($queja->celula)) ?></h2>
                                    <h6>Célula</h6>
                                    <i class="icofont icofont-ui-alarm"></i>
                                </div>
                            </div>
                        </div>
                        <!-- user card end -->



                    </div>

                    <!-- Page-body start -->
                    <div class="page-body">
                        <!-- Datos Generales -->
                        <div class="row">
                            <div class="col-md-4">
                                <!-- Comentario del cliente -->
                                <div class="card bg-c-green green-contain-card ">
                                    <div class="card-header bg-success">
                                        <div class="card-header-left">
                                            <h5 class="text-white">Descripción de <?=ucwords(strtolower($queja->textTipo)) ?></h5>
                                        </div>
                                        <div class="card-header-right">

                                            <A class="btn btn-sm btn-success btn-out-dashed"  href="<?=CController::createUrl('quejasugerencia/index') ?>" style="margin-top: -6px;">REGRESAR</A>
                                        </div>
                                    </div>

                                    <div class="card m-b-0 " style="min-height: 230px;max-height: 230px;">
                                        <div class=" card-block-big p-t-50 p-b-50 ">

                                            <div class="content-box" style="color: gray;  text-align: center; ">
                                                <!-- Your text -->


                                                <p><?=ucfirst(strtolower($queja->descripcion)) ?></p>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <!-- fin comentario -->

                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-block user-detail-card" style="min-height: 300px; max-height: 300px;">
                                                <div class="row" style="padding: 20px;">

                                                    <div class="col-sm-12 user-detail">
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h7 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Empleado :</h7>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h7 class="m-b-30"><strong>E<?=getTxtCte($queja->idEmpleado) ?> <?=ucwords(mb_strtolower($queja->empleado)) ?></strong></h7>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-home"></i>Método Contacto :</h7>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h7 class="m-b-30" style="font-size: 12px;"><?=ucfirst(mb_strtolower($queja->metodoContacto)) ?>                                               </h7>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-home"></i>Tipo Incidencia :</h7>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h7 class="m-b-30" style="font-size: 12px;"><?=ucfirst(mb_strtolower($queja->tipoIncidencia)) ?>                                               </h7>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-calendar"></i>Cierre de <?=ucwords(mb_strtolower($queja->textTipo)) ?>:</h7>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h7 class="m-b-30" style="font-size: 12px;"><?=empty($queja->fechaCierre)?"En Proceso ...":$queja->fechaCierre." | ".$queja->usuarioCierre ?></h7>


                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-calendar"></i>Acuerdo:</h7>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h7 class="m-b-30" style="font-size: 12px;"><?=empty($queja->fechaAcuerdo)?"En Espera ...":$queja->fechaAcuerdo." | ".$queja->acuerdo." | ".$queja->textEstatusAcuerdo ?></h7>
                                                                <?php
                                                                if(isset($queja->observacionAcuerdo)){
                                                                    ?>
                                                                    <i class="icofont icofont-ui-messaging text-warning float-right" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=ucfirst(strtolower($queja->observacionAcuerdo)) ?>" style="cursor: pointer"></i>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-user"></i>Responsable Final:</h7>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <h7 class="m-b-30">
                                                                    <strong>
                                                                        <?=isset($queja->empleadoCierre)?"E".getTxtCte($queja->idEmpleadoCierre)." ".ucwords(mb_strtolower($queja->empleadoCierre)):"En Espera ..." ?>
                                                                    </strong>
                                                                </h7>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col-md-7">
                                <!-- historico basico -->
                                <div class="card">
                                    <div class="card-header bg-primary">
                                        <div class="card-header-left">
                                            <h5 class="text-white">Seguimiento de la <?=ucwords(strtolower($queja->textTipo)) ?></h5>
                                        </div>
                                        <div class="card-header-right">
                                            <?php
                                            if($queja->estatus!="C"){
                                                ?>
                                                <button class="btn btn-sm btn-primary btn-out-dashed"  data-toggle="modal" data-target="#mdNuevoSeguimiento" type="button" style="margin-top: -6px;">AGREGAR</button>
                                            <?php
                                            }
                                            ?>




                                        </div>
                                    </div>
                                    <div class="card-block mt-4" style="min-height: 350px; max-height: 350px; overflow-y: auto; overflow-x: hidden  ">

                                            <?php
                                            if(!empty($seguimientos)){
                                                foreach ($seguimientos as $seguimiento){
                                                    ?>
                                                    <div class="card-comment ">
                                                        <div class="card-block-small">
                                                            <img class="img-radius img-50" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/user2.png " alt="user-1">
                                                            <div class="comment-desc">
                                                                <h6><?=ucwords(strtolower($seguimiento->nombre)) ?> (<?=$seguimiento->usuario ?>)</h6>
                                                                <p style="color: #0b9c8f; font-size: 12px;"><?=ucfirst(strtolower($seguimiento->comentario)) ?></p>
                                                                <div class="comment-btn">


                                                                    <label class="label label-md bg-success " style="text-align: center; font-size: 11px; "><?=$seguimiento->tipoSeguimiento ?></label>


                                                                </div>
                                                                <div class="date">
                                                                    <span style="font-size: 11px; color: gray"><?=$seguimiento->fechaAlta ?></span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                        <?php
                                                }
                                                ?>

                                            <?php
                                            }else{
                                                ?>
                                                <div class="content-box" style="color: gray;  text-align: center; margin-top: 70px;">
                                                    <!-- Your text -->
                                                    <h1>Vaya! No hay ningún<br>comentario</h1>

                                                    <p>Si deseas agregar algun seguimiento<br>
                                                        presiona en el botón de agreagar en esta sección.</p>
                                                </div>
                                                <?php
                                            }
                                            ?>




                                    </div>


                                </div>
                                <!-- fin historico basico -->
                            </div>
                            <div class="col-md-5">
                                <!-- historico basico -->
                                <div class="card">
                                    <div class="card-header bg-warning">
                                        <div class="card-header-left">
                                            <h5 class="text-white">Adjuntos de la <?=ucwords(strtolower($queja->textTipo)) ?></h5>
                                        </div>
                                        <div class="card-header-right">
                                            <?php
                                            if($queja->estatus!="C"){
                                                ?>
                                                <button class="btn btn-sm btn-warning btn-out-dashed" type="button"  data-toggle="modal" data-target="#mdNuevoAdjunto" style="margin-top: -6px;">AGREGAR</button>
                                                <?php
                                            }
                                            ?>



                                        </div>
                                    </div>
                                    <div class="card-block mt-4" style="min-height: 350px; max-height: 350px; overflow-y: auto; overflow-x: hidden  ">


                                        <?php
                                        if(!empty($adjuntos)){
                                            foreach ($adjuntos as $adjunto){
                                                ?>
                                                <div class="card-comment ">
                                                    <div class="card-block-small">

                                                        <?php
                                                        $bolImagen=false;
                                                        $ext=pathinfo($adjunto->archivo, PATHINFO_EXTENSION);
                                                        $allowed =  array('png' ,'jpg','jpeg','PNG','JPG','JPEG');
                                                        $excel = array('XLSX','XLS');
                                                        $word = array('DOC','DOCX');
                                                        $comprimido = array('ZIP','RAR');
                                                        $mensajes = array('MSG');
                                                        $pdfs = array('PDF');
                                                        $archivos = array('ZIP','RAR','PDF','DOCX','XLSX','XLS');
                                                        $archivo="";
                                                        if(in_array(strtoupper($ext),$allowed)){
                                                            //CASO ARCHIVOS DE IMAGEN
                                                            $archivo=  Yii::app()->request->baseUrl."/library/qs/".$adjunto->archivo ;
                                                            $bolImagen=true;

                                                        }elseif (in_array(strtoupper($ext),$excel)){
                                                            $archivo=Yii::app()->request->baseUrl."/images/icons/excel.png";

                                                        }elseif (in_array(strtoupper($ext),$word)){
                                                            $archivo=Yii::app()->request->baseUrl."/images/icons/word.png";

                                                        }
                                                        elseif (in_array(strtoupper($ext),$comprimido)){
                                                            $archivo=Yii::app()->request->baseUrl."/images/icons/zip.png";
                                                        }
                                                        elseif (in_array(strtoupper($ext),$pdfs)){
                                                            $archivo=Yii::app()->request->baseUrl."/images/icons/pdf.png";
                                                        }
                                                        elseif (in_array(strtoupper($ext),$mensajes)){
                                                            $archivo=Yii::app()->request->baseUrl."/images/icons/outlook.png";
                                                        }
                                                        ?>
                                                        <?php
                                                        if($bolImagen==true){
                                                            ?>

                                                            <a href="<?=$archivo ?>" data-lightbox="1" data-title="<?=ucfirst(mb_strtolower($adjunto->adjunto)) ?>">
                                                                <img src="<?=$archivo ?>" data-toggle="tooltip" title="" alt="" class="img-40" data-original-title="Ver Imagen">
                                                            </a>
                                                            <?php
                                                        }else{
                                                            ?>
                                                            <a href="<?=Yii::app()->request->baseUrl ?>/library/qs/<?=$adjunto->archivo ?>" target="_blank">
                                                                <img src="<?=$archivo ?>" data-toggle="tooltip" title="" alt="" class="img-40" data-original-title="Descargar Archivo">
                                                            </a>

                                                            <?php
                                                        }
                                                        ?>



                                                        <div class="comment-desc " style="margin-left: -15px;">
                                                            <h6 class="tex"><?=ucwords(mb_strtolower($adjunto->nombre)) ?>
                                                                <small class="ml-2 text-info">Agregó adjunto</small>
                                                                <br><span style="font-size: 11px; margin-top: -10px;" class="text-danger"><i class="fas fa-calendar mr-1" aria-hidden="true"></i> <?=$adjunto->fechaAlta ?></span>
                                                            </h6>

                                                            <p style=" font-size: 12px;">"<?=ucfirst(mb_strtolower($adjunto->adjunto)) ?>"</p>


                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                            }
                                            ?>

                                            <?php
                                        }else{
                                            ?>
                                            <div class="content-box" style="color: gray;  text-align: center; margin-top: 70px;">
                                                <!-- Your text -->
                                                <h1>Vaya! No hay<br>ningún adjunto</h1>

                                                <p>Si deseas agregar algun texto o correo<br>
                                                    presiona en el botón de agregar en esta sección.</p>
                                            </div>
                                            <?php
                                        }
                                        ?>





                                    </div>
                                </div>
                                <!-- fin historico basico -->
                            </div>

                        </div>
                        <!-- fin Datos Generales -->
                        <!-- Encabezado -->

                        <!-- Fin encabezado -->





                        <?php
                        if($queja->estatus!="C"){
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-lg btn-danger col-12"  data-toggle="modal" data-target="#mdCerrar">Cerrar <?=ucwords(strtolower($queja->textTipo)) ?></button>
                                </div>

                            </div>
                            <?php
                        }
                        ?>



                    </div>
                    <!-- Page-body end -->
                </div>
            </div><!-- content -->
        </div>
        <div class="span-5 last">
            <div id="sidebar">
            </div><!-- sidebar -->
        </div>


    </div>
</div>


<div class="modal fade" id="mdNuevoSeguimiento" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'seguimiento-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
                <div class="modal-header bg-primary">
                    <h4 class="modal-title">Nuevo Seguimiento</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body ">
                  <div class="container">
                      <p>Registro de algún comentario o seguimiento de la <?=ucwords(strtolower($queja->textTipo)) ?>.</p>
                      <?php
                      if(!empty($nuevoSeguimiento->errors)){
                          ?>
                          <div class="alert alert-danger icons-alert" style="font-size: 12px;">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <i class="icofont icofont-close-line-circled"></i>
                              </button>
                              <p><strong>Atención!</strong> <?=$form->errorSummary($nuevoSeguimiento) ?></p>
                          </div>

                          <?php
                      }
                      ?>
                      <div class="container">
                          <div class="row ">
                              <div class="col-12 ">
                                  <div class="col-lg-12 col-md-12 ">
                                      <?php
                                      $seguimientos = CHtml::listData(ViewTipoSeguimiento::model()->findAll(), 'id', 'descripcion');
                                      echo $form->dropDownList($nuevoSeguimiento, 'idCatTipoSeguimiento', $seguimientos, array('class' => "form-control", 'placeholder' => "Método Contacto ...", 'empty' => "Tipo Seguimiento ..."));
                                      ?>
                                  </div>
                              </div>
                              <div class="col-12 mt-3">
                                  <div class="col-lg-12 col-md-12 ">
                                      <?php echo $form->textArea($nuevoSeguimiento, 'comentario', array('class' => "form-control" ,'rows'=>"3",'placeholder' => "Comentario de Seguimiento",'title' => "Comentario de Seguimiento",  'value' => $nuevoSeguimiento->comentario)); ?>
                                  </div>

                              </div>
                              <div class="col-12 mt-3">
                                  <div class="col-lg-12 col-md-12 mb-3 ">
                                      <?php
                                      echo $form->dropDownList($nuevoSeguimiento, 'participacionTercero', array("S" => "PARTICIPACION POSITIVA","N" => "SIN PARTICIPACION"), array('class' => "form-control", 'placeholder' => "Método Contacto ...", 'empty' => "Participacion del Empleado ..."));
                                      ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>

                    <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light " id="btnGuardarSeg" name="btnGuardarSeg" value="1">GUARDAR</button>
                </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>


<div class="modal fade" id="mdNuevoAdjunto" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'adjunto-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'enableAjaxValidation'=>false,
                'htmlOptions' => array('enctype'=>'multipart/form-data'), // la
            ));
            ?>
            <div class="modal-header bg-warning">
                <h4 class="modal-title">Nuevo ingreso de Adjunto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body ">
                <div class="container">
                    <label class="text-secondary" style="font-size: 12px;">Registro de adjuntos de  <?=ucwords(strtolower($queja->textTipo)) ?>.  Agrega un comentario o identificaión del archivo a adjuntar. Archivos permitidos: Word, Excel, PDF, ZIP, RAR o MSG (Outlook) e imagenes.</label>
                   <br><span class="text-danger" style="font-size: 12px;">Ambos campos son obligatorios</span>
                    <?php
                    if(!empty($nuevoAdjunto->errors)){
                        ?>
                        <div class="alert alert-danger icons-alert" style="font-size: 12px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="icofont icofont-close-line-circled"></i>
                            </button>
                            <p><strong>Atención!</strong> <?=$form->errorSummary($nuevoAdjunto) ?></p>
                        </div>

                        <?php
                    }
                    ?>
                        <div class="row ">



                            <div class="col-sm-12">
                                <div class="input-group input-group-button">
                                    <?php echo $form->textArea($nuevoAdjunto, 'adjunto', array('class' => "form-control" ,'rows'=>"4",'required' => 'required',
                                        'placeholder' => "Agrega un comentario o seguimiento del proceso",'title' => "Comentario de Seguimiento",  'value' => $nuevoSeguimiento->comentario)); ?>

                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="input-group input-group-warning">

                                    <input type="text" class="form-control fileDownBoton" placeholder="Seleccion un archivo ..." style="background-color: white" required>

                                    <span class="input-group-addon fileUpBoton">
                                       <i class="fas fa-arrow-circle-up"></i>
                                    </span>
                                </div>
                            </div>
                            <?php
                            echo $form->fileField($nuevoAdjunto, 'archivo',  array('class' => "form-control fileSelectBoton",
                                "placeholder" => "Selecciona un archivo o imagen",'required' => 'required',
                                "value" => $nuevoAdjunto->archivo));
                            ?>


                        </div>

                </div>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal"><i class="fas fa-times mr-2"></i> CERRAR</button>

                <button type="submit" class="btn btn-sm btn-warning waves-effect waves-light " id="btnGuardarAdjunto" name="btnGuardarAdjunto" value="1"><i class="fas fa-save mr-2"></i> GUARDAR</button>
            </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>

<div class="modal fade" id="mdCerrar" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'adjunto-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Cerrar <?=ucwords(strtolower($queja->textTipo)) ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body ">
                <div class="container">
                    <p>Ingrese la información requerida para cerrar la queja o sugerencia. Todos los campos son requeridos.</p>
                    <?php
                    if(!empty($clienteVisita->errors) || !empty($quejaSugerenciaEdit->errors)){
                        ?>
                        <div class="alert alert-danger icons-alert" style="font-size: 12px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="icofont icofont-close-line-circled"></i>
                            </button>
                            <p><strong>Atención!</strong> <?=$form->errorSummary($clienteVisita) ?></p>
                            <?php
                            if(!empty($quejaSugerenciaEdit->errors)){
                                ?>

                                <p><?=$form->errorSummary($quejaSugerenciaEdit) ?></p>

                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>

                        <div class="row ">
                               <div class="col-lg-6 col-md-6 ">
                                   <label>Fecha de Visita</label>
                                   <?php
                                   if(empty( $quejaSugerenciaEdit->fechaAcuerdo)){

                                       $quejaSugerenciaEdit->fechaAcuerdo=date("Y-m-d");
                                   }

                                   echo $form->dateField($quejaSugerenciaEdit, 'fechaAcuerdo',
                                       array('class' => "form-control" ,'placeholder' => "fechaAcuerdo",'title' => "Fecha de Acuerdo",
                                           'value' => $quejaSugerenciaEdit->fechaAcuerdo));
                                   ?>
                               </div>
                               <div class="col-lg-6 col-md-6 ">
                                   <label>Hora de Visita</label>
                                   <?php
                                   if(empty( $clienteVisita->_horaVisita)){
                                       $clienteVisita->_horaVisita=date("H:00");
                                   }

                                   echo $form->timeField($clienteVisita, '_horaVisita',
                                       array('class' => "form-control", 'placeholder' => "Hora Visita", 'title' => "Hora Visita",'min'=>"8:00", 'max'=>"18:00",
                                           'step'=>3600,"value" => $clienteVisita->_horaVisita));
                                   ?>
                               </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 mt-3 ">
                                <label>Tipo de Acuerdo</label>
                                <?php
                                $acuerdos = CHtml::listData(Catacuerdo::model()->findAll(), 'id', 'descripcion');
                                echo $form->dropDownList($quejaSugerenciaEdit, 'idCatAcuerdo', $acuerdos, array('class' => "form-control", 'empty' => "-- Selecciona --"));
                                ?>
                            </div>
                        </div>
                         <div class="row">
                             <div class="col-lg-12 col-md-12 mt-3">
                                 <label>Comentarios u observaciones</label>
                                 <?php echo $form->textArea($quejaSugerenciaEdit, 'observacionAcuerdo', array('class' => "form-control" ,'rows'=>"3",'placeholder' => "Observación de Acuerdo",'title' => "Observación de Acuerdo",  'value' => $quejaSugerenciaEdit->observacionAcuerdo)); ?>
                             </div>
                         </div>

                         <div class="row">
                             <div class="col-lg-12 col-md-12 mt-3">
                                 <label>Tipo de Visita Asignada</label>
                                 <?php
                                 $visitas = CHtml::listData(CatTipovisita::model()->findAll(), 'id', 'descripcion');
                                 echo $form->dropDownList($clienteVisita, 'idCatTipoVisita', $visitas, array('class' => "form-control", 'placeholder' => "Método Contacto ...", 'empty' => "Tipo de Visita ..."));
                                 ?>
                             </div>
                        </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 mt-3 ">
                            <label>Empleado Responsable Final</label>
                            <?php
                            $empleadosResponsables = CHtml::listData(Viewempleado::model()->findAll(), 'id', 'nombre');
                            echo $form->dropDownList($quejaSugerenciaEdit, 'idEmpleadoCierre', $empleadosResponsables, array('class' => "form-control", 'empty' => "-- Selecciona --"));
                            ?>
                        </div>
                    </div>

                    <br>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal"><i class="fas fa-times"></i> CERRAR</button>

                <button type="submit" class="btn btn-sm btn-danger waves-effect waves-light sendForm"
                        id="btnCerrar"
                        name="btnCerrar"
                        value="1">
                    <i class="fas fa-save"></i> GUARDAR</button>
            </div>
            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>

<script>
    <?php
    if(!empty($nuevoSeguimiento->errors)){
    ?>
    window.onload = function() {
        $('#mdNuevoSeguimiento').modal('show');
    };
    <?php
    }
    ?>


    <?php
    if(!empty($nuevoAdjunto->errors)){
    ?>
    window.onload = function() {
        $('#mdNuevoAdjunto').modal('show');
    };
    <?php
    }
    ?>

    <?php
    if(!empty($clienteVisita->errors) || !empty($quejaSugerenciaEdit->errors)){
    ?>
    window.onload = function() {
        $('#mdCerrar').modal('show');
    };
    <?php
    }
    ?>




</script>

