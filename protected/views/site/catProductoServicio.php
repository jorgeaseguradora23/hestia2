<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-listine-dots bg-c-pink"></i>
                    <div class="d-inline">
                        <h4>Modal</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Modal</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page body start -->
    <div class="page-body button-page">
        <div class="row">
            <!-- Static Example modal start-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Static Example</h5>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <!-- popup example start -->
                        <div class="bd-example bd-example-modal">
                            <div class="modal">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Modal body text goes here.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary mobtn" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary mobtn">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- popup example end -->
                    </div>
                </div>
            </div>
            <!-- Static Example modal start-->
            <!-- bootstrap modal start -->
            <div class="col-lg-12 col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Bootstrap modals</h5>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <p>use code<code> modal-lg, modal-sm</code> to use large and small popup modal.</p>
                        <ul>
                            <li>
                                <!-- Modal static-->
                                <button type="button" class="btn btn-default waves-effect" data-toggle="modal" data-target="#default-Modal">Static</button>

                            </li>
                            <li>
                                <!-- Modal large-->
                                <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#large-Modal">Large</button>
                                <div class="modal fade" id="large-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Modal title</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <h5>Default Modal</h5>
                                                <p>This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Modal small-->
                                <button type="button" class="btn btn-success waves-effect" data-toggle="modal" data-target="#small-Modal">Small</button>
                                <div class="modal fade" id="small-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Modal title</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <h5>Small Modal</h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing .</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Bootstrap modal end -->
            <!-- Custom modal start -->
            <div class="col-lg-12 col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Custom modals</h5>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <p>use custom modals tabs, overflow, lightbox, Multi modal.</p>
                        <ul>
                            <li>
                                <!-- Modal static-->
                                <button type="button" class="btn btn-warning waves-effect" data-toggle="modal" data-target="#Modal-tab">Tabs</button>
                                <!-- Modal -->
                                <div class="modal fade modal-flex" id="Modal-tab" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">Home</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab-profile" role="tab">Profile</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab-messages" role="tab">Messages</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab-settings" role="tab">Settings</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content modal-body">
                                                    <div class="tab-pane active" id="tab-home" role="tabpanel">
                                                        <h6>Home</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing</p>
                                                    </div>
                                                    <div class="tab-pane" id="tab-profile" role="tabpanel">
                                                        <h6>Profile</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing</p>
                                                    </div>
                                                    <div class="tab-pane" id="tab-messages" role="tabpanel">
                                                        <h6>Messages</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing</p>
                                                    </div>
                                                    <div class="tab-pane" id="tab-settings" role="tabpanel">
                                                        <h6>Settings</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Modal large-->
                                <button type="button" class="btn btn-danger waves-effect" data-toggle="modal" data-target="#Modal-overflow">Overflow</button>
                                <!-- Modal -->
                                <div class="modal fade modal-flex" id="Modal-overflow" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body model-container">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <h5 class="font-header">Some text above the scrollable box</h5>
                                                <p>This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                                                <div class="overflow-container">
                                                    <h5>Overflow container</h5>
                                                </div>
                                                <img src="assets/images/Modal/overflow.jpg" alt="" class="img img-fluid">
                                                <p class="p-t-15">Similique velit aut et cumque illum consequatur doloribus quis ipsam sunt sint qui impedit nihil voluptate animi nesciunt corporis aspernatur quaerat sed rem voluptas commodi magnam porro eum sunt quis ullam aut odit laudantium quia soluta corrupti aut qui officiis hic voluptatibus aut itaque voluptates qui expedita minus autem aliquid et debitis omnis provident quia voluptate illo tempora illum maiores perferendis dolorem recusandae ut reprehenderit ad est eum occaecati quam non et quod amet illo id doloremque vitae porro porro sit amet voluptatem quia laboriosam aliquam quia quis facilis eveniet dolorum sunt neque rerum earum ut eaque ab tenetur quia nam quis rerum cumque eos excepturi nostrum qui distinctio porro enim vitae eligendi accusantium quia possimus et autem error repellendus vitae ad quia laborum minima autem nulla optio ad ea nobis animi illo voluptates cum recusandae temporibus voluptate amet quam excepturi odio quia suscipit inventore et rerum quos rerum aut doloribus aut consequatur earum impedit reiciendis saepe voluptates exercitationem maxime culpa saepe ducimus culpa ut aliquam magnam aut veniam sit totam architecto vel distinctio aspernatur aut qui labore quaerat rerum voluptatem sapiente sint sed explicabo et hic laboriosam sit nesciunt et minus et aut dignissimos ut porro incidunt sint et nihil id tempora aut et illum molestiae aperiam minus earum repellendus tempora illo itaque amet facilis quia rem iure quod corrupti dolores et sequi sunt ipsa labore quia unde qui blanditiis ut eos at quia beatae sit repellat quibusdam neque natus expedita sed perspiciatis atque quas voluptatem autem velit enim qui omnis molestiae et repellat sapiente corporis ipsam sed veritatis in quo quod et occaecati quia rerum iusto cumque accusamus numquam sunt quo sequi id molestiae consequatur doloribus molestiae autem nisi nostrum blanditiis et nihil sed nobis incidunt omnis quos minima eius quo accusamus qui ea minus aut est tempora quia inventore ad delectus vel et accusamus fuga eius ipsa aliquam alias sint maxime quae enim atque corrupti libero eos nesciunt et voluptas velit numquam illo non qui quaerat enim omnis et ex asperiores inventore quisquam est enim labore ut nobis consequatur fuga ut quo vel molestiae alias eius quod aspernatur laudantium pariatur eius fuga inventore esse at alias repudiandae perspiciatis id qui fuga consequatur recusandae atque iste commodi sapiente eaque labore ipsa aut sint quo vel recusandae ab iusto ducimus minus voluptas ex et illo commodi ipsa pariatur qui quae adipisci saepe dicta delectus nostrum illum incidunt laboriosam est maxime eum debitis et explicabo quia doloribus quod occaecati tempore tempora labore nihil enim recusandae et dolorum temporibus molestiae sint non porro neque minus provident reprehenderit similique illum voluptate qui dicta dolorum totam quia ut nihil dolore fugiat laboriosam molestiae eveniet omnis consequatur non magni nemo consequatur laboriosam non facilis harum eaque illo pariatur rerum dolores quis autem nemo aut enim tenetur pariatur et non quam repudiandae quia aliquam sunt corporis aperiam natus aut reprehenderit non non illum cum laboriosam nulla quaerat eligendi laudantium perspiciatis.</p>
                                            </div>
                                            <div class="p-15">
                                                <h5 class="font-header">Some text above the scrollable box</h5>
                                                <p>This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Modal small-->
                                <button type="button" class="btn btn-info waves-effect" data-toggle="modal" data-target="#Modal-lightbox">Light Box</button>
                                <!-- Modal -->
                                <div class="modal fade" id="Modal-lightbox" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <img src="assets/images/Modal/overflow.jpg" alt="" class="img img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Modal small-->
                                <button type="button" class="btn btn-success waves-effect m-b-10" data-toggle="modal" data-target="#myModal">Multi Model</button>
                                <!-- Modal -->
                                <div class="modal fade modal-flex" id="Modal-Multi" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title">Modal title</h4>
                                            </div>
                                            <div class="modal-body text-center">
                                                <div>
                                                    <button type="button" class="btn btn-default waves-effect m-t-20 " data-toggle="modal" data-target="#meta-Modal">Click Here!</button>
                                                </div>
                                                <div class="modal fade" id="meta-Modal" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-sm" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Modal title</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h5>Small Modal</h5>
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing .</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="m-t-10">Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Multi Model Start-->
                                <div class="modal fade" id="myModal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Modal 1</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="container"></div>
                                            <div class="modal-body">Content for the dialog / modal goes here.
                                                <p class="p-t-40 p-b-40">more content</p>
                                                <a data-toggle="modal" href="#myModal2" class="btn btn-primary">Launch modal</a>
                                            </div>
                                            <div class="modal-footer"> <a href="#" data-dismiss="modal" class="btn">Close</a>
                                                <a href="#" class="btn btn-primary">Save changes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="myModal2">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Modal 2</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="container"></div>
                                            <div class="modal-body">Content for the dialog / modal goes here.
                                                <p class="p-t-30 p-b-40">come content</p>
                                                <a data-toggle="modal" href="#myModal3" class="btn btn-primary">Launch modal</a>
                                            </div>
                                            <div class="modal-footer"> <a href="#" data-dismiss="modal" class="btn">Close</a>
                                                <a href="#" class="btn btn-primary">Save changes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="myModal3">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Modal 3</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="container"></div>
                                            <div class="modal-body">Content for the dialog / modal goes here.
                                                <a data-toggle="modal" href="#myModal4" class="btn btn-primary">Launch modal</a>
                                            </div>
                                            <div class="modal-footer"> <a href="#" data-dismiss="modal" class="btn">Close</a>
                                                <a href="#" class="btn btn-primary">Save changes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="myModal4">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Modal 4</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="container"></div>
                                            <div class="modal-body">Content for the dialog / modal goes here.</div>
                                            <div class="modal-footer"> <a href="#" data-dismiss="modal" class="btn">Close</a>
                                                <a href="#" class="btn btn-primary">Save changes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Multi Model End-->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Custom modal end -->
            <!-- Sweet alert start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Bootstrap modals</h5>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <p>use button<code> onclick="_gaq.push(['_trackEvent', 'example', 'try', 'sweet-1']);"</code> to use effect.</p>
                        <ul>
                            <li>
                                <button type="button" class="btn btn-primary sweet-1 m-b-10" onclick="_gaq.push(['_trackEvent', 'example', 'try', 'sweet-1']);">Basic</button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-success alert-success-msg m-b-10" onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-success']);">Success</button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-warning alert-confirm m-b-10" onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-confirm']);">Confirm</button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-danger alert-success-cancel m-b-10" onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-success-cancel']);">Success or Cancel</button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-primary alert-prompt m-b-10" onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-prompt']);">Prompt</button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-success alert-ajax m-b-10" onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-ajax']);">Ajax</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Sweet alert end -->
            <!-- Animation modal start / Nifty Modal Window Effects start-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Bootstrap modals</h5>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <p>use button with class<code> md-effect-1 to 18</code> to use effect.</p>
                        <div class="animation-model">
                            <button type="button" class="btn btn-default btn-outline-default waves-effect md-trigger" data-modal="modal-1">Fade in &amp; Scale</button>
                            <button type="button" class="btn btn-primary btn-outline-primary waves-effect md-trigger" data-modal="modal-2">Slide in (right)</button>
                            <button type="button" class="btn btn-success btn-outline-success waves-effect md-trigger" data-modal="modal-3">Slide in (bottom)</button>
                            <button type="button" class="btn btn-warning btn-outline-warning waves-effect md-trigger" data-modal="modal-4">Newspaper</button>
                            <button type="button" class="btn btn-danger btn-outline-danger waves-effect md-trigger" data-modal="modal-5">Fall</button>
                            <button type="button" class="btn btn-info btn-outline-info waves-effect md-trigger" data-modal="modal-6">Side Fall</button>
                            <button type="button" class="btn btn-default btn-outline-default waves-effect md-trigger" data-modal="modal-7">Sticky Up</button>
                            <button type="button" class="btn btn-primary btn-outline-primary waves-effect md-trigger" data-modal="modal-8">3D Flip (horizontal)</button>
                            <button type="button" class="btn btn-success btn-outline-success waves-effect md-trigger" data-modal="modal-9">3D Flip (vertical)</button>
                            <button type="button" class="btn btn-warning btn-outline-warning waves-effect md-trigger" data-modal="modal-10">3D Sign</button>
                            <button type="button" class="btn btn-danger btn-outline-danger waves-effect md-trigger" data-modal="modal-11">Super Scaled</button>
                            <button type="button" class="btn btn-info btn-outline-info waves-effect md-trigger" data-modal="modal-12">Just Me</button>
                            <button type="button" class="btn btn-primary btn-outline-primary waves-effect md-trigger" data-modal="modal-13">3D Slit</button>
                            <button type="button" class="btn btn-success btn-outline-success waves-effect md-trigger" data-modal="modal-14">3D Rotate Bottom</button>
                            <button type="button" class="btn btn-warning btn-outline-warning waves-effect md-trigger" data-modal="modal-15">3D Rotate In Left</button>
                            <button type="button" class="btn btn-danger btn-outline-danger waves-effect md-trigger md-setperspective" data-modal="modal-17">Let me in</button>
                            <button type="button" class="btn btn-default btn-outline-default waves-effect md-trigger md-setperspective" data-modal="modal-18">Make way!</button>
                            <button type="button" class="btn btn-primary btn-outline-primary waves-effect md-trigger md-setperspective" data-modal="modal-19">Slip from top</button>
                            <!-- animation modal Dialogs start -->
                            <div class="md-modal md-effect-1" id="modal-1">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-2" id="modal-2">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-3" id="modal-3">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-4" id="modal-4">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-5" id="modal-5">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-6" id="modal-6">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-7" id="modal-7">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-8" id="modal-8">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-9" id="modal-9">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-10" id="modal-10">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-11" id="modal-11">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-12" id="modal-12">
                                <div class="md-content">
                                    <h3><span class="text-muted text-center">Modal Dialog</span></h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-13" id="modal-13">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-14" id="modal-14">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-15" id="modal-15">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-17" id="modal-17">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-18" id="modal-18">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-19" id="modal-19">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="md-modal md-effect-20" id="modal-20">
                                <div class="md-content">
                                    <h3>Modal Dialog</h3>
                                    <div>
                                        <p>This is a modal window. You can do the following things with it:</p>
                                        <ul>
                                            <li><strong>Read:</strong> modal windows will probably tell you something important so don't forget to read what they say.</li>
                                            <li><strong>Look:</strong> a modal window enjoys a certain kind of attention; just look at it and appreciate its presence.</li>
                                            <li><strong>Close:</strong> click on the button below to close the modal.</li>
                                        </ul>
                                        <button type="button" class="btn btn-primary waves-effect md-close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <!--animation modal  Dialogs ends -->
                            <div class="md-overlay"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Animation modal end / Nifty Modal Window Effects end-->
        </div>
    </div>
    <!-- Page body end -->
</div>
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-ui-calendar bg-c-pink"></i>
                    <div class="d-inline">
                        <h4>Form Picker</h4>
                        <span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Form Picker</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Default Date-Picker card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>Default Date-Picker</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date</h4>
                                <p>Add type<code>&lt;input type="date"&gt;</code></p>
                                <input class="form-control" type="date">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Month</h4>
                                <p>Add type<code>&lt;input type="month"&gt;</code></p>
                                <input class="form-control" type="month">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Week</h4>
                                <p>Add type<code>&lt;input type="week"&gt;</code></p>
                                <input class="form-control" type="week">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date-time-local</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <input class="form-control" type="datetime-local">
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Time</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <input class="form-control" type="time">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Default Date-Picker card end -->
                <!-- Date-Dropper card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>Date-Dropper</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Default</h4>
                                <p>Add <code>id="#dropper-default"</code></p>
                                <input id="dropper-default" class="form-control" type="text" placeholder="Select your date">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">With Animation</h4>
                                <p>Add <code>id="#dropper-animation"</code></p>
                                <input id="dropper-animation" class="form-control" type="text" placeholder="Select your animation" readonly="readonly">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date format</h4>
                                <p>Add <code>id="#dropper-format"</code></p>
                                <input id="dropper-format" class="form-control" type="text" placeholder="Select your format">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Language Support</h4>
                                <p>Add <code>id="#dropper-lang"</code></p>
                                <input id="dropper-lang" class="form-control" type="text" placeholder="Language Support">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Lock Support</h4>
                                <p>Add <code>id="#dropper-lock"</code></p>
                                <input id="dropper-lock" class="form-control" type="text" placeholder="Select your date" readonly="readonly">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Max Year</h4>
                                <p>Add <code>id="#dropper-max-year"</code></p>
                                <input id="dropper-max-year" class="form-control" type="text" placeholder="Max Year 2020">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Min Year</h4>
                                <p>Add <code>id="#dropper-min-year"</code></p>
                                <input id="dropper-min-year" class="form-control" type="text" placeholder="Min Year 1990">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Year-range</h4>
                                <p>Add <code>id="#year-range"</code></p>
                                <input id="year-range" class="form-control" type="text" placeholder="Select your date">
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Custom Width</h4>
                                <p>Add <code>id="#dropper-width"</code></p>
                                <input id="dropper-width" class="form-control" type="text" placeholder="Select your date">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Date-Dropper card end -->
                <!-- Color & Style Dropper card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>Color &amp; Style Dropper</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Primary Color</h4>
                                <p>Add <code>id="#dropper-dangercolor"</code></p>
                                <input id="dropper-dangercolor" class="form-control" type="text" placeholder="Select your time">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Background Color</h4>
                                <p>Add <code>id="#dropper-backcolor"</code></p>
                                <input id="dropper-backcolor" class="form-control" type="text" placeholder="Select your time">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Text Color</h4>
                                <p>Add <code>id="#dropper-txtcolor"</code></p>
                                <input id="dropper-txtcolor" class="form-control" type="text" placeholder="Select your time">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Border-radius</h4>
                                <p>Add <code>id="#dropper-radius"</code></p>
                                <input id="dropper-radius" class="form-control" type="text" placeholder="Select your time">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Border Style</h4>
                                <p>Add <code>id="#dropper-border"</code></p>
                                <input id="dropper-border" class="form-control" type="text" placeholder="Select your time">
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Shadow Color</h4>
                                <p>Add <code>id="#dropper-shadow"</code></p>
                                <input id="dropper-shadow" class="form-control" type="text" placeholder="Select your time">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Color & Style Dropper card end -->
                <!-- Bootstrap Date-Picker card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>Bootstrap Date-Picker</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date Time Picker</h4>
                                <p>Add type<code>&lt;input type="date"&gt;</code></p>
                                <div class="form-group">
                                    <div class="input-group date" id="datetimepicker1">
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon ">
                                       <span class="icofont icofont-ui-calendar"></span>
                                                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Time Picker</h4>
                                <p>Add type<code>&lt;input type="week"&gt;</code></p>
                                <div class="form-group">
                                    <div class="input-group date" id="datetimepicker3">
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon ">
                                       <span class="icofont icofont-ui-calendar"></span>
                                                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">No Icon (Input Field Only)</h4>
                                <p>Add type<code>&lt;input type="week"&gt;</code></p>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="datetimepicker4">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Enabled/Disabled Dates</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <div class="form-group">
                                    <div class="input-group date" id="datetimepicker5">
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon ">
                                       <span class="icofont icofont-ui-calendar"></span>
                                                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Custom Icons</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class="input-group date" id="datetimepicker8">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon ">
                                    <span class="icofont icofont-ui-calendar"></span>
                                                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Birthday Calender</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <input type="text" name="birthdate" class="form-control" value="10/24/1984">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-8 m-b-30">
                                <h4 class="sub-title">Linked Pickers</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker6">
                                                <input type="text" class="form-control">
                                                <span class="input-group-addon ">
                                             <span class="icofont icofont-ui-calendar"></span>
                                                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker7">
                                                <input type="text" class="form-control">
                                                <span class="input-group-addon ">
                                             <span class="icofont icofont-ui-calendar"></span>
                                                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date Range Picker</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <input type="text" name="daterange" class="form-control" value="01/01/2015 - 01/31/2015">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Min View Mode</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <div class="input-group date" id="datetimepicker10">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon ">
                                    <span class="icofont icofont-ui-calendar"></span>
                                                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Disabled Days Of The Week</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class="input-group date" id="datetimepicker11">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon ">
                                    <span class="icofont icofont-ui-calendar">
                                    </span>
                                                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">View Mode</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class="input-group date" id="datetimepicker9">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon ">
                                    <span class="icofont icofont-ui-calendar">
                                    </span>
                                                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date Range Picker Blank Input</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <input type="text" name="datefilter" class="form-control" value="">
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Multiple Option</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div id="reportrange" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                                    <span>February 24, 2019 - March 25, 2019</span> <b class="caret"></b>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4 m-b-30">
                                <h4 class="sub-title">Date Picker With Today</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class="input-group date input-group-date-custom">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon ">
                                    <i class="icofont icofont-clock-time"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-8">
                                <h4 class="sub-title">Date Time Picker</h4>
                                <p>Add type<code>&lt;input type="datetime-local"&gt;</code></p>
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-sm form-control" name="start">
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="input-sm form-control" name="end">
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <h4 class="sub-title">Multi Select Dates</h4>
                                <p>Add type<code>&lt;input type="time"&gt;</code></p>
                                <div class="input-group date multiple-select">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon ">
                                    <i class="icofont icofont-clock-time"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Bootstrap Date-Picker card end -->
                <!-- Color Picker card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>Color Picker</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4 class="sub-title">Flat Mode</h4>
                                <div class="form-group">
                                    <input type="text" id="flat" style="display: none;"><div class="sp-container sp-light sp-flat sp-palette-buttons-disabled sp-palette-disabled sp-initial-disabled"><div class="sp-palette-container"><div class="sp-palette sp-thumb sp-cf"></div><div class="sp-palette-button-container sp-cf"><button type="button" class="sp-palette-toggle">less</button></div></div><div class="sp-picker-container"><div class="sp-top sp-cf"><div class="sp-fill"></div><div class="sp-top-inner"><div class="sp-color" style="background-color: rgb(255, 0, 0);"><div class="sp-sat"><div class="sp-val"><div class="sp-dragger" style="top: 130.594px; left: -5px;"></div></div></div></div><div class="sp-clear sp-clear-display" title="Clear Color Selection" style="display: none;"></div><div class="sp-hue"><div class="sp-slider" style="top: -3px;"></div></div></div><div class="sp-alpha" style=""><div class="sp-alpha-inner"><div class="sp-alpha-handle" style="left: 169px;"></div></div></div></div><div class="sp-input-container sp-cf"><input class="sp-input" type="text" spellcheck="false"></div><div class="sp-initial sp-thumb sp-cf"></div><div class="sp-button-container sp-cf"><a class="sp-cancel" href="#">cancel</a><button type="button" class="sp-choose">choose</button></div></div></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <h4 class="sub-title">Flat Mode With Clear</h4>
                                <div class="form-group">
                                    <input type="text" id="flatClearable" style="display: none;"><div class="sp-container sp-light sp-flat sp-clear-enabled sp-palette-buttons-disabled sp-palette-disabled sp-initial-disabled"><div class="sp-palette-container"><div class="sp-palette sp-thumb sp-cf"></div><div class="sp-palette-button-container sp-cf"><button type="button" class="sp-palette-toggle">less</button></div></div><div class="sp-picker-container"><div class="sp-top sp-cf"><div class="sp-fill"></div><div class="sp-top-inner"><div class="sp-color" style="background-color: rgb(255, 0, 0);"><div class="sp-sat"><div class="sp-val"><div class="sp-dragger" style="display: none;"></div></div></div></div><div class="sp-clear sp-clear-display" title="Clear Color Selection"></div><div class="sp-hue"><div class="sp-slider" style="display: none;"></div></div></div><div class="sp-alpha" style=""><div class="sp-alpha-inner"><div class="sp-alpha-handle" style="display: none;"></div></div></div></div><div class="sp-input-container sp-cf"><input class="sp-input" type="text" spellcheck="false"></div><div class="sp-initial sp-thumb sp-cf"></div><div class="sp-button-container sp-cf"><a class="sp-cancel" href="#">cancel</a><button type="button" class="sp-choose">choose</button></div></div></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <h4 class="sub-title"> No Icon (Input Field Only)</h4>
                                <div class="form-group">
                                    <input type="color" name="color">
                                    <input type="color" name="color2" value="#3355cc">
                                    <hr>
                                    <input type="color">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Color Picker card end -->
                <!-- Mini Color card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>Mini Color</h5>
                        <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
                        <div class="card-header-right">                                                             <i class="icofont icofont-spinner-alt-5"></i>                                                         </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">Control-Types</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">Hue (default)</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="hue-demo" class="form-control demo minicolors-input" data-control="hue" value="#ff6161" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(255, 97, 97);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 150px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(255, 0, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 0px; left: 93px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">Saturation</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="saturation-demo" class="form-control demo minicolors-input" data-control="saturation" value="#0088cc" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(0, 136, 204);"></span></span><div class="minicolors-panel minicolors-slider-saturation"><div class="minicolors-slider minicolors-sprite" style="background-color: rgb(0, 136, 204);"><div class="minicolors-picker" style="top: 0px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite"><div class="minicolors-grid-inner" style="opacity: 1;"></div><div class="minicolors-picker" style="top: 30px; left: 83.3333px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">Brightness</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="brightness-demo" class="form-control demo minicolors-input" data-control="brightness" value="#00ffff" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(0, 255, 255);"></span></span><div class="minicolors-panel minicolors-slider-brightness"><div class="minicolors-slider minicolors-sprite" style="background-color: rgb(0, 255, 255);"><div class="minicolors-picker" style="top: 0px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite"><div class="minicolors-grid-inner" style="opacity: 0;"></div><div class="minicolors-picker" style="top: 0px; left: 75px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">Wheel</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="wheel-demo" class="form-control demo minicolors-input" data-control="wheel" value="#ff99ee" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(255, 153, 238);"></span></span><div class="minicolors-panel minicolors-slider-wheel"><div class="minicolors-slider minicolors-sprite" style="background-color: rgb(255, 153, 238);"><div class="minicolors-picker" style="top: 0px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 97.9813px; left: 55.7164px;"><div></div></div></div></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">Input Modes</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-12 m-b-30">
                                                    <h4 class="sub-title">Text field </h4>
                                                    <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="text-field" class="form-control demo minicolors-input" value="#70c24a" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(112, 194, 74);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 107.917px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(81, 255, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 35px; left: 93px;"><div></div></div></div></div></div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <h4 class="sub-title">Hidden Input</h4>
                                                    <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="hidden" id="hidden-input" class="demo minicolors-input" value="#db913d" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(219, 145, 61);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 136.709px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(255, 136, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 21px; left: 109px;"><div></div></div></div></div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Brightness</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left minicolors-inline"><input type="text" id="inline" class="form-control demo minicolors-input" data-inline="true" value="#4fc8db" size="7"><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 71.6071px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 221, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 21px; left: 96px;"><div></div></div></div></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">Positions</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">bottom left (default)</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="position-bottom-left" class="form-control demo minicolors-input" data-position="bottom left" value="#0088cc" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(0, 136, 204);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 66.6667px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 170, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 30px; left: 150px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">top left</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-top minicolors-position-left"><input type="text" id="position-top-left" class="form-control demo minicolors-input" data-position="top left" value="#0088cc" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(0, 136, 204);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 66.6667px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 170, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 30px; left: 150px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">bottom right</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-right"><input type="text" id="position-bottom-right" class="form-control demo minicolors-input" data-position="bottom right" value="#0088cc" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(0, 136, 204);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 66.6667px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 170, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 30px; left: 150px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="sub-title">top right</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-top minicolors-position-right"><input type="text" id="position-top-right" class="form-control demo minicolors-input" data-position="top right" value="#0088cc" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(0, 136, 204);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 66.6667px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 170, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 30px; left: 150px;"><div></div></div></div></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">RGB(A)</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">RGB</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="rgb" class="form-control demo minicolors-input" data-format="rgb" value="rgb(33, 147, 58)" size="20" data-opacity="1.00"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(33, 147, 58);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 94.5175px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 255, 55);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 63px; left: 117px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">RGBA</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-with-opacity minicolors-position-bottom minicolors-position-left"><input type="text" id="rgba" class="form-control demo minicolors-input" data-format="rgb" data-opacity="0.50" value="rgba(52, 64, 158, 0.5)" size="25"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="opacity: 0.5; background-color: rgb(52, 64, 158);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 52.8302px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker" style="top: 75px;"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 30, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 57px; left: 101px;"><div></div></div></div></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="sub-title">More</h4>
                                <div class="card-block inner-card-block">
                                    <div class="row m-b-30">
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Opacity</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-with-opacity minicolors-position-bottom minicolors-position-left"><input type="text" id="opacity" class="form-control demo minicolors-input" data-opacity="0.50" value="#766fa8" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="opacity: 0.5; background-color: rgb(118, 111, 168);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 46.9298px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker" style="top: 75px;"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(30, 0, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 51px; left: 51px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Keywords</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="keywords" class="form-control demo minicolors-input" data-keywords="transparent, initial, inherit" value="transparent" size="11"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="opacity: 0;"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 0px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(255, 0, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 150px; left: 0px;"><div></div></div></div></div></div>
                                        </div>
                                    </div>
                                    <div class="row m-b-30">
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Default Value</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="default-value" class="form-control demo minicolors-input" data-defaultvalue="#ff6600" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(255, 102, 0);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 140px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(255, 102, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 0px; left: 150px;"><div></div></div></div></div></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Letter Case</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="letter-case" class="form-control demo minicolors-input" data-lettercase="uppercase" value="#abcdef" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(171, 205, 239);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 62.5px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 128, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 9px; left: 43px;"><div></div></div></div></div></div>
                                        </div>
                                    </div>
                                    <div class="row m-b-30">
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Swatches</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-position-bottom minicolors-position-left"><input type="text" id="swatches" class="form-control demo minicolors-input" data-swatches="#fff|#000|#f00|#0f0|#00f|#ff0|#0ff" value="#abcdef" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(171, 205, 239);"></span></span><div class="minicolors-panel minicolors-slider-hue minicolors-with-swatches"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 62.5px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 128, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 9px; left: 43px;"><div></div></div></div><ul class="minicolors-swatches"><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(255, 255, 255);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(0, 0, 0);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(255, 0, 0);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(0, 255, 0);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(0, 0, 255);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(255, 255, 0);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(0, 255, 255);"></span></li></ul></div></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="sub-title">Swatches and opacity</h4>
                                            <div class="minicolors minicolors-theme-bootstrap minicolors-with-opacity minicolors-position-bottom minicolors-position-left"><input type="text" id="swatches-2" class="form-control demo minicolors-input" data-format="rgb" data-opacity="0.50" data-swatches="#fff|#000|#f00|#0f0|#00f|#ff0|rgba(0,0,255,0.5)" value="rgba(14, 206, 235, .5)" size="25"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="opacity: 0.5; background-color: rgb(14, 206, 235);"></span></span><div class="minicolors-panel minicolors-slider-hue minicolors-with-swatches"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 71.7195px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker" style="top: 75px;"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(0, 221, 255);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 11px; left: 142px;"><div></div></div></div><ul class="minicolors-swatches"><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(255, 255, 255);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(0, 0, 0);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(255, 0, 0);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(0, 255, 0);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(0, 0, 255);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(255, 255, 0);"></span></li><li class="minicolors-swatch minicolors-sprite"><span class="minicolors-swatch-color" style="background-color: rgb(0, 0, 255); opacity: 0.5;"></span></li></ul></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Mini Color card end -->
            </div>
        </div>
    </div>
    <!-- Page body end -->
</div>