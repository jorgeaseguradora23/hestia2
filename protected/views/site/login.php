<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Acceso';
$this->breadcrumbs = array(
    'Ingresar',
);
?>
<!-- Container-fluid starts -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <!-- Authentication card start -->
            <div class="login-card card-block auth-body mr-auto ml-auto">
                <?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('class' => 'md-float-material'),
        ));
?>
                <form class="md-float-material">
                    <div class="text-center">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/auth/logo-dark.png" alt="logo.png">
                    </div>
                    <div class="auth-box">
                        <div class="row m-b-20">
                            <div class="col-md-12">
                                <h3 class="text-left txt-primary">Cronos 2.0 - Acceso</h3>
                            </div>
                        </div>
                        <hr/>
                        <div class="input-group">
                            <input type="text" class="form-control" id="LoginForm_username" name="LoginForm[username]" placeholder="Usuario" required>
                            <span class="md-line"></span>
                        </div>
                        <div class="input-group">
                            <input type="password" class="form-control" name="LoginForm[password]" id="LoginForm_password" placeholder="Contraseña" required>
                            <span class="md-line"></span>
                        </div>
                        
                        <?php echo $form->error($model, 'username', array('class' => 'alert alert-warning')); ?>
                        <?php echo $form->error($model, 'password', array('class' => 'alert alert-warning')); ?>

                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <?php echo CHtml::submitButton('Ingresar', array('class' => 'btn btn-primary btn-md btn-block waves-effect text-center m-b-20', 'style' => '')); ?>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                        <hr/>
                        <div class="row">
                            <div class="col-md-10">
                                <p class="text-inverse text-left m-b-0">Bienvenido al sistema de control</p>
                                <p class="text-inverse text-left"><b>Intermedia Web SA de CV</b></p>
                            </div>
                            <div class="col-md-2">
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/auth/Logo-small-bottom.png" alt="small-logo.png">
                            </div>
                        </div>

                    </div>
                </form>
                <!-- end of form -->
            </div>
            <!-- Authentication card end -->
        </div>
        <!-- end of col-sm-12 -->
    </div>
    <!-- end of row -->
</div>
<!-- end of container-fluid -->
