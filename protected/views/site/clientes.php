<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-users bg-c-blue"></i>

                    <div class="d-inline">
                        <h4>Cartera de Clientes</h4>
                        <span>Concentrado de Clientes directos y por agencia</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 right">

                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Clientes</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Cartera Clientes</a>
                            </li>
                        </ul>
                    </div>



            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">
        <div class="row">
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-pie-chart bg-c-yellow card1-icon"></i>
                        <span class="text-warning f-w-600">Quejas o sugerencias</span>
                        <h4>6 seguimiento</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-blue f-16 icofont icofont-warning m-r-10"></i>Get more space
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-ui-home bg-c-pink card1-icon"></i>
                        <span class="text-c-pink f-w-600">Clientes</span>
                        <h4>255 Activos</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-pink f-16 icofont icofont-calendar m-r-10"></i>Last 24 hours
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-warning-alt bg-c-green card1-icon"></i>
                        <span class="text-c-green f-w-600">Máxima Categoria</span>
                        <h4>15 Clientes</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-green f-16 icofont icofont-tag m-r-10"></i>Tracked from microsoft
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-social-twitter bg-c-yellow card1-icon"></i>
                        <span class="text-c-yellow f-w-600">Prospectos</span>
                        <h4>63 Activos</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-yellow f-16 icofont icofont-refresh m-r-10"></i>Just update
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->




        </div>
        <!-- Config. table start -->
        <div class="card">
            <div class="card-header">
                <h5>Resultados de la Consulta</h5>
                <span>Top 10 Clientes alto nivel</span>
                <div class="card-header-right" style="margin-right: 10px;">
                    <a href="<?=CController::createUrl('site/_nuevoCliente') ?>"><button class="btn btn-sm btn-info">NUEVO CLIENTE</button></a>
                    <button class="btn btn-sm btn-primary waves-effect"  data-toggle="modal" data-target="#default-Modal">FILTRAR</button>
                    <button class="btn btn-sm btn-success waves-effect" >CSV</button>
                    <button class="btn btn-sm btn-danger waves-effect"  >PDF</button>


                </div>
            </div>
            <div class="card-block">
                <div class="">
                    <div class="dt-responsive table-responsive" >
                        <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="row" >
                                <div class="col-xs-12 col-sm-12 col-lg-12" >
                                    <table width="100%" id="res-config" class="table compact table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 1544px; font-size: 12px; ">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >Cve</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 120px;" >Nombre</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 241px;" >Tipo</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 115px;" aria-label="Office: activate to sort column ascending">Nivel</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 54px;" aria-label="Age: activate to sort column ascending">Categoria</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 117px;" aria-label="Start date: activate to sort column ascending">Sector</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 89px;" aria-label="Salary: activate to sort column ascending">Tipo Servicio</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px; display: none;" aria-label="E-mail: activate to sort column ascending">Estatus</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 67px;" aria-label="Extn.: activate to sort column ascending">Fecha Alta</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px; display: none;" aria-label="E-mail: activate to sort column ascending">Acciones</th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr role="row" class="odd">
                                            <td tabindex="0" class="sorting_1">C00658</td>
                                            <td>Coca Cola de México SA de CV</td>
                                            <td>Directo</td>
                                            <td>AAA</td>
                                            <td>Cliente</td>
                                            <td>Industrial</td>
                                            <td style="">Contrato</td>
                                            <td style=""><label class="label label-md label-success" style="min-width: 40px; text-align: center;">ACTIVO</label></td>

                                            <td style="">2019-01-01 (user)</td>
                                            <td >
                                                <a href="#!" class="m-r-15 text-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar" aria-describedby="tooltip913847"><i class="icofont icofont-ui-edit"></i></a>
                                                <a href="<?=CController::createUrl('site/_detalleCliente') ?>" class="m-r-15 text-info" data-toggle="tooltip" data-placement="top" title=""  data-original-title="Detalles" aria-describedby="tooltip913847"><i class="icofont  icofont-info"></i></a>
                                            </td>
                                        </tr>
                                        <tr role="row" class="odd">
                                            <td tabindex="0" class="sorting_1">C00659</td>
                                            <td>Aegncia de publicidad de algo SA de CV</td>
                                            <td>Agencia</td>
                                            <td>AA+</td>
                                            <td>Prospecto</td>
                                            <td>Publicidad</td>
                                            <td style="">Eventual</td>
                                            <td style=""><label class="label label-md label-danger" style="min-width: 40px; text-align: center;">BAJA</label></td>
                                            <td style="">2019-01-01 (user)</td>
                                            <td >
                                                <a href="#!" class="m-r-15 text-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar" aria-describedby="tooltip913847"><i class="icofont icofont-ui-edit"></i></a>
                                                <a href="<?=CController::createUrl('site/_detalleCliente') ?>" class="m-r-15 text-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalles" aria-describedby="tooltip913847"><i class="icofont  icofont-info"></i></a>
                                            </td>
                                        </tr>
                                        <tr role="row" class="odd">
                                            <td tabindex="0" class="sorting_1">C00659</td>
                                            <td>Aegncia de publicidad de algo y muchas letras mas SA de CV</td>
                                            <td>Agencia</td>
                                            <td>AA+</td>
                                            <td>Prospecto</td>
                                            <td>Publicidad</td>
                                            <td style="">Eventual</td>
                                            <td style=""><label class="label label-md label-danger" style="min-width: 40px; text-align: center;">BAJA</label></td>
                                            <td style="">2019-01-01 (user)</td>
                                           <td >
                                                <a href="#!" class="m-r-15 text-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar" aria-describedby="tooltip913847"><i class="icofont icofont-ui-edit"></i></a>
                                                <a href="<?=CController::createUrl('site/_detalleCliente') ?>" class="m-r-15 text-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalles" aria-describedby="tooltip913847"><i class="icofont  icofont-info"></i></a>
                                            </td>

                                        </tr>
                                      </tbody>
                                    </table>
                                    </br>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Config. table end -->

    </div>
</div>
<!-- MODAL FILTROS -->
<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info" >
                <h4 class="modal-title">Filtros de Búsqueda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <p>Selecciona uno o varios campos de búsqueda, a continuación presiona 'Aplicar' para ejecutar tu petición.</p>
                <div class="container">
                    <div class="row " >
                        <div class="col-12 m-b-15">
                            <h4 class="sub-title text-muted"><strong>Fecha de alta</strong></h4>
                            <div class="row">
                                <div class="col-6"><input class="form-control " type="date"></div>
                                <div class="col-6"><input class="form-control " type="date"></div>
                            </div>


                        </div>
                        <div class="col-4 m-b-15">


                            <select name="select" class="form-control ">
                                <option value="">Tipo ...</option>
                                <option value="D">Directo</option>
                                <option value="A">Agencia</option>
                            </select>
                        </div>


                        <div class="col-4 m-b-15">


                            <select name="select" class="form-control ">
                                <option value="">Categoria ...</option>
                                <option value="C">Cliente</option>
                                <option value="P">Prospecto</option>
                            </select>
                        </div>
                        <div class="col-4 m-b-15">


                            <select name="select" class="form-control ">
                                <option value="">Estatus ...</option>
                                <option value="D">Activo</option>
                                <option value="A">Baja</option>
                            </select>
                        </div>
                        <div class="col-4 m-b-15">

                            <select name="select" class="form-control ">
                                <option value="opt1">Nivel Cte ...</option>
                                <option value="opt2">Type 2</option>
                                <option value="opt3">Type 3</option>
                                <option value="opt4">Type 4</option>
                                <option value="opt5">Type 5</option>
                                <option value="opt6">Type 6</option>
                                <option value="opt7">Type 7</option>
                                <option value="opt8">Type 8</option>
                            </select>
                        </div>
                        <div class="col-8 m-b-15">

                            <select name="select" class="form-control ">
                                <option value="opt1">Sector ...</option>
                                <option value="opt2">Type 2</option>
                                <option value="opt3">Type 3</option>
                                <option value="opt4">Type 4</option>
                                <option value="opt5">Type 5</option>
                                <option value="opt6">Type 6</option>
                                <option value="opt7">Type 7</option>
                                <option value="opt8">Type 8</option>
                            </select>
                        </div>

                        <div class="col-sm-12 col-xl-12 m-b-15">

                            <select name="select" class="form-control">
                                <option value="opt1">Tipo Servicio ...</option>
                                <option value="opt2">Type 2</option>
                                <option value="opt3">Type 3</option>
                                <option value="opt4">Type 4</option>
                                <option value="opt5">Type 5</option>
                                <option value="opt6">Type 6</option>
                                <option value="opt7">Type 7</option>
                                <option value="opt8">Type 8</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>
                <button type="button" class="btn btn-sm btn-info waves-effect waves-light ">APLICAR</button>
            </div>
        </div>
    </div>
</div>
