<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-user-alt-3 bg-c-green"></i>

                    <div class="d-inline">
                        <h4>COCA COLA DE MEXICO SA DE CV</h4>
                        <span>Agencia (Contrato)</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cartera de Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Detalle Cliente</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page-body start -->
    <div class="page-body">
        <!-- Encabezado -->
        <div class="card card-statistics ">
            <div class="card-header ">
                <div class="card-header-left ">
                    <h5>Statistics of visits and total revenue</h5>
                </div>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="icofont icofont-simple-left "></i></li>
                        <li><i class="icofont icofont-maximize full-card"></i></li>
                        <li><i class="icofont icofont-minus minimize-card"></i></li>
                        <li><i class="icofont icofont-refresh reload-card"></i></li>
                        <li><i class="icofont icofont-error close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block ">
                <div class="row ">
                    <div class="col-sm-4 ">
                        <div class="row stats-block">
                            <div class="col-lg-6 ">
                                <div class="progressbar-v-1">
                                    <div class="chart" data-percent="90" data-barcolor="#4680FE" data-trackcolor="#dbdada" data-linewidth="6" data-barsize="110">
                                        <div class="chart-percent"><span>90</span>%</div>
                                        <canvas height="110" width="110"></canvas></div>
                                </div>
                            </div>
                            <div class="col-lg-6 ">
                                <h2 class="f-w-400 ">5,879</h2>
                                <p class="text-muted">Total users</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 ">
                        <div class="row stats-block">
                            <div class="col-lg-6 ">
                                <div class="progressbar-v-1">
                                    <div class="chart" data-percent="70" data-barcolor="#FC6180" data-trackcolor="#dbdada" data-linewidth="6" data-barsize="110">
                                        <div class="chart-percent"><span>70</span>%</div>
                                        <canvas height="110" width="110"></canvas></div>
                                </div>
                            </div>
                            <div class="col-lg-6 ">
                                <h2 class="f-w-400 ">$2,456</h2>
                                <p class="text-muted ">Total rent</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 ">
                        <div class="row stats-block">
                            <div class="col-lg-6 ">
                                <div class="progressbar-v-1">
                                    <div class="chart" data-percent="75" data-barcolor="#FFB64D" data-trackcolor="#dbdada" data-linewidth="6" data-barsize="110">
                                        <div class="chart-percent"><span>75</span>%</div>
                                        <canvas height="110" width="110"></canvas></div>
                                </div>
                            </div>
                            <div class="col-lg-6 ">
                                <h2 class="f-w-400 ">3,198</h2>
                                <p class="text-muted ">Total revenue</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="b-t-default p-t-20 m-t-30">
                    <div class="row ">
                        <div class="col-sm-4 ">
                            <h6 class="d-inline-block m-r-10 ">User growth</h6>
                            <span class="label bg-c-blue ">28%</span>
                        </div>
                        <div class="col-sm-4 ">
                            <h6 class="d-inline-block m-r-10 ">Earning growth</h6>
                            <span class="label bg-c-pink ">12%</span>
                        </div>
                        <div class="col-sm-4 ">
                            <h6 class="d-inline-block m-r-10 ">Trade growth</h6>
                            <span class="label bg-c-yellow ">56%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin encabezado -->
        <!-- Datos Generales -->
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block user-detail-card">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/Group-user.jpg " alt="" class="img-fluid p-b-10">
                                        <div class="contact-icon">
                                            <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View"><i class="icofont icofont-eye m-0"></i></button>
                                            <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Print"><i class="icofont icofont-printer m-0"></i></button>
                                            <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Download"><i class="icofont icofont-download-alt m-0"></i></button>
                                            <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Share"><i class="icofont icofont-share m-0"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 user-detail">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Razon Social :</h6>
                                            </div>
                                            <div class="col-sm-7">
                                                <h6 class="m-b-30">Coca cola de México</h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-calendar"></i>Date of brith :</h6>
                                            </div>
                                            <div class="col-sm-7">
                                                <h6 class="m-b-30">24 October 2017</h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>Email Id :</h6>
                                            </div>
                                            <div class="col-sm-7">
                                                <h6 class="m-b-30"><a href="mailto:dummy@example.com">techdemo@example.com</a></h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-home"></i>Dirección :</h6>
                                            </div>
                                            <div class="col-sm-7">
                                                <h6 class="m-b-30">Valle de Selenga 117, Col. Valle de Aragón 3ra Secc., Ecatepec, Estado de México, CP 55280</h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Tel / Móvil :</h6>
                                            </div>
                                            <div class="col-sm-7">
                                                <h6 class="m-b-30">551276 4555 / Móvil 5512764056</h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-web"></i>Website :</h6>
                                            </div>
                                            <div class="col-sm-7">
                                                <h6 class="m-b-30"><a href="#!">http://www.demotheme.com</a></h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30"><i class="icofont icofont-fax"></i>Fax :</h6>
                                            </div>
                                            <div class="col-sm-7">
                                                <h6 class="m-b-30">+(723)542-2823</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <!-- Comentario del cliente -->
                        <div class="card bg-c-green green-contain-card ">
                            <div class="card-block-big p-t-30 ">
                                <h4 class="p-t-0">Comentarios</h4>
                            </div>
                            <div class="card m-b-0 ">
                                <div class=" card-block-big p-t-50 p-b-50 ">
                                    <img src="assets/images/widget/user2.png " alt=" " class="img-circle top ">
                                    <div class="d-inline-block m-l-20 ">
                                        <h6 class="f-w-400 ">Gregory Doe</h6>
                                        <p class="text-muted ">CEO of Music shop</p>
                                    </div>
                                    <span class="f-w-400 f-right ">8 min ago</span>
                                    <p class="text-muted ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                                        of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                    <div class="icon-card d-inline-block ">
                                        <i class="icofont icofont-share text-muted "></i>
                                        <span class="text-muted m-l-10 ">2,578</span>
                                    </div>
                                    <div class="icon-card d-inline-block p-l-20 ">
                                        <i class="icofont icofont-heart-alt text-c-pink "></i>
                                        <span class="text-c-pink m-l-10 ">5,784</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- fin comentario -->
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- historico basico -->
                <div class="card">
                    <div class="card-header">
                        <div class="card-header-left">
                            <h5>Actividad</h5>
                        </div>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="icofont icofont-simple-left "></i></li>
                                <li><i class="icofont icofont-maximize full-card"></i></li>
                                <li><i class="icofont icofont-minus minimize-card"></i></li>
                                <li><i class="icofont icofont-refresh reload-card"></i></li>
                                <li><i class="icofont icofont-error close-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>5 mins ago</small>
                                <p class="text-muted m-b-30">jennifer sent you an attachament</p>
                            </div>
                        </div>
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>45 mins ago</small>
                                <p class="text-muted m-b-30">Paul has sent a request for access to the project folder</p>
                            </div>
                        </div>
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>2 days ago</small>
                                <p class="text-muted m-b-30">Demin change the dedline on the project</p>
                            </div>
                        </div>
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>12 mins ago</small>
                                <p class="text-muted m-b-30">jennifer invite you to join the project</p>
                            </div>
                        </div>
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>60 mins ago</small>
                                <p class="text-muted m-b-30">Josephine share a folder with you</p>
                            </div>
                        </div>
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>43 mins ago</small>
                                <p class="text-muted m-b-0">Josephine share a folder with you</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin historico basico -->
            </div>
        </div>
        <!-- fin Datos Generales -->

        <!-- Inicio Servicios -->

            <div class="card card-current-prog">
                <div class="card-header">
                    <div class="card-header-left">
                        <h5>SERVICIOS CONTRATADOS</h5>
                    </div>
                    <div class="card-header-right">

                    </div>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>
                                    <div class="card-img">
                                        <a href="#!"><img class="img-rounded" src="assets/images/user.png" alt=""></a>
                                    </div>
                                    <div class="card-contain">
                                        <h6 class="f-w-600">Web application</h6>
                                        <p class="text-muted">Design &amp; development</p>
                                    </div>
                                </td>
                                <td>
                                    <div class="last-line">
                                        <p class="">Last Updated Today at 4:24 AM</p>
                                    </div>
                                    <div class="card-icon-time">
                                        <a href="#!"><i class="icofont icofont-ui-clock text-muted"></i></a>
                                        <p class="text-muted">15:32</p>
                                    </div>
                                    <div class="card-icon-time">
                                        <a href="#!"><i class="icofont icofont-ui-messaging text-muted"></i></a>
                                        <p class="text-muted">984</p>
                                    </div>
                                </td>
                                <td>
                                    <div class="progress">
                                        <div class="progress-bar bg-c-blue" style="width:50%">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="card-img">
                                        <a href="#!"><img class="img-rounded" src="assets/images/user.png" alt=""></a>
                                    </div>
                                    <div class="card-contain">
                                        <h6 class="f-w-600">login module</h6>
                                        <p class="text-muted">Development</p>
                                    </div>
                                </td>
                                <td>
                                    <div class="last-line">
                                        <p class="">Last Updated Today at 2:24 AM</p>
                                    </div>
                                    <div class="card-icon-time">
                                        <a href="#!"><i class="icofont icofont-ui-clock text-muted"></i></a>
                                        <p class="text-muted">14:45</p>
                                    </div>
                                    <div class="card-icon-time">
                                        <a href="#!"><i class="icofont icofont-ui-messaging text-muted"></i></a>
                                        <p class="text-muted">657</p>
                                    </div>
                                </td>
                                <td>
                                    <div class="progress">
                                        <div class="progress-bar bg-c-pink" style="width:90%">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="card-img">
                                        <a href="#!"><img class="img-rounded" src="assets/images/user.png" alt=""></a>
                                    </div>
                                    <div class="card-contain">
                                        <h6 class="f-w-600">Module testing</h6>
                                        <p class="text-muted">Testing</p>
                                    </div>
                                </td>
                                <td>
                                    <div class="last-line">
                                        <p class="">Last Updated Today at 2:24 AM</p>
                                    </div>
                                    <div class="card-icon-time">
                                        <a href="#!"><i class="icofont icofont-ui-clock text-muted"></i></a>
                                        <p class="text-muted">20:00</p>
                                    </div>
                                    <div class="card-icon-time">
                                        <a href="#!"><i class="icofont icofont-ui-messaging text-muted"></i></a>
                                        <p class="text-muted">200</p>
                                    </div>
                                </td>
                                <td>
                                    <div class="progress">
                                        <div class="progress-bar bg-c-yellow" style="width:70%">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        <!-- Fin servicios -->





    </div>
    <!-- Page-body end -->
</div>
