<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-user-alt-3 bg-c-green"></i>

                    <div class="d-inline">
                        <h4>Nuevo Prospecto Cliente</h4>
                        <span>Llena todos los formularios para registrar un cliente en el sistema</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cartera de Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Nuevo Cliente</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page-body start -->
    <div class="page-body">

        <div class="row">
            <div class="col-lg-12">
                <!-- tab header start -->
                <div class="tab-header card">
                    <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab" id="btnTab1"><h4>1. <span style="font-size: 14px">Información General</span></h4> </a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#binfo" role="tab" id="btnTab2"><h4>2. <span style="font-size: 14px">Datos de Facturación</span></h4> </a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#contacts" role="tab" id="btnTab3"><h4>3. <span style="font-size: 14px">Contáctos</span></h4> </a>

                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#servContratados" role="tab" id="btnTab4"><h4>4. <span style="font-size: 14px">Servicios contratados</span></h4> </a>

                            <div class="slide"></div>
                        </li>
                    </ul>
                </div>
                <!-- tab header end -->
                <!-- tab content start -->
                <div class="tab-content">
                    <!-- tab panel personal start -->
                    <div class="tab-pane active" id="personal" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Información General del cliente</h5>
                                <span>Por favor ingresa todos los campos requeridos y presiona en Siguiente para continuar</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?= CController::createUrl('site/clientes') ?>"><button class="btn btn-sm btn-danger">SALIR</button></a>
                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1" >
                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-info">PROSPECTO O CLIENTE</h5>

                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <i class="icofont icofont-users-social st-icon bg-info txt-lite-color"></i>

                                                <div class="text-left">
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="Nombre o Razón Social">
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control " id="slTipoCliente" onchange="showAgencia()">
                                                                <option value="">Tipo de Cliente ...</option>
                                                                <option value="D">Directo</option>
                                                                <option value="A">Agencia</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="Calle">
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="No Exterior">
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="No Interior">
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="Telefono Local">
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="Telefono Móvil">
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="eMail">
                                                        </div>

                                                    </div>
                                                    <div class="row" >
                                                        <div class="col-2 m-b-15">
                                                            <input type="text" class="form-control" placeholder="C.P.">
                                                        </div>
                                                        <div class="col-4 ">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Alcaldía / Municipio ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Colonia ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-3 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Nivel Cliente ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-3 m-b-15">
                                                            <select name="select" class="form-control " id="slTipoServicio" onchange="showContrato()">
                                                                <option value="">Tipo de Servicio ...</option>
                                                                <option value="C">Contrato</option>
                                                                <option value="E">Eventual</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Sector ...</option>
                                                                <option value="opt2">Sector 1</option>
                                                                <option value="opt3">Sector 2</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-12 m-b-15">
                                                            <textarea rows="4" cols="5" class="form-control" placeholder="Comentario"></textarea>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                        <div class="card widget-statstic-card" id="cardCtesAgencias" style="background-color: #fdfdfd; display: none;">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-info">CLIENTES DE AGENCIA</h5><br>

                                                </div>
                                            </div>
                                            <div class="card-block">

                                                <i class="icofont icofont-briefcase-alt-1 st-icon bg-info txt-lite-color"></i>

                                                <div class="text-left">
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="Nombre o Razón Social">
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="RFC" maxlength="13">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 m-b-15">
                                                            <textarea rows="3" cols="5" class="form-control" placeholder="Observación"></textarea>
                                                        </div>


                                                    </div>




                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <button class="btn btn-sm btn-success"  >AGREGAR</button>
                                            </div>
                                        </div>
                                        <div class="card widget-statstic-card"  id="cardContrato" style="background-color: #fdfdfd; display: none;">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-info">INFORMACION DE CONTRATO</h5>
                                                </div>
                                            </div>
                                            <div class="card-block">

                                                <i class="icofont icofont-paper st-icon bg-info txt-lite-color"></i>

                                                <div class="text-left">
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <input class="form-control " type="date">
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <input class="form-control " type="date">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 m-b-15">
                                                            <input type="file" class="form-control">
                                                        </div>


                                                    </div>






                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                    <div class="col-lg-2 m-b-15 col-md-2 col-sm-1"></div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-info" onclick="document.getElementById('btnTab2').click();" >SIGUIENTE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab pane personal end -->
                    <!-- tab pane info start -->
                    <div class="tab-pane" id="binfo" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Datos de Facturación</h5>
                                <span>Por favor ingresa todos los campos requeridos y presiona en Siguiente para continuar. (Puede registrar el número de razones sociales que desee)</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?= CController::createUrl('site/clientes') ?>"><button class="btn btn-sm btn-danger">SALIR</button></a>
                                    <a href="#"><button class="btn btn-sm btn-success">AGREGAR</button></a>
                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-1  m-b-15"></div>
                                    <div class="col-lg-6 col-md-8 col-sm-10 " >
                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-success">INFORMACIÓN FISCAL (1)</h5>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <i class="icofont icofont-id-card st-icon bg-success text-c-green"></i>


                                                <div class="text-left">
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="Nombre o Razón Social">
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="RFC">
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="Calle">
                                                        </div>
                                                        <div class="col-2 m-b-15">
                                                            <input type="text" class="form-control" placeholder="No Exterior">
                                                        </div>
                                                        <div class="col-4 m-b-15">
                                                            <input type="text" class="form-control" placeholder="No Interior">
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-2 m-b-15">
                                                            <input type="text" class="form-control" placeholder="C.P.">
                                                        </div>
                                                        <div class="col-4 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Alcaldía / Municipio ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Colonia ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="row">

                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Uso CFDI ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Método de Pago ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>


                                                    </div>
                                                    <div class="row">

                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Forma de Pago ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <input type="text" class="form-control" placeholder="eMail">
                                                        </div>


                                                    </div>

                                                </div>
                                            </div>
                                            <div class="card-footer " style="background-color: #fdfdfd; margin-top: -30px; ">
                                                <button class="btn btn-sm btn-warning right"  >QUITAR</button>

                                                <br><br>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3 m-b-15 col-md-2 col-sm-1"></div>

                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-muted" onclick="document.getElementById('btnTab1').click();" >ATRAS</button>
                                    <button class="btn btn-sm btn-info" onclick="document.getElementById('btnTab3').click();" >SIGUIENTE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab pane info end -->
                    <!-- tab pane contact start -->
                    <div class="tab-pane" id="contacts" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Contactos para el Cliente</h5>
                                <span>Por favor ingresa todos los campos requeridos y presiona en Siguiente para continuar. (Puede registrar el número de contactos que desee)</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?= CController::createUrl('site/clientes') ?>"><button class="btn btn-sm btn-danger">SALIR</button></a>
                                    <a href="#"><button class="btn btn-sm btn-success">AGREGAR</button></a>
                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-1  m-b-15"></div>
                                    <div class="col-lg-6 col-md-8 col-sm-10 card" style="background-color: #fdfdfd" >
                                        <div class="card-block user-detail-card">
                                            <div class="card-header">
                                                <h5 style="color: #93139a;">INFORMACIÓN DEL CONTACTO (1)</h5>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/Group-user.jpg " alt="" class="img-fluid p-b-10">

                                                </div>
                                                <div class="col-sm-8 user-detail">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Nombre :</h6>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" placeholder="Nombre ">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>Email:</h6>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" placeholder="eMail">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Télefono :</h6>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" placeholder="Télefono o Móvil">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="card-footer " style="background-color: #fdfdfd">
                                            <button class="btn btn-sm btn-warning right"  >QUITAR</button>

                                            <br><br>
                                        </div>




                                    </div>
                                    <div class="col-lg-3 m-b-15 col-md-2 col-sm-1"></div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-muted" onclick="document.getElementById('btnTab2').click();" >ATRAS</button>
                                    <button class="btn btn-sm btn-info" onclick="document.getElementById('btnTab4').click();" >SIGUIENTE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab pane info start -->
                    <div class="tab-pane" id="servContratados" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Servicios contratados</h5>
                                <span>Por favor ingresa los campos solicitados y presiona en 'Agregar'. Una vez que se hayan ingresado todos los servicios presiona 'Finalizar'.</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?= CController::createUrl('site/clientes') ?>"><button class="btn btn-sm btn-danger">SALIR</button></a>
                                    <a href="#"><button class="btn btn-sm btn-success">AGREGAR</button></a>


                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-1  m-b-15"></div>
                                    <div class="col-lg-6 col-md-8 col-sm-10 " >
                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="" style="color: #39adb5;">PRODUCTO O SERVICIO A CONTRATAR</h5>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <i class="icofont icofont-cart-alt st-icon  text-c-green" style="background-color: #39adb5"></i>


                                                <div class="text-left">
                                                    <div class="row">

                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Producto o Servicio ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <select name="select" class="form-control ">
                                                                <option value="opt1">Tipo de Servicio ...</option>
                                                                <option value="opt2">Nivel 1</option>
                                                                <option value="opt3">Nivel 2</option>

                                                            </select>
                                                        </div>


                                                    </div>
                                                    <div class="row">

                                                        <div class="col-3 m-b-15">
                                                            <input type="number" class="form-control" placeholder="Cantidad">
                                                        </div>
                                                        <div class="col-3 m-b-15">
                                                            <input type="number" class="form-control" placeholder="Importe">
                                                        </div>
                                                        <div class="col-3 m-b-15">
                                                            <input class="form-control " type="date">
                                                        </div>
                                                        <div class="col-3 m-b-15">
                                                            <input class="form-control " type="date">
                                                        </div>

                                                    </div>



                                                </div>
                                            </div>
                                            <div class="card-footer " style="background-color: #fdfdfd; margin-top: -30px; ">
                                                <button class="btn btn-sm btn-warning right"  >QUITAR</button>

                                                <br><br>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-lg-3 m-b-15 col-md-2 col-sm-1"></div>

                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-muted" onclick="document.getElementById('btnTab3').click();" >ATRAS</button>
                                    <button class="btn btn-sm btn-primary"  >TERMINAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab pane info end -->
                </div>
                <!-- tab content end -->
            </div>
        </div>

    </div>
    <!-- Page-body end -->
</div>
<script>
    function showAgencia() {
        if ($("#slTipoCliente").val() == "A") {
            document.getElementById("cardCtesAgencias").style.display = "";
        } else {
            document.getElementById("cardCtesAgencias").style.display = "none";
        }


    }

    function showContrato() {

        if ($("#slTipoServicio").val() == "C") {
            document.getElementById("cardContrato").style.display = "";
        } else {
            document.getElementById("cardContrato").style.display = "none";
        }


    }
</script>