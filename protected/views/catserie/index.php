<?php
$tipos= array('F'=>'Factura','N'=>'Nota de Crédito','C'=>'Cotización');
?>
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-database-add bg-c-blue"></i>

                    <div class="d-inline">
                        <h4>Catálogo de Series</h4>
                        <span>Administración de catálogos del sistema</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 right">

                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Catálogos</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Series</a>
                        </li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">

        <!-- Config. table start -->
        <div class="card">
            <div class="card-header">
                <h5>Resultados de la Consulta</h5>
                <span>
					<?php if (isset($_POST["btnBuscar"]) && !empty($_POST['Catserie']['cp'])) { ?>
						Se muestran todos los resultados con el CP: <?=$_POST['Catserie']['cp']?>
					<?php }else{ ?>
						Realiza una búsqueda para mostrar registros
					<?php } ?>
				</span>
                <div class="card-header-right" style="margin-right: 10px;">

                    <button class="btn btn-sm btn-info waves-effect"  data-toggle="modal" data-target="#nuevoRegistro" onclick="setNuevo();">NUEVO VALOR</button>
                    <button class="btn btn-sm btn-primary waves-effect"  data-toggle="modal" data-target="#default-Modal">FILTRAR</button>
                    <button class="btn btn-sm btn-success waves-effect" >CSV</button>
                    <button class="btn btn-sm btn-danger waves-effect"  >PDF</button>


                </div>
            </div>
            <div class="card-block">
                <div class="">
                    <div class="dt-responsive table-responsive" >
                        <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="row" >
                                <div class="col-xs-12 col-sm-12 col-lg-12" >
                                    <table width="100%" id="res-config" class="table compact table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 1544px; font-size: 12px; ">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >ID Cátalogo</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >Serie</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >Estatus</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >Tipo</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >Usuario</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >Fecha Alta</th>




                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 10px; display: none;" aria-label="">-</th>


                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($catalogo as $item) {
                                                ?>
                                                <tr role="row" class="odd">
                                                    <td><?= $item->id ?></td>
                                                    <td><?= $item->serie ?></td>
                                                    <td><?= ($item->estatus=="A")?"Alta":"Baja" ?></td>
                                                    <td><?= $tipos[$item->tipo] ?></td>
                                                    <td><?= $item->usuario ?></td>
                                                    <td><?= $item->fechaAlta ?></td>




                                                    <td align="center">
													<?php if($item->estatus=="A"){ ?>
                                                        <?= CHtml::link('Dar de Baja <i class="icofont icofont-ui-close"></i>', CController::createUrl('Catserie/eliminar', array('id' => $item->id,'tipo'=>'B')), array("class" => "m-r-15 text-danger", 'confirm' => 'Estás seguro de querer dar de baja este registro?')); ?>
													<?php }else{ ?>
                                                        <?= CHtml::link('Dar de Alta <i class="icofont icofont-ui-check"></i>', CController::createUrl('Catserie/eliminar', array('id' => $item->id,'tipo'=>'A')), array("class" => "m-r-15 text-success", 'confirm' => 'Estás seguro de querer dar de alta este registro?')); ?>
													<?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>


                                        </tbody>
                                    </table>
                                    </br>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Config. table end -->

    </div>
</div>
<!-- MODAL FILTROS -->
<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <div class="modal-content">
  <?php
  $bolError=false;
  $form2 = $this->beginWidget('CActiveForm', array(
  'id' => 'filtrosBusqueda',
  'enableClientValidation' => true,
  'clientOptions' => array(
  'validateOnSubmit' => true,
  ),
  ));
  ?>
  <div class="modal-header bg-primary" >
  <h4 class="modal-title">Filtros de Búsqueda</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">×</span>
  </button>
  </div>
  <div class="modal-body">
  <?php
  if(!empty($busqueda->errors)){
  $bolError=true;
  ?>
  <div class="alert alert-danger icons-alert" >
  <button type="button" id="errorNewFil" name="errorNewFil" class="close" data-dismiss="alert" aria-label="Close">
  <i class="icofont icofont-close-line-circled"></i>
  </button>
  <p><strong>Atención!</strong> <?=$form2->errorSummary($busqueda) ?></p>
  </div>

  <?php
  }
  ?>
  <p>Selecciona uno o varios campos de búsqueda, a continuación presiona 'Aplicar' para ejecutar tu petición.</p>
  <div class="container">
  <div class="row " >
  <div class="col-12 m-b-15">
  <h4 class="sub-title text-muted"><strong>SELECCIONA UNA OPCIÓN</strong></h4>

  <div class="row">
  <div class="col-4">
  <?php
  echo $form2->textField($busqueda, 'serie', array('class' => "form-control", 'placeholder' => "Serie", 'title' => "Serie"));

  ?>
  </div>
  <div class="col-4">
  <?php
  echo $form2->dropDownList($busqueda, 'estatus', array('A'=>'Alta','B'=>'Baja'), array('class' => "form-control", 'empty' => "Selecciona Estatus..."));

  ?>
  </div>
  <div class="col-4">
  <?php
  echo $form2->dropDownList($busqueda, 'tipo', $tipos, array('class' => "form-control", 'empty' => "Selecciona Tipo..."));

  ?>
  </div>
  </div>

  </div>

  <input type="hidden" value="" id="rowElimina" name="rowElimina" >

  </div>

  </div>

  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>
  <a href="<?=CController::createUrl('Catserie/index') ?>"> <button type="button" class="btn btn-sm btn-warning waves-effect waves-light ">LIMPIAR</button></a>
  <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light " id="btnBuscar" name="btnBuscar" value="1">APLICAR</button>
  </div>
  <?php $this->endWidget(); ?>
  </div>
  </div>
  </div>


<!-- MODAL NEW -->
<div class="modal fade" id="nuevoRegistro" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $bolError = false;
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-header bg-info" >
                <h4 class="modal-title">Nueva Serie</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                if (!empty($new->errors)) {
                    $bolError = true;
                    ?>
                    <div class="alert alert-danger icons-alert" >
                        <button type="button" id="errorNew" name="errorNew" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="icofont icofont-close-line-circled"></i>
                        </button>
                        <p><strong>Atención!</strong> <?= $form->errorSummary($new) ?></p>
                    </div>

                    <?php
                }
                ?>
                <p>Ingrese toda la información que se le solicita.</p>
                <div class="container">
                    <div class="row " >
                        <div class="col-12 m-b-15">
                            <h4 class="sub-title text-muted"><strong>INFORMACIÓN</strong></h4>
                            <div class="row">
                                <div class="col-6">
                                    <?php echo $form->textField($new, 'serie', array('class' => "form-control limpiar", 'placeholder' => "Serie", 'title' => "Serie")); ?>
                                </div>
                                <div class="col-6">
                                    <?php echo $form->dropDownList($new, 'tipo', $tipos, array('class' => "form-control limpiar", 'empty' => "Selecciona Tipo...")); ?>
                                </div>
							</div>
							
                        </div>




                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>
                <a href="<?= CController::createUrl('Catserie/index') ?>"> <button type="button" class="btn btn-sm btn-warning waves-effect waves-light ">LIMPIAR</button></a>
                <button type="submit" class="btn btn-sm btn-info waves-effect waves-light " id="btnNuevo" name="btnNuevo" value="1">APLICAR</button>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>


<?php if ($bolError == true) { ?>
    <script>
        window.onload = function () {

            $('#nuevoRegistro').modal('show');
        };

    </script>
<?php } ?>
<script>
    function setNuevo() {
        //LIMPIAR CAMPOS

        $(".limpiar").val("");
        $("#errorNew").click();


    }
</script>
<?php if (Yii::app()->user->hasFlash('error')) { ?>
<script>
    window.onload = function () {
        PNotify.desktop.permission();
        (new PNotify({
            title: 'Cuidado!',
            type: 'danger',
            text: '<?= Yii::app()->user->getFlash('error'); ?>',
            desktop: {
                desktop: true,
                icon: 'assets/images/pnotify/success.png'
            }
        })
                ).get().click(function (e) {
            if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target))
                return;
            // alert('Hey! You clicked the desktop notification!');
        });
    }


</script>
<?php } ?>
<?php if (Yii::app()->user->hasFlash('success')) { ?>
<script>
    window.onload = function () {
        PNotify.desktop.permission();
        (new PNotify({
            title: 'Proceso Completado!',
            type: 'success',
            text: '<?= Yii::app()->user->getFlash('success'); ?>',
            desktop: {
                desktop: true,
                icon: 'assets/images/pnotify/success.png'
            }
        })
                ).get().click(function (e) {
            if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target))
                return;
            // alert('Hey! You clicked the desktop notification!');
        });
    }


</script>
<?php } ?>