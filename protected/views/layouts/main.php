<!DOCTYPE html>
<html lang="es">
    <head>
        <title><?php echo CHtml::encode($this->pageTitle); ?> </title>
        <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 10]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="#">
        <meta name="keywords" content="">
        <meta name="author" content="#">
        <!-- Favicon icon -->
        <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/favicon.ico" type="image/x-icon">
        <!-- Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <!-- Required Fremwork -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/bootstrap/css/bootstrap.min.css">
        <!-- sweet alert framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/sweetalert/css/sweetalert.css">
        <!-- C3 chart -->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/c3/css/c3.css" type="text/css" media="all">



        <!-- lightbox Fremwork -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/lightbox2/css/lightbox.min.css">


        <!-- themify-icons line icon -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/icon/themify-icons/themify-icons.css">
        <!-- ico font -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/icon/icofont/css/icofont.css">
        <!-- flag icon framework css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/flag-icon/flag-icon.min.css">
        <!-- font awesome -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/icon/font-awesome/css/font-awesome.min.css">
        <!-- Date-time picker css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
        <!-- Date-range picker css  -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
        <!-- Date-Dropper css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datedropper/css/datedropper.min.css" />
        <!-- Color Picker css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/spectrum/css/spectrum.css" />
        <!-- Mini-color css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery-minicolors/css/jquery.minicolors.css" />
        <!-- notify js Fremwork -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/css/pnotify.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/css/pnotify.brighttheme.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/css/pnotify.buttons.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/css/pnotify.history.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/css/pnotify.mobile.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/pnotify/notify.css">

        <!-- Menu-Search css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/menu-search/css/component.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/css/component.css">
        <!-- Data Table Css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/data-table/css/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/data-table/extensions/responsive/css/responsive.dataTables.css">
        <!-- Style.css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/css/jquery.mCustomScrollbar.css">
        <!--forms-wizard css-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery.steps/css/jquery.steps.css">

        <!-- Required Fremwork -->



        <!-- jpro forms css -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/j-pro/css/demo.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/j-pro/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/j-pro/css/j-pro-modern.css">


        <!-- Select 2 css -->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/select2/css/select2.min.css"/>
        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/funciones.js"></script>
        <script>
            var baseUrlAx = '<?php echo Yii::app()->request->baseUrl; ?>/index.php/';
            var pathUrl = '<?=Yii::app()->getBaseUrl() ?>';
        </script>
        <script src="https://kit.fontawesome.com/4997cd7d58.js" crossorigin="anonymous"></script>
    </head>

    
    <body>
        <!-- Pre-loader start -->
        <div class="theme-loader">
            <div class="ball-scale">
                <div class='contain'>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pre-loader end -->
        <div id="pcoded" class="pcoded">
            <div class="pcoded-overlay-box"></div>
            <div class="pcoded-container navbar-wrapper">

                <nav class="navbar header-navbar pcoded-header " style="background-color: #096699">
                    <div class="navbar-wrapper" >

                        <div class="navbar-logo  " style="background-color: #096699">
                            <a class="mobile-menu" id="mobile-collapse" href="#!">
                                <i class="ti-menu"></i>
                            </a>
                            <a class="mobile-search morphsearch-search" href="#">
                                <i class="ti-search"></i>
                            </a>
                            <a href="index.html">
                                <img class="img-fluid" style="padding-left: 15px;" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/logo.png" alt="Theme-Logo" />
                            </a>
                            <a class="mobile-options">
                                <i class="ti-more"></i>
                            </a>
                        </div>

                        <div class="navbar-container container-fluid" >

                            <ul class="nav-right " style="background-color: #096699">
                                <li>
                                    <a href="#!" onclick="javascript:toggleFullScreen()">
                                        <i class="ti-fullscreen text-white"></i>
                                    </a>
                                </li>
                                <li class="header-notification">
                                    <a href="#!">
                                        <i class="ti-bell text-white"></i>
                                        <span class="badge bg-c-pink"></span>
                                    </a>
                                    <ul class="show-notification">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user "><?php echo ucwords(strtolower(Yii::app()->user->nombre)); ?></h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Joseph William</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Sara Soudein</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="header-notification">
                                    <a href="#!" class="displayChatbox">
                                        <i class="ti-comments text-white"></i>
                                        <span class="badge bg-c-green"></span>
                                    </a>
                                </li>
                                <li class="user-profile header-notification ">
                                    <a href="#!">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-4.jpg" class="img-radius" alt="User-Profile-Image">
                                        <span class="text-white"><?php echo ucwords(strtolower(Yii::app()->user->nombre)); ?></span>
                                        <i class="ti-angle-down text-white"></i>
                                    </a>
                                    <ul class="show-notification profile-notification">

                                        <li>
                                            <a href="user-profile.html">
                                                <i class="ti-user"></i> Perfil
                                            </a>
                                        </li>
                                        <li>
                                            <a href="email-inbox.html">
                                                <i class="ti-email"></i> Mis Mensajes
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?php echo CController::createUrl('site/logout') ?>">
                                                <i class="ti-layout-sidebar-left"></i> Cerrar Sesión
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <!-- search -->
                            <div id="morphsearch" class="morphsearch">
                                <form class="morphsearch-form">
                                    <input class="morphsearch-input" type="search" placeholder="Search..." />
                                    <button class="morphsearch-submit" type="submit">Search</button>
                                </form>
                                <div class="morphsearch-content">
                                    <div class="dummy-column">
                                        <h2>People</h2>
                                        <a class="dummy-media-object img-radius" href="#!">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-4.jpg" class="img-radius" alt="Sara Soueidan" />
                                            <h3>Sara Soueidan</h3>
                                        </a>
                                        <a class="dummy-media-object img-radius" href="#!">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-2.jpg" class="img-radius" alt="Shaun Dona" />
                                            <h3>Shaun Dona</h3>
                                        </a>
                                    </div>
                                    <div class="dummy-column">
                                        <h2>Popular</h2>
                                        <a class="dummy-media-object img-radius" href="#!">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-3.jpg" class="img-radius" alt="PagePreloadingEffect" />
                                            <h3>Page Preloading Effect</h3>
                                        </a>
                                        <a class="dummy-media-object img-radius" href="#!">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-4.jpg" class="img-radius" alt="DraggableDualViewSlideshow" />
                                            <h3>Draggable Dual-View Slideshow</h3>
                                        </a>
                                    </div>
                                    <div class="dummy-column">
                                        <h2>Recent</h2>
                                        <a class="dummy-media-object img-radius" href="#!">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-2.jpg" class="img-radius" alt="TooltipStylesInspiration" />
                                            <h3>Tooltip Styles Inspiration</h3>
                                        </a>
                                        <a class="dummy-media-object img-radius" href="#!">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-3.jpg" class="img-radius" alt="NotificationStyles" />
                                            <h3>Notification Styles Inspiration</h3>
                                        </a>
                                    </div>
                                </div>
                                <!-- /morphsearch-content -->
                                <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
                            </div>
                            <!-- search end -->
                        </div>
                    </div>
                </nav>

                <!-- Sidebar chat start -->
                <div id="sidebar" class="users p-chat-user showChat">
                    <div class="had-container">
                        <div class="card card_main p-fixed users-main">
                            <div class="user-box">
                                <div class="card-block">
                                    <div class="right-icon-control">
                                        <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                        <div class="form-icon">
                                            <i class="icofont icofont-search"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="main-friend-list">
                                    <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                        <a class="media-left" href="#!">
                                            <img class="media-object img-radius img-radius" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-3.jpg" alt="Generic placeholder image ">
                                            <div class="live-status bg-success"></div>
                                        </a>
                                        <div class="media-body">
                                            <div class="f-13 chat-header">Josephin Doe</div>
                                        </div>
                                    </div>
                                    <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                        <a class="media-left" href="#!">
                                            <img class="media-object img-radius" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-2.jpg" alt="Generic placeholder image">
                                            <div class="live-status bg-success"></div>
                                        </a>
                                        <div class="media-body">
                                            <div class="f-13 chat-header">Lary Doe</div>
                                        </div>
                                    </div>
                                    <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                        <a class="media-left" href="#!">
                                            <img class="media-object img-radius" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                            <div class="live-status bg-success"></div>
                                        </a>
                                        <div class="media-body">
                                            <div class="f-13 chat-header">Alice</div>
                                        </div>
                                    </div>
                                    <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                        <a class="media-left" href="#!">
                                            <img class="media-object img-radius" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                                            <div class="live-status bg-success"></div>
                                        </a>
                                        <div class="media-body">
                                            <div class="f-13 chat-header">Alia</div>
                                        </div>
                                    </div>
                                    <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                        <a class="media-left" href="#!">
                                            <img class="media-object img-radius" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-2.jpg" alt="Generic placeholder image">
                                            <div class="live-status bg-success"></div>
                                        </a>
                                        <div class="media-body">
                                            <div class="f-13 chat-header">Suzen</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Sidebar inner chat start-->
                <div class="showChat_inner">
                    <div class="media chat-inner-header">
                        <a class="back_chatBox">
                            <i class="icofont icofont-rounded-left"></i> Josephin Doe
                        </a>
                    </div>
                    <div class="media chat-messages">
                        <a class="media-left photo-table" href="#!">
                            <img class="media-object img-radius img-radius m-t-5" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                        </a>
                        <div class="media-body chat-menu-content">
                            <div class="">
                                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                                <p class="chat-time">8:20 a.m.</p>
                            </div>
                        </div>
                    </div>
                    <div class="media chat-messages">
                        <div class="media-body chat-menu-reply">
                            <div class="">
                                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                                <p class="chat-time">8:20 a.m.</p>
                            </div>
                        </div>
                        <div class="media-right photo-table">
                            <a href="#!">
                                <img class="media-object img-radius img-radius m-t-5" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                            </a>
                        </div>
                    </div>
                    <div class="chat-reply-box p-b-20">
                        <div class="right-icon-control">
                            <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                            <div class="form-icon">
                                <i class="icofont icofont-paper-plane"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Sidebar inner chat end-->
                <div class="pcoded-main-container">
                    <div class="pcoded-wrapper">
                        <nav class="pcoded-navbar">
                            <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                            <div class="pcoded-inner-navbar main-menu">
                                <?php
                                $criteriaMenu= new CDbCriteria();
                                $criteriaMenu->select="DISTINCT(menu),icon,idCatMenu";
                                $criteriaMenu->compare("idCatPerfil",Yii::app()->user->idCatPerfil );

                                $criteriaMenu->compare("seccion","1");
                                $Menus=Viewperfilboton::model()->findAll($criteriaMenu);
                                if(count($Menus)>0){
                                    ?>
                                    <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation">Navegación</div>
                                    <?php
                                }
                                ?>


                                <ul class="pcoded-item pcoded-left-item" item-border="true" item-border-style="none" subitem-border="true">
                                    <?php

                                    foreach ($Menus as $menu){
                                        ?>
                                        <li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style6">
                                            <a href="javascript:void(0)">
                                                <span class="pcoded-micon"><i class="<?=$menu->icon ?>"></i><b>D</b></span>
                                                <span class="pcoded-mtext" data-i18n="nav.dash.main"><?=$menu->menu ?></span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                            <ul class="pcoded-submenu">
                                                <?php
                                                $criteriaBoton=new CDbCriteria();
                                                $criteriaBoton->compare("idCatPerfil",Yii::app()->user->idCatPerfil );
                                                $criteriaBoton->compare("idCatMenu",$menu->idCatMenu );
                                                $criteriaBoton->addCondition("idPadre is NULL");
                                                $criteriaBoton->order="boton";
                                                $botones=Viewperfilboton::model()->findAll($criteriaBoton);
                                                foreach ($botones as $boton){
                                                    if($boton->hijos==0){
                                                        ?>
                                                        <li class="">
                                                            <a href="<?php echo CController::createUrl($boton->link) ?>">
                                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                                <span class="pcoded-mtext" data-i18n="nav.dash.default"><?=$boton->boton ?></span>
                                                                <span class="pcoded-mcaret"></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style6">
                                                            <a href="javascript:void(0)">
                                                                <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                                                <span class="pcoded-mtext"><?=$boton->boton ?></span>
                                                                <span class="pcoded-mcaret"></span>
                                                            </a>
                                                            <?php
                                                            $criteriaBotonSub=new CDbCriteria();
                                                            $criteriaBotonSub->compare("idCatPerfil",Yii::app()->user->idCatPerfil );
                                                            $criteriaBotonSub->compare("idCatMenu",$menu->idCatMenu );
                                                            $criteriaBotonSub->compare("idPadre",$boton->id );
                                                            $botonesSub=Viewperfilboton::model()->findAll($criteriaBotonSub);
                                                            foreach ($botonesSub as $botonSub){
                                                                ?>
                                                                <ul class="pcoded-submenu">
                                                                    <li class=" ">
                                                                        <a href="<?php echo CController::createUrl($botonSub->link) ?>">
                                                                            <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                                            <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout"><?=$botonSub->boton ?></span>
                                                                            <span class="pcoded-mcaret"></span>
                                                                        </a>
                                                                    </li>


                                                                </ul>
                                                                <?php
                                                            }
                                                            ?>

                                                        </li>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>



                                            </ul>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                </ul>
                                <?php
                                $criteriaMenu= new CDbCriteria();
                                $criteriaMenu->select="DISTINCT(menu),icon,idCatMenu";
                                $criteriaMenu->compare("idCatPerfil",Yii::app()->user->idCatPerfil );
                                $criteriaMenu->compare("seccion","2");
                                $Menus=Viewperfilboton::model()->findAll($criteriaMenu);
                                if(count($Menus)>0){
                                    ?>
                                    <div class="pcoded-navigatio-lavel" data-i18n="nav.category.ui-element" menu-title-theme="theme1">Configuraciones</div>
                                    <?php
                                }
                                ?>
                                <ul class="pcoded-item pcoded-left-item" item-border="true" item-border-style="none" subitem-border="true">
                                    <?php

                                    foreach ($Menus as $menu){
                                        ?>
                                        <li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style6">
                                            <a href="javascript:void(0)">
                                                <span class="pcoded-micon"><i class="<?=$menu->icon ?>"></i><b>D</b></span>
                                                <span class="pcoded-mtext" data-i18n="nav.dash.main"><?=$menu->menu ?></span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                            <ul class="pcoded-submenu">
                                                <?php
                                                $criteriaBoton=new CDbCriteria();
                                                $criteriaBoton->compare("idCatPerfil",Yii::app()->user->idCatPerfil );
                                                $criteriaBoton->compare("idCatMenu",$menu->idCatMenu );
                                                $botones=Viewperfilboton::model()->findAll($criteriaBoton);
                                                foreach ($botones as $boton){
                                                    if($boton->hijos==0){
                                                        ?>
                                                        <li class="">
                                                            <a href="<?php echo CController::createUrl($boton->link) ?>">
                                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                                <span class="pcoded-mtext" data-i18n="nav.dash.default"><?=$boton->boton ?></span>
                                                                <span class="pcoded-mcaret"></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style6">
                                                            <a href="javascript:void(0)">
                                                                <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                                                <span class="pcoded-mtext"><?=$boton->boton ?></span>
                                                                <span class="pcoded-mcaret"></span>
                                                            </a>
                                                            <?php
                                                            $criteriaBotonSub=new CDbCriteria();
                                                            $criteriaBotonSub->compare("idCatPerfil",Yii::app()->user->idCatPerfil );
                                                            $criteriaBotonSub->compare("idCatMenu",$menu->idCatMenu );
                                                            $criteriaBotonSub->compare("idPadre",$boton->id );
                                                            $botonesSub=Viewperfilboton::model()->findAll($criteriaBotonSub);
                                                            foreach ($botonesSub as $botonSub){
                                                                ?>
                                                                <ul class="pcoded-submenu">
                                                                    <li class=" ">
                                                                        <a href="<?php echo CController::createUrl($botonSub->link) ?>">
                                                                            <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                                            <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout"><?=$botonSub->boton ?></span>
                                                                            <span class="pcoded-mcaret"></span>
                                                                        </a>
                                                                    </li>


                                                                </ul>
                                                                <?php
                                                            }
                                                            ?>

                                                        </li>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>



                                            </ul>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                </ul>
                            </div>
                        </nav>
                        <div class="pcoded-content">
                            <div class="pcoded-inner-content">
                                <div class="main-body">

                                    <?php echo $content; ?>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Required Jquery -->

        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/popper.js/js/popper.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/bootstrap/js/bootstrap.min.js"></script>
        <!-- jquery slimscroll js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
        <!-- modernizr js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/modernizr/js/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/modernizr/js/css-scrollbars.js"></script>
        <!-- Chart js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/chart.js/js/Chart.js"></script>
        <!-- c3 chart js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/c3/js/c3.js"></script>
        <!-- counter js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/jquery.easypiechart.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/counter.js"></script>
        <!-- Morris Chart js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/raphael/js/raphael.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/morris.js/js/morris.js"></script>
        <!-- Float Chart js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/chart/float/jquery.flot.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/chart/float/jquery.flot.categories.js "></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/chart/float/jquery.flot.pie.js "></script>

        <!-- Rickshow Chart js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/d3/js/d3.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/chart/rickshow/d3.layout.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/rickshaw/js/rickshaw.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/jquery.sparkline.js"></script>
        <!-- gauge js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/gauge/gauge.min.js "></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/amchart/amcharts.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/amchart/serial.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/amchart/gauge.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/amchart/pie.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/amchart/light.js"></script>

        <!-- peity Chart js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/peity/js/jquery.peity.js "></script>
        <!-- knob js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/chart/knob/jquery.knob.js"></script>

        <!-- sweet alert js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/sweetalert/js/sweetalert.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/modalCliente.js"></script>

        <!-- modalEffects js nifty modal window effects -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/modalEffects.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/classie.js"></script>
        <!-- data-table js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/data-table/js/jszip.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/data-table/js/pdfmake.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/data-table/js/vfs_fonts.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/data-table/extensions/responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
        <!-- am chart -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/amchart/amcharts.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/widget/amchart/serial.min.js"></script>

        <!-- Todo js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/todo/todo.js "></script>
        <!--Forms - Wizard js-->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery.cookie/js/jquery.cookie.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery.steps/js/jquery.steps.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery-validation/js/jquery.validate.js"></script>




        <!-- pnotify js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.desktop.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.buttons.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.confirm.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.callbacks.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.animate.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.history.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.mobile.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/../bower_components/pnotify/js/pnotify.nonblock.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/pnotify/notify.js"></script>


        <!-- i18next.min.js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/i18next/js/i18next.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
        <!-- Custom js -->
        <

        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/j-pro/js/custom/review-form.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/data-table/extensions/responsive/js/responsive-custom.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/dashboard/custom-dashboard.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/SmoothScroll.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/pcoded.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/demo-12.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/js/script.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/pages/dashboard/crm-dashboard.min.js"></script>

        <!-- Select 2 js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/bower_components/select2/js/select2.js"></script>





        <!-- Globales especificos js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/aknInit.js"></script>
        <script>
            $( "a" ).dblclick(function() {
                return false
            });
        </script>



    </body>

</html>
