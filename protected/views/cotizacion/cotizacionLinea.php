<div class="card-block">
    <div class="float-right" style="margin-top: -60px;">
        <span class="text-danger" style="font-size: 18px"><strong> #<?= $datosBase->folio ?>/<?= $datosBase->id ?></strong></span>
    </div>
    <div class="row invoive-info">
        <div class="col-sm-8 col-xs-12 invoice-client-info">
            <span class="text-muted"><strong>Información del Cliente</strong></span>
            <h3 class="m-t-5"><?= $datosBase->nombre ?></h3>
            <p class="m-0 p-t-10" sty><i class="fas fa-map-marked-alt m-r-5 text-purple"></i> <?= $datosCliente->calle ?>, No. <?= $datosCliente->numExt ?><?=!empty($datosCliente->numInt)?" Int ".$datosCliente->numInt:"" ?></p>
            <p class="m-0 p-l-25">Col. <?= $datosCliente->colonia ?>, <?= $datosCliente->municipio ?>,</p>
            <p class="m-0 p-l-25">Estado. <?= $datosCliente->estado ?>, <?= $datosCliente->cp ?>, <?= $datosCliente->pais ?></p>
            <p class="m-0"><i class="fas fa-phone-alt text-info m-r-5"></i> <?= $datosCliente->telefono ?></p>
            <p><?= $datosCliente->eMail ?></p>
        </div>
        <div class="col-sm-4">
            <span class="text-muted"><strong>Información Cotización</strong></span>
            <table class="table invoice-table invoice-order table-borderless" style="width: 100%; margin-top: 20px; font-size: 13px;">
                <tbody>
                <tr >
                    <th>Fecha registro</th>
                    <td align="right">
                        <i class="fas fa-calendar-alt mr-2 text-success"></i> <?= date("m/d/Y",strtotime($datosBase->fechaAlta)) ?>

                    </td>
                </tr>
                <tr >
                    <th>Hora registro</th>
                    <td align="right">
                        <i class="fas fa-clock  text-warning"></i> <?= date("H:i:s",strtotime($datosBase->fechaAlta)) ?> Hrs

                    </td>
                </tr>
                <tr>
                    <th>Vencimiento</th>
                    <td align="right"> <i class="fas fa-calendar-alt mr-2 text-danger"></i> <?= date("m/d/Y",strtotime($datosBase->fechaVencimiento)) ?></td>
                </tr>



                <tr>
                    <th>Moneda</th>
                    <td align="right">
                        <?=$datosBase->moneda ?>
                    </td>
                </tr>
                <tr>
                    <th>Tipo Cambio MN</th>
                    <td align="right">
                        $<?= number_format($datosBase->tipoCambio,2) ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive" style="min-height: 300px;">
                <table class="table  invoice-detail-table" >
                    <thead>
                    <tr class="text-white" style="background-color: steelblue">
                        <th style="width: 50px; ">Cant.</th>
                        <th>Producto</th>
                        <th>P. Unit</th>
                        <th>Subtotal</th>
                        <th>Desc.</th>
                        <th>Subtotal</th>
                        <th>Tras.</th>
                        <th>Ret.</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $acMoneda="";
                    if($datosBase->moneda=="USD"){
                        $acMoneda="US";
                    }elseif($datosBase->moneda=="CAD"){
                        $acMoneda="CAD";
                    }if($datosBase->moneda=="EUR"){
                        $acMoneda="€";
                    }
                    $subtotal=0;
                    $retenidos=0;
                    $trasladados=0;
                    $descuentos=0;
                    $total=0;
                    $sumaTotal=0;
                    $sumaSubtotal=0;
                    $sumaDescuetno=0;
                    $sumaRetenidos=0;
                    $sumaTrasladados=0;
                    foreach ($productos as $item) {
                        $subtotal=$item->precio * $item->cantidad;
                        $subtotalDesc=$subtotal-$item->descuento;
                        $totalP=$subtotalDesc+$item->traslados-$item->retencion;
                        $sumaTotal+=$totalP;
                        $sumaSubtotal+=$subtotal;
                        $sumaDescuetno+=$item->descuento;
                        $sumaRetenidos+=$item->retencion;
                        $sumaTrasladados+=$item->traslados;
                        ?>

                        <tr>
                            <td style="text-align: left;"><?= $item->cantidad ?></td>
                            <td style="text-align: left"><?= ucfirst(mb_strtolower($item->descripcion)) ?> <?= ucfirst(mb_strtolower($item->observacion)) ?></td>
                            <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($item->precio,2) ?></td>
                            <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($subtotal,2) ?></td>
                            <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($item->descuento,2) ?></td>
                            <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($subtotalDesc,2) ?></td>
                            <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($item->traslados,2) ?></td>
                            <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($item->retencion,2) ?></td>
                            <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($totalP,2) ?></td>

                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-responsive invoice-table invoice-total">
                <tbody>
                <tr>
                    <th>Sub Total :</th>
                    <td><?=$acMoneda ?>$<?= number_format($sumaSubtotal, 2) ?></td>
                </tr>
                <?php
                if($sumaDescuetno>0){
                    ?>
                    <tr>
                        <th>Descuento :</th>
                        <td><?=$acMoneda ?>$<?= number_format($sumaDescuetno, 2) ?></td>
                    </tr>
                    <tr>
                        <th>Subtotal :</th>
                        <td><?=$acMoneda ?>$<?= number_format($sumaSubtotal-$sumaDescuetno, 2) ?></td>
                    </tr>
                <?php
                }
                ?>
                <?php
                if($sumaTrasladados>0) {
                    ?>
                    <tr>
                        <th>Imp. Trasladados:</th>
                        <td><?=$acMoneda ?>$<?= number_format($sumaTrasladados, 2) ?></td>
                    </tr>
                    <?php
                }
                ?>
                <?php
                if($sumaRetenidos>0) {
                    ?>
                    <tr>
                        <th>Imp. Retenidos:</th>
                        <td><?=$acMoneda ?>$<?= number_format($sumaRetenidos, 2) ?></td>
                    </tr>
                    <?php
                }
                ?>



                <tr class="text-info">
                    <td>
                        <hr>
                        <h5 class="text-primary">Total :</h5>
                    </td>
                    <td>
                        <hr>
                        <h5 class="text-primary"><?=$acMoneda ?>$<?= number_format($sumaTotal, 2) ?></h5>
                    </td>
                </tr>
                </tbody>
            </table>
            <div id="invoice-footer">
                <div class="row">
                    <div class="col-md-7 col-sm-12">
                        <h6><strong>Notas y Observaciones</strong></h6>
                        <p><?=ucfirst(mb_strtolower($datosBase->observacion)) ?></p>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>