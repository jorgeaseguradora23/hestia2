<body>
<meta charset="UTF-8">
<div style="display: inline-block;width: 100%; font-family: Helvetica, Arial, sans-serif">
        <table style="width:100%;">
            <tr>
                <td style="text-align: center;background-color: #096699;padding-top:10px;padding-bottom:10px;"><img src="<?= Yii::app()->basePath ?>/../themes/default/assets/images/logo.png"/></td>
            </tr>
            <tr>
                <td align="right" style="padding-top: 5px;color: red">#<?= $cotizacion->folio ?>/<?= $cotizacion->id ?></td>
            </tr>
        </table>

        <table style="width:100%; margin-top: 10px;">
            <tr style="vertical-align: top;">
                <td style="width:70%;font-size: 12px;">


                    <span style="color: slategray" ><strong>Información del Cliente</strong></span>
                    <h2 style="margin-top: 0px"><?= $cliente->nombre ?></h2>
                    <?= $cliente->calle ?>, No. <?= $cliente->numExt ?><?=!empty($cliente->numInt)?" Int ".$cliente->numInt:"" ?><br>
                    Col. <?= $cliente->colonia ?>, <?= $cliente->municipio ?>,<br>
                    Estado. <?= $cliente->estado ?>, <?= $cliente->cp ?>, <?= $cliente->pais ?><br>
                    Tel. <?= $cliente->telefono ?>
                </td>
                <td style="width:20%;font-size: 12px;">
                    <p style="color: slategray" ><strong>Información General</strong></p>
                    <p style="font-size: 10px;line-height:15px;  "><strong>
                            FECHA REGISTRO<br>
                            HORA REGISTRO<br>
                            VENCIMIENTO<br>
                            MONEDA<br>
                            TIPO CAMBIO MN
                        </strong>
                    </p>
                </td>
                <td style="width:10%;font-size: 12px;" align="right">
                    <p style="color: slategray" ><strong>&nbsp;</strong></p>
                    <p style="font-size: 10px;line-height:15px;  ">
                        <?= date("m/d/Y",strtotime($cotizacion->fechaAlta)) ?><br>
                        <?= date("H:i:s",strtotime($cotizacion->fechaAlta)) ?> HRS<br>
                        <?= date("m/d/Y",strtotime($cotizacion->fechaVencimiento)) ?><br>
                        <?=$cotizacion->moneda ?><br>
                        $<?=number_format($cotizacion->tipoCambio,2) ?>
                    </p>


                </td>

        </table>

        <table  style="width:100%;font-size: 11px; margin-top: 20px;" cellpadding="0" cellspacing="0">
            <tr style="background-color:#096699;color:#fff;font-size:12px;font-weight: bold;text-align: center;vertical-align: middle;">
                <td>Cant.</td>
                <td height="20">Producto</td>
                <td>P. Unit</td>
                <td>Subtotal</td>
                <td>Desc.</td>
                <td>SubTotal</td>
                <td>Tras.</td>
                <td>Ret.</td>
                <td>Total</td>
            </tr>
            <?php
            $descuentoTotal = 0;
            $acMoneda="";
            if($cotizacion->moneda=="USD"){
                $acMoneda="US";
            }elseif($cotizacion->moneda=="CAD"){
                $acMoneda="CAD";
            }if($cotizacion->moneda=="EUR"){
                $acMoneda="€";
            }
            $retenidos=0;
            $trasladados=0;
            $descuentos=0;
            $total=0;
            $sumaTotal=0;
            $sumaSubtotal=0;
            $sumaDescuetno=0;
            $sumaRetenidos=0;
            $sumaTrasladados=0;
            foreach ($productosRegistrados as $item) {
                $subtotal=$item->precio * $item->cantidad;
                $subtotalDesc=$subtotal-$item->descuento;
                $totalP=$subtotalDesc+$item->traslados-$item->retencion;
                $sumaTotal+=$totalP;
                $sumaSubtotal+=$subtotal;
                $sumaDescuetno+=$item->descuento;
                $sumaRetenidos+=$item->retencion;
                $sumaTrasladados+=$item->traslados;
                ?>
                <tr>
                <td style="text-align: left; height: 30px;"><?= $item->cantidad ?></td>
                <td style="text-align: left"><?= ucfirst(mb_strtolower($item->descripcion)) ?> <?= ucfirst(mb_strtolower($item->observacion)) ?></td>
                <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($item->precio,2) ?></td>
                <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($subtotal,2) ?></td>
                <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($item->descuento,2) ?></td>
                <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($subtotalDesc,2) ?></td>
                <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($item->traslados,2) ?></td>
                <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($item->retencion,2) ?></td>
                <td style="text-align: right"><?=$acMoneda ?>$<?= number_format($totalP,2) ?></td>
                </tr>
            <?php }
            ?>
        </table>


        <table  style="width:100%;font-size: 12px;text-align: right;font-weight: bold; margin-top: 20px;">
            <tr>
                <td style="width:50%">&nbsp;</td>
                <td style="width:35%;">Sub Total :</td>
                <td style="width:15%;color:#096699;"><?=$acMoneda ?>$<?= number_format($sumaSubtotal, 2) ?></td>
            </tr>
            <?php
            if($sumaDescuetno>0){
                ?>
                <tr>
                    <td style="width:50%">&nbsp;</td>
                    <td style="width:35%;">Descuento :</td>
                    <td style="width:15%;color:#096699;"><?=$acMoneda ?>$<?= number_format($sumaDescuetno, 2) ?></td>
                </tr>
                <tr>
                    <td style="width:50%">&nbsp;</td>
                    <td style="width:35%;">Sub Total :</td>
                    <td style="width:15%;color:#096699;"><?=$acMoneda ?>$<?= number_format($sumaSubtotal-$sumaDescuetno, 2) ?></td>
                </tr>


                <?php
            }
            ?>

            <?php
            if($sumaTrasladados>0) {
                ?>
                <tr>
                    <td style="width:50%">&nbsp;</td>
                    <td style="width:35%;">Imp. Trasladados :</td>
                    <td style="width:15%;color:#096699;"><?=$acMoneda ?>$<?= number_format($sumaTrasladados, 2) ?></td>
                </tr>

                <?php
            }
            ?>
            <?php
            if($sumaRetenidos>0) {
                ?>
                <tr>
                    <td style="width:50%">&nbsp;</td>
                    <td style="width:35%;">Imp. Retenidos :</td>
                    <td style="width:15%;color:#096699;"><?=$acMoneda ?>$<?= number_format($sumaRetenidos, 2) ?></td>
                </tr>

                <?php
            }
            ?>
            <tr>
                <td style="width:50%">&nbsp;</td>
                <td style="width:35%;">Total :</td>
                <td style="width:15%;color:#096699;"><?=$acMoneda ?>$<?= number_format($sumaTotal, 2) ?></td>
            </tr>

        </table>
    <table style="width:100%;font-size: 10px;text-align: left; margin-top: 160px;">
        <tr>
            <td>
                <string>TERMINOS Y CONDICIONES</string>

                <p>•  El presupuesto tendrá un ajuste referente a la inflación al cumplir un año de servicio.
                   <br>
                    •  La información cuenta con 1 año de consulta histórica en línea.
                    <br>
                    •  Cualquier aclaración o re facturación del servicio, se tendrá que solicitar dentro del mismo mes de facturación.
                    <br>
                    •  La vigencia de esta propuesta es de 30 días.
                    <br>
                    •  Los envíos de Newsletter se harán llegar hasta un máximo de 25 direcciones.
                    <br>
                    •  En nuestros monitoreos se compromete un 99% de seguridad en la detección de notas.
                    <br>
                    •  El horario comprometido para el monitoreo puede oscilar hasta 15 minutos de desfase.
                    <br>
                    •  Por estar supeditados a la naturaleza de los medios de comunicación en este tipo de servicio no aplican los esquemas de amonestación.
                </p>







            </td>
        </tr>
    </table>

</div>
</body>