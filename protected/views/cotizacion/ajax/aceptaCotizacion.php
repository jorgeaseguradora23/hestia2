<div class="modal-content">
    <?php
	$tiposServicio = CHtml::listData(Cattiposervicio::model()->findAll(), 'id', 'descripcion');
	$tiposMonitoreo = CHtml::listData(Cattipomonitoreo::model()->findAll(), 'id', 'descripcion');
    ?>
	<div class="modal-header bg-success">
		<h4 class="modal-title">
			<i class="fas fa-clipboard-check m-r-5"></i> Cotización Aceptada
		</h4>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
	</div>
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'nuevo-form',
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
	));
	?>
	<div class="modal-body ">


			<p>Ingresa todos los campos solicitados para realizar el cierre de la cotización .</p>
		<?php
		if (!empty($cotizacion->errors)) {
			$bolError = true;
			?>
			<div class="alert alert-danger icons-alert" >
				<button type="button" id="errorNewFil" name="errorNewFil" class="close" data-dismiss="alert" aria-label="Close">
					<i class="icofont icofont-close-line-circled"></i>
				</button>
				<p><strong>Atención!</strong> <?= $form->errorSummary($cotizacion) ?></p>
			</div>

			<?php
		}
		?>


		<div class="row " style="padding: 13px;">

			<div class="col-lg-12">


				<div class="row">
					<div class="col-lg-6 col-md-12 m-b-15">
						<label class="text-muted" style="font-size: 12px">Autorización de Cierre</label>
							<?php echo $form->textField($cotizacion, 'personaCierre', array('class' => "form-control", 'placeholder' => "Persona que autoriza cierre")); ?>
						</div>
					<div class="col-lg-6 col-md-12 m-b-15">
						<label class="text-muted" style="font-size: 12px">Email Persona que autoriza</label>
							<?php echo $form->textField($cotizacion, 'emailCierre', array('class' => "form-control", 'placeholder' => "Email que autoriza cierre")); ?>
						<?php echo $form->hiddenField($cotizacion, 'id', array('class' => "form-control", 'placeholder' => "Email que autoriza cierre")); ?>

					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 m-b-15">
						<label class="text-muted" style="font-size: 12px">Descripción o contenido de la confirmaciónn</label>
							<?php echo $form->textArea($cotizacion, 'contenidoCierre', array('class' => "form-control", 'rows' => "3", 'placeholder' => "Contenido de correo que autoriza cierre")); ?>

					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-12 m-b-15">
						<label class="text-muted" style="font-size: 12px">Fecha Inicio Servicio</label>

						<?php //if ($cotizacion->estatus == "A") { ?>

						<?php
						$fechaInicioD=date("Y-m-d");
						if(($cotizacion->_fechaInicio!="")){
							$fechaInicioD=$cotizacion->_fechaInicio;
						}

						echo $form->dateField($cotizacion, '_fechaInicio', array('class' => "form-control",
							'placeholder' => "Fecha Alta",
							'title' => "Fecha Alta",
							"value" => $fechaInicioD

						)); ?>
						<?php /*} else { ?>
                                    <?php
                                    $productoCliente = Clienteproductoservicio::model()->find('idCotizacion=:idCotizacion', array(':idCotizacion' => $cotizacion->id));
                                    $cotizacion->_fechaInicio = $productoCliente->fechaInicio;
                                    $cotizacion->_fechaFinal = $productoCliente->fechaFinal;
                                    $cotizacion->_idCatTipoMonitoreo = $productoCliente->idCatTipoMonitoreo;
                                    $cotizacion->_idCatTipoServicio = $productoCliente->idCatTipoServicio;
                                    echo $form->dateField($cotizacion, '_fechaInicio', array('class' => "form-control", 'placeholder' => "Fecha Alta", 'title' => "Fecha Alta", 'readOnly' => 'readOnly'));
                                    ?>
                                <?php }*/ ?>
					</div>
					<div class="col-lg-6 col-md-12 m-b-15">
						<label class="text-muted" style="font-size: 12px">Fecha Final Servicio</label>

							<?php echo $form->dateField($cotizacion, '_fechaFinal', array('class' => "form-control", 'placeholder' => "Fecha Vencimiento", 'title' => "Fecha Vencimiento")); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 m-b-15">
						<label class="text-muted" style="font-size: 12px">Tipo de Servicio</label>
							<?php echo $form->dropDownList($cotizacion, '_idCatTipoServicio', $tiposServicio, array('class' => "form-control", 'empty' => "Tipo servicio")); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-12 m-b-15">
						<label class="text-muted" style="font-size: 12px">Tipo de Cargo</label>
						<?php echo $form->dropDownList($cotizacion, 'cargoProveedor', array("N" => "SIN CARGOS",
							"S" => "CARGO PROVEEDOR"),
							array('class' => "form-control",
								"onchange" => "setCargo();",
								'empty' => "-- Selecciona --")); ?>
					</div>


					<div class="col-lg-6 col-md-12 m-b-15">
						<label class="text-muted" style="font-size: 12px">Costo al Provedor</label>
						<?php
						$readOnlycargo="false";
						$atributo="";
						if($cotizacion->cargoProveedor=="N"){
							$readOnlycargo=true;
							$atributo="readOnly";
						}
						echo $form->numberField($cotizacion, 'costoProveedor', array('class' => "form-control", $atributo => $readOnlycargo,
							'placeholder' => "Costo a Proveedor","step" => "any", 'title' => "Fecha Vencimiento")); ?>
						<input type="hidden" value="0" id="idCotizacionCierre" name="idCotizacionCierre">
					</div>

				</div>
			</div>
		</div>

	</div>
	<?php $this->endWidget(); ?>
		<div class="modal-footer">
			<button class="btn btn-sm btn-success" onclick="cierreCotizacionSet(true,0)" type="button"  >FINALIZAR</button>

		</div>

</div>


