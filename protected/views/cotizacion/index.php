<?php
/* @var $this ClienteController */
/* @var $dataProvider CActiveDataProvider */
$cotizacionesPendinetes=0;
$cotizacionesAceptadas=0;
$cotizacionesRechazadas=0;
$efectividad=0;
foreach ($cotizaciones as $cotizacion){

        switch ($cotizacion->estatusText){
            case "ACEPTADO":
                $cotizacionesAceptadas+=1;
                break;
            case "PENDIENTE":
                $cotizacionesPendinetes+=1;
                break;
            case "RECHAZADO":
                $cotizacionesRechazadas+=1;
                break;
        }

}
?>



<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-ui-chat bg-danger"></i>

                    <div class="d-inline">
                        <h4>Cotizaciones</h4>
                        <span>Seguimiento a clientes en Cotizaciones.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 right">

                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cotizaciones</a>
                        </li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">

        <div class="row">
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3 ">
                <div class="card widget-card-1 ">
                    <div class="card-block-small">
                        <i class="icofont icofont-pie-chart bg-c-yellow card1-icon"></i>
                        <span class="text-warning f-w-600">Cotizaciones Pendientes</span>
                        <h4><?=$cotizacionesPendinetes ?> en seguimiento</h4>
                        <div>
                            <span class="f-left m-t-10 text-muted">
                                <i class="text-c-blue f-16 icofont icofont-warning m-r-10"></i>Operación Continua
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-ui-home bg-c-pink card1-icon"></i>
                        <span class="text-c-pink f-w-600">Cotizaciones rechazadas</span>
                        <h4><?=$cotizacionesRechazadas ?> negativos</h4>
                        <div>
                            <span class="f-left m-t-10 text-muted">
                                <i class="text-c-pink f-16 icofont icofont-calendar m-r-10"></i>Cotizaciones sin aceptación
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-warning-alt bg-c-green card1-icon"></i>
                        <span class="text-c-green f-w-600">Cotizaciones Aceptadas</span>
                        <h4><?=$cotizacionesAceptadas ?> Completadas</h4>
                        <div>
                            <span class="f-left m-t-10 text-muted">
                                <i class="text-c-green f-16 icofont icofont-tag m-r-10"></i>Todas las cotizaciones aceptadas
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-social-twitter bg-c-yellow card1-icon"></i>
                        <span class="text-c-yellow f-w-600">Efectividad</span>
                        <?php
                        $porcentajeAceptadas=0;
                        if(count($cotizaciones)>0){
                            $porcentajeAceptadas=    number_format($cotizacionesAceptadas/count($cotizaciones)*100,2);
                        }

                        ?>
                        <h4><?= $porcentajeAceptadas ?> %</h4>
                        <div>
                            <span class="f-left m-t-10 text-muted">
                                <i class="text-c-yellow f-16 icofont icofont-refresh m-r-10"></i>Calculado sobre valores completos
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->




        </div>
        <!-- Config. table start -->
        <div class="card">
            <div class="card-header">
                <h5>Resultados de la Consulta</h5>

                <div class="card-header-right" style="margin-right: 10px;">
                    <a href="<?=CController::createUrl('cotizacion/create') ?>"><button class="btn btn-sm btn-info">NUEVA COTIZACIÓN</button></a>
                    <button class="btn btn-sm btn-primary waves-effect"  data-toggle="modal" data-target="#default-Modal">FILTRAR</button>
                    <button class="btn btn-sm btn-success waves-effect" >CSV</button>
                    <button class="btn btn-sm btn-danger waves-effect"  >PDF</button>


                </div>
            </div>
            <div class="card-block">
                <div class="">
                    <div class="dt-responsive table-responsive" >
                        <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="row" >
                                <div class="col-xs-12 col-sm-12 col-lg-12" >
                                    <table width="100%" id="res-config" class="table compact table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 1544px; font-size: 12px; ">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 30px;">Folio</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"  >Nombre Cliente o Prospecto</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 60px;"  >Tipo Cliente</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;" aria-label="">Pertenece a</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;" >Estatus</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 70px;" >Alta</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 70px;" >Vencimiento</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;" aria-label="">Tipo</th>
                                            <th style="width: 100px;">Importe</th>
                                            <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1"
                                                style="width: 100px; display: none;" aria-label="E-mail: activate to sort column ascending">Acciones</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($cotizaciones as $cotizacion){


                                            ?>

                                            <tr role="row" class="odd" style="font-size: 12px;">
                                                <td><?=$cotizacion->folio ?></td>
                                                <td tabindex="0" class="sorting_1">
                                                    <?= getTxtCte($cotizacion->idCliente) ?> - <?= ucwords(strtolower($cotizacion->nombre)) ?>
                                                    <br>
                                                    <?php

                                                    if($cotizacion->categoriaCliente=="CLIENTE"){
                                                        ?>
                                                        <div class="badge badge-success">Cliente</div>
                                                    <?php
                                                    }else{
                                                        ?>
                                                        <div class="badge badge-warning">Prospecto</div>
                                                        <?php
                                                    }
                                                    ?>

                                                </td>
                                                <td><?=$cotizacion->textTipoCliente ?></td>
                                                <td><?=ucwords(strtolower($cotizacion->padre)) ?></td>

                                                <td tabindex="0" class="sorting_1">

                                                    <?php
                                                    switch ($cotizacion->estatusText){
                                                        case "ACEPTADO":
                                                            ?>
                                                            <div class="badge badge-success">Aceptado</div>
                                                    <?php
                                                            break;
                                                        case "PENDIENTE":
                                                            ?>
                                                            <div class="badge badge-warning">Pendiente</div>
                                                            <?php
                                                            break;
                                                        case "RECHAZADO":
                                                            ?>
                                                            <div class="badge badge-danger">Rechazado</div>
                                                            <?php
                                                            break;
                                                    }
                                                    ?>
                                                </td>

                                                <td>
                                                    <i class="fas fa-calendar-alt text-success"></i> <span class="text-success " > <?=date("Y-m-d",strtotime($cotizacion->fechaAlta))?>
                                                    <br>
                                                    <i class="fas fa-clock text-success"></i> <span class="text-success " > <?=date("H:i:s",strtotime($cotizacion->fechaAlta))?>
                                                    <br>
                                                    <i class="fas fa-user text-success"></i> <i class="text-muted " ><?=$cotizacion->usuario ?></i>
                                                </td>



                                                <td>
                                                    <i class="fas fa-calendar-alt text-warning"></i> <span class="text-warning " > <?=date("Y-m-d",strtotime($cotizacion->fechaVencimiento))?>
                                                    <br>
                                                    <i class="fas fa-clock text-warning"></i> <span class="text-warning " > <?=date("H:i:s",strtotime($cotizacion->fechaVencimiento))?>

                                                </td>
                                                <td><?=ucwords(mb_strtolower(Cattipocotizacion::model()->findByPk($cotizacion->tipo)->descripcion)) ?></td>
                                                <td align="right">$<?=number_format($cotizacion->total,2) ?></td>
                                                <td align="center">
                                                    <?php
                                                    switch ($cotizacion->estatusText){
                                                        case "ACEPTADO":
                                                            ?>
                                                            <a href="<?=CController::createUrl('cotizacion/view', array('id'=>$cotizacion->id))?>"
                                                               class="btn btn-mini btn-success" style="min-width: 80px"> <i class="fas fa-check"></i> REVISAR</a>
                                                            <?php
                                                            break;
                                                        case "PENDIENTE":
                                                            ?>
                                                            <a href="<?=CController::createUrl('cotizacion/view', array('id'=>$cotizacion->id))?>"
                                                               class="btn btn-mini btn-warning"  style="min-width: 80px"> <i class="fas fa-clock-o"></i> SEGUIMIENTO</a>
                                                            <?php
                                                            break;
                                                        case "RECHAZADO":
                                                            ?>
                                                            <a href="<?=CController::createUrl('cotizacion/view', array('id'=>$cotizacion->id))?>"
                                                               class="btn btn-mini btn-danger"  style="min-width: 80px"> <i class="fas fa-times"></i> REVISAR</a>
                                                            <?php
                                                            break;
                                                    }
                                                    ?>




                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>


                                        </tbody>
                                    </table>
                                    </br>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Config. table end -->

    </div>
</div>
<!-- MODAL FILTROS -->
<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-header bg-info" >
                <h4 class="modal-title">Filtros de Búsqueda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <p>Selecciona uno o varios campos de búsqueda, a continuación presiona 'Aplicar' para ejecutar tu petición.</p>
                <div class="container">
                    <div class="ccol-lg-12 col-md-12 m-b-15">
                        <h4 class="sub-title text-muted"><strong>Opciones de búsqueda</strong></h4>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            echo $form->textField($busqueda, '_idClienteNumero', array('class' => "form-control",'placeholder' => "Número de Cliente"));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            echo $form->dropDownList($busqueda, 'tipo', array("N" => "Normal","L" => "Licitación"), array('class' => "form-control",'empty' => "Tipo ..."));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 m-b-15">
                            <?php
                            $clientes = CHtml::listData(Cliente::model()->findAll(), 'id', 'nombre');
                            echo $form->dropDownList($busqueda, 'idCliente', $clientes, array('class' => "form-control",'empty' => "Cliente ..."));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            Fecha Alta
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            echo $form->dateField($busqueda, '_fechaAltaInicio', array('class' => "form-control", 'placeholder' => "Fecha Alta", 'title' => "Fecha Alta"));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            echo $form->dateField($busqueda, '_fechaAltaFin', array('class' => "form-control", 'placeholder' => "Fecha Alta", 'title' => "Fecha Vencimiento"));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            Fecha Vencimiento
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            echo $form->dateField($busqueda, '_fechaVencimientoInicio', array('class' => "form-control", 'placeholder' => "Fecha Alta", 'title' => "Fecha Alta"));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            echo $form->dateField($busqueda, '_fechaVencimientoFin', array('class' => "form-control", 'placeholder' => "Fecha Alta", 'title' => "Fecha Vencimiento"));
                            ?>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>
                <a href="<?=CController::createUrl('unidad/index') ?>"> <button type="button" class="btn btn-sm btn-warning waves-effect waves-light ">LIMPIAR</button></a>
                <button type="submit" class="btn btn-sm btn-info waves-effect waves-light ">APLICAR</button>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
