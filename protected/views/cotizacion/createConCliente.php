<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-ui-user bg-warning"></i>

                    <div class="d-inline">
                        <h4>Nueva Cotización <?= getTxtCte($cliente->id) ?> - <?= $cliente->nombre ?></h4>
                        <span>Ingrese toda la información de los campos requeridos para registrar la cotización.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cotizaciones</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Nueva Cotización</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->


    <!-- Page-body start -->
    <div class="page-body">

        <div class="row">
            <div class="col-lg-12">


                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>
                <div class="card">
                    <div class="card-header bg-warning">
                        <h5 class="text-white">Información General</h5>
                        <div class="card-header-right" style="margin-right: 10px;">
                            <?php
                            if(isset($_GET["mn"])) {
                                if ($_GET["mn"] == "index") {
                                    ?>
                                    <a href="<?= CController::createUrl('cotizacion/create') ?>">
                                        <button class="btn btn-sm btn-warning btn-out-dashed" type="button">SALIR
                                        </button>
                                    </a>
                                    <?php
                                }
                            } else {
                                    ?>
                                    <a href="<?= CController::createUrl('cotizacion/view', array('id' => $_GET["idP"])) ?>">
                                        <button class="btn btn-sm btn-warning btn-out-dashed" type="button">SALIR
                                        </button>
                                    </a>
                                    <?php
                                }

                            ?>

                        </div>
                    </div>
                    <div class="card-body">
                        <?php
                        if (!empty($cotizacion->errors)) {
                            ?>
                            <div class="alert alert-danger icons-alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled"></i>
                                </button>
                                <p><strong>Atención!</strong> <?= $form->errorSummary($cotizacion) ?></p>
                            </div>

                            <?php
                        }
                        ?>
                        <div style="padding-left: 30px; padding-right: 30px;padding-top: 20px;padding-bottom: 10px;">
                            <div class="row">
                                <div class="col-lg-3 col-md-12 m-b-15">
                                    <label class="text-muted" >Fecha de Alta</label>

                                    <?php echo $form->dateField($cotizacion, 'fechaAlta', array('class' => "form-control", 'placeholder' => "Fecha Alta", 'title' => "Fecha Alta", "value" => date("Y-m-d",strtotime($cotizacion->fechaAlta)))); ?>
                                </div>
                                <div class="col-lg-3 col-md-12 m-b-15">
                                    <label class="text-muted" >Fecha de Vencimiento</label>
                                    <?php echo $form->dateField($cotizacion, 'fechaVencimiento', array('class' => "form-control", 'placeholder' => "Fecha Vencimiento", 'title' => "Fecha Vencimiento")); ?>
                                </div>
                                <div class="col-lg-2 col-md-12 m-b-15">
                                    <label class="text-muted" >Serie del Documento</label>
                                    <?php
                                    $series = CHtml::listData(Catserie::model()->findAll(), 'id', 'serie');
                                    echo $form->dropDownList($cotizacion, 'serie', $series, array('class' => "form-control", 'empty' => "Serie"));
                                    ?>
                                </div>
                                <div class="col-lg-2 col-md-12 m-b-15">
                                    <label class="text-muted" >Tipo de Cotización</label>
                                    <?php

                                    $tiposCot = CHtml::listData(Cattipocotizacion::model()->findAll(), 'id', 'descripcion');
                                    echo $form->dropDownList($cotizacion, 'tipo', $tiposCot, array('class' => "form-control", 'placeholder' => "Tipo ...", 'title' => "Tipo"));
                                    ?>
                                </div>
                                <div class="col-lg-2 col-md-12 m-b-15">
                                    <label class="text-muted" >Moneda</label>
                                    <?php
                                    $monedas = CHtml::listData(Catmoneda::model()->findAll(), 'id', 'descripcion');
                                    echo $form->dropDownList($cotizacion, 'idCatMoneda', $monedas, array('class' => "form-control", 'empty' => "-- Selecciona --", 'title' => "Moneda"));

                                    ?>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-12 col-md-12 m-b-15">
                                    <label class="text-muted" >Observaciones, Comentarios o Adjuntos</label>
                                    <?php echo $form->textArea($cotizacion, 'observacion', array('class' => "form-control", 'rows' => "3", 'placeholder' => "Observación", 'title' => "Observación")); ?>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-warning">
                        <h5 class="text-white">Información del Servicio</h5>

                    </div>
                    <div class="card-body">
                        <?php
                        $producto = new Cotizacionproductoservicio;
                        ?>
                        <div style="padding-left: 30px; padding-right: 30px;padding-top: 20px;padding-bottom: 10px;">
                            <div class="row">
                                <div class="col-lg-1 col-md-12 m-b-15">
                                    <label class="text-muted" >Cantidad</label>
                                    <?php echo $form->numberField($producto, 'cantidad', array('class' => "form-control agregarText", 'placeholder' => "Cant")); ?>
                                </div>
                                <div class="col-lg-5 col-md-12 m-b-15">
                                    <label class="text-muted" >Selecciona un Servicio</label>
                                    <?php
                                    $productos = CHtml::listData(Productoservicio::model()->findAll(), 'id', 'descripcion');
                                    echo $form->dropDownList($producto, 'idProductoServicio', $productos, array('class' => "form-control  agregarDrop", 'empty' => "Selecciona un Servicio",));
                                    ?>
                                </div>
                                <div class="col-lg-2 col-md-12 m-b-15">
                                    <label class="text-muted" >Precio</label>
                                    <?php echo $form->textField($producto, 'precio', array('class' => "form-control agregarText", 'placeholder' => "Precio")); ?>
                                </div>
                                <div class="col-lg-2 col-md-12 m-b-15">
                                    <label class="text-muted" >Esquema de Impuesto</label>
                                    <?php
                                    $esquemas = CHtml::listData(Catesquemaimpuesto::model()->findAll(), 'id', 'descripcion');
                                    echo $form->dropDownList($producto, 'idCatEsquemaImpuesto', $esquemas, array('class' => "form-control agregarDrop", 'empty' => "-- Selecciona --",));
                                    ?>
                                </div>
                                <div class="col-lg-2 col-md-12 m-b-15">
                                    <label class="text-muted" >Descuento</label>
                                    <?php echo $form->textField($producto, 'descuento', array('class' => "form-control agregarText", 'placeholder' => "Ingresa el importe")); ?>
                                </div>
                                <div class="col-lg-12 col-md-12 m-b-15">
                                    <label class="text-muted" >Observación</label>
                                    <?php echo $form->textArea($producto, 'observacion', array('class' => "form-control agregarText", "rows"=> "4",'placeholder' => "Observación o especificaión del servicio")); ?>
                                </div>




                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-success background-success" style="display:none" id="productoSuccess">
                                        <strong>Exito!</strong> El servicio ha sido añadido correctamente.
                                    </div>
                                    <div class="alert alert-danger background-danger" style="display:none" id="productoError">
                                        <strong>Error!</strong> El servicio no ha podido ser añadido con la información proporcionada.
                                    </div>
                                </div>

                            </div>

                            <div class="row float-right mt-2">
                                <div class="col-lg-12 col-md-12 m-b-15">
                                    <button class="btn btn-sm btn-success " id="agregarProducto">AGREGAR SERVICIO</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-warning">
                        <h5 class="text-white">Servicios Agregados</h5>

                    </div>
                    <div class="card-body">
                        <div style="padding-left: 30px; padding-right: 30px;padding-top: 20px;padding-bottom: 10px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table" id="productosAgregados">
                                            <thead>
                                                <tr>
                                                    <th>Cantidad</th>
                                                    <th>Producto</th>
                                                    <th>P. Unitario</th>
                                                    <th>Subtotal</th>
                                                    <th>Descuento</th>
                                                    <th>Imp. Ret</th>
                                                    <th>Imp. Trs</th>

                                                    <th>Total</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr style="display:none">
                                                    <td>' + data.cantidad + '</td>
                                                    <td>' + data.producto + '</td>
                                                    <td>' + data.precio + '</td>
                                                    <td>' + data.subtotal + '</td>
                                                    <td>' + data.tralados + '</td>
                                                    <td>' + data.subtotal + '</td>
                                                    <td>' + data.retencion + '</td>
                                                    <td>' + data.total + '</td>
                                                    <td>Eliminar</td>
                                                </tr>
                                                <?php foreach ($productosRegistrados as $productoRegistrado)

                                                { ?>

                                                    <tr>
                                                        <td>
                                                            <input name="idProductoServicio[]" type="hidden" value="<?= $productoRegistrado['idProductoServicio'] ?>">
                                                            <input name="cantidad[]" type="hidden" value="<?= $productoRegistrado['cantidad'] ?>">
                                                            <input name="descuento[]" type="hidden" value="<?= $productoRegistrado['descuento'] ?>">
                                                            <input name="precio[]" type="hidden" value="<?= $productoRegistrado['precio'] ?>">
                                                            <input name="retencion[]" type="hidden" value="<?= number_format($productoRegistrado['retencion'],2) ?>">
                                                            <input name="tralados[]" type="hidden" value="<?= number_format($productoRegistrado['tralados'],2) ?>">
                                                            <input name="subtotal[]" type="hidden" value="<?= $productoRegistrado['subtotal'] ?>">
                                                            <input name="observacion[]" type="hidden" value="<?= $productoRegistrado['observacion'] ?>">
                                                            <input name="producto[]" type="hidden" value="<?= $productoRegistrado['producto'] ?>">
                                                            <input name="idImpuesto[]" type="hidden" value="<?= $productoRegistrado['idImpuesto'] ?>">
                                                            <input name="total[]" type="hidden" value="<?= $productoRegistrado['total'] ?>">

                                                            <?= $productoRegistrado['cantidad'] ?></td>
                                                        <td><?= $productoRegistrado['producto'] ?></td>
                                                        <td>$<?= number_format($productoRegistrado['precio'],2) ?></td>
                                                        <td>$<?= number_format($productoRegistrado['subtotal'],2) ?></td>
                                                        <td>$<?= number_format($productoRegistrado['descuento'],2) ?></td>
                                                        <td>$<?= number_format($productoRegistrado['retencion'],2) ?></td>
                                                        <td>$<?= number_format($productoRegistrado['tralados'],2) ?></td>
                                                        <td>$<?= number_format($productoRegistrado['total'],2) ?></td>
                                                        <td>
                                                            <button class="btn btn-danger btn-sm btn-outline-danger  borrarProducto" style="max-width: 40px;"><i class="fas fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row float-right mt-2">
                                <div class="col-lg-12 col-md-12 m-b-15">
                                    <button class="btn btn-sm btn-warning sendForm" type="submit"  >Guardar Cotización</button>
                                </div>

                            </div>



                        </div>
                    </div>
                </div>


                <?php $this->endWidget(); ?>
                <!-- tab content end -->
            </div>
        </div>

    </div>
    <!-- Page-body end -->
</div>




<script>


    $('#agregarProducto').click(function () {
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('agregarProducto'),
    'success' => 'js:agregarProducto',
    'dataType' => 'json',
));
?>
        return false;
    });
    $(".agregarProducto").keydown(function (e) {
        e.stopImmediatePropagation();
        var code = e.which; // recommended to use e.which, it's normalized across browsers
        if (code == 32 || code == 13 || code == 188 || code == 186) {
            e.preventDefault();
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('agregarProducto'),
    'success' => 'js:agregarProducto',
    'dataType' => 'json',
));
?>
        }
    });
    function agregarProducto(data) {

        if (data.status == "correcto") {
            //alert('Success: '+data.status);
            var element = '<tr><td><input name="idProductoServicio[]" type="hidden" value="' + data.idProductoServicio + '">' +
                '<input name="retencion[]" type="hidden" value="' + data.retencion + '">' +
                '<input name="tralados[]" type="hidden" value="' + data.tralados + '">' +
                '<input name="subtotal[]" type="hidden" value="' + data.subtotal + '">' +
                '<input name="observacion[]" type="hidden" value="' + data.observacion + '"> ' +
                '<input name="cantidad[]" type="hidden" value="' + data.cantidad + '">' +
                '<input name="producto[]" type="hidden" value="' + data.producto + '">' +
                '<input name="idCatEsquemaImpuesto[]" type="hidden" value="' + data.idCatEsquemaImpuesto + '">' +
                '<input name="descuento[]" type="hidden" value="' + data.descuento + '">' +
                '<input name="idImpuesto[]" type="hidden" value="' + data.idImpuesto + '">' +
                '<input name="total[]" type="hidden" value="' + data.total + '">' +
                '<input name="precio[]" type="hidden" value="' + data.precio + '">' + data.cantidad + '</td> ' +
                '<td>' + data.producto + ' ' + data.observacion + '</td> ' +
                '<td>' + new Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(data.precio) + '</td> ' +
                '<td>' + new Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(data.subtotal)  + '</td> ' +
                '<td>' + new Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(data.descuento)  + '</td> ' +
                '<td>' + new Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(data.retencion)  + '</td> ' +
                '<td>' + new Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(data.tralados)  + '</td>' +
                '<td>' + new Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(data.total)   + '</td>' +
                '<td> <button class="btn btn-danger btn-sm btn-outline-danger  borrarProducto" style="max-width: 40px;"><i class="fas fa-trash"></i></button></td></tr>';
                        $('#Cotizacionproductoservicio_idProductoServicio').val('');
                        $('#Cotizacionproductoservicio_cantidad').val('');
                        $('#Cotizacionproductoservicio_idCatEsquemaImpuesto').val('');
                        $('#Cotizacionproductoservicio_descuento').val('');
                        $('#Cotizacionproductoservicio_precio').val('');
                        $('#Cotizacionproductoservicio_observacion').val('');

                        $('#productosAgregados tr:last').after(element);
                        $('#productoSuccess').show();
                        setTimeout(function () {
                            $("#productoSuccess").hide()
                        }, 2000);
                    } else {
                        $('#productoError').show();
                        setTimeout(function () {
                            $("#productoError").hide()
                        }, 2000);
                    }
                }
                ;
                $(document).on('click', '.borrarProducto', function () {
                    $(this).parent().parent().remove();
                    return false;
                });


                $("#Cotizacionproductoservicio_idProductoServicio").change(function () {
            <?=
            CHtml::ajax(array(
                'type' => 'POST',
                'url' => CController::createUrl('getPrice'),
                'success' => 'js:llenarPrecio',
                'dataType' => 'json',
            ));
            ?>
    });
    function llenarPrecio(data) {
        $("#Cotizacionproductoservicio_precio").val(data.precio);
    }
</script>