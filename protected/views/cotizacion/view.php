<?php
$tiposServicio = CHtml::listData(Cattiposervicio::model()->findAll(), 'id', 'descripcion');
$tiposMonitoreo = CHtml::listData(Cattipomonitoreo::model()->findAll(), 'id', 'descripcion');
?>
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-ui-user bg-warning"></i>

                    <div class="d-inline">
                        <p>Detalle Cotización <strong><?=$cotizacionDetalle->folio ?></strong></p>
                        <h4 style="margin-top: -10px;">
                            <?= getTxtCte($cliente->id) ?> - <?= $cliente->nombre ?>
                        </h4>
                       <div class="row">
                           <div class="pcoded-badge label label-success mt-2"
                                style="font-size: 12px; min-width: 80px; max-width: 80px; text-align: center">
                               <strong><?= ucwords(strtolower($cotizacionDetalle->textTipoCliente)) ?></strong>
                           </div>
                           <div class="pcoded-badge label label-info mt-2"
                                style="font-size: 12px; min-width: 130px; max-width: 130px; text-align: center">
                               <strong><?= ucfirst(strtolower(Cattipocotizacion::model()->findByPk($cotizacionDetalle->tipo)->descripcion)) ?></strong>
                           </div>
                       </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cotizaciones</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Detalle Cotización</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <br>
        <div class="row align-items-end float-right ">
            <div class="col-12 float-right">
                <a href="<?= CController::createUrl('cotizacion/index') ?>" class="btn btn-secondary waves-effect btn-sm waves-light float-right">SALIR</a>
                <a href="#" onclick="printCotizacion()" style="display: none" id="btnImprimir"
                   class="btn btn-primary btn-print-invoice btn-sm float-right m-r-10">
                    <i class="fas fa-print m-r-10"></i> IMPRIMIR</a>

            </div>
            <input id="idPrint" name="idPrint" type="hidden">
        </div>
    </div>



    <!-- Page-header end -->


    <!-- Page-body start -->
    <div class="page-body">

        <div class="row">

            <div class="col-lg-12">
                <?php
                $cotizacionesAnteriores = array();
                if (!empty($cotizacion->idCotizacion)) {
                    $idCotizacionAnterior = $cotizacion->idCotizacion;
                    do {
                        $cotizacionAnterior = Cotizacion::model()->findByPk($idCotizacionAnterior);
                        $cotizacionesAnteriores[] = $cotizacionAnterior;
                        if (empty($cotizacionAnterior->idCotizacion)) {
                            //echo 'aqui';die;
                            $salir = true;
                        } else {
                            $salir = false;
                            //echo 'aqui2';die;
                            $idCotizacionAnterior = $cotizacionAnterior->idCotizacion;
                        }
                    } while ($salir == false);
                };
                ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-md-12 col-xl-12">
                                <div class="card project-task">
                                    <div class="card-header bg-primary text-white">
                                        <div class="card-header-left  ">
                                            <h5 class="card-title text-white" style="font-size: 16px;">
                                                <i class="fas fa-history m-r-10"></i> Historial de Cotizaciones</h5>
                                        </div>

                                    </div>
                                    <div class="card-block p-b-10" style="height: 350px; overflow-y: auto; margin-bottom: 30px;">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Fecha</th>
                                                    <th>Observación</th>
                                                    <th>Importe</th>
                                                    <th>Acción</th>
                                                </tr>
                                                </thead>
                                                <tbody style="font-size: 11px;">
                                                <?php
                                                foreach ($cotSeguimientos as $cotizacionAnterior) { ?>
                                                    <tr style="cursor:pointer;" class="">
                                                        <td class="cotizacionLink"  data-id="<?= $cotizacionAnterior->id ?>">
                                                            <samll><a class=""
                                                               href="#"
                                                               style="font-size: 12px;"
                                                               >#<?= $cotizacionAnterior->folio ?>-<?= $cotizacionAnterior->id ?></a>
                                                            </samll>
                                                        </td>
                                                        <td style="max-width: 40px;" width="40px">
                                                            <i class="fas fa-calendar-alt mr-2 text-info"></i> <?= date("d/m/Y",strtotime($cotizacionAnterior->fechaAlta)) ?>
                                                            <br>
                                                            <i class="fas fa-clock mr-2 text-success"></i> <?= date("H:i:s",strtotime($cotizacionAnterior->fechaAlta)) ?>

                                                        </td>
                                                        <td width="80px" style="max-width: 80px;">
                                                            <span style=" white-space: pre-wrap; font-size: 11px; line-height: 16px;"><?=ucfirst(mb_strtolower($cotizacionAnterior->observacion)) ?> </span>
                                                        </td>
                                                        <td align="right"><strong>$<?= number_format($cotizacionAnterior->total, 2) ?></strong></td>
                                                        <td>
                                                            <?php if ($cotizacion->estatus == "A") { ?>
                                                                <a href="javascript:void(0)" style="font-size: 16px;" class="float-right text-success"
                                                                   data-toggle="modal" data-target="#cierreCotizacion" type="button"
                                                                   onclick="$('#idCotizacionCierre').val(<?=$cotizacionAnterior->id ?>)"
                                                                   title="Aceptar Propuesta">
                                                                    <i class="fas fa-check"></i></a>

                                                                <a href="#" style="font-size: 16px;"
                                                                   class="float-right text-danger m-r-5"
                                                                   data-toggle="modal" data-target="#rechazo" type="button"
                                                                   title="Rechazar Propuesta"><i class="fas fa-times"></i></a>

                                                                <a href="<?= CController::createUrl('cotizacion/createConCliente', array('idCliente' => $cliente->id,
                                                                    'idCotizacionBase' => $cotizacionAnterior->id, 'idP' => $_GET["id"])) ?>" style="font-size: 16px;"
                                                                   class="float-right text-warning m-r-5"
                                                                   title="Nueva Cotización"><i class="fas fa-plus"></i></a>
                                                                <?php
                                                            }else{
                                                                if($cotizacionAnterior->estatusText=="ACEPTADO"){
                                                                    ?>
                                                                    <div class="badge badge-success" style="min-width: 70px;"><?=$cotizacionAnterior->estatusText ?></div>
                                                                    <?php
                                                                }else{
                                                                    ?>
                                                                    <div class="badge badge-danger" style="min-width: 70px;"><?=$cotizacionAnterior->estatusText ?></div>
                                                                    <?php
                                                                }
                                                                ?>

                                                            <?php
                                                            }
                                                            ?>

                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                <tr>
                                                    <td class="cotizacionLink" data-id="<?= $cotizacionDetalle->id ?>">#<?= $cotizacionDetalle->folio ?>-<?= $cotizacionDetalle->id ?></td>
                                                    <td>
                                                        <i class="fas fa-calendar-alt mr-2 text-info"></i> <?= date("d/m/Y",strtotime($cotizacionDetalle->fechaAlta)) ?>
                                                        <br>
                                                        <i class="fas fa-clock mr-2 text-success"></i> <?= date("H:i:s",strtotime($cotizacionDetalle->fechaAlta)) ?>
                                                    </td>
                                                    <td width="80px" style="max-width: 80px;">
                                                        <span style=" white-space: pre-wrap; font-size: 11px; line-height: 16px;"><?=ucfirst(mb_strtolower($cotizacionDetalle->observacion)) ?> </span>
                                                    </td>
                                                    <td align="right"><strong>$<?= number_format($cotizacionDetalle->total, 2) ?></strong></td>


                                                    <td>
                                                        <?php if ($cotizacion->estatus == "A") { ?>
                                                            <a href="javascript:void(0)" style="font-size: 16px;" class="float-right text-success"
                                                               data-toggle="modal" data-target="#cierreCotizacion" type="button"
                                                               onclick="cierreCotizacionSet(false,<?=$cotizacionDetalle->id ?>)"
                                                               title="Aceptar Propuesta">
                                                                <i class="fas fa-check"></i></a>

                                                            <a href="#" style="font-size: 16px;"
                                                               class="float-right text-danger m-r-5"
                                                               data-toggle="modal" data-target="#rechazo" type="button"
                                                               title="Rechazar Propuesta"><i class="fas fa-times"></i></a>

                                                            <a href="<?= CController::createUrl('cotizacion/createConCliente', array('idCliente' => $cliente->id,
                                                                'idCotizacionBase' => $cotizacionDetalle->id, 'idP' => $_GET["id"])) ?>" style="font-size: 16px;"
                                                               class="float-right text-warning m-r-5"
                                                               title="Nueva Cotización"><i class="fas fa-plus"></i></a>
                                                            <?php
                                                        }else{
                                                            if($cotizacionDetalle->estatusText=="ACEPTADO"){
                                                                ?>
                                                                <div class="badge badge-success" style="min-width: 70px;"><?=$cotizacionDetalle->estatusText ?></div>
                                                                <?php
                                                            }else{
                                                                ?>
                                                                <div class="badge badge-danger" style="min-width: 70px;"><?=$cotizacionDetalle->estatusText ?></div>
                                                                <?php
                                                            }
                                                            ?>


                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header bg-success" style="max-height: 70px; min-height: 70px; height: 70px;">
                                        <div class="row">
                                            <div class="col-12">
                                                <h5 class="card-title text-white" style="font-size: 16px;">
                                                    <i class="fas fa-comment m-r-10"></i> Seguimientos</h5>
                                                <button class="btn btn-sm btn-success float-right" data-toggle="modal" data-target="#nuevoSeguimiento">Agregar</button>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="card-content" aria-expanded="true">
                                        <div class="card-body">
                                            <div class="row ">
                                                <div class="col-12 col-md-12">
                                                        <div class="card-content">
                                                            <div class="card-block mt-1" style="min-height: 350px; max-height: 350px; overflow-y: auto; overflow-x: hidden  ">
                                                                <?php
                                                                foreach ($buscarSeguimientos as $buscarSeguimiento) {
                                                                    ?>
                                                                    <div class="card-comment ">
                                                                        <div class="card-block-small">
                                                                            <img class="img-radius img-50" src="<?php echo Yii::app()->request->baseUrl; ?>/styles/app-assets/images/portrait/small/avatar-s-4.png" alt="user-1">
                                                                            <div class="comment-desc">
                                                                                <h6><?=$buscarSeguimiento->usuario ?></h6>
                                                                                <p style="color: #0b9c8f; font-size: 12px;"><?=$buscarSeguimiento->comentario ?></p>
                                                                                <div class="comment-btn">
                                                                                    <label class="label label-md bg-success "
                                                                                           style="text-align: center; font-size: 11px; ">EN SEGUIMIENTO</label>
                                                                                </div>
                                                                                <div class="date">
                                                                                    <span style="font-size: 11px; color: gray"><?=$buscarSeguimiento->fechaAlta ?></span>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>




                                                                    <?php
                                                                }
                                                                ?>








                                                            </div>




                                                        </div>
                                                    </div>


                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="card cotizacion" id="cotizacion<?= $cotizacion->id ?>">
                            <div class="card-body">
                               <br><br><br>

                                <div id="divCotizacion" style="min-height: 870px;">
                                    <div class="card-content" style=" overflow-y: auto">

                                        <div class="content-box " style="margin-top: 10%; color: gray;  text-align: center; ;">
                                            <!-- Your text -->
                                            <img src="/px/styles/img/sad.png">
                                            <h1 class="text-muted mt-5">Opss! No hay nada que mostrar.</h1>

                                            <p>Por favor, selecciona una cotización para <br>visualizar su contenido.
                                            </p>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php foreach ($cotizacionesAnteriores as $cotizacionAnterior) { ?>
                            <div class="card cotizacion"  style="display: none" id="cotizacion<?= $cotizacionAnterior->id ?>">
                                <div class="card-header">
                                    <div class="card-header-right" style="margin-right: 10px;">
                                        <a href="<?= CController::createUrl('cotizacion/imprimir', array('id' => $cotizacionAnterior->id)) ?>" class="btn btn-primary btn-print-invoice btn-sm">IMPRIMIR</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div>
                                        <div class="row invoice-contact">
                                            <div class="col-md-8">
                                                <div class="invoice-box row">
                                                    <div class="col-sm-12">
                                                        <table class="table table-responsive invoice-table table-borderless">
                                                            <tbody>
                                                                <tr>
                                                                    <td><img src="assets/images/logo-blue.png" class="m-b-10" alt=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cotización #<?= $cotizacionAnterior->id ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Dirección en México.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><a href="mailto:demo@gmail.com" target="_top">demo@gmail.com</a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>+91 919-91-91-919</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row invoive-info">
                                                <div class="col-md-4 col-xs-12 invoice-client-info">
                                                    <h6>Información del Cliente:</h6>
                                                    <h6 class="m-0"><?= $cliente->nombre ?></h6>
                                                    <p class="m-0 m-t-10"><?= $cliente->calle ?>, <?= $cliente->numExt ?> - <?= $cliente->numInt ?>.</p>
                                                    <p class="m-0"><?= $cliente->telefono ?></p>
                                                    <p><?= $cliente->eMail ?></p>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <h6>Información de Cotización:</h6>
                                                    <table class="table table-responsive invoice-table invoice-order table-borderless">
                                                        <tbody>
                                                            <tr>
                                                                <th>Fecha Alta :</th>
                                                                <td><?= $cotizacionAnterior->fechaAlta ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Fecha Vencimiento :</th>
                                                                <td><?= $cotizacionAnterior->fechaVencimiento ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Status :</th>
                                                                <td>
                                                                    <span class="label label-warning"><?= ($cotizacionAnterior->estatus == "A") ? "Abierta" : "Confirmada" ?></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Id :</th>
                                                                <td>
                                                                    #<?= $cotizacionAnterior->id ?>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <h6 class="m-b-20">Número de cotización <span>#<?= $cotizacionAnterior->id ?></span></h6>
                                                    <h6 class="text-uppercase text-primary">Total:
                                                        <span>$<?= number_format($cotizacionAnterior->total, 2) ?></span>
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="table-responsive">
                                                        <table class="table  invoice-detail-table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Producto</th>
                                                                    <th>Cantidad</th>
                                                                    <th>Precio</th>
                                                                    <th>Esquema de Impuesto</th>
                                                                    <th>Descuento</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $productosRegistradosCotizacionAnterior = Cotizacionproductoservicio::model()->findAll('idCotizacion=:idCotizacion', array(':idCotizacion' => $cotizacion->id));
                                                                $descuentoTotal = 0;
                                                                foreach ($productosRegistradosCotizacionAnterior as $productoRegistrado) {
                                                                    ?>
                                                                    <?php
                                                                    $productoModel = Productoservicio::model()->findByPk($productoRegistrado->idProductoServicio);
                                                                    $catImpuestoModel = Catesquemaimpuesto::model()->findByPk($productoRegistrado->idCatEsquemaImpuesto);
                                                                    $impuestos = Catdesgloceimpuesto::model()->findAll(":idCatEsquemaImpuesto=:idCatEsquemaImpuesto", array(":idCatEsquemaImpuesto" => $catImpuestoModel->id));
                                                                    $impRetenido = 0;
                                                                    $impTrasladado = 0;
                                                                    foreach ($impuestos as $impuesto) {
                                                                        $impuestoAAplicar = Catimpuesto::model()->findByPk($impuesto->idCatImpuesto);
                                                                        if ($impuestoAAplicar->aplicacion == '-') {
                                                                            $impRetenido += (($productoRegistrado->precio * $productoRegistrado->cantidad) - $productoRegistrado->descuento) * ($impuestoAAplicar->tasa / 100);
                                                                        } else {
                                                                            $impTrasladado += (($productoRegistrado->precio * $productoRegistrado->cantidad) - $productoRegistrado->descuento) * ($impuestoAAplicar->tasa / 100);
                                                                        }
                                                                    }

                                                                    $total = ($productoRegistrado->precio * $productoRegistrado->cantidad) - $productoRegistrado->descuento - $impRetenido + $impTrasladado;
                                                                    $descuentoTotal += $productoRegistrado->descuento;
                                                                    ?>
                                                                    <tr>
                                                                        <td><?= $productoModel->descripcion ?></td>
                                                                        <td><?= $productoRegistrado->cantidad ?></td>
                                                                        <td><?= $productoRegistrado->precio ?></td>
                                                                        <td>
                                                                            <?php
                                                                            $catEsquemaImpuesto = Catesquemaimpuesto::model()->findByPk($productoRegistrado->idCatEsquemaImpuesto);
                                                                            $catImpuestoModel->descripcion;
                                                                            ?>
                                                                        </td>
                                                                        <td><?= $productoRegistrado->descuento ?></td>
                                                                        <td><?= $total ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-responsive invoice-table invoice-total">
                                                        <tbody>
                                                            <tr>
                                                                <th>Sub Total :</th>
                                                                <td>$<?= number_format($cotizacionAnterior->subtotal, 2) ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Impuestos:</th>
                                                                <td>$<?= number_format($cotizacionAnterior->impuestoTrasladado - $cotizacionAnterior->impuestoRetenido, 2) ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Descuento :</th>
                                                                <td>$<?= number_format($descuentoTotal, 2) ?></td>
                                                            </tr>
                                                            <tr class="text-info">
                                                                <td>
                                                                    <hr>
                                                                    <h5 class="text-primary">Total :</h5>
                                                                </td>
                                                                <td>
                                                                    <hr>
                                                                    <h5 class="text-primary">$<?= number_format($cotizacionAnterior->total, 2) ?></h5>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h6>Terms And Condition :</h6>
                                                    <p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <?php } ?>
                    </div>

                </div>




                <div class="row text-center">

                </div>
            </div>
        </div>

    </div>
    <!-- Page-body end -->
</div>

<div class="modal fade " id="cierreCotizacion" tabindex="-1" role="dialog" style="z-index: 1050;">
    <div class="modal-dialog" role="document" id="divCreateModal">


    </div>
</div>

<div class="modal fade " id="rechazo" tabindex="-1" role="dialog" style="z-index: 1050;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">
                    <i class="fas fa-times-circle m-r-5"></i> Cotización Rechazada
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'rechazo-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-body ">

                <div class="row " style="padding: 13px;">
                    <div class="col-lg-12">
                        <p>Ingresa todos los campos solicitados para realizar el cierre de la cotización .</p>
                    </div>
                </div>

                <?php
                if (!empty($rechazo->errors)) {
                    $bolError = true;
                    ?>
                    <div class="alert alert-danger icons-alert" >
                        <button type="button" id="errorNewFil" name="errorNewFil" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="icofont icofont-close-line-circled"></i>
                        </button>
                        <p><strong>Atención!</strong> <?= $form->errorSummary($rechazo) ?></p>
                    </div>

                    <?php
                }
                ?>


                <div class="row " style="padding: 13px;">

                    <div class="col-lg-12">


                        <div class="row">
                            <div class="col-lg-6 col-md-12 m-b-15">
                                <label class="text-muted" style="font-size: 12px">Autorización de Cierre</label>
                                    <?php echo $form->textField($rechazo, 'personaCierre', array('class' => "form-control", 'placeholder' => "Persona que autoriza cierre")); ?>

                            </div>
                            <div class="col-lg-6 col-md-12 m-b-15">
                                <label class="text-muted" style="font-size: 12px">Email Persona que autoriza</label>
                                    <?php echo $form->textField($rechazo, 'emailCierre', array('class' => "form-control", 'placeholder' => "Email que autoriza cierre")); ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 m-b-15">
                                <label class="text-muted" style="font-size: 12px">Descripción o motivo del rechazo</label>
                                    <?php echo $form->textArea($rechazo, 'contenidoCierre', array('class' => "form-control", 'rows' => "3", 'placeholder' => "Contenido de correo que autoriza cierre")); ?>

                            </div>
                        </div>



                    </div>
                </div>

            </div>
            <?php if ($cotizacion->estatus == "A") { ?>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-danger" type="submit" id="btnRechazoCot" value="1" name="btnRechazoCot"  >FINALIZAR</button>

                </div>
            <?php } ?>
        </div>


        <?php $this->endWidget(); ?>

    </div>
</div>

<div class="modal animated rollIn text-left " id="nuevoSeguimiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel41" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'seguimiento-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-header bg-success">
                <h4 class="modal-title text-white" id="myModalLabel41">Nuevo Seguimiento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                if (!empty($nuevoCotizacionComentario->errors)) {
                    ?>
                    <div class="alert alert-danger icons-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="icofont icofont-close-line-circled"></i>
                        </button>
                        <p><strong>Atención!</strong> <?= $form->errorSummary($nuevoCotizacionComentario) ?></p>
                    </div>

                    <?php
                }
                ?>
                <div class="col-lg-12 col-md-12 m-b-15">
                    <label class="text-muted" >Comentario de Seguimiento</label>
                    <?php echo $form->textArea($nuevoCotizacionComentario, 'comentario', array('class' => "form-control agregarText",
                        'rows' => "5",
                        'placeholder' => "Comentario de Seguimiento")); ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-sm btn-success">Guardar</button>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<script>

    function showCotizacion(id){
        $.ajax({
            data: "id=" + id ,
            type: "POST",
            url: "<?=CController::createUrl('showCotizacion') ?>",
            beforeSend: function(){
                $("#divCotizacion").html("<div class=\"preloader3 loader-block\">\n" +
                    "                                                                    <div class=\"circ1 loader-success\"></div>\n" +
                    "                                                                    <div class=\"circ2 loader-success\"></div>\n" +
                    "                                                                    <div class=\"circ3 loader-success\"></div>\n" +
                    "                                                                    <div class=\"circ4 loader-success\"></div>\n" +
                    "                                                                </div>");
            },
            success: function(response)
            {


                //  $("#Eventoseguimiento_descripcion").val("");
                // $("#Eventoseguimiento_idCatTipoSeguimiento").val("");
                $("#divCotizacion").html(response);
            }
        });
    }

    var idActual=0;


    function printCotizacion() {
        document.location.href='<?= CController::createUrl('cotizacion/imprimir') ?>?id=' + $("#idPrint").val();
    }


    $(document).ready(function () {
        $('.cotizacionLink').click(function () {
            showCotizacion($(this).attr('data-id'));
            //$("#menuOn" + $(this).attr('data-id')).show();
            //$("#menuOff" + $(this).attr('data-id')).hide();
            $("#idPrint").val($(this).attr('data-id'));

            $("#btnImprimir").show();
            if(idActual!=0){
                //$("#menuOn" + idActual).hide();
                //$("#menuOff" + idActual).show();
            }
            idActual=$(this).attr('data-id');

        });

        <?php if ($cotizacion->estatus == "A") { ?>
            $('.cotizacionLink').mouseover(function () {
                //alert($(this).attr('data-id'));
                $("#menuOn" + $(this).attr('data-id')).show();
                $("#menuOff" + $(this).attr('data-id')).hide();
                //showCotizacion($(this).attr('data-id'));
            });
            $('.cotizacionLink').mouseleave(function () {
                //alert($(this).attr('data-id'));
                if(idActual!=$(this).attr('data-id')){
                    $("#menuOn" + $(this).attr('data-id')).hide();
                    $("#menuOff" + $(this).attr('data-id')).show();
                }

                //showCotizacion($(this).attr('data-id'));
            });
        <?php
        }
        ?>


        <?php if (!empty($cotizacion->errors)) { ?>
            $('#cierreCotizacion').modal('show');
        <?php } ?>
        <?php if (!empty($rechazo->errors)) { ?>
            $('#rechazo').modal('show');
        <?php } ?>


    });

    function setCargo() {
        $("#Cotizacion_costoProveedor").val("0");
        if($("#Cotizacion_cargoProveedor").val()=="N"){
            $("#Cotizacion_costoProveedor").prop("readOnly",true);
        }else if($("#Cotizacion_cargoProveedor").val()=="S"){
            $("#Cotizacion_costoProveedor").prop("readOnly",false);
        }
    }

</script>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/controllers/cotizacion-controller.js"></script>