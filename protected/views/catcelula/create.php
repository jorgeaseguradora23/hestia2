<?php
/* @var $this CatcelulaController */
/* @var $model Catcelula */

$this->breadcrumbs=array(
	'Catcelulas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Catcelula', 'url'=>array('index')),
	array('label'=>'Manage Catcelula', 'url'=>array('admin')),
);
?>

<h1>Create Catcelula</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>