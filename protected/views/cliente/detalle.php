<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-user-alt-3 bg-<?=$Cliente->colorEstatus ?>"></i>

                    <div class="d-inline">

                        <h4><?=getTxtCte($Cliente->id) ?> - <?=$Cliente->nombre ?> </h4>
                        <span>Tipo Cliente: <?=$Cliente->textTipoCliente ?></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cartera de Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Detalle Cliente</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->


    <div class="row">

        <!-- user card start -->
        <div class="col-sm-4">
            <div class="card bg-<?=$Cliente->colorEstatus ?> text-white widget-visitor-card">
                <div class="card-block-small text-center">
                    <h2><?=ucwords(strtolower($Cliente->textEstatus)) ?></h2>
                    <h6><?=$Cliente->categoriaCliente ?></h6>
                    <i class="ti-user"></i>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card bg-c-blue text-white widget-visitor-card">
                <div class="card-block-small text-center">
                    <h2><?=ucwords(mb_strtolower($Cliente->fuenteContratacion)) ?></h2>
                    <h6>Fuente de Contratación</h6>
                    <i class="icofont icofont-paper"></i>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card bg-c-yellow text-white widget-visitor-card">
                <div class="card-block-small text-center">
                    <h2><?=ucwords(strtolower($Cliente->sector)) ?></h2>
                    <h6>Sector</h6>
                    <i class="icofont icofont-ui-alarm"></i>
                </div>
            </div>
        </div>
        <!-- user card end -->



    </div>

    <!-- Page-body start -->
    <div class="page-body">
        <!-- Datos Generales -->
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block user-detail-card" style="min-height: 340px;">
                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-3">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/client.png " width="180" alt="" class="img-fluid p-b-10 mt-3">
                                        <div class="contact-icon">
                                            <?php
                                            $star1="secondary";
                                            $star2="secondary";
                                            $star3="secondary";
                                            $star4="secondary";
                                            if($Cliente->ponderacion=="A"){
                                                $star1="warning";
                                                $star2="secondary";
                                                $star3="secondary";
                                                $star4="secondary";
                                            }elseif($Cliente->ponderacion=="AA"){
                                                $star1="warning";
                                                $star2="warning";
                                                $star3="secondary";
                                                $star4="secondary";
                                            }elseif($Cliente->ponderacion=="AAA"){
                                                $star1="warning";
                                                $star2="warning";
                                                $star3="warning";
                                                $star4="secondary";
                                            }elseif($Cliente->ponderacion=="AAAA"){
                                                $star1="warning";
                                                $star2="warning";
                                                $star3="warning";
                                                $star4="warning";
                                            }
                                            ?>
                                           <div class="row ml-3 mt-3">
                                               <button class="btn btn-<?=$star1 ?> btn-icon mr-1"><i class="icofont icofont-star" style="margin-left: 5px;"></i></button>
                                               <button class="btn btn-<?=$star2 ?> btn-icon mr-1"><i class="icofont icofont-star" style="margin-left: 5px;"></i></button>
                                               <button class="btn btn-<?=$star3 ?> btn-icon mr-1"><i class="icofont icofont-star" style="margin-left: 5px;"></i></button>
                                               <button class="btn btn-<?=$star4 ?> btn-icon mr-1"><i class="icofont icofont-star" style="margin-left: 5px;"></i></button>
                                           </div>





                                        </div>
                                    </div>
                                    <div class="col-sm-8 user-detail" >
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h7 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Nombre / Razón Social :</h7>
                                            </div>
                                            <div class="col-sm-7">
                                                <h7 class="m-b-30"><strong><?=ucwords(strtolower($Cliente->nombre)) ?></strong></h7>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-calendar"></i>Fecha Prospección :</h7>
                                            </div>
                                            <div class="col-sm-7">
                                                <h7 class="m-b-30" style="font-size: 12px;"><?=$Cliente->fechaProspeccion ?> (<?=$Cliente->motivoProspeccion ?>) </h7>
                                                <?php
                                                if(isset($Cliente->observacionProspeccion)){
                                                    ?>
                                                    <i class="icofont icofont-ui-messaging text-warning float-right" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$Cliente->observacionProspeccion ?>" style="cursor: pointer"></i>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-calendar"></i>Fecha Alta :</h7>
                                            </div>
                                            <div class="col-sm-7">
                                                <h7 class="m-b-30" style="font-size: 12px;"><?=empty($Cliente->fechaAlta)?"En Proceso ...":$Cliente->fechaAlta ?></h7>
                                                <?php
                                                if(isset($Cliente->observacionAlta)){
                                                    ?>
                                                    <i class="icofont icofont-ui-messaging text-warning float-right" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$Cliente->observacionProspeccion ?>" style="cursor: pointer"></i>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-calendar"></i>Fecha Baja :</h7>
                                            </div>
                                            <div class="col-sm-7">
                                                <h7 class="m-b-30" style="font-size: 12px;"><?=empty($Cliente->fechaBaja)?"Activo":$Cliente->fechaBaja ?></h7>
                                                <?php
                                                if(isset($Cliente->observacionBaja)){
                                                    ?>
                                                    <i class="icofont icofont-ui-messaging text-warning float-right" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$Cliente->observacionProspeccion ?>" style="cursor: pointer"></i>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h7 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-home"></i>Dirección :</h7>
                                            </div>
                                            <div class="col-sm-7" >
                                                <h7 class="m-b-30" style="font-size: 12px;"><?=ucfirst(strtolower($Cliente->calle)) ?> No. <?=$Cliente->numExt ?>,
                                                    <?=ucfirst(strtolower($Cliente->tipoColonia)) ?> <?=ucfirst(strtolower($Cliente->colonia)) ?>, <?=ucfirst(strtolower($Cliente->municipio)) ?>, <?=ucwords(strtolower($Cliente->estado)) ?>, CP <?=(($Cliente->cp)) ?>
                                                </h7>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h6 class="f-w-400 m-b-30" style="font-size: 12px;"><i class="icofont icofont-ui-touch-phone"></i>Tel / Móvil :</h6>
                                            </div>
                                            <div class="col-sm-7">
                                                <h6 class="m-b-30" style="font-size: 12px;"><?=$Cliente->telefono ?><?=empty($Cliente->movil)?"":"/Móvil ".$Cliente->movil ?></h6>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                    <!-- Comentario del cliente -->
                    <div class="card bg-c-green green-contain-card " >
                        <div class="card-block  " >
                            <h5 class="p-t-0 text-white">Comentarios</h5>
                        </div>
                        <div class="card m-b-0 " style="min-height: 280px;max-height: 280px;">
                            <div class=" card-block-big p-t-50 p-b-50 ">

                                <?php
                                if(!empty($Cliente->comentario)){
                                    ?>
                                    <p class="text-muted "><?=ucfirst(strtolower($Cliente->comentario)) ?></p>
                                <?php
                                }else{
                                    ?>
                                    <div class="content-box" style="color: gray;  text-align: center; margin-top: 15px;">
                                        <!-- Your text -->
                                        <h1>Bien! No hay comentarios.</h1>

                                        <p>No es información requerida, pero ayuda <br>
                                            a tener un mejor panorama del cliente.</p>
                                    </div>
                                <?php
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                    <!-- fin comentario -->

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-statistics ">
                    <div class="card-header ">
                        <div class="card-header-left ">
                            <h5>Estadisticas de comportamiento</h5>
                        </div>
                        <div class="card-header-right">
                            <a href="<?=CController::createUrl('cliente/index') ?>" class="btn btn-sm btn-warning">Regresar</a>
                        </div>
                    </div>
                    <div class="card-block ">
                        <div class="row ">
                            <div class="col-sm-4 ">
                                <div class="row stats-block">
                                    <div class="col-lg-6 ">
                                        <div class="progressbar-v-1">
                                            <div class="chart" data-percent="90" data-barcolor="#4680FE" data-trackcolor="#dbdada" data-linewidth="6" data-barsize="110" >
                                                <div class="chart-percent"><span style="padding-left: 110px;">90</span>%</div>
                                                <canvas height="110" width="110"></canvas></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ">
                                        <h2 class="f-w-400 ">5,879</h2>
                                        <p class="text-muted">Ingresos</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 ">
                                <div class="row stats-block">
                                    <div class="col-lg-6 ">
                                        <div class="progressbar-v-1">
                                            <div class="chart" data-percent="70" data-barcolor="#FC6180" data-trackcolor="#dbdada" data-linewidth="6" data-barsize="110">
                                                <div class="chart-percent"><span style="padding-left: 110px;">70</span>%</div>
                                                <canvas height="110" width="110"></canvas></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ">
                                        <h2 class="f-w-400 ">$2,456</h2>
                                        <p class="text-muted ">Total rent</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 ">
                                <div class="row stats-block">
                                    <div class="col-lg-6 ">
                                        <div class="progressbar-v-1">
                                            <div class="chart" data-percent="75" data-barcolor="#FFB64D" data-trackcolor="#dbdada" data-linewidth="6" data-barsize="110">
                                                <div class="chart-percent"><span style="padding-left: 110px;">75</span>%</div>
                                                <canvas height="110" width="110"></canvas></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ">
                                        <h2 class="f-w-400 ">3,198</h2>
                                        <p class="text-muted ">Total revenue</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-t-default p-t-20 m-t-30">
                            <div class="row ">
                                <div class="col-sm-4 ">
                                    <h6 class="d-inline-block m-r-10 ">User growth</h6>
                                    <span class="label bg-c-blue ">28%</span>
                                </div>
                                <div class="col-sm-4 ">
                                    <h6 class="d-inline-block m-r-10 ">Earning growth</h6>
                                    <span class="label bg-c-pink ">12%</span>
                                </div>
                                <div class="col-sm-4 ">
                                    <h6 class="d-inline-block m-r-10 ">Trade growth</h6>
                                    <span class="label bg-c-yellow ">56%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <!-- historico basico -->
                <div class="card">
                    <div class="card-header bg-warning">
                        <div class="card-header-left">
                            <h5 class="text-white">Datos de Facturación</h5>
                        </div>
                        <div class="card-header-right">

                        </div>
                    </div>
                    <div class="card-block mt-4" style="min-height: 350px; max-height: 350px; overflow-y: auto; overflow-x: hidden  ">

                        <?php
                        if(!empty($datosFacturacion)){
                            ?>
                            <div class="table-responsive" style="max-height: 340px;">
                                <table class="table table-hover" >
                                    <tbody>
                                    <?php
                                    foreach ($datosFacturacion as $dato){
                                        ?>
                                        <tr>
                                            <td><img class="img-rounded" width="80px;" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/user-3.png " alt="chat-user">
                                            </td>
                                            <td width="50px">
                                                <h6><?=ucwords(strtolower($dato->razonSocial)) ?></h6>
                                                <span class="text-muted" style="font-size: 12px; ">
                                                <i class="icofont icofont-ui-touch-phone mr-2"></i><?=$dato->rfc ?><br>
                                                <i class="icofont icofont-ui-email mr-2"></i>
                                                    <?=ucwords(strtolower($dato->direccion)) ?><br>
                                                 <i class="icofont icofont-ui-file mr-2"></i>
                                                    Uso CFDI:  <?=ucfirst(strtolower($dato->usoCFDI)) ?><br>
                                                 <i class="icofont icofont-ui-email mr-2"></i>
                                                    Método Pago: <?=ucfirst(strtolower($dato->metodoPago)) ?><br>
                                                <i class="icofont icofont-ui-file mr-2"></i>
                                                    Forma Pago: <?=ucfirst(strtolower($dato->formaPago)) ?>
                                            </span>

                                            </td>


                                        </tr>
                                        <?php
                                    }
                                    ?>


                                    </tbody>
                                </table>
                            </div>
                        <?php
                        }else{
                            ?>
                            <div class="content-box" style="color: gray;  text-align: center; margin-top: 70px;">
                                <!-- Your text -->
                                <h1>Vaya! No hay datos<br>de facturación</h1>

                                <p>Si deseas agregar información fiscal<br>
                                    dirígete  a la edición del cliente.</p>
                            </div>
                            <?php
                        }
                        ?>



                    </div>
                </div>
                <!-- fin historico basico -->
            </div>
            <div class="col-md-6">
                <!-- historico basico -->
                <div class="card">
                    <div class="card-header bg-danger">
                        <div class="card-header-left">
                            <h5 class="text-white">Contactos</h5>
                        </div>
                        <div class="card-header-right">

                        </div>
                    </div>
                    <div class="card-block mt-4" style="min-height: 350px; max-height: 350px; overflow-y: auto; overflow-x: hidden  ">
                        <?php
                        if(!empty($Contactos)){
                            ?>
                            <div class="table-responsive" style="max-height: 340px;">
                                <table class="table table-hover" >
                                    <tbody>
                                    <?php
                                    foreach ($Contactos as $Contacto){
                                        ?>
                                        <tr>
                                            <td><a href="#!"><img class="img-rounded" width="80px;" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/Group-user.jpg " alt="chat-user"></a>
                                            </td>
                                            <td>
                                                <h6><?=ucwords(strtolower($Contacto->nombreContacto)) ?></h6>
                                                <span class="text-muted" style="font-size: 12px;"> <i class="icofont icofont-ui-touch-phone mr-2"></i><?=$Contacto->telefono ?><br><i class="icofont icofont-ui-email mr-2"></i><?=strtolower($Contacto->eMail) ?></span>

                                            </td>


                                        </tr>
                                        <?php
                                    }
                                    ?>


                                    </tbody>
                                </table>
                            </div>
                            <?php
                        }else{
                            ?>
                            <div class="content-box" style="color: gray;  text-align: center; margin-top: 70px;">
                                <!-- Your text -->
                                <h1>Vaya! No hay<br>contactos</h1>

                                <p>Si deseas agregar algun contacto<br>
                                    dirígete  a la edición del cliente.</p>
                            </div>
                        <?php
                        }
                        ?>

                    </div>


                </div>
                <!-- fin historico basico -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <!-- historico basico -->
                <div class="card">
                    <div class="card-header bg-info">
                        <div class="card-header-left">
                            <h5 class="text-white">Actividad</h5>
                        </div>
                        <div class="card-header-right">

                        </div>
                    </div>
                    <div class="card-block mt-4" style="min-height: 350px;">
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>5 mins ago</small>
                                <p class="text-muted m-b-30">jennifer sent you an attachament</p>
                            </div>
                        </div>
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>45 mins ago</small>
                                <p class="text-muted m-b-30">Paul has sent a request for access to the project folder</p>
                            </div>
                        </div>
                        <div class="card-notification">
                            <div class="card-noti-conatin">
                                <small>2 days ago</small>
                                <p class="text-muted m-b-30">Demin change the dedline on the project</p>
                            </div>
                        </div>



                    </div>
                </div>
                <!-- fin historico basico -->
            </div>
            <div class="col-md-6">
                <!-- historico basico -->
                <div class="card">
                    <div class="card-header bg-primary">
                        <div class="card-header-left">
                            <h5 class="text-white">CONTRATOS DEL CLIENTE</h5>
                        </div>
                        <div class="card-header-right">
                            <button class="btn btn-primary btn-sm"
                                    data-toggle="modal"
                                    data-target="#nuevoServicio"
                                    style="border: dashed 1px white; margin-top: -5px; margin-right: 10px;">AGREGAR</button>
                        </div>
                    </div>
                    <div class="card-block mt-2 " style="min-height: 370px;" >
                        <div class="table-responsive" style="max-height: 370px;">
                            <?php
                            if(!empty($contratos)){
                                ?>
                                <table class="table table-hover" style="border-top: solid white">
                                    <thead >
                                    <tr >
                                        <th colspan="2">Contrato</th>
                                        <th>Condición</th>
                                        <th>Alta</th>
                                        <th>Vigencia</th>
                                        <th width="60px;">Estatus</th>

                                    </tr>
                                    </thead>
                                    <tbody style="min-height: 360px; max-height: 360px; overflow-y: auto; overflow-x: hidden  ">
                                    <?php
                                    foreach ($contratos as $contrato) {
                                        ?>
                                        <tr>
                                            <td width="30px;"><?=$contrato->id ?></td>
                                            <td>

                                                <span class="d-inline-block m-l-20 m-b-0">
                                                    <?=Catperiodicidad::model()->findByPk($contrato->idCatPeriodicidad)->descripcion ?>

                                                    <small><?=$contrato->facturacion=="F"?"<br>Ultimos 5 días":"" ?></small>
                                                    <small><?=$contrato->facturacion=="I"?"<br>Primeros 5 días":"" ?></small>


                                            </td>
                                            <td>
                                                <small class="text-primary">
                                                    Tipo: <?=$contrato->tipoContrato=="F"?"Finito":"Indeterminado" ?>
                                                    <br><?php
                                                    if($contrato->tipoFacturacion=="O"){
                                                        echo "Orden de Compra";
                                                    }elseif($contrato->tipoFacturacion=="M"){
                                                        echo "Manual";
                                                    }elseif($contrato->tipoFacturacion=="A"){
                                                        echo "Automatico";
                                                    }
                                                    ?>
                                                </small>
                                            </td>
                                            <td>
                                                <span class="d-inline-block  text-muted" style="font-size: 12px;"><?=$contrato->fechaAlta ?> <br><?=$contrato->usuario ?></span>

                                            </td>
                                            <td>
                                                <span class="d-inline-block  text-muted" style="font-size: 12px;">
                                                <span class="text-success"><i class="fas fa-calendar-alt"></i> <?=$contrato->fechaInicio ?></span>
                                                <br>
                                                <span class="text-danger"><i class="fas fa-calendar-alt"></i> <?=$contrato->fechaFinal ?></span>

                                                </span>

                                            </td>
                                            <td>
                                                <?php
                                                if($contrato->estatus=="A"){
                                                    ?>
                                                    <p class="d-inline-block "><label class="label label-md bg-success" style="min-width: 50px; text-align: center; ">ACTIVO</label></p>
                                                <?php
                                                }else{
                                                    ?>
                                                    <p class="d-inline-block "><label class="label label-md bg-danger" style="min-width: 50px; text-align: center; ">BAJA</label></p>
                                                    <?php
                                                }
                                                ?>


                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                    </tbody>
                                </table>
                                <?php
                            }else{
                                ?>
                                <div class="content-box" style="color: gray;  text-align: center; margin-top: 100px;">
                                    <!-- Your text -->
                                    <h1>Vaya! No hay Contratos</h1>

                                    <p>Si requieres generar un contrato,<br>
                                        Presiona el botón agregar.</p>
                                </div>
                                <?php
                            }
                            ?>




                        </div>
                    </div>
                </div>
                <!-- fin historico basico -->
            </div>
        </div>
        <!-- fin Datos Generales -->
        <!-- Encabezado -->

        <!-- Fin encabezado -->


        <!-- Inicio Servicios -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-current-prog ">
                    <div class="card-header bg-success">
                        <div class="card-header-left">
                            <h5 class="text-white">SERVICIOS DEL CLIENTE</h5>
                        </div>
                        <div class="card-header-right">

                        </div>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive mt-2" style="min-height: 300px; max-height: 300px;" >
                            <table class="table">
                                <tbody>

                                <?php
                                if (!empty($servicios)){
                                    ?>
                                    <?php
                                    foreach ($servicios as $servicio){
                                        ?>
                                        <tr>
                                            <td>
                                                <div class="card-img">
                                                    <a href="#!"><img class="img-rounded" src="assets/images/user.png" alt=""></a>
                                                </div>
                                                <div class="card-contain">
                                                    <h6 class="f-w-600"><?=$servicio->producto ?></h6>
                                                    <p class="text-muted"><?=$servicio->tipoServicio ?> | <?=$servicio->monitoreo ?></p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="last-line">
                                                    <p class=""><i class="icofont icofont-ui-calendar"></i> <?=$servicio->fechaInicio ?> al <?=$servicio->fechaFinal ?></p>
                                                </div>
                                                <div class="card-icon-time">
                                                    <a href="#!"><i class="icofont icofont-ui-calculator text-muted"></i></a>
                                                    <p class="text-muted">$<?=number_format($servicio->precio,2) ?></p>
                                                </div>
                                                <div class="card-icon-time">
                                                    <?php
                                                    if(empty($servicio->idClienteContrato)){
                                                        ?>
                                                        <button class="btn btn-sm btn-success" data-toggle="modal"
                                                                onclick="setContrato(<?=$servicio->id ?>)"
                                                                data-target="#asignaContrato" >Contrato</button>
                                                    <?php
                                                    }else{
                                                        ?>

                                                        <p class="text-success">Contrato F<?=$servicio->idClienteContrato ?></p>
                                                        <?php
                                                    }
                                                    ?>


                                                </div>
                                            </td>
                                            <?php
                                            if($servicio->diasServicio>0){
                                                $avance=$servicio->diasTrancurridos/$servicio->diasServicio;
                                            }else{
                                                $avance=100;
                                            }

                                            ?>
                                            <td>

                                                <div class="progress">
                                                    <div class="progress-bar bg-c-blue" style="width:<?=$avance*100 ?>%">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                <?php
                                }else{
                                    ?>
                                    <div class="content-box" style="color: gray;  text-align: center; margin-top: 80px;">
                                        <!-- Your text -->
                                        <h1>Vaya! No hay servicios contratados</h1>

                                        <p>Confirma o completa un proceso de<br>
                                            cotizaciónpara generar servicios en el cliente.</p>
                                    </div>
                                    <?php
                                }
                                ?>




                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <!-- Fin servicios -->

        <div class="row">
            <div class="col-md-6">
                <!-- historico basico -->
                <div class="card">
                    <div class="card-header " style="background-color: #0d7091">
                        <div class="card-header-left">
                            <h5 class="text-white">Quejas o Sugerencias</h5>
                        </div>
                        <div class="card-header-right">

                        </div>
                    </div>

                    <div class="card-block mt-2 " style="min-height: 390px; max-height: 390px; overflow-y: auto; overflow-x: hidden  ">
                        <div class="table-responsive" style="max-height: 370px;">
                            <?php
                            if(!empty($quejas)){
                                ?>
                                <table class="table table-hover" style="border-top: solid white">
                                    <thead >
                                    <tr >
                                        <th>Empleado / Queja o Sugerencia</th>
                                        <th>Alta</th>
                                        <th>Estatus</th>
                                        <th>Tono</th>
                                    </tr>
                                    </thead>

                                    <?php
                                    foreach ($quejas as $queja) {
                                        $color="";
                                        if($queja->estatus=="P"){
                                            $color="warning";
                                        }elseif($queja->estatus=="A"){
                                            $color="success";
                                        }elseif($queja->estatus=="C"){
                                            $color="danger";
                                        }
                                        ?>

                                        <tr onclick="document.location.href='<?=CController::createUrl('quejasugerencia/seguimiento') ?>?idQS=<?=$queja->id ?>'" >

                                            <td width="250px;">
                                                <div class="task-contain">

                                                <span class="d-inline-block m-l-20 m-b-0">E<?=getTxtCte($queja->idEmpleado) ?> <?=ucwords(strtolower($queja->empleado)) ?>
                                                <br>
                                                    <p class="text-muted ml-3" style="font-size: 11px; width: 100px; "><?=ucfirst(strtolower(substr($queja->descripcion,1,40))) ?></p>
                                                </span>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="d-inline-block  text-muted" style="font-size: 12px;"><?=$queja->fechaAlta ?> <br><?=$queja->usuario ?></p>

                                            </td>
                                            <td>
                                                <p class="d-inline-block "><label class="label label-md bg-<?=$color ?>" style="min-width: 40px; text-align: center; "><?=$queja->txtEstatus ?></label></p>

                                            </td>
                                            <td>
                                                <p class="d-inline-block "><label class="label label-md bg-<?=$queja->color ?>" style="min-width: 40px; text-align: center; "><?=$queja->tono ?></label></p>

                                            </td>
                                        </tr>

                                        <?php
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            <?php
                            }else{
                                ?>
                                <div class="content-box" style="color: gray;  text-align: center; margin-top: 100px;">
                                    <!-- Your text -->
                                    <h1>Bien! No hay Quejas</h1>

                                    <p>Podría ser bueno tener una sugerencia,<br>
                                        pero mejor sin quejas.</p>
                                </div>
                                <?php
                            }
                            ?>




                        </div>
                    </div>
                </div>
                <!-- fin historico basico -->
            </div>
            <div class="col-md-6">
                <!-- historico basico -->
                <div class="card">
                    <div class="card-header " style="background-color: lightseagreen">
                        <div class="card-header-left">
                            <h5 class="text-white">Cotizaciones</h5>
                        </div>
                        <div class="card-header-right">

                        </div>
                    </div>
                    <div class="card-block mt-2 " style="min-height: 390px; max-height: 390px; overflow-y: auto; overflow-x: hidden  ">
                        <div class="table-responsive" style="max-height: 370px;">
                            <?php
                            if(!empty($cotizaciones)){
                                ?>
                                <table class="table table-hover" style="border-top: solid white">
                                    <thead >
                                    <tr >
                                        <th>Folio / Tipo</th>
                                        <th>Alta</th>
                                        <th>Vencimiento</th>
                                        <th>Total</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($cotizaciones as $cotizacion) {
                                        ?>
                                        <tr>
                                            <td>
                                                <div class="task-contain">

                                                <span class="d-inline-block m-l-20 m-b-0"><?=ucwords(strtolower($cotizacion->folio)) ?>
                                                    <?=$cotizacion->textTipo ?>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="d-inline-block  text-muted" style="font-size: 12px;"><?=$cotizacion->fechaAlta ?> <br><?=$cotizacion->usuario ?></span>

                                            </td>
                                            <td>
                                                <span class="d-inline-block  text-muted" style="font-size: 12px;"><?=$cotizacion->fechaVencimiento ?> </span>

                                            </td>
                                            <td>
                                                <p class="d-inline-block "><label class="label label-md bg-info" style="min-width: 40px; text-align: center; ">$<?=number_format($cotizacion->total,2) ?></label></p>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                    </tbody>
                                </table>
                                <?php
                            }else{
                                ?>
                                <div class="content-box" style="color: gray;  text-align: center; margin-top: 100px;">
                                    <!-- Your text -->
                                    <h1>Vaya! No hay Cotizaciones</h1>

                                    <p>Si requieres generar una cotización,<br>
                                        dirigite a Desarrollo > Cotizaciones.</p>
                                </div>
                                <?php
                            }
                            ?>




                        </div>
                    </div>
                </div>
                <!-- fin historico basico -->
            </div>

        </div>



    </div>
    <!-- Page-body end -->
</div>


<div class="modal fade" id="nuevoServicio"   aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $bolError=false;
            $form2 = $this->beginWidget('CActiveForm', array(
                'id' => 'servicios-form',
                'method' => 'POST',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-header bg-primary" >
                <h6 class="modal-title">Nuevo contrato de servicio</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                if(!empty($nuevoServicio->errors)){
                    $bolError=true;
                    ?>
                    <div class="alert alert-danger icons-alert" >
                        <button type="button" id="errorNewFil" name="errorNewFil" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="icofont icofont-close-line-circled"></i>
                        </button>
                        <p><strong>Atención!</strong> <?=$form2->errorSummary($nuevoServicio) ?></p>
                    </div>

                    <?php
                }
                ?>
                <div class="container">
                    <div class="row " >
                        <div class="col-12 m-b-15">
                            <h4 class="sub-title text-muted"><strong>INFORMACIÓN DEL CONTRATO</strong></h4>
                            <div class="row">
                                <div class="col-12">
                                    <label style="font-size: 12px;">Tipo de Contrato</label>
                                    <?php echo $form2->dropDownList($nuevoServicio, 'tipoContrato',
                                        array("F"=>"FINITO","I"=>"INDETERMINADO"),
                                        array('class' => "form-control col-sm-12","onchange" =>  "setTipoContrato()", 'empty' => "-- Selecciona --")); ?>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 5px;">
                                <div class="col-6">
                                    <label style="font-size: 12px;">Fecha Inicio</label>
                                    <?php
                                    if(empty($nuevoServicio->fechaInicio)){
                                        $nuevoServicio->fechaInicio=date("Y-m-d");
                                    }

                                    echo $form2->dateField($nuevoServicio, 'fechaInicio', array(
                                            'class' => "form-control",
                                        'placeholder' => "Fecha de Visita", 'title' => "Fecha de Visita","value" =>  $nuevoServicio->fechaInicio)); ?>
                                </div>
                                <div class="col-6">
                                    <label style="font-size: 12px;">Fecha Final</label>
                                    <?php echo $form2->dateField($nuevoServicio, 'fechaFinal', array(
                                        'class' => "form-control",
                                        'placeholder' => "Fecha de Visita", 'title' => "Fecha de Visita")); ?>
                                </div>
                            </div>


                            <div class="row" style="margin-top: 5px;">
                                <div class="col-6">
                                    <label style="font-size: 12px;">Periodicidad Facturación</label>
                                    <?php
                                    $periodicidades = CHtml::listData(Catperiodicidad::model()->findAll(), 'id', 'descripcion');
                                    echo $form2->dropDownList($nuevoServicio, 'idCatPeriodicidad',
                                        $periodicidades,
                                        array('class' => "form-control col-sm-12","onchange" => "setPeriodicidad()",'empty' => "-- Selecciona --")); ?>
                                </div>
                                <div class="col-6">
                                    <label style="font-size: 12px;">Facturación</label>
                                    <?php
                                    $periodo=array("F" => "ULTIMOS 5 DIAS","I" => "PRIMEROS 5 DIAS","E" => "EVENTO");
                                    echo $form2->dropDownList($nuevoServicio, 'facturacion',
                                        $periodo,
                                        array('class' => "form-control col-sm-12",'empty' => "-- Selecciona --")); ?>
                                </div>

                            </div>

                            <div class="row" style="margin-top: 5px;">
                                <div class="col-12">
                                    <label style="font-size: 12px;">Tipo de Facturación</label>
                                    <?php echo $form2->dropDownList($nuevoServicio, 'tipoFacturacion',
                                        array("O"=>"ORDEN DE COMPRA","A" =>"AUTOMATIZADO","M" => "MANUAL"),
                                        array('class' => "form-control col-sm-12",'empty' => "-- Selecciona --")); ?>
                                </div>
                            </div>






                        </div>

                        <input type="hidden" value="" id="rowElimina" name="rowElimina" >

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal"><i class="fas fa-times"></i>  CERRAR</button>
                <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light " id="btnGuardar" name="btnGuardar" value="1">
                    <i class="fas fa-save"></i> GUARDAR</button>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

</div>

<div class="modal fade" id="asignaContrato"   aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $bolError=false;
            $form2 = $this->beginWidget('CActiveForm', array(
                'id' => 'contrato-form',
                'method' => 'POST',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-header bg-success" >
                <h6 class="modal-title">Asignación de Contrato</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container">
                    <div class="row" style="margin-top: 5px;">
                        <div class="col-12">
                            <label style="font-size: 12px;">Selecciona un Contrato</label>
                            <?php
                            $criteria=new CDbCriteria();
                            $criteria->compare("idCliente",$_GET["id"]);
                            $contratos = CHtml::listData(ViewClienteContrato::model()->findAll($criteria), 'id', 'descripcion');
                            echo $form2->dropDownList($updateClietePS, 'idClienteContrato',
                                $contratos,
                                array('class' => "form-control col-sm-12",'empty' => "-- Selecciona --"));
                            echo $form2->hiddenField($updateClietePS, 'id', array(
                                'class' => "form-control",
                                'placeholder' => "Fecha de Visita", 'title' => "Fecha de Visita"));

                            ?>
                        </div>
                    </div>


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal"><i class="fas fa-times"></i>  CERRAR</button>
                <button type="submit" class="btn btn-sm btn-success waves-effect waves-light " id="btnGuardar" name="btnGuardar" value="1">
                    <i class="fas fa-save"></i> GUARDAR</button>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        <?php
        if(!empty($nuevoServicio->errors)){
            ?>
            $("#nuevoServicio").modal("show");
        <?php
        }
        ?>
    });

    function setTipoContrato() {

        if($("#Clientecontrato_tipoContrato").val()=="F"){
            $("#Clientecontrato_fechaFinal").val("");
        }else{
            $("#Clientecontrato_fechaFinal").val("2099-12-31");
        }

    }

    function setPeriodicidad(){
        if($("#Clientecontrato_idCatPeriodicidad").val()=="1"){

        }else{

        }
    }

    function setContrato(id) {
        $("#Clienteproductoservicio_id").val(id);

    }

</script>