<?php
/* @var $this ClienteController */
/* @var $dataProvider CActiveDataProvider */
?>

<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-users bg-c-blue"></i>

                    <div class="d-inline">
                        <h4>Clientes</h4>
                        <span>Cartera de clientes y prospectos del sistema.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 right">

                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cartera de Clientes</a>
                        </li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">
        <div class="row">
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-pie-chart bg-c-yellow card1-icon"></i>
                        <span class="text-warning f-w-600">Servicios Pendientes</span>
                        <h4>6 seguimiento</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-blue f-16 icofont icofont-warning m-r-10"></i>Get more space
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-ui-home bg-c-pink card1-icon"></i>
                        <span class="text-c-pink f-w-600">Clientes</span>
                        <h4>255 Activos</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-pink f-16 icofont icofont-calendar m-r-10"></i>Last 24 hours
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-warning-alt bg-c-green card1-icon"></i>
                        <span class="text-c-green f-w-600">Máxima Categoria</span>
                        <h4>15 Clientes</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-green f-16 icofont icofont-tag m-r-10"></i>Tracked from microsoft
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-social-twitter bg-c-yellow card1-icon"></i>
                        <span class="text-c-yellow f-w-600">Prospectos</span>
                        <h4>63 Activos</h4>
                        <div>
                            <span class="f-left m-t-10 text-muted">
                                <i class="text-c-yellow f-16 icofont icofont-refresh m-r-10"></i>Just update
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->




        </div>
        <!-- Config. table start -->
        <div class="card " style="min-height: 600px;">
            <div class="card-header">
                <h5>Resultados de la Consulta</h5>

                <div class="card-header-right" style="margin-right: 10px;">
                    <a href="<?=CController::createUrl('cliente/create') ?>"><button class="btn btn-sm btn-info">NUEVO CLIENTE</button></a>
                    <button class="btn btn-sm btn-primary waves-effect"  data-toggle="modal" data-target="#default-Modal">FILTRAR</button>
                    <button class="btn btn-sm btn-success waves-effect" >CSV</button>
                    <button class="btn btn-sm btn-danger waves-effect"  >PDF</button>


                </div>
            </div>
            <div class="card-block ">
                <div class="">
                    <div class="dt-responsive table-responsive " >
                        <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="row" >
                                <div class="col-xs-12 col-sm-12 col-lg-12" >
                                    <?php
                                    if(!empty($Clientes)){
                                        ?>

                                        <table width="100%" id="res-config" class="table compact table-striped table-bordered nowrap dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="res-config_info" style="width: 1544px; font-size: 12px; ">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px;"  aria-sort="ascending" >Cliente</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 120px;" >Tipo</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 50px;" >Categoria</th>


                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 54px;" aria-label="Age: activate to sort column ascending">Fecha Prospección</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 117px;" aria-label="Start date: activate to sort column ascending">Fecha Alta</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 89px;" aria-label="Salary: activate to sort column ascending">Fecha Baja</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px; display: none;" aria-label="">Sector</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 115px;" aria-label="Office: activate to sort column ascending">Ponderiación</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 115px;" aria-label="Office: activate to sort column ascending">Fuente Cont.</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px; display: none;" aria-label="">Estatus</th>
                                                <th class="sorting" tabindex="0" aria-controls="res-config" rowspan="1" colspan="1" style="width: 40px; display: none;" aria-label="E-mail: activate to sort column ascending">Acciones</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($Clientes as $cliente){
                                                ?>
                                                <tr role="row" class="odd" style="font-size: 12px;">
                                                    <td tabindex="0" class="sorting_1"><?=getTxtCte($cliente->id) ?> - <?=$cliente->nombre ?></td>
                                                    <td><?=$cliente->textTipoCliente ?></td>
                                                    <td><?php
                                                        if($cliente->categoria =="P"){
                                                            ?>
                                                            <label class="label label-md bg-warning" style="min-width: 40px; text-align: center; "><?=$cliente->categoriaCliente ?></label>
                                                            <?php

                                                        }elseif($cliente->categoria =="C"){
                                                            ?>
                                                            <label class="label label-md bg-info" style="min-width: 40px; text-align: center; "><?=$cliente->categoriaCliente ?></label>
                                                            <?php
                                                        }
                                                        ?></td>
                                                    <td><?=$cliente->fechaProspeccion ?> | <?=$cliente->usuarioProspeccion ?></td>
                                                    <?php
                                                    if(isset($cliente->fechaAlta)){
                                                        ?>
                                                        <td><?=$cliente->fechaAlta ?> | <?=$cliente->usuarioAlta ?></td>
                                                        <?php
                                                    }else {
                                                        ?>
                                                        <td style="">-</td>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                    if(isset($cliente->fechaBaja)){
                                                        ?>
                                                        <td style=""><?=$cliente->fechaBaja ?> | <?=$cliente->usuarioBaja ?></td>
                                                        <?php
                                                    }else {
                                                        ?>
                                                        <td style="">-</td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td><?=$cliente->sector ?></td>
                                                    <td>
                                                        <?php
                                                        $star1="default";
                                                        $star2="default";
                                                        $star3="default";
                                                        $star4="default";
                                                        if($cliente->ponderacion=="A"){
                                                            $star1="warning";
                                                            $star2="default";
                                                            $star3="default";
                                                            $star4="default";
                                                        }elseif($cliente->ponderacion=="AA"){
                                                            $star1="warning";
                                                            $star2="warning";
                                                            $star3="default";
                                                            $star4="default";
                                                        }elseif($cliente->ponderacion=="AAA"){
                                                            $star1="warning";
                                                            $star2="warning";
                                                            $star3="warning";
                                                            $star4="default";
                                                        }elseif($cliente->ponderacion=="AAAA"){
                                                            $star1="warning";
                                                            $star2="warning";
                                                            $star3="warning";
                                                            $star4="warning";
                                                        }
                                                        ?>
                                                        <a href="#!"><i class="icofont icofont-star  f-18 text-<?=$star1 ?>"></i></a>
                                                        <a href="#!"><i class="icofont icofont-star  f-18 text-<?=$star2 ?>"></i></a>
                                                        <a href="#!"><i class="icofont icofont-star  f-18 text-<?=$star3 ?>"></i></a>
                                                        <a href="#!"><i class="icofont icofont-star  f-18 text-<?=$star4 ?>"></i></a>
                                                    </td>
                                                    <td><?=$cliente->fuenteContratacion ?></td>
                                                    <td style=""><label class="label label-md bg-<?=$cliente->colorEstatus ?>" style="min-width: 40px; text-align: center; "><?=$cliente->textEstatus ?></label></td>

                                                    <td >
                                                        <?php
                                                        if(Yii::app()->user->idCatPerfil !=4 && Yii::app()->user->idCatPerfil !=5){
                                                            ?>
                                                            <a href="<?=CController::createUrl('cliente/edit', array("id" => $cliente->id)) ?>" class="m-r-15 text-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar" aria-describedby="tooltip913847"><i class="icofont icofont-ui-edit"></i></a>
                                                        <?php
                                                        }
                                                        ?>

                                                        <a href="<?=CController::createUrl('cliente/detalle',array("id"=>$cliente->id)) ?>" class="m-r-15 text-info" data-toggle="tooltip" data-placement="top" title=""  data-original-title="Detalles" aria-describedby="tooltip913847"><i class="icofont  icofont-info"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>


                                            </tbody>
                                        </table>

                                        <?php
                                    }else{
                                        ?>
                                        <div class="container">
                                            <div class="card-content" style=" overflow-y: auto">

                                                <div class="content-box " style="margin-top: 10%; color: gray;  text-align: center; ;">
                                                    <!-- Your text -->
                                                    <img src="/px/styles/img/sad.png">
                                                    <h1 class="text-muted mt-5">Opss! No hay nada que mostrar.</h1>

                                                    <p>Si deseas realizar una búsqueda, selecciona uno o más <br>
                                                        filtros de las
                                                        opciones 'Filtros de Búsqueda' y presiona en Buscar.
                                                    </p>
                                                </div>

                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>

                                    </br>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Config. table end -->

    </div>
</div>
<!-- MODAL FILTROS -->
<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-header bg-info" >
                <h4 class="modal-title">Filtros de Búsqueda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <p>Selecciona uno o varios campos de búsqueda, a continuación presiona 'Aplicar' para ejecutar tu petición.</p>
                <div class="container">
                        <div class="ccol-lg-12 col-md-12 m-b-15">
                            <h4 class="sub-title text-muted"><strong>Opciones de búsqueda</strong></h4>
                        </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            $tiposCte = CHtml::listData(Cattipocliente::model()->findAll(), 'id', 'descripcion');
                            echo $form->dropDownList($ClienteBusqueda, 'idCatTipoCliente', $tiposCte, array('class' => "form-control", 'placeholder' => "Tipo ...", 'empty' => "Tipo ..."));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            echo $form->dropDownList($ClienteBusqueda, 'categoria', array("P" => "PROSPECTO","C" => "CLIENTE"), array('class' => "form-control", 'placeholder' => "Categoria ...", 'empty' => "Categoria ..."));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            $sectores = CHtml::listData(Catsector::model()->findAll(), 'id', 'descripcion');
                            echo $form->dropDownList($ClienteBusqueda, 'idCatSector', $sectores, array('class' => "form-control", 'placeholder' => "Sector ...", 'empty' => "Sector ..."));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            $ponderaciones = CHtml::listData(Catponderacion::model()->findAll(), 'id', 'descripcion');
                            echo $form->dropDownList($ClienteBusqueda, 'idCatPonderacion', $ponderaciones, array('class' => "form-control", 'placeholder' => "Ponderación ...", 'empty' => "Ponderación ..."));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                            $sectores = CHtml::listData(Catfuentecontratacion::model()->findAll(), 'id', 'descripcion');
                            echo $form->dropDownList($ClienteBusqueda, 'idCatFuenteContratacion', $sectores, array('class' => "form-control", 'placeholder' => "Fuente Contratación ...", 'empty' => "Fuente Contratación ..."));
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-6 m-b-15">
                            <?php
                           echo $form->dropDownList($ClienteBusqueda, 'estatus', array("A"=>"ACTIVO","B" => "BAJA"), array('class' => "form-control", 'placeholder' => "Estatus ...", 'empty' => "Estatus ..."));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 m-b-15">
                            <?php

                            echo $form->textField($ClienteBusqueda, 'nombre',
                                array('class' => "form-control", 'placeholder' => "Nombre o Razon Social", 'empty' => "Fuente Contratación ..."));
                            ?>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>
                <a href="<?=CController::createUrl('unidad/index') ?>"> <button type="button" class="btn btn-sm btn-warning waves-effect waves-light ">LIMPIAR</button></a>
                <button type="submit" class="btn btn-sm btn-info waves-effect waves-light ">APLICAR</button>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
