<?php
/* @var $this ClienteController */
/* @var $model Cliente */
$tiposCliente = array('D' => 'Directo', 'A' => 'Agencia');
$niveles = CHtml::listData(Catnivelcliente::model()->findAll(), 'id', 'descripcion');
$sectores = CHtml::listData(Catsector::model()->findAll(), 'id', 'descripcion');
$tiposServicio = array('C' => 'Contrato', 'E' => 'Eventual');
?>

<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-user-alt-3 bg-c-green"></i>

                    <div class="d-inline">
                        <h4>Nuevo Prospecto Cliente</h4>
                        <span>Llena todos los formularios para registrar un cliente en el sistema</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cartera de Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Nuevo Cliente</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page-body start -->
    <div class="page-body">

        <div class="row">
            <div class="col-lg-12">
                <!-- tab header start -->
                <div class="tab-header card">
                    <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab" id="btnTab1"><h4>1. <span style="font-size: 14px">Información General</span></h4> </a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#binfo" role="tab" id="btnTab2"><h4>2. <span style="font-size: 14px">Datos de Facturación</span></h4> </a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#contacts" role="tab" id="btnTab3"><h4>3. <span style="font-size: 14px">Contáctos</span></h4> </a>

                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#servContratados" role="tab" id="btnTab4"><h4>4. <span style="font-size: 14px">Servicios contratados</span></h4> </a>

                            <div class="slide"></div>
                        </li>
                    </ul>
                </div>
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'cliente-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                ));
                ?>
                <!-- tab header end -->
                <!-- tab content start -->
                <div class="tab-content">
                    <!-- tab panel personal start -->
                    <div class="tab-pane active" id="personal" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Información General del cliente</h5>
                                <span>Por favor ingresa todos los campos requeridos y presiona en Siguiente para continuar</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?= CController::createUrl('clientes/index') ?>"><button class="btn btn-sm btn-danger">SALIR</button></a>
                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1" >
                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-info">PROSPECTO O CLIENTE</h5>

                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <i class="icofont icofont-users-social st-icon bg-info txt-lite-color"></i>

                                                <div class="text-left">
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->textField($model, 'nombre', array('class' => 'form-control', 'placeholder' => 'Nombre o Razón Social')); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->dropDownList($model, 'tipo', $tiposCliente, array('class' => 'form-control', 'empty' => 'Tipo de Cliente', 'id' => 'slTipoCliente')); ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->textField($model, 'calle', array('class' => 'form-control', 'placeholder' => 'Calle')); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->textField($model, 'numExt', array('class' => 'form-control', 'placeholder' => 'No Exterior')); ?>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->textField($model, 'numInt', array('class' => 'form-control', 'placeholder' => 'No Interior')); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->textField($model, 'telefono', array('class' => 'form-control', 'placeholder' => 'Teléfono local')); ?>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->textField($model, 'movil', array('class' => 'form-control', 'placeholder' => 'Teléfono Móvil')); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->textField($model, 'eMail', array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                                                        </div>

                                                    </div>
                                                    <div class="row" >
                                                        <div class="col-4">
                                                            <div class="input-group">
                                                                <?php echo $form->textField($model, 'cp', array('class' => 'form-control', 'placeholder' => 'C.P.')); ?>
                                                                <span class="input-group-addon"  id="buscar_cp">
                                                                    <i class="icofont icofont-search"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 ">
                                                            <?php echo $form->dropDownList($model, 'colonia', $colonias, array('class' => 'form-control', 'empty' => 'Colonia')); ?>
                                                        </div>
                                                        <div class="col-4">
                                                            <?php echo $form->textField($model, 'municipio', array('class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Municipio')); ?>
                                                        </div>
                                                    </div>
                                                    <div class="row" >
                                                        <div class="col-4 ">
                                                            <?php echo $form->textField($model, 'ciudad', array('class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Ciudad')); ?>
                                                        </div>
                                                        <div class="col-4 ">
                                                            <?php echo $form->textField($model, 'estado', array('class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Estado')); ?>
                                                        </div>
                                                        <div class="col-4 ">
                                                            <?php echo $form->textField($model, 'pais', array('class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Pais')); ?>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-3 m-b-15">
                                                            <?php echo $form->dropDownList($model, 'idCatNivelCliente', $niveles, array('class' => 'form-control', 'empty' => 'Nivel de cliente')); ?>
                                                        </div>
                                                        <div class="col-3 m-b-15">
                                                            <?php echo $form->dropDownList($model, 'tipoServicio', $tiposServicio, array('class' => 'form-control', 'empty' => 'Tipo de servicio', 'id' => "slTipoServicio")); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->dropDownList($model, 'idCatSector', $sectores, array('class' => 'form-control', 'empty' => 'Sector')); ?>
                                                        </div>
                                                        <div class="col-12 m-b-15">
                                                            <?php echo $form->textArea($model, 'comentario', array('class' => 'form-control', 'placeholder' => 'Comentario', 'rows' => "4", 'style' => 'resize:none')); ?>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                        <div class="card widget-statstic-card" id="cardCtesAgencias" style="background-color: #fdfdfd; display: none;">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-info">CLIENTES DE AGENCIA</h5><br>

                                                </div>
                                            </div>
                                            <div id="agenciaElements">
                                                <div class="card-block">
                                                    <i class="icofont icofont-briefcase-alt-1 st-icon bg-info txt-lite-color"></i>
                                                    <div class="text-left">
                                                        <div class="row">
                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->textField($model, 'agenciaNombre', array('class' => 'form-control agregarAgencia', 'placeholder' => 'Nombre o Razón Social')); ?>
                                                            </div>
                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->textField($model, 'agenciaRFC', array('class' => 'form-control agregarAgencia', 'placeholder' => 'RFC')); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 m-b-15">
                                                                <?php echo $form->textArea($model, 'agenciaObservacion', array('class' => 'form-control', 'placeholder' => 'Observación', 'rows' => "4", 'style' => 'resize:none')); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-footer">
                                                <button class="btn btn-sm btn-success" id="agregarAgencia">AGREGAR</button>
                                                <br />
                                                <br />
                                                <div class="alert alert-success background-success" style="display:none" id="agenciaSuccess">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                                    </button>
                                                    <strong>Exito!</strong> La agencia ha sido añadida correctamente.
                                                </div>
                                                <div class="alert alert-danger background-danger" style="display:none" id="agenciaError">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <i class="icofont icofont-close-line-circled text-white"></i>
                                                    </button>
                                                    <strong>Error!</strong> La agencia no ha podido ser añadida con la información proporcionada.
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table" id="agenciasAgregadas">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre</th>
                                                            <th>RFC</th>
                                                            <th>Observación</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr style="display:none">
                                                            <td>Mark</td>
                                                            <td>Otto</td>
                                                            <td>@mdo</td>
                                                            <td>eliminar</td>
                                                        </tr>
                                                        <?php foreach ($agencias as $agencia) { ?>
                                                            <tr>
                                                                <td>
                                                                    <input name="agenciaNombre[]" type="hidden" value="<?= $agencia['agenciaNombre'] ?>">
                                                                    <input name="agenciaRFC[]" type="hidden" value="<?= $agencia['agenciaRFC'] ?>">
                                                                    <input name="agenciaObservacion[]" type="hidden" value="<?= $agencia['agenciaObservacion'] ?>"><?= $agencia['agenciaNombre'] ?>
                                                                </td>
                                                                <td><?= $agencia['agenciaRFC'] ?></td>
                                                                <td><?= $agencia['agenciaObservacion'] ?></td>
                                                                <td><button class="btn btn-danger btn-outline-danger btn-icon borrarAgencia"><i class="icofont icofont-close"></i></button></td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="card widget-statstic-card"  id="cardContrato" style="background-color: #fdfdfd; display: none;">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-info">INFORMACION DE CONTRATO</h5>
                                                </div>
                                            </div>
                                            <div class="card-block">

                                                <i class="icofont icofont-paper st-icon bg-info txt-lite-color"></i>

                                                <div class="text-left">
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->dateField($model, "fechaInicioContrato", array('class' => 'form-control')); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->dateField($model, "fechaInicioContrato", array('class' => 'form-control')); ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 m-b-15">
                                                            <?php echo $form->fileField($model, "archivoContrato", array('class' => 'form-control')); ?>
                                                        </div>


                                                    </div>






                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                    <div class="col-lg-2 m-b-15 col-md-2 col-sm-1"></div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-info" onclick="document.getElementById('btnTab2').click();return false;" >SIGUIENTE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab pane personal end -->
                    <!-- tab pane info start -->
                    <div class="tab-pane" id="binfo" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Datos de Facturación</h5>
                                <span>Por favor ingresa todos los campos requeridos y presiona en Siguiente para continuar. (Puede registrar el número de razones sociales que desee)</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?= CController::createUrl('clientes/index') ?>"><button class="btn btn-sm btn-danger">SALIR</button></a>
                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1" >
                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-success">INFORMACIÓN FISCAL</h5>
                                                </div>
                                            </div>
                                            <div id="fiscalElements">
                                                <div class="card-block">
                                                    <i class="icofont icofont-id-card st-icon bg-success text-c-green"></i>
                                                    <div class="text-left">
                                                        <div class="row">
                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->textField($model, 'fiscalNombre', array('class' => 'form-control', 'placeholder' => 'Nombre o Razón Social')); ?>
                                                            </div>
                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->textField($model, 'fiscalRFC', array('class' => 'form-control', 'placeholder' => 'RFC')); ?>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->textField($model, 'fiscalCalle', array('class' => 'form-control', 'placeholder' => 'Calle')); ?>
                                                            </div>
                                                            <div class="col-2 m-b-15">
                                                                <?php echo $form->textField($model, 'fiscalNoExterior', array('class' => 'form-control', 'placeholder' => 'No Exterior')); ?>
                                                            </div>
                                                            <div class="col-4 m-b-15">
                                                                <?php echo $form->textField($model, 'fiscalNoInterior', array('class' => 'form-control', 'placeholder' => 'No Interior')); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" >
                                                            <div class="col-4">
                                                                <div class="input-group">
                                                                    <?php echo $form->textField($model, 'fiscalCp', array('class' => 'form-control', 'placeholder' => 'C.P.')); ?>
                                                                    <span class="input-group-addon" id="buscar_cp_fiscal">
                                                                        <i class="icofont icofont-search"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-4 ">
                                                                <?php echo $form->dropDownList($model, 'fiscalColonia', $colonias, array('class' => 'form-control', 'empty' => 'Colonia')); ?>
                                                            </div>
                                                            <div class="col-4">
                                                                <?php echo $form->textField($model, 'fiscalMunicipio', array('class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Municipio')); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" >
                                                            <div class="col-4 ">
                                                                <?php echo $form->textField($model, 'fiscalCiudad', array('class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Ciudad')); ?>
                                                            </div>
                                                            <div class="col-4 ">
                                                                <?php echo $form->textField($model, 'fiscalEstado', array('class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Estado')); ?>
                                                            </div>
                                                            <div class="col-4 ">
                                                                <?php echo $form->textField($model, 'fiscalPais', array('class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => 'Pais')); ?>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">

                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->dropDownList($model, 'fiscalUsoCfdi', array('1' => 'opción uno', '2' => 'opción dos'), array('class' => 'form-control', 'empty' => 'Uso de CFDI')); ?>
                                                            </div>
                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->dropDownList($model, 'fiscalMetodoPago', array('1' => 'opción uno', '2' => 'opción dos'), array('class' => 'form-control', 'empty' => 'Método de pago')); ?>
                                                            </div>


                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->dropDownList($model, 'fiscalFormaPago', array('1' => 'opción uno', '2' => 'opción dos'), array('class' => 'form-control', 'empty' => 'Forma de Pago')); ?>
                                                            </div>
                                                            <div class="col-6 m-b-15">
                                                                <?php echo $form->textField($model, 'fiscalEmail', array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <button class="btn btn-sm btn-success" id="agregarFiscal">AGREGAR</button>
                                                    <br />
                                                    <br />
                                                    <div class="alert alert-success background-success" style="display:none" id="fiscalSuccess">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <i class="icofont icofont-close-line-circled text-white"></i>
                                                        </button>
                                                        <strong>Exito!</strong> La información fiscal ha sido añadida correctamente.
                                                    </div>
                                                    <div class="alert alert-danger background-danger" style="display:none" id="fiscalError">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <i class="icofont icofont-close-line-circled text-white"></i>
                                                        </button>
                                                        <strong>Error!</strong> La información fiscal no ha podido ser añadida con la información proporcionada.
                                                    </div>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table" id="fiscalAgregadas">
                                                        <thead>
                                                            <tr>
                                                                <th>Nombre</th>
                                                                <th>RFC</th>
                                                                <th>Email</th>
                                                                <th>Eliminar</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr style="display:none">
                                                                <td>Mark</td>
                                                                <td>Otto</td>
                                                                <td>@mdo</td>
                                                                <td>eliminar</td>
                                                            </tr>
                                                            <?php foreach ($fiscales as $fiscal) { ?>
                                                                <tr>
                                                                    <td>
                                                                        <input name="fiscalNombre[]" type="hidden" value="<?= $fiscal['fiscalNombre'] ?>">
                                                                        <input name="fiscalRFC[]" type="hidden" value="<?= $fiscal['fiscalRFC'] ?>">
                                                                        <input name="fiscalCalle[]" type="hidden" value="<?= $fiscal['fiscalCalle'] ?>">
                                                                        <input name="fiscalNoExterior[]" type="hidden" value="<?= $fiscal['fiscalNoExterior'] ?>">
                                                                        <input name="fiscalNoInterior[]" type="hidden" value="<?= $fiscal['fiscalNoInterior'] ?>">
                                                                        <input name="fiscalCp[]" type="hidden" value="<?= $fiscal['fiscalCp'] ?>">
                                                                        <input name="fiscalColonia[]" type="hidden" value="<?= $fiscal['fiscalColonia'] ?>">
                                                                        <input name="fiscalMunicipio[]" type="hidden" value="<?= $fiscal['fiscalMunicipio'] ?>">
                                                                        <input name="fiscalCiudad[]" type="hidden" value="<?= $fiscal['fiscalCiudad'] ?>">
                                                                        <input name="fiscalEstado[]" type="hidden" value="<?= $fiscal['fiscalEstado'] ?>">
                                                                        <input name="fiscalPais[]" type="hidden" value="<?= $fiscal['fiscalPais'] ?>">
                                                                        <input name="fiscalUsoCfdi[]" type="hidden" value="<?= $fiscal['fiscalUsoCfdi'] ?>">
                                                                        <input name="fiscalMetodoPago[]" type="hidden" value="<?= $fiscal['fiscalMetodoPago'] ?>">
                                                                        <input name="fiscalFormaPago[]" type="hidden" value="<?= $fiscal['fiscalFormaPago'] ?>">
                                                                        <input name="fiscalEmail[]" type="hidden" value="<?= $fiscal['fiscalEmail'] ?>"><?= $fiscal['fiscalNombre'] ?>
                                                                    </td>
                                                                    <td><?= $fiscal['fiscalRFC'] ?></td>
                                                                    <td><?= $fiscal['fiscalEmail'] ?></td>
                                                                    <td><button class="btn btn-danger btn-outline-danger btn-icon borrarFiscal"><i class="icofont icofont-close"></i></button></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-3 m-b-15 col-md-2 col-sm-1"></div>

                                </div>
                                <div class="card-footer">
                                    <div class="card-footer-right" style="margin-right: 10px;">
                                        <button class="btn btn-sm btn-muted" onclick="document.getElementById('btnTab1').click();return false;" >ATRAS</button>
                                        <button class="btn btn-sm btn-info" onclick="document.getElementById('btnTab3').click();return false;" >SIGUIENTE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab pane info end -->
                    <!-- tab pane contact start -->
                    <div class="tab-pane" id="contacts" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Contactos para el Cliente</h5>
                                <span>Por favor ingresa todos los campos requeridos y presiona en Siguiente para continuar. (Puede registrar el número de contactos que desee)</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?= CController::createUrl('clientes/index') ?>"><button class="btn btn-sm btn-danger">SALIR</button></a>
                                    <a href="#"><button class="btn btn-sm btn-success">AGREGAR</button></a>
                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1" >
                                        <div class="card-block user-detail-card">
                                            <div class="card-header">
                                                <h5 style="color: #93139a;">INFORMACIÓN DEL CONTACTO</h5>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/Group-user.jpg " alt="" class="img-fluid p-b-10">

                                                </div>
                                                <div class="col-sm-8 user-detail">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Nombre :</h6>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <?php echo $form->textField($model, 'contactoNombre', array('class' => 'form-control', 'placeholder' => 'Nombre')); ?>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-email"></i>Email:</h6>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <?php echo $form->textField($model, 'contactoEmail', array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-touch-phone"></i>Télefono :</h6>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <?php echo $form->textField($model, 'contactoTelefono', array('class' => 'form-control', 'placeholder' => 'Teléfono')); ?>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="card-footer">
                                                    <button class="btn btn-sm btn-success" id="agregarContacto">AGREGAR</button>
                                                    <br />
                                                    <br />
                                                    <div class="alert alert-success background-success" style="display:none" id="contactoSuccess">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <i class="icofont icofont-close-line-circled text-white"></i>
                                                        </button>
                                                        <strong>Exito!</strong> El contacto ha sido añadido correctamente.
                                                    </div>
                                                    <div class="alert alert-danger background-danger" style="display:none" id="contactoError">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <i class="icofont icofont-close-line-circled text-white"></i>
                                                        </button>
                                                        <strong>Error!</strong> El contacto no ha podido ser añadido con la información proporcionada.
                                                    </div>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table" id="contactoAgregadas">
                                                        <thead>
                                                            <tr>
                                                                <th>Nombre</th>
                                                                <th>Email</th>
                                                                <th>Teléfono</th>
                                                                <th>Eliminar</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr style="display:none">
                                                                <td>Mark</td>
                                                                <td>Otto</td>
                                                                <td>@mdo</td>
                                                                <td>eliminar</td>
                                                            </tr>
                                                            <?php foreach ($contactos as $contacto) { ?>
                                                                <tr>
                                                                    <td>
                                                                        <input name="contactoNombre[]" type="hidden" value="<?= $contacto['contactoNombre'] ?>">
                                                                        <input name="contactoEmail[]" type="hidden" value="<?= $contacto['contactoEmail'] ?>">
                                                                        <input name="contactoTelefono[]" type="hidden" value="<?= $contacto['contactoTelefono'] ?>"><?= $contacto['contactoNombre'] ?>
                                                                    </td>
                                                                    <td><?= $contacto['contactoEmail'] ?></td>
                                                                    <td><?= $contacto['contactoTelefono'] ?></td>
                                                                    <td><button class="btn btn-danger btn-outline-danger btn-icon borrarContacto"><i class="icofont icofont-close"></i></button></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>




                                    </div>
                                    <div class="col-lg-3 m-b-15 col-md-2 col-sm-1"></div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-muted" onclick="document.getElementById('btnTab2').click();return false;" >ATRAS</button>
                                    <button class="btn btn-sm btn-info" onclick="document.getElementById('btnTab4').click();return false;" >SIGUIENTE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab pane info start -->
                    <div class="tab-pane" id="servContratados" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Servicios contratados</h5>
                                <span>Por favor ingresa los campos solicitados y presiona en 'Agregar'. Una vez que se hayan ingresado todos los servicios presiona 'Finalizar'.</span>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1" >
                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="" style="color: #39adb5;">PRODUCTO O SERVICIO A CONTRATAR</h5>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <i class="icofont icofont-cart-alt st-icon  text-c-green" style="background-color: #39adb5"></i>


                                                <div class="text-left">
                                                    <div class="row">

                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->dropDownList($model, 'productoProducto', array('1' => 'opción uno', '2' => 'opción dos'), array('class' => 'form-control', 'empty' => 'Producto o Servicio')); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->dropDownList($model, 'productoTipo', array('1' => 'opción uno', '2' => 'opción dos'), array('class' => 'form-control', 'empty' => 'Tipo de Servicio')); ?>
                                                        </div>


                                                    </div>
                                                    <div class="row">

                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->numberField($model, 'productoCantidad', array('class' => 'form-control', 'placeholder' => 'Cantidad')); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->numberField($model, 'productoImporte', array('class' => 'form-control', 'placeholder' => 'Importe')); ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->dateField($model, 'productoFechaInicio', array('class' => 'form-control')); ?>
                                                        </div>
                                                        <div class="col-6 m-b-15">
                                                            <?php echo $form->dateField($model, 'productoFechaFin', array('class' => 'form-control')); ?>
                                                        </div>

                                                    </div>



                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                    <button class="btn btn-sm btn-success" id="agregarProducto">AGREGAR</button>
                                                    <br />
                                                    <br />
                                                    <div class="alert alert-success background-success" style="display:none" id="productoSuccess">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <i class="icofont icofont-close-line-circled text-white"></i>
                                                        </button>
                                                        <strong>Exito!</strong> El producto ha sido añadido correctamente.
                                                    </div>
                                                    <div class="alert alert-danger background-danger" style="display:none" id="productoError">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <i class="icofont icofont-close-line-circled text-white"></i>
                                                        </button>
                                                        <strong>Error!</strong> El producto no ha podido ser añadido con la información proporcionada.
                                                    </div>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table" id="productoAgregadas">
                                                        <thead>
                                                            <tr>
                                                                <th>Producto</th>
                                                                <th>Tipo</th>
                                                                <th>Cantidad</th>
                                                                <th>Importe</th>
                                                                <th>Eliminar</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr style="display:none">
                                                                <td>Mark</td>
                                                                <td>Otto</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>eliminar</td>
                                                            </tr>
                                                            <?php foreach ($productos as $producto) { ?>
                                                                <tr>
                                                                    <td>
                                                                        <input name="productoProducto[]" type="hidden" value="<?= $producto['productoProducto'] ?>">
                                                                        <input name="productoTipo[]" type="hidden" value="<?= $producto['productoTipo'] ?>">
                                                                        <input name="productoCantidad[]" type="hidden" value="<?= $producto['productoCantidad'] ?>">
                                                                        <input name="productoImporte[]" type="hidden" value="<?= $producto['productoImporte'] ?>">
                                                                        <input name="productoFechaInicio[]" type="hidden" value="<?= $producto['productoFechaInicio'] ?>">
                                                                        <input name="productoFechaFin[]" type="hidden" value="<?= $producto['productoFechaFin'] ?>"><?= $producto['productoProducto'] ?>
                                                                    </td>
                                                                    <td><?= $producto['productoTipo'] ?></td>
                                                                    <td><?= $producto['productoCantidad'] ?></td>
                                                                    <td><?= $producto['productoImporte'] ?></td>
                                                                    <td><button class="btn btn-danger btn-outline-danger btn-icon borrarProducto"><i class="icofont icofont-close"></i></button></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-lg-3 m-b-15 col-md-2 col-sm-1"></div>

                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-muted" onclick="document.getElementById('btnTab3').click();" >ATRAS</button>
                                    <button class="btn btn-sm btn-primary" name="terminar">TERMINAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab pane info end -->
                </div>
                <?php $this->endWidget(); ?>
                <!-- tab content end -->
            </div>
        </div>

    </div>
    <!-- Page-body end -->
</div>
<script>
    $(document).ready(function () {
        if ($("#slTipoCliente").val() == "A") {
            $("#cardCtesAgencias").show();
        }
    });
    $("#slTipoCliente").change(function () {
        if ($("#slTipoCliente").val() == "A") {
            $("#cardCtesAgencias").show();
        } else {
            $("#cardCtesAgencias").hide();
        }
    });
    $("#slTipoServicio").change(function () {
        if ($("#slTipoServicio").val() == "C") {
            $("#cardContrato").show();
        } else {
            $("#cardContrato").hide();
        }
    });

    $("#Cliente_cp").keydown(function (e) {
        e.stopImmediatePropagation();
        var code = e.which; // recommended to use e.which, it's normalized across browsers
        if (code == 32 || code == 13 || code == 188 || code == 186) {
            e.preventDefault();
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('getCP'),
    'success' => 'js:llenarDireccion',
    'dataType' => 'json',
));
?>
        }
    });

    $("#buscar_cp").click(function () {
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('getCP'),
    'success' => 'js:llenarDireccion',
    'dataType' => 'json',
));
?>
    });
    function llenarDireccion(data) {
        if (data.status == "correcto") {
            $("#Cliente_colonia").empty();
            $("#Cliente_colonia").html(data.colonias);
            $("#Cliente_municipio").val(data.municipio);
            $("#Cliente_ciudad").val(data.ciudad);
            $("#Cliente_estado").val(data.estado);
            $("#Cliente_pais").val('México');
        }
    }
    ;

    $("#Cliente_fiscalCp").keydown(function (e) {
        e.stopImmediatePropagation();
        var code = e.which; // recommended to use e.which, it's normalized across browsers
        if (code == 32 || code == 13 || code == 188 || code == 186) {
            e.preventDefault();
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('GetCPFiscal'),
    'success' => 'js:llenarDireccionFiscal',
    'dataType' => 'json',
));
?>
        }
    });

    $("#buscar_cp_fiscal").click(function () {
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('GetCPFiscal'),
    'success' => 'js:llenarDireccionFiscal',
    'dataType' => 'json',
));
?>
    });
    function llenarDireccionFiscal(data) {
        if (data.status == "correcto") {
            $("#Cliente_fiscalColonia").empty();
            $("#Cliente_fiscalColonia").html(data.colonias);
            $("#Cliente_fiscalMunicipio").val(data.municipio);
            $("#Cliente_fiscalCiudad").val(data.ciudad);
            $("#Cliente_fiscalEstado").val(data.estado);
            $("#Cliente_fiscalPais").val('México');
        }
    }
    ;


    $('#agregarAgencia').click(function () {
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('agregarAgencia'),
    'success' => 'js:agregarAgencia',
    'dataType' => 'json',
));
?>
        return false;
    });
    $(".agregarAgencia").keydown(function (e) {
        e.stopImmediatePropagation();
        var code = e.which; // recommended to use e.which, it's normalized across browsers
        if (code == 32 || code == 13 || code == 188 || code == 186) {
            e.preventDefault();
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('agregarAgencia'),
    'success' => 'js:agregarAgencia',
    'dataType' => 'json',
));
?>
        }
    });
    function agregarAgencia(data) {
        if (data.status == "correcto") {
            //alert('Success: '+data.status);
            var element = '<tr><td><input name="agenciaNombre[]" type="hidden" value="' + data.nombre + '"><input name="agenciaRFC[]" type="hidden" value="' + data.rfc + '"><input name="agenciaObservacion[]" type="hidden" value="' + data.observacion + '">' + data.nombre + '</td><td>' + data.rfc + '</td><td>' + data.observacion + '</td><td><button class="btn btn-danger btn-outline-danger btn-icon borrarAgencia"><i class="icofont icofont-close"></i></button></td></tr>';
            $('#Cliente_agenciaNombre').val('');
            $('#Cliente_agenciaRFC').val('');
            $('#Cliente_agenciaObservacion').val('');
            $('#agenciasAgregadas tr:last').after(element);
            $('#agenciaError').hide();
            $('#agenciaSuccess').show();
        } else {
            $('#agenciaSuccess').hide();
            $('#agenciaError').show();
        }
    }
    ;
    $(document).on('click', '.borrarAgencia', function () {
        $(this).parent().parent().remove();
        return false;
    });


    $('#agregarFiscal').click(function () {
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('agregarFiscal'),
    'success' => 'js:agregarFiscal',
    'dataType' => 'json',
));
?>
        return false;
    });
    function agregarFiscal(data) {
        if (data.status == "correcto") {
            //alert('Success: '+data.status);
            var element = '<tr><td><input name="fiscalNombre[]" type="hidden" value="' + data.nombre + '"><input name="fiscalRFC[]" type="hidden" value="' + data.rfc + '"><input name="fiscalCalle[]" type="hidden" value="' + data.calle + '"><input name="fiscalNoExterior[]" type="hidden" value="' + data.noExterior + '"><input name="fiscalNoInterior[]" type="hidden" value="' + data.noInterior + '"><input name="fiscalCp[]" type="hidden" value="' + data.cp + '"><input name="fiscalColonia[]" type="hidden" value="' + data.colonia + '"><input name="fiscalMunicipio[]" type="hidden" value="' + data.municipio + '"><input name="fiscalCiudad[]" type="hidden" value="' + data.ciudad + '"><input name="fiscalEstado[]" type="hidden" value="' + data.estado + '"><input name="fiscalPais[]" type="hidden" value="' + data.pais + '"><input name="fiscalUsoCfdi[]" type="hidden" value="' + data.usoCfdi + '"><input name="fiscalMetodoPago[]" type="hidden" value="' + data.metodoPago + '"><input name="fiscalFormaPago[]" type="hidden" value="' + data.formaPago + '"><input name="fiscalEmail[]" type="hidden" value="' + data.email + '">' + data.nombre + '</td><td>' + data.rfc + '</td><td>' + data.email + '</td><td><button class="btn btn-danger btn-outline-danger btn-icon borrarFiscal"><i class="icofont icofont-close"></i></button></td></tr>';
            $('#Cliente_fiscalNombre').val('');
            $('#Cliente_fiscalRFC').val('');
            $('#Cliente_fiscalCalle').val('');
            $('#Cliente_fiscalNoExterior').val('');
            $('#Cliente_fiscalNoInterior').val('');
            $('#Cliente_fiscalCp').val('');
            $('#Cliente_fiscalColonia').val('');
            $('#Cliente_fiscalMunicipio').val('');
            $('#Cliente_fiscalCiudad').val('');
            $('#Cliente_fiscalEstado').val('');
            $('#Cliente_fiscalPais').val('');
            $('#Cliente_fiscalUsoCfdi').val('');
            $('#Cliente_fiscalMetodoPago').val('');
            $('#Cliente_fiscalFormaPago').val('');
            $('#Cliente_fiscalEmail').val('');
            $('#fiscalAgregadas tr:last').after(element);
            $('#fiscalError').hide();
            $('#fiscalSuccess').show();
        } else {
            $('#fiscalSuccess').hide();
            $('#fiscalError').show();
        }
    }
    ;
    $(document).on('click', '.borrarFiscal', function () {
        $(this).parent().parent().remove();
        return false;
    });
    
    $('#agregarContacto').click(function () {
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('agregarContacto'),
    'success' => 'js:agregarContacto',
    'dataType' => 'json',
));
?>
        return false;
    });
    function agregarContacto(data) {
        if (data.status == "correcto") {
            //alert('Success: '+data.status);
            var element = '<tr><td><input name="contactoNombre[]" type="hidden" value="' + data.nombre + '"><input name="contactoEmail[]" type="hidden" value="' + data.email + '"><input name="contactoTelefono[]" type="hidden" value="' + data.telefono + '">' + data.nombre + '</td><td>' + data.email + '</td><td>' + data.telefono + '</td><td><button class="btn btn-danger btn-outline-danger btn-icon borrarContacto"><i class="icofont icofont-close"></i></button></td></tr>';
            $('#Cliente_contactoNombre').val('');
            $('#Cliente_contactoEmail').val('');
            $('#Cliente_contactoTelefono').val('');
            $('#contactoAgregadas tr:last').after(element);
            $('#contactoError').hide();
            $('#contactoSuccess').show();
        } else {
            $('#contactoSuccess').hide();
            $('#contactoError').show();
        }
    }
    ;
    $(document).on('click', '.borrarContacto', function () {
        $(this).parent().parent().remove();
        return false;
    });
    
    $('#agregarProducto').click(function () {
<?=
CHtml::ajax(array(
    'type' => 'POST',
    'url' => CController::createUrl('agregarProducto'),
    'success' => 'js:agregarProducto',
    'dataType' => 'json',
));
?>
        return false;
    });
    function agregarProducto(data) {
        if (data.status == "correcto") {
            //alert('Success: '+data.status);
            var element = '<tr><td><input name="productoProducto[]" type="hidden" value="' + data.producto + '"><input name="productoTipo[]" type="hidden" value="' + data.tipo + '"><input name="productoCantidad[]" type="hidden" value="' + data.cantidad + '"><input name="productoImporte[]" type="hidden" value="' + data.importe + '"><input name="productoFechaInicio[]" type="hidden" value="' + data.fechaInicio + '"><input name="productoFechaFin[]" type="hidden" value="' + data.fechaFin + '">' + data.producto + '</td><td>' + data.tipo + '</td><td>' + data.cantidad + '</td><td>' + data.importe + '</td><td><button class="btn btn-danger btn-outline-danger btn-icon borrarProducto"><i class="icofont icofont-close"></i></button></td></tr>';
            $('#Cliente_productoProducto').val('');
            $('#Cliente_productoTipo').val('');
            $('#Cliente_productoCantidad').val('');
            $('#Cliente_productoImporte').val('');
            $('#Cliente_productofechaInicio').val('');
            $('#Cliente_productofechaFin').val('');
            $('#productoAgregadas tr:last').after(element);
            $('#productoError').hide();
            $('#productoSuccess').show();
        } else {
            $('#productoSuccess').hide();
            $('#productoError').show();
        }
    }
    ;
    $(document).on('click', '.borrarProducto', function () {
        $(this).parent().parent().remove();
        return false;
    });
</script>