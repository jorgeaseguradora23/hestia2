<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-ui-user bg-warning"></i>

                    <div class="d-inline">
                        <h4>Nuevo Prospecto</h4>
                        <span>Llena todos los formularios para registrar un nuevo prospecto en el sistema</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Cartera Clientes</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Prospectos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->


    <!-- Page-body start -->
    <div class="page-body">

        <div class="row">
            <div class="col-lg-12">
                <!-- tab header start -->
                <div class="tab-header card">
                    <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                        <li class="nav-item">
                            <a class="nav-link <?php if($infoGeneralCompleto==true){echo "active";} ?>" data-toggle="tab" href="#personal" role="tab" id="btnTab1"><h4>1. <span style="font-size: 14px">Información General</span></h4> </a>
                            <div class="slide"></div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link <?php if($infoFacturacion==true){echo "active";} ?>" data-toggle="tab" href="#contacts" role="tab" id="btnTab3"><h4>2. <span style="font-size: 14px">Datos de Facturación</span></h4> </a>

                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  <?php if($infoContactos==true){echo "active";} ?>" data-toggle="tab" href="#servContratados" role="tab" id="btnTab4"><h4>3. <span style="font-size: 14px">Contáctos</span></h4> </a>

                            <div class="slide"></div>
                        </li>
                    </ul>
                </div>
                <!-- tab header end -->
                <!-- tab content start -->
                <div class="tab-content">
                    <!-- tab panel personal start -->
                    <div class="tab-pane <?php if($infoGeneralCompleto==true){echo "active";} ?>" id="personal" role="tabpanel">
                        <div class="card">
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'login-form',
                                'enableClientValidation' => true,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                ),
                            ));
                            ?>
                            <div class="card-header">
                                <h5>CLIENTE, AGENCIA, CUENTA O GRUPO</h5>
                                <span>Por favor ingresa todos los campos requeridos y presiona en Siguiente para continuar</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?=CController::createUrl('cliente/index') ?>"><button class="btn btn-sm btn-danger" type="button">SALIR</button></a>
                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1" >
                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-info">&nbsp;</h5>

                                                </div>
                                            </div>

                                            <div class="card-block">

                                                <i class="icofont icofont-users-social st-icon bg-info txt-lite-color"></i>

                                                <div class="text-left">

                                                    <?php
                                                    if(!empty($model->errors)){
                                                        ?>
                                                        <div class="alert alert-danger icons-alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled"></i>
                                                            </button>
                                                            <p><strong>Atención!</strong> <?=$form->errorSummary($model) ?></p>
                                                        </div>

                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="row">

                                                        <div class="col-lg-6 col-md-12">
                                                            <h6 class="text-info">INFORMACIÓN GENERAL</h6>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Nombre o Razón Social</label>
                                                                    <!-- <label class="text-muted" style="font-size: 12px">CALLE</label> -->
                                                                    <?php echo $form->textField($model, 'nombre', array('class' => "form-control", 'placeholder' => "Nombre",'title' => "Nombre",  'value' => $model->nombre)); ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12  col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Calle o avenida</label>
                                                                    <?php echo $form->textField($model, 'calle', array('class' => "form-control", 'placeholder' => "calle",'type' => 'number','title' => "calle",  'value' => $model->calle)); ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">No. Exterior</label>
                                                                    <?php echo $form->textField($model, 'numExt', array('class' => "form-control", 'placeholder' => "Num Exterior",'title' => "Num Exterior",  'value' => $model->numExt)); ?>
                                                                </div>
                                                                <div class="col-lg-6 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">No. Interior</label>
                                                                    <?php echo $form->textField($model, 'numInt', array('class' => "form-control", 'placeholder' => "Num Interior",'title' => "Num Interior",  'value' => $model->numInt)); ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Teléfono</label>
                                                                    <?php echo $form->textField($model, 'telefono', array('class' => "form-control", 'placeholder' => "Teléfono",'title' => "Teléfono",  'value' => $model->telefono)); ?>
                                                                </div>
                                                                <div class="col-lg-6 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Móvil</label>
                                                                    <?php echo $form->textField($model, 'movil', array('class' => "form-control", 'placeholder' => "Móvil",'title' => "Móvil",  'value' => $model->movil)); ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-5 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Código Postal</label>

                                                                    <div class="input-group">
                                                                        <?php echo $form->numberField($clienteColonia, 'cp', array('class' => "form-control","autocomplete" => "off" ,'placeholder' => "C.P.",'title' => "C.P.",  'value' => $clienteColonia->cp)); ?>
                                                                        <button class="btn btn-sm btn-info" type="button" onclick="buscarCP()"><i class="fas fa-search"></i></button>

                                                                    </div>
                                                                    <input type="hidden" value="" id="cpBuscadoCte" name="cpBuscadoCte" >
                                                                </div>

                                                                <div class="col-lg-7 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Municipio o Alcaldía</label>
                                                                    <?php echo $form->textField($clienteColonia, 'municipio', array('class' => "form-control","readOnly" => "true" ,'placeholder' => "Municipio",'title' => "Municipio",  'value' => $clienteColonia->municipio)); ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Colonia</label>
                                                                    <div id="selectCol" name="selectCol">
                                                                        <?php
                                                                        $colonias = CHtml::listData(Catcolonia::model()->findAll($criteriaColoniaCte), 'id', 'colonia');

                                                                        echo $form->dropDownList($clienteColonia, 'colonia', $colonias, array('class' => "form-control", 'placeholder' => "-- Selecciona --",'title' => "Colonia", 'empty' => "-- Selecciona --"));
                                                                        ?>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Estado</label>
                                                                    <?php echo $form->textField($clienteColonia, 'estado', array('class' => "form-control","readOnly" => "true" ,'placeholder' => "Estado",'title' => "Estado",  'value' => $clienteColonia->estado)); ?>
                                                                </div>
                                                                <div class="col-lg-6 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Ciudad</label>
                                                                    <?php echo $form->textField($clienteColonia, 'ciudad', array('class' => "form-control","readOnly" => "true" ,'placeholder' => "Ciudad",'title' => "Ciudad",  'value' => $clienteColonia->ciudad)); ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">País</label>
                                                                    <?php echo $form->textField($clienteColonia, 'pais', array('class' => "form-control","readOnly" => "true" ,'placeholder' => "País",'title' => "País",  'value' => $clienteColonia->pais)); ?>
                                                                </div>
                                                                <div class="col-lg-6 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Tipo de Cliente</label>
                                                                    <?php
                                                                    $tiposClientes = CHtml::listData(Cattipocliente::model()->findAll(), 'id', 'descripcion');
                                                                    echo $form->dropDownList($model, 'idCatTipoCliente', ($tiposClientes), array('class' => "form-control","onChange"=> "setTipoCliente();", 'placeholder' => "Tipo ...",'title' => "Tipo", 'empty' => "Tipo ..."));
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-lg-12 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Agenscia o grupo al que pertenece</label>
                                                                    <?php

                                                                    $criteriaCt=new CDbCriteria();
                                                                    $criteriaCt->compare("idCatTipoCliente","4");
                                                                    $criteriaCt->compare("idCatTipoCliente","3", true," OR ");
                                                                    $criteriaCt->order="nombre";
                                                                    $clientes = CHtml::listData(Cliente::model()->findAll($criteriaCt), 'id', 'nombre');
                                                                    $attrDisbled="";
                                                                    $strDisabled="";
                                                                    if($model->idCatTipoCliente!="2"){
                                                                        $attrDisbled="disabled";
                                                                        $strDisabled="disabled";
                                                                    }
                                                                    echo $form->dropDownList($model, 'idCliente', ($clientes), array('class' => "form-control ",
                                                                        $attrDisbled => $strDisabled,
                                                                        'placeholder' => "Agencia o Grupo ...", "onchange" => "datosAgencia()",
                                                                        'title' => "Agencia o Grupo", 'empty' => "-- Selecciona --"));
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-12">
                                                            <h6 class="text-info">INFORMACIÓN DE SERVICIO</h6>
                                                            <hr>
                                                           <div class="row">
                                                               <div class="col-12 m-b-15">
                                                                   <label class="text-muted" style="font-size: 13px;">Celula</label>
                                                                   <?php
                                                                   $celulas = CHtml::listData(Catcelula::model()->findAll(), 'id', 'descripcion');
                                                                   echo $form->dropDownList($model, 'idCatCelula', $celulas, array('class' => "form-control", 'placeholder' => "Célula ...",'title' => "Célula", 'empty' => "Célula ..."));
                                                                   ?>
                                                               </div>
                                                           </div>


                                                            <div class="row">
                                                                <div class="col-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Sector</label>
                                                                    <?php
                                                                    $sectores = CHtml::listData(Catsector::model()->findAll(), 'id', 'descripcion');
                                                                    echo $form->dropDownList($model, 'idCatSector', $sectores, array('class' => "form-control", 'placeholder' => "Sector ...",'title' => "Sector", 'empty' => "Sector ..."));
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Ponderación</label>
                                                                    <?php
                                                                    $ponderacion = CHtml::listData(Catponderacion::model()->findAll(), 'id', 'descripcion');
                                                                    echo $form->dropDownList($model, 'idCatPonderacion', $ponderacion, array('class' => "form-control", 'placeholder' => "Ponderación ...",'title' => "Ponderación", 'empty' => "Ponderación ..."));
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Fuente de Contratación</label>
                                                                    <?php
                                                                    $fuentesC = CHtml::listData(Catfuentecontratacion::model()->findAll(), 'id', 'descripcion');
                                                                    echo $form->dropDownList($model, 'idCatFuenteContratacion', $fuentesC, array('class' => "form-control", 'placeholder' => "Fuente Contratación ...",'title' => "Fuente Contratación", 'empty' => "Fuente Contratación ..."));
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Correo(s) Electrónico(s)</label>
                                                                    <?php echo $form->textArea($model, 'eMail', array('class' => "form-control" ,'rows'=>"5",'placeholder' => "Email General",'title' => "Email General",  'value' => $model->eMail)); ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Correo(s) Electronico(s) para facturación</label>
                                                                    <?php echo $form->textArea($model, 'eMailFacturacion', array('class' => "form-control",'rows'=>"3","data-placement"=>"top",  "data-original-title"=>"Para ingresar más de un correo separaló con ; entre cada uno (ej. hola@micorreo.com; adios@micorreo.com)",'placeholder' => "Grupo email Facturación",'title' => "Grupo email Facturación",  'value' => $model->eMailFacturacion)); ?>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 m-b-15">
                                                                    <label class="text-muted" style="font-size: 13px;">Comentarios u observaciones</label>
                                                                    <?php echo $form->textArea($model, 'comentario', array('class' => "form-control", "rows" => "4",'placeholder' => "Comentario",'title' => "Comentario",  'value' => $model->comentario)); ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 m-b-15 col-md-2 col-sm-1"></div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-info" type="submit"  >SIGUIENTE</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- tab pane personal end -->
                    <!-- tab pane contact start -->
                    <div class="tab-pane <?php if($infoFacturacion==true){echo "active";} ?>" id="contacts" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Datos para emisión de CFDI</h5>
                                <span>Por favor ingresa todos los campos requeridos y presiona en Siguiente para continuar. Puede ingresar el número de razones sociales que sean necesarios. </span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?=CController::createUrl('cliente/index') ?>"><button class="btn btn-sm btn-danger" type="button">SALIR</button></a>

                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-1  m-b-15"></div>
                                    <div class="col-lg-6 col-md-8 col-sm-10" style="background-color: #fdfdfd" >

                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-success">INFORMACIÓN FISCAL PARA FACTURACIÓN</h5>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <i class="icofont icofont-id-card st-icon bg-success text-c-green"></i>


                                                <div class="text-left">
                                                    <?php
                                                    if(!empty($clienteDatosFacturacion->errors)){
                                                        ?>
                                                        <div class="alert alert-danger icons-alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled"></i>
                                                            </button>
                                                            <p><strong>Atención!</strong> <?=$form->errorSummary($clienteDatosFacturacion) ?></p>
                                                        </div>

                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-lg-8 col-md-12  m-b-15">
                                                            <?php echo $form->textField($clienteDatosFacturacion, 'razonSocial', array('class' => "form-control", 'placeholder' => "Razón Social",'title' => "Razón Social",  'value' => $clienteDatosFacturacion->razonSocial)); ?>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12  m-b-15">
                                                            <?php echo $form->textField($clienteDatosFacturacion, 'rfc', array('class' => "form-control thresold-i", 'autocomplete' => "off", 'placeholder' => "RFC",'title' => "RFC",  'value' => $clienteDatosFacturacion->rfc)); ?>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12 m-b-15">
                                                            <?php echo $form->textField($clienteDatosFacturacion, 'calle', array('class' => "form-control", 'placeholder' => "Calle",'title' => "Calle",  'value' => $clienteDatosFacturacion->calle)); ?>
                                                        </div>
                                                        <div class="col-lg-3 col-md-12 m-b-15">
                                                            <?php echo $form->textField($clienteDatosFacturacion, 'numExt', array('class' => "form-control", 'placeholder' => "Num Exterior",'title' => "Num Exterior",  'value' => $clienteDatosFacturacion->numExt)); ?>
                                                        </div>
                                                        <div class="col-lg-3 col-md-12 m-b-15">
                                                            <?php echo $form->textField($clienteDatosFacturacion, 'numInt', array('class' => "form-control", 'placeholder' => "Num Interior",'title' => "Num Interior",  'value' => $clienteDatosFacturacion->numInt)); ?>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-2 col-md-12 m-b-15">
                                                            <?php echo $form->numberField($dfColonia, 'cp', array('name'=>"dfColonia[cp]",'id'=>"dfColonia_cp",'class' => "form-control","autocomplete" => "off" ,'placeholder' => "C.P.",'title' => "C.P.",  'value' => $dfColonia->cp)); ?>
                                                            <input type="hidden" value="" id="cpBuscadoDf" name="cpBuscadoDf" >
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 m-b-15">
                                                            <?php
                                                            $colonias = CHtml::listData(Catcolonia::model()->findAll($criteriaColoniaDf), 'id', 'colonia');

                                                            echo $form->dropDownList($dfColonia, 'colonia', $colonias, array('name'=>"dfColonia[colonia]",'id'=>"dfColonia_colonia",'class' => "form-control", 'placeholder' => "Colonia ...",'title' => "Colonia", 'empty' => "Colonia ..."));
                                                            ?>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 m-b-15">
                                                            <?php echo $form->textField($dfColonia, 'municipio', array('name'=>"dfColonia[municipio]",'id'=>"dfColonia_municipio",'class' => "form-control","readOnly" => "true" ,'placeholder' => "Municipio",'title' => "Municipio",  'value' => $dfColonia->municipio)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-12 m-b-15">
                                                            <?php echo $form->textField($dfColonia, 'estado', array('name'=>"dfColonia[estado]",'id'=>"dfColonia_estado",'class' => "form-control","readOnly" => "true" ,'placeholder' => "Estado",'title' => "Estado",  'value' => $dfColonia->estado)); ?>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 m-b-15">
                                                            <?php echo $form->textField($dfColonia, 'ciudad', array('name'=>"dfColonia[ciudad]",'id'=>"dfColonia_ciudad",'class' => "form-control","readOnly" => "true" ,'placeholder' => "Ciudad",'title' => "Ciudad",  'value' => $dfColonia->ciudad)); ?>
                                                        </div>
                                                        <div class="col-lg-4 col-md-12 m-b-15">
                                                            <?php echo $form->textField($dfColonia, 'pais', array('name'=>"dfColonia[pais]",'id'=>"dfColonia_pais",'class' => "form-control","readOnly" => "true" ,'placeholder' => "País",'title' => "País",  'value' => $dfColonia->pais)); ?>
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 m-b-15">
                                                            <?php
                                                            $usos = CHtml::listData(Catusocfdi::model()->findAll(), 'id', 'descripcion');
                                                            echo $form->dropDownList($clienteDatosFacturacion, 'idCatUsoCFDI', $usos, array('class' => "form-control", 'placeholder' => "Uso del CFDI ...",'title' => "Uso del CFDI", 'empty' => "Uso del CFDI ..."));
                                                            ?>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12 m-b-15">
                                                            <?php
                                                            $pagos = CHtml::listData(Catmetodopago::model()->findAll(), 'id', 'descripcion');
                                                            echo $form->dropDownList($clienteDatosFacturacion, 'idCatMetodoPago', $pagos, array('class' => "form-control", 'placeholder' => "Método Pago ...",'title' => "Método Pago", 'empty' => "Método Pago ..."));
                                                            ?>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 m-b-15">
                                                            <?php
                                                            $fPagos = CHtml::listData(Catformapago::model()->findAll(), 'id', 'descripcion');
                                                            echo $form->dropDownList($clienteDatosFacturacion, 'idCatFormaPago', $fPagos, array('class' => "form-control", 'placeholder' => "Forma de Pago ...",'title' => "Forma de Pago", 'empty' => "Forma de Pago ..."));
                                                            ?>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <button class="btn btn-sm btn-success" type="submit" id="btnAgregarDatosFacturacion" name="btnAgregarDatosFacturacion" value="1">AGREGAR</button>
                                                        </div>
                                                    </div>




                                                </div>
                                            </div>

                                        </div>
                                        <?php
                                        if ( !empty($dfAdd) && is_array($dfAdd) ) {
                                            ?>
                                            <div class="card project-task" style="background-color: #fdfdfd">
                                                <div class="row">

                                                    <div class="col-12">


                                                        <div class="card-header">
                                                            <div class="card-header-left ">
                                                                <h5>Direcciones Fiscales agregadas</h5>
                                                            </div>

                                                        </div>
                                                        <div class="card-block p-b-0">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover">

                                                                    <tbody>
                                                                    <?php
                                                                    $cont = 0;
                                                                    if (!empty($dfAdd) && is_array($dfAdd)) {
                                                                        foreach ($dfAdd as $dato) {

                                                                            ?>

                                                                            <tr data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$dato["razonSocial"] ?>">
                                                                                <td style="width: 130px;max-width: 130px;">
                                                                                    <img class="img-rounded" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/user-3.png" alt="chat-user">
                                                                                </td>
                                                                                <td >
                                                                                    <h6 style="margin-top: 20px">
                                                                                        <i class="icofont icofont-ui-user text-primary"></i> <strong style="margin-left: 15px;">
                                                                                            <?php
                                                                                            $razonSocial=$dato["razonSocial"];
                                                                                            echo substr(((ucwords(strtolower($razonSocial)))),0,25);
                                                                                            if(strlen($razonSocial)>25){
                                                                                                echo " ...";
                                                                                            }
                                                                                            ?> </strong>
                                                                                    </h6>
                                                                                     <p class="text-muted" style="font-size: 12px; ">
                                                                                         <i class="icofont icofont-ui-home text-warning"></i>

                                                                                        <span style="margin-left: 15px;">
                                                                                            <?=ucwords(strtolower($dato["calle"])) ?>
                                                                                        No. <?=strtolower($dato["numExt"]) ?> <?=strtolower($dato["numInt"]) ?>
                                                                                        </span>
                                                                                            </br>

                                                                                        <?php
                                                                                        $criteriaColonia=new CDbCriteria();
                                                                                        $criteriaColonia->compare("id",$dato["idCatColonia"]);
                                                                                        $fiscales=Catcolonia::model()->findAll($criteriaColonia);
                                                                                        foreach($fiscales as $fiscal){
                                                                                            ?>
                                                                                         <span style="margin-left: 30px;">
                                                                                            <?php
                                                                                            echo "Col. ".$fiscal->colonia;
                                                                                            ?>
                                                                                         </span>
                                                                                            <br>
                                                                                         <span style="margin-left: 30px;">
                                                                                             <?php
                                                                                            echo $fiscal->municipio.", ";
                                                                                            echo ucwords(strtolower($fiscal->pais));
                                                                                            echo ", ".$fiscal->cp;
                                                                                        }
                                                                                        ?>
                                                                                        </span>
                                                                                    </p>
                                                                                </td>
                                                                                <td >
                                                                                    <h6 style="margin-top: 20px">
                                                                                        <i class="icofont icofont-contacts text-danger"></i>
                                                                                        <strong style="margin-left: 15px;">RFC: <?= strtoupper($dato["rfc"]) ?>
                                                                                        </strong>
                                                                                    </h6>

                                                                                    <p class="text-muted" style="font-size: 12px;">
                                                                                        <i class="icofont icofont-ui-clip-board text-c-lite-green" style="font-size: 14px;"></i>
                                                                                        <span style="margin-left: 15px;">
                                                                                              <?php
                                                                                              $strUsoCFDI=Catusocfdi::model()->findByPk($dato["idCatUsoCFDI"])->descripcion;
                                                                                              echo substr((($strUsoCFDI)),0,25);
                                                                                              if(strlen($strUsoCFDI)>25){
                                                                                                  echo " ...";
                                                                                              }
                                                                                              ?>
                                                                                        </span>
                                                                                        <br><i class="icofont icofont-ui-calendar text-c-orenge" style="font-size: 14px;"></i>
                                                                                        <span style="margin-left: 15px;">
                                                                                            <?php
                                                                                              $strMetodoPago=strtoupper(Catmetodopago::model()->findByPk($dato["idCatMetodoPago"])->claveSAT);
                                                                                              echo substr((($strMetodoPago)),0,32);
                                                                                              if(strlen($strMetodoPago)>32){
                                                                                                  echo " ...";
                                                                                              }
                                                                                              ?>
                                                                                        </span>
                                                                                        <br><i class="icofont icofont-credit-card text-success" style="font-size: 14px;"></i>
                                                                                        <span style="margin-left: 15px;">
                                                                                        <?php
                                                                                              $strFormaPago=strtoupper(Catformapago::model()->findByPk($dato["idCatFormaPago"])->descripcion);
                                                                                              echo substr((($strFormaPago)),0,32);
                                                                                              if(strlen($strFormaPago)>32){
                                                                                                  echo " ...";
                                                                                              }
                                                                                              ?>
                                                                                        </span>

                                                                                    </p>

                                                                                </td>
                                                                                <td style="width: 40px">
                                                                                    <input name="dfAdd[]"
                                                                                           id="idCatProductoServicio<?= $cont ?>"
                                                                                           type="hidden"
                                                                                           value="<?= $cont ?>,<?= strtoupper($dato["rfc"]) ?>,<?= $dato["idCliente"] ?>,<?= $dato["razonSocial"] ?>,<?= $dato["calle"] ?>,<?= $dato["numExt"] ?>,<?= $dato["numInt"] ?>,<?= $dato["idCatColonia"] ?>,<?= $dato["idCatUsoCFDI"] ?>,<?= $dato["idCatMetodoPago"] ?>,<?= $dato["idCatFormaPago"] ?>">
                                                                                    <button class="btn btn-sm btn-outline-danger"
                                                                                            type="submit"
                                                                                            id="btnEliminarDf"
                                                                                            name="btnEliminarDf"
                                                                                            value="<?= $cont ?>"
                                                                                            style="max-width: 40px;">
                                                                                        <i class="icofont icofont-ui-close"></i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>




                                                                            <?php
                                                                            $cont += 1;
                                                                        }
                                                                    }
                                                                    ?>

                                                                    </tbody>
                                                                </table>


                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>


                                    </div>
                                    <div class="col-lg-3 m-b-15 col-md-2 col-sm-1"></div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">
                                    <button class="btn btn-sm btn-muted" onclick="document.getElementById('btnTab1').click();"  type="button" >ATRAS</button>
                                    <button class="btn btn-sm btn-info" onclick="document.getElementById('btnTab4').click();" type="button" >SIGUIENTE</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- tab pane info start -->
                    <div class="tab-pane <?php if($infoContactos==true){echo "active";} ?>" id="servContratados" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5>Contactáctos del Prospecto</h5>
                                <span>Por favor ingresa los campos solicitados y presiona en 'Agregar'. Puede ingresar el número de contactos que sean necesarios.</span>
                                <div class="card-header-right" style="margin-right: 10px;">
                                    <a href="<?=CController::createUrl('cliente/index') ?>"><button class="btn btn-sm btn-danger" type="button">SALIR</button></a>
                                </div>
                            </div>
                            <div class="card-body center" >
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-1  m-b-15"></div>
                                    <div class="col-lg-6 col-md-8 col-sm-10 " >

                                        <div class="card widget-statstic-card" style="background-color: #fdfdfd">
                                            <div class="card-header">
                                                <div class="card-header-left">
                                                    <h5 class="text-success">INFORMACIÓN DEL CONTACTO</h5>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <i class="icofont icofont-id-card st-icon bg-success text-c-green"></i>


                                                <div class="text-left">
                                                    <?php
                                                    if(!empty($contactos->errors)){
                                                        ?>
                                                        <div class="alert alert-danger icons-alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="icofont icofont-close-line-circled"></i>
                                                            </button>
                                                            <p><strong>Atención!</strong> <?=$form->errorSummary($contactos) ?></p>
                                                        </div>

                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 m-b-15">
                                                            <?php echo $form->textField($contactos, 'nombreContacto', array('class' => "form-control","autocomplete"=>"off",'placeholder' => "Nombre",'title' => "Nombre",  'value' => $contactos->nombreContacto)); ?>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 m-b-15">
                                                            <?php echo $form->textField($contactos, 'telefono', array('class' => "form-control","autocomplete"=>"off",'placeholder' => "Teléfono",'title' => "Teléfono",  'value' => $contactos->telefono)); ?>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 m-b-15">
                                                            <?php echo $form->textField($contactos, 'eMail', array('class' => "form-control","autocomplete"=>"off",'placeholder' => "Email",'title' => "Email",  'value' => $contactos->eMail)); ?>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <button class="btn btn-sm btn-success" type="submit" id="btnAgregarContacto" name="btnAgregarContacto" value="1">AGREGAR</button>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>

                                        </div>
                                        <?php
                                        if ( !empty($contactoAdd) && is_array($contactoAdd) ) {
                                            ?>
                                            <div class="card project-task" style="background-color: #fdfdfd">
                                                <div class="row">

                                                    <div class="col-12">


                                                        <div class="card-header">
                                                            <div class="card-header-left ">
                                                                <h5>Contáctos Agregados</h5>
                                                            </div>

                                                        </div>
                                                        <div class="card-block p-b-0">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover">

                                                                    <tbody>
                                                                    <?php
                                                                    $cont = 0;
                                                                    if (!empty($contactoAdd) && is_array($contactoAdd)) {
                                                                        foreach ($contactoAdd as $dato) {

                                                                            ?>

                                                                            <tr>
                                                                                <td style="max-width: 120px; width: 120px;">
                                                                                    <img class="img-rounded" width="90px" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/default/assets/images/widget/Group-user.jpg" alt="chat-user">
                                                                                </td>
                                                                                <td >
                                                                                    <h6 style="margin-top: 15px;"><i class="icofont icofont-ui-user text-primary"></i><strong style="margin-left: 15px;"><?=ucwords(strtolower($dato["nombreContacto"])) ?></strong></h6>
                                                                                    <p class="text-muted" style="font-size: 12px">
                                                                                        <i class="icofont icofont-ui-touch-phone text-danger"></i></i><span style="margin-left: 15px;"><?=ucwords(strtolower($dato["telefono"])) ?></span><br>
                                                                                        <i class="icofont icofont-ui-email text-success"></i><span style="margin-left: 15px;"><?=strtolower($dato["eMail"]) ?></span>

                                                                                    </p>
                                                                                </td>

                                                                                <td style="width: 50px" align="center">
                                                                                    <input name="contactoAdd[]"
                                                                                           id="inpC<?= $cont ?>"
                                                                                           type="hidden"
                                                                                           value="<?= $cont ?>,<?= strtoupper($dato["nombreContacto"]) ?>,<?= $dato["telefono"] ?>,<?= $dato["eMail"] ?>">
                                                                                    <button class="btn btn-sm btn-outline-danger"
                                                                                            type="submit"
                                                                                            id="btnEliminarContacto"
                                                                                            name="btnEliminarContacto"
                                                                                            value="<?= $cont ?>"
                                                                                            style="max-width: 40px;">
                                                                                        <i class="icofont icofont-ui-close"></i>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>




                                                                            <?php
                                                                            $cont += 1;
                                                                        }
                                                                    }
                                                                    ?>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-lg-3 m-b-15 col-md-2 col-sm-1"></div>

                            </div>
                            <div class="card-footer">
                                <div class="card-footer-right" style="margin-right: 10px;">

                                    <button class="btn btn-sm btn-muted" onclick="document.getElementById('btnTab3').click();" type="button" >ATRAS</button>
                                    <button type="button" class="btn btn-sm btn-primary alert-confirm" id="btnFin" >TERMINAR</button>
                                    <input id="btnTerminar" name="btnTerminar" value="0" type="hidden">

                                </div>
                            </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>
                    <!-- tab pane info end -->
                </div>
                <!-- tab content end -->
            </div>
        </div>

    </div>
    <!-- Page-body end -->
</div>

<div class="sweet-overlay" tabindex="-1" style="opacity: -0.05; display: none;"></div>
<div class="sweet-alert hideSweetAlert" data-custom-class="" data-has-cancel-button="true" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="true" data-animation="pop" data-timer="null" style="display: none; margin-top: -171px; opacity: -0.05;">
    <div class="sa-icon sa-error" style="display: none;">
              <span class="sa-x-mark">
                <span class="sa-line sa-left"></span>
                <span class="sa-line sa-right"></span>
              </span>
    </div>
    <div class="sa-icon sa-warning" style="display: block;">
        <span class="sa-body"></span>
        <span class="sa-dot"></span>
    </div>
    <div class="sa-icon sa-info" style="display: none;"></div><div class="sa-icon sa-success" style="display: none;">
        <span class="sa-line sa-tip"></span>
        <span class="sa-line sa-long"></span>

        <div class="sa-placeholder"></div>
        <div class="sa-fix"></div>
    </div><div class="sa-icon sa-custom" style="display: none;"></div><h2>Are you sure?</h2>
    <p style="display: block;">Your will not be able to recover this imaginary filefdfsdf!</p>
    <fieldset>
        <input type="text" tabindex="3" placeholder="">
        <div class="sa-input-error"></div>
    </fieldset><div class="sa-error-container">
        <div class="icon">!</div>
        <p>Not valid!</p>
    </div><div class="sa-button-container">
        <button class="cancel" tabindex="2" style="display: inline-block; box-shadow: none;">Cancel</button>
        <div class="sa-confirm-button-container">
            <button class="confirm" tabindex="1" style="display: inline-block; background-color: rgb(140, 212, 245); box-shadow: none;">Yes, delete it!</button><div class="la-ball-fall">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#Catcolonia_cp").keypress(function (e) {

            if (e.which == 13) {

                $('#Candidato_idCatLocalidad').empty();
                buscarCP();
                e.preventDefault();
                return false;
            }

        });

    });

    function buscarCP() {
        $.ajax({
            type: "POST",
            url: "<?=CController::createUrl('cliente/getCodPostal') ?>?id=" + $("#Catcolonia_cp").val(),
            beforeSend: function () {

                $('#Catcolonia_colonia').empty();

            },
            success: function (response) {
                $("#selectCol").html();
                addOption=' <select class="form-control" name="Catcolonia[colonia]" id="Catcolonia_colonia">';
                addOption+="<option >-- Selecciona --</option>";
                datos = JSON.parse(response.trim());
                for (i = 0; i < datos.length; i++) {
                    addOption+="<option value='" + datos[i].id + "'>" + datos[i].asentamiento + "</option>";
                    $("#Catcolonia_municipio").prop('readOnly', false);
                    $("#Catcolonia_municipio").focus();
                    $('#Catcolonia_municipio').val(datos[i].municipio);
                    $("#Catcolonia_municipio").prop('readOnly', true);

                    $("#Catcolonia_estado").prop('readOnly', false);
                    $("#Catcolonia_estado").focus();
                    $('#Catcolonia_estado').val(datos[i].estado);
                    $("#Catcolonia_estado").prop('readOnly', true);

                    $("#Catcolonia_ciudad").prop('readOnly', false);
                    $("#Catcolonia_ciudad").focus();
                    $('#Catcolonia_ciudad').val(datos[i].ciudad);
                    $("#Catcolonia_ciudad").prop('readOnly', true);

                    $("#Catcolonia_pais").prop('readOnly', false);
                    $("#Catcolonia_pais").focus();
                    $('#Catcolonia_pais').val(datos[i].pais);
                    $("#Catcolonia_pais").prop('readOnly', true);
                }

                addOption+='</select>';

                $("#selectCol").html(addOption);
                $("select").formSelect();
            }
        });
    }

    function setTipoCliente(){
        if($("#Cliente_idCatTipoCliente").val()=="2"){
            $("#Cliente_idCliente").prop('disabled', false);
        }else{
            $("#Cliente_idCliente").prop('disabled', true);
        }
    }

    function datosAgencia() {


        $.ajax({
            type: "POST",
            url: "<?=CController::createUrl('cliente/getDatosAgencia') ?>?id=" +$("#Cliente_idCliente").val(),
            beforeSend: function () {

                $('#Catcolonia_colonia').empty();

            },
            success: function (response) {

                $("#selectCol").html();
                addOption=' <select class="form-control" name="Catcolonia[colonia]" id="Catcolonia_colonia">';
                addOption+="<option >-- Selecciona --</option>";
                datos = JSON.parse(response.trim());
                var idCatColonia=0;
                for (i = 0; i < datos.length; i++) {
                    addOption+="<option value='" + datos[i].id + "'>" + datos[i].asentamiento + "</option>";
                    $("#Catcolonia_municipio").prop('readOnly', false);
                    $("#Catcolonia_municipio").focus();
                    $('#Catcolonia_municipio').val(datos[i].municipio);
                    $("#Catcolonia_municipio").prop('readOnly', true);

                    $("#Catcolonia_estado").prop('readOnly', false);
                    $("#Catcolonia_estado").focus();
                    $('#Catcolonia_estado').val(datos[i].estado);
                    $("#Catcolonia_estado").prop('readOnly', true);

                    $("#Catcolonia_ciudad").prop('readOnly', false);
                    $("#Catcolonia_ciudad").focus();
                    $('#Catcolonia_ciudad').val(datos[i].ciudad);
                    $("#Catcolonia_ciudad").prop('readOnly', true);

                    $("#Catcolonia_pais").prop('readOnly', false);
                    $("#Catcolonia_pais").focus();
                    $('#Catcolonia_pais').val(datos[i].pais);
                    $("#Catcolonia_pais").prop('readOnly', true);

                    $('#Cliente_calle').val(datos[i].calle);
                    $('#Cliente_numExt').val(datos[i].numExt);
                    $('#Cliente_numInt').val(datos[i].numInt);
                    $('#Catcolonia_cp').val(datos[i].cp);

                    idCatColonia=(datos[i].idCatColonia);

                }

                addOption+='</select>';

                $("#selectCol").html(addOption);


                $('#Catcolonia_colonia').val(idCatColonia);
            }
        });
    }
</script>




