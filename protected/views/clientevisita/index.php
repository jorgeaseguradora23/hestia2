<style>
.select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #fff transparent transparent transparent;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 10px;
    right: 15px;
}
.select2-container {
    width: 100% !important;
}
.select2-container--default .select2-selection--single {
    color: #fff;
    height: auto;
}
.table {
  width: 100%;
  border-spacing: 0;
  border-collapse: separate;
  table-layout: fixed;
  margin-bottom: 50px;
}
.hour{
  vertical-align:middle !important;
}
.day{
  padding-left:0px !important;
}
.calendario {
  font-family: sans-serif;
  width: 100%;
  border-spacing: 0;
  border-collapse: separate;
  table-layout: fixed;
  margin-bottom: 50px;
}
.calendario thead tr th {
  background: #626E7E;
  color: #d1d5db;
  padding: 0.5em;
  overflow: hidden;
  text-align:center;
}
.calendario thead tr th:first-child {
  border-radius: 3px 0 0 0;
}
.calendario thead tr th:last-child {
  border-radius: 0 3px  0 0;
}
.calendario thead tr th .day {
  display: block;
  font-size: 1.2em;
  border-radius: 50%;
  /*width: 30px;*/
  height: 30px;
  padding: 5px;
  line-height: 1.8;
}
.calendario thead tr th .day.active {
  background: #d1d5db;
  color: #626E7E;
}
.calendario thead tr th .short {
  display: none;
}
.calendario thead tr th i {
  vertical-align: middle;
  font-size: 2em;
}
.calendario tbody tr {
  background: #d1d5db;
}
.calendario tbody tr:nth-child(odd) {
  background: #c8cdd4;
}
.calendario tbody tr:nth-child(4n+0) td {
  /*border-bottom: 1px solid #626E7E;*/
}
.calendario tbody tr td {
  text-align: center;
  vertical-align: middle;
  border-left: 1px solid #626E7E;
  position: relative;
  height: 32px;
  /*cursor: pointer;*/
}
.calendario tbody tr td:last-child {
  border-right: 1px solid #626E7E;
}
.calendario tbody tr td.hour {
  font-size: 2em;
  padding: 0;
  color: #626E7E;
  background: #fff;
  border-bottom: 1px solid #626E7E;
  border-collapse: separate;
  min-width: 100px;
  cursor: default;
}
.calendario tbody tr td.hour span {
  display: block;
}
@media (max-width: 60em) {
  .calendario thead tr th .long {
    display: none;
  }
  .calendario thead tr th .short {
    display: block;
  }
  .calendario tbody tr td.hour span {
    transform: rotate(270deg);
    -webkit-transform: rotate(270deg);
    -moz-transform: rotate(270deg);
  }
}
@media (max-width: 27em) {
  .calendario thead tr th {
    font-size: 65%;
  }
  .calendario thead tr th .day {
    display: block;
    font-size: 1.2em;
    border-radius: 50%;
    width: 20px;
    height: 20px;
    margin: 0 auto 5px;
    padding: 5px;
  }
  .calendario thead tr th .day.active {
    background: #d1d5db;
    color: #626E7E;
  }
  .calendario tbody tr td.hour {
    font-size: 1.7em;
  }
  .calendario tbody tr td.hour span {
    transform: translateY(16px) rotate(270deg);
    -webkit-transform: translateY(16px) rotate(270deg);
    -moz-transform: translateY(16px) rotate(270deg);
  }
}

.select2-container--default .select2-selection--single .select2-selection__rendered {
    background-color: #4680ff;
    color: #fff;
    padding: 8px 30px 8px 20px;
}
</style>
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-database-add bg-c-blue"></i>

                    <div class="d-inline">
                        <h4>Visitas a Clientes</h4>
                        <span>Administración de visitas a Clientes</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 right">

                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Visitas a Clientes</a>
                        </li>
                    </ul>
                </div>



            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">

        <!-- Config. table start -->
        <?php
					
          
          /*$eventos= Clientevisita::model()->findAll('DATE(fechaVisita)>=:fechaVisitaInicio and DATE(fechaVisita)<=:fechaVisitaFin order by fechaVisita', array(':fechaVisitaInicio'=>date('Y-m-d',$estaSemanaCompleto),':fechaVisitaFin'=>date('Y-m-d',strtotime('+6 day',$estaSemanaCompleto))));
          $eventosCalendario=array();
          foreach($eventos as $evento){
            $eventoCalendario['id']=$evento->id;
            $eventoCalendario['cliente']=Cliente::model()->findByPk($evento->idCliente)->nombre;
            $eventoCalendario['empleado']=Empleado::model()->findByPk($evento->idEmpleado)->nombrePila;
            $eventoCalendario['observacion']=$evento->observacion;
            $eventosCalendario["{$evento->fechaVisita}"][]=$eventoCalendario;
          }
          var_dump($eventosCalendario);*/
          ?>
        <div class="card">
            <div class="card-header">

                <div class="card-header-right" style="margin-right: 10px;">
                    <a  href="<?=CController::createUrl('clientevisita/create')?>">
                        <button class="btn btn-sm btn-success waves-effect" ><i class="fas fa-plus text-white"></i> NUEVO EVENTO</button>
                    </a>
                    <button class="btn btn-sm btn-primary waves-effect"  data-toggle="modal" data-target="#default-Modal"><i class="fas fa-filter text-white"></i> FILTRAR</button>
                    <!--<button class="btn btn-sm btn-success waves-effect" >CSV</button>
                    <button class="btn btn-sm btn-danger waves-effect"  >PDF</button>-->
                </div>
            </div>
            <div class="card-block" >
                <br><br>
                <div class="" >
                    <div class="dt-responsive table-responsive" style=" overflow-x: hidden" >
                        <div id="res-config_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="row" >

                                <div class="col-xs-12 col-sm-12 col-lg-12" style="text-align:left;">

                                <?php
                                $parametrosAtras=array();
                                $parametrosAtras['Clientevisita']['fechaVisita']=date('Y-m-d',strtotime('last monday',$estaSemanaCompleto));
                                if(isset($_GET['Clientevisita']['idEmpleado']) && !empty($_GET['Clientevisita']['idEmpleado'])){
                                  $parametrosAtras['Clientevisita']['idEmpleado']=$_GET['Clientevisita']['idEmpleado'];
                                }
                                if(isset($_GET['Clientevisita']['idCliente']) && !empty($_GET['Clientevisita']['idCliente'])){
                                  $parametrosAtras['Clientevisita']['idCliente']=$_GET['Clientevisita']['idCliente'];
                                }
                                $parametrosAdelante=array();
                                $parametrosAdelante['Clientevisita']['fechaVisita']=date('Y-m-d',strtotime('next monday',$estaSemanaCompleto));
                                if(isset($_GET['Clientevisita']['idEmpleado']) && !empty($_GET['Clientevisita']['idEmpleado'])){
                                  $parametrosAdelante['Clientevisita']['idEmpleado']=$_GET['Clientevisita']['idEmpleado'];
                                }
                                if(isset($_GET['Clientevisita']['idCliente']) && !empty($_GET['Clientevisita']['idCliente'])){
                                  $parametrosAdelante['Clientevisita']['idCliente']=$_GET['Clientevisita']['idCliente'];
                                }
                                $meses="";
                                $indexAnterior=0;
                                for($y=0;$y<7;$y++){

                                    $mesIndex=date("m",strtotime($estaSemanaID[$y]))*1;
                                    if($indexAnterior==0){
                                        $meses.=$arrayMeses[$mesIndex-1]." ".date("Y",strtotime($estaSemanaID[$y]));
                                    }elseif ($indexAnterior!=$mesIndex){
                                        $meses.=" - ".$arrayMeses[$mesIndex-1]." ".date("Y",strtotime($estaSemanaID[$y]));
                                    }
                                    $indexAnterior=$mesIndex;
                                }

                                ?>


                                </div>
                              </div>

                            <div class="row" >
                                <div class="col-xs-12 col-sm-12 col-lg-12" style="margin: 20px;" >

                                    <h3>
                                        <a  href="<?=CController::createUrl('clientevisita/index', $parametrosAtras)?>" class="  " style="font-size: 23px; color: grey">
                                            <i class="fas fa-chevron-left"></i>
                                        </a>
                                        <a  href="<?=CController::createUrl('clientevisita/index', $parametrosAdelante)?>" class="  " style="font-size: 23px;margin-left: 15px; margin-right: 35px;  color: grey">
                                            <i class="fas fa-chevron-right"></i>
                                        </a>
                                        <?=$meses ?></h3>
                                    <hr>
                                    <table class="table table-hover table-borderless ">
                                      <thead>
                                        <tr>
                                          <th width="80px" style="border-top: none"></th>
                                            <?php
                                            $diasAcro=["LUN","MAR","MIE","JUE","VIE","SAB","DOM"];
                                            $diasComplero=["Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo"];
                                            for($y=0;$y<7;$y++){
                                            if($estaSemanaID[$y]!=date("Y-m-d")){
                                                    ?>
                                                    <th align="center" class="center" style="border-top: none" >
                                                        <div class="row"  height="auto">
                                                            <div class="col-12 center" style="text-align: center">
                                                          <span style="font-size: 12px; color: gray">
                                                              <?=$diasAcro[$y] ?>
                                                          <br>
                                                              <span style="font-size: 25px; color: black"><?=date("d",strtotime($estaSemanaID[$y]))?></span>
                                                          </span>
                                                            </div>
                                                        </div>
                                                    </th>
                                                <?php
                                                }else{
                                                ?>
                                                <th style="border-bottom: steelblue solid 5px; border-top: none">
                                                    <div class="row"  height="auto">
                                                        <div class="col-12 center" style="text-align: center">
                                                      <span style="font-size: 12px; color: gray">
                                                          <?=$diasAcro[$y] ?>
                                                      <br>
                                                          <a class="btn btn-primary btn-icon text-white" style="background-color: steelblue" ><strong><?=date("d",strtotime($estaSemanaID[$y]))?></strong></a>

                                                      </span>
                                                        </div>
                                                    </div>

                                                </th>
                                            <?php
                                                }
                                            }
                                            ?>
                                            <th width="40px" style="border-top: none"></th>
                                        </tr>
                                      </thead>
                                        <tbody>
                                        <?php for($j=8;$j<=18;$j++){ ?>
                                            <tr style="min-height: 120px; height: 120px; " >
                                                <td class="hour" valign="top" align="right" style="font-size: 12px; vertical-align: top" >
                                                    <div style="height: 120px; ">
                                                        <?=$j?>:00 Hrs
                                                    </div>

                                                </td>
                                                <?php
                                                for($i=0;$i<=6;$i++){
                                                    $fondo="";
                                                    if($estaSemanaID[$i]==date("Y-m-d")){
                                                        $fondo="border-left: solid #cadff0 2px; border-right: solid #cadff0 2px; ";
                                                    }else{
                                                        $fondo="border-left: solid #f6f7fb 1px; border-right: solid #f6f7fb 1px; ";
                                                    }
                                                    $criteria= new CDbCriteria;
                                                    $criteria->addCondition('DATE(fechaVisita)=:fechaVisita');
                                                    $criteria->params[':fechaVisita']=$estaSemanaID[$i];
                                                    $criteria->addCondition('Hour(fechaVisita)=:horaVisita');
                                                    $criteria->params[':horaVisita']=$j;
                                                    $criteria->order='fechaVisita';

                                                    if(!empty($_GET['Clientevisita']['idEmpleado'])){
                                                        $criteria->addCondition('idEmpleado=:idEmpleado');
                                                        $criteria->params[':idEmpleado']=$_GET['Clientevisita']['idEmpleado'];
                                                    }
                                                    if(!empty($_GET['Clientevisita']['idCliente'])){
                                                        $criteria->addCondition('idCliente=:idCliente');
                                                        $criteria->params[':idCliente']=$_GET['Clientevisita']['idCliente'];
                                                    }
                                                    $eventos= ViewClienteVisita::model()->findAll($criteria);
                                                    echo "<td align='left' style='".$fondo."'><div class='row' style='padding: 0px;'>";
                                                    if(!empty($eventos)){
                                                        $columnas=count($eventos);
                                                        $ancho=12/$columnas;
                                                        $contadorColumna=1;
                                                        foreach($eventos as $key=>$evento){

                                                            $valorVacio=60-(date("i",strtotime($evento->horaVisita)));
                                                            $altoDivEvento=number_format(($valorVacio/60)*120,0);
                                                            $marginTopEvento=number_format(120-(($valorVacio/60)*120),0);
                                                            if($altoDivEvento<=45){
                                                                $marginTopEvento=120-45;
                                                            }
                                                            if($key>0){

                                                            }
                                                            if($evento->estatus=='P'){
                                                                if($evento->tipo=='R') {
                                                                    $btnEstatus = 'darkred';
                                                                }else{
                                                                    $btnEstatus='#eab365';
                                                                }
                                                            }elseif($evento->estatus=='A'){
                                                                $btnEstatus='#96c156';
                                                            }else{

                                                                    $btnEstatus='#96c156';

                                                            }
                                                            ?>
                                                            <div class="col-<?=$ancho ?>">
                                                                <?php
                                                                $descripcionEvento=$evento->observacion;
                                                                $espaciosEvento="";
                                                                switch ($columnas){
                                                                    case 1:
                                                                        $espaciosEvento="padding-left: 11px; padding-right: 11px;";
                                                                        break;
                                                                    case 2:
                                                                        if($contadorColumna==1){
                                                                            $espaciosEvento="padding-left: 11px; padding-right: 1px;";
                                                                        }else{
                                                                            $espaciosEvento="padding-right: 11px; padding-left: 1px;";

                                                                        }
                                                                        break;
                                                                    case 3:
                                                                        if($contadorColumna==1){
                                                                            $espaciosEvento="padding-left: 11px; ";
                                                                        }else if($contadorColumna==2){
                                                                            $espaciosEvento="padding-right: 2px; padding-left: 2px;";
                                                                        }else{
                                                                            $espaciosEvento="padding-right: 11px;";
                                                                        }
                                                                        break;
                                                                }
                                                                $contadorColumna+=1;
                                                                ?>
                                                                <div class="row mytooltip" style="<?=$espaciosEvento ?>  margin-top: <?=$marginTopEvento ?>px;">
                                                                    <div class="col-12"
                                                                         onclick="document.location.href='<?=CController::createUrl('clientevisita/view', array('id'=>$evento->id)) ?>'"
                                                                         style='overflow: hidden;text-overflow: ellipsis;white-space: nowrap;
                                                                                 min-height: 45px;height: <?=$altoDivEvento ?>px ; padding: 12px; cursor:pointer;
                                                                                 background-color: <?=$btnEstatus ?>; color: white; font-size: 14px; border-radius: 10px;'>
                                                                        <strong><?=ucwords(mb_strtolower($evento->nombre)) ?></strong><br>
                                                                        <?php
                                                                        if($altoDivEvento>46){
                                                                            ?>
                                                                            <span style="font-size: 12px;"> <?=date("H:i",strtotime($evento->horaVisita)) ?> Hrs.</span><br>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <?php
                                                                        if($altoDivEvento>64) {
                                                                            ?>
                                                                            <i style="font-size: 12px;"><?=ucwords(mb_strtolower($evento->tipoVisita)) ?></i>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </div>
                                                                    <span class="tooltip-content5 "  >
                                                                  <span class="tooltip-text3" >
                                                                      <span class="tooltip-inner2"
                                                                            style="font-size: 12px; text-align: left;
                                                                            color: white; ">
                                                                          <span style="font-size: 15px; color: <?=$btnEstatus ?>" ><strong><i class="far fa-user mr-2"></i> <?=ucwords(mb_strtolower($evento->nombre)) ?></strong></span>
                                                                          <span style="font-size: 12px; margin-top: 10px;"><i class="far fa-clock mr-2"></i> <?=$diasComplero[$i]?> <?=date("d, H:i",strtotime($evento->fechaVisita)) ?> Hrs.</span>
                                                                          <span style="margin-top: 5px;"><i class="far fa-address-book mr-2"></i> <i><?=ucwords(mb_strtolower($evento->tipoVisita)) ?></i></span>
                                                                          <span style="margin-top: 5px;white-space: pre-wrap; color: darkgrey"><i class="far fa-comment-alt mr-2"></i> <?=ucfirst(mb_strtolower($descripcionEvento)) ?></span>
                                                                      </span>
                                                                  </span>
                                                                </span>

                                                                </div>

                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    echo '</div></td>';
                                                }
                                                ?>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                    <div>
                                                                            </div>
                                </div>
                            </div>

                        </div>
                        <span>
					<?php if ((isset($_GET['Clientevisita']['idEmpleado']) && !empty($_GET['Clientevisita']['idEmpleado'])) || (isset($_GET['Clientevisita']['idCliente']) && !empty($_GET['Clientevisita']['idCliente']) )) { ?>
                        Se muestran todos los resultados del <?=date('j',$estaSemanaCompleto) . ' ' . $arrayMeses[date('n',$estaSemanaCompleto)-1]?> al <?=date('j',strtotime('+6 day',$estaSemanaCompleto)) . ' ' . $arrayMeses[date('n',strtotime('+6 day',$estaSemanaCompleto))-1];?>
                        del empleado: <?=(!empty($_GET['Clientevisita']['idEmpleado']))?$empleados[$_GET['Clientevisita']['idEmpleado']]:"Todos los empleados"?>
                        y del cliente: <?=(!empty($_GET['Clientevisita']['idCliente']))?$clientes[$_GET['Clientevisita']['idCliente']]:"Todos los empleados"?>
                    <?php }else{ ?>
                        Se muestran los resultados de la semana del <?=date('j',$estaSemanaCompleto) . ' ' . $arrayMeses[date('n',$estaSemanaCompleto)-1]?> al <?=date('j',strtotime('+6 day',$estaSemanaCompleto)) . ' ' . $arrayMeses[date('n',strtotime('+6 day',$estaSemanaCompleto))-1];?>
                    <?php } ?>
					<br />
				</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Config. table end -->

    </div>
</div>
<div class="modal fade" id="default-Modal"  role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <div class="modal-content">
  <?php
  $bolError=false;
  $form2 = $this->beginWidget('CActiveForm', array(
  'id' => 'filtrosBusqueda',
  'method' => 'GET',
  'action' => CController::createUrl('clientevisita/index'),
  'enableClientValidation' => true,
  'clientOptions' => array(
  'validateOnSubmit' => true,
  ),
  ));
  ?>
  <div class="modal-header bg-primary" >
  <h4 class="modal-title">Filtros de Búsqueda</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">×</span>
  </button>
  </div>
  <div class="modal-body">
  <?php
  if(!empty($busqueda->errors)){
  $bolError=true;
  ?>
  <div class="alert alert-danger icons-alert" >
  <button type="button" id="errorNewFil" name="errorNewFil" class="close" data-dismiss="alert" aria-label="Close">
  <i class="icofont icofont-close-line-circled"></i>
  </button>
  <p><strong>Atención!</strong> <?=$form2->errorSummary($busqueda) ?></p>
  </div>

  <?php
  }
  ?>
  <p>Selecciona uno o varios campos de búsqueda, a continuación presiona 'Aplicar' para ejecutar tu petición.</p>
  <div class="container">
  <div class="row " >
  <div class="col-12 m-b-15">
  <h4 class="sub-title text-muted"><strong>SELECCIONA UNA OPCIÓN</strong></h4>

  <div class="row">
    <div class="col-12">
      <?php echo $form2->dateField($busqueda, 'fechaVisita', array('class' => "form-control", 'placeholder' => "Fecha de Visita", 'title' => "Fecha de Visita")); ?>
    </div>
  </div>
  <br />
  <div class="row">
    <div class="col-12">
    <?php echo $form2->dropDownList($busqueda, 'idEmpleado', $empleados, array('class' => "js-example-basic-single col-sm-12",'empty' => "Selecciona el Empleado ... ")); ?>
    </div>
  </div>
  <br />
  ¿

  </div>

  <input type="hidden" value="" id="rowElimina" name="rowElimina" >

  </div>

  </div>

  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal">CERRAR</button>
  <a href="<?=CController::createUrl('CATCOLONIA/index') ?>"> <button type="button" class="btn btn-sm btn-warning waves-effect waves-light ">LIMPIAR</button></a>
  <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light " id="btnBuscar" name="btnBuscar" value="1">APLICAR</button>
  </div>
  <?php $this->endWidget(); ?>
  </div>
  </div>
  </div>


<div class="popover bs-popover-left fade " role="tooltip" id="popover783611" x-placement="left">
    <div class="arrow" style="top: 36px;">

    </div><h3 class="popover-header">Hide Method Popover</h3><div class="popover-body">Fruitcake candy cheesecake jelly beans cake gummies. Cotton candy I love sweet roll jujubes pastry cake halvah cake.</div></div>

  <script>
    $(document).ready(function(){
	// Single Search Select
    $(".js-example-basic-single").select2();
    });
  </script>