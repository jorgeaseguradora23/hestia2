<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b {
        border-color: #fff transparent transparent transparent;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 10px;
        right: 15px;
    }
    .select2-container {
        width: 100% !important;
    }
    .select2-container--default .select2-selection--single {
        color: #fff;
        height: auto;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        background-color: white ;
        color: #535251;
        height: 36px;
        padding: 4px 30px 8px 20px;
    }
</style>
<div class="page-wrapper">
    <!-- Page-header start -->
    <?php
    $diasComplero=["Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo"];
    $arrayMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    ?>



    <!-- Page-header end -->


    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-sm-4">
                <?php
                $colorCalificacion="secondary";
                if($visita->idCatTono==1){
                    $colorCalificacion="success";
                }elseif($visita->idCatTono==2){
                    $colorCalificacion="danger";
                }elseif($visita->idCatTono==3){
                    $colorCalificacion="warning";
                }

                ?>

                <div class="card bg-<?=$colorCalificacion ?> text-white widget-visitor-card">
                    <div class="card-block-small text-center">
                        <h2>
                           <?=isset($visita->tono)?ucwords(mb_strtolower($visita->tono)):"En espera" ?>
                        </h2>
                        <h6>Calificación Evento</h6>
                        <i class="ti-user"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card bg-<?php
                        if($visita->tipo=="P"){
                            echo "success";
                        }elseif($visita->tipo=="R"){
                            echo "danger";
                        }elseif($visita->tipo=="E"){
                            echo "warning";
                        }
                ?> text-white widget-visitor-card">
                    <div class="card-block-small text-center">
                        <h2><?php
                            if($visita->tipo=="P"){
                                echo "Proactivo";
                            }elseif($visita->tipo=="R"){
                                echo "Reactivo";
                            }elseif($visita->tipo=="E"){
                                echo "En respuesta";
                            }
                            ?></h2>
                        <h6>Tipo de Evento</h6>
                        <i class="icofont icofont-paper"></i>
                    </div>
                </div>
            </div>
            <?php
            $tituloMensaje="";
            $valorEvento="";
            if($visita->confirmado=="N"){
                $tituloMensaje="¿Confirmar Evento?";
                $valorEvento="S";
            }else{
                $tituloMensaje="¿Evento Pendiente?";
                $valorEvento="N";
            }
            ?>
            <div class="col-sm-4"
                 <?php
                  if ($visita->estatus == "P") {
                      ?>
                      onclick="setConfirmacion('<?= $tituloMensaje ?>','<?= $valorEvento ?>')"
                      style="cursor: pointer"
                      <?php
                  }
                      ?>
                 >
                <div class="card bg-<?=$visita->estatusColor ?> text-white widget-visitor-card">
                    <div class="card-block-small text-center">
                        <h2><?=$visita->estatusEvento ?></h2>
                        <h6>Estatus del Evento</h6>
                        <i class="icofont icofont-ui-alarm"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xl-12">
                <div class="card client-description">
                    <div class="card-block">
                        <div class="row p-t-10">
                            <div class="col-sm-4 text-center">
                                <div class="row  " style="text-align: center">
                                    <table width="100%">
                                        <tr style="height: 40px;">

                                            <td align="center">
                                              <div class=" center" style="width: 200px; border: solid 1px gray; border-radius: 8px;
                                               margin-top: 25px;
                                               box-shadow: 5px 10px 18px #888888; height: 190px;">
                                                  <div class="col-12 bg-danger" style="height: 45px; border-radius-left-top: 8px; padding-top: 10px;  ">
                                                      <?php
                                                      $mes=date("m",strtotime($visita->fechaVisita));
                                                      $mes=$mes-1;
                                                      ?>
                                                      <strong style="font-size: 16px;"><?= $arrayMeses[$mes] ?> <?= date("Y",strtotime($visita->fechaVisita)) ?></strong>

                                                  </div>

                                                  <div  style="height: 78px;" >
                                                      <span style="font-size: 80px;">
                                                          <?= date("d",strtotime($visita->fechaVisita)) ?>
                                                      </span>

                                                  </div>

                                                  <span style="font-size: 14px; "><br><strong class="text-danger"><?=$diasComplero[date("N",strtotime($visita->fechaVisita)) -1] ?></strong></span>



                                              </div>
                                            </td>
                                        </tr>
                                    </table>


                                </div>

                                <div class="mt-5">
                                    <a href="<?= CController::createUrl('clientevisita/index') ?>" >
                                        <button class="btn btn-warning m-l-5 m-b-10"><i class="fas fa-arrow-left text-white"></i> Regresar</button>
                                    </a>
                                    <?php

                                    if ($visita->estatus == "P") { ?>

                                        <a href="<?= CController::createUrl('clientevisita/createConIdVisita', array('id' => $visita->id)) ?>">
                                            <button class="btn btn-primary m-r-5 m-b-10"><i class="fas fa-calendar-day text-white"></i> Reagendar</button>
                                        </a>

                                    <?php } ?>
                                </div>



                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-6">
                                        <h4><?= ucwords(mb_strtolower($visita->nombre)) ?></h4>
                                        <span class="text-success">Tipo: <?=ucfirst(mb_strtolower($visita->tipoVisita)) ?></span>
                                        <p class="m-b-5 m-t-20"><span class="f-w-900">Folio: </span><?= $visita->id ?></p>
                                        <p class="m-b-5"><span class="f-w-900">Dirección :</span>
                                            <?= $visita->calle ?> No. <?= $visita->numExt ?> <?=!empty($visita->numInt)?", Int. ".$visita->numInt:"" ?>
                                        </p>
                                        <p class="m-b-5"><span class="f-w-900">Teléfono :</span> <?=getTextPhone($visita->telefono) ?></p>
                                        <?php
                                        if(isset($visita->fechaAtencion)){
                                            ?>
                                        <p class="m-b-5"><span class="f-w-900">Atención: </span><span style="font-weight: normal"><?= $visita->fechaAtencion ?> | <?= $visita->usuarioAtencion ?></span></p>
                                        <?php
                                        }
                                        ?>

                                        <span class="task-card-img d-block mt-4">
                                                <a href="#!">
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/Empleados/<?=$visita->foto ?>"
                                                     data-toggle="tooltip" title="" alt="" class="img-40" data-original-title="<?=ucwords(mb_strtolower($visita->empleado)) ?>">
                                            </a>
                                            <?php
                                            foreach ($invitados as $invitado) {
                                                ?>
                                                <a href="#!"
                                                   <?php
                                                   if ($visita->estatus == "P") {
                                                       ?>
                                                       onclick="quitarInvitado('<?=ucwords(mb_strtolower($invitado->nombre)) ?>','<?=$invitado->idInvitado ?>')"
                                                        <?php
                                                   }
                                                   ?>

                                                >
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/Empleados/<?=$invitado->foto ?>"
                                                        data-toggle="tooltip" title="" alt="" class="img-40" style="margin-left: 0px;" data-original-title="<?=ucwords(mb_strtolower($invitado->nombre)) ?>">
                                                </a>
                                            <?php
                                            }
                                            if ($visita->estatus == "P") {
                                                ?>
                                                <button class="img-40 btn btn-info" data-toggle="modal" data-target="#nuevoInvitado"><i class="fas fa-plus" style="margin-left: -6px"></i></button>
                                            <?php
                                            }
                                            ?>



                                        </span>

                                    </div>
                                    <div class="col-6" style="padding-right: 25px; padding-top: 5px;">
                                        <div class=""
                                             style="background-color: #fdfdeb; padding: 20px; min-height: 250px;
                                              box-shadow: 5px 5px 15px #ccc9c9 ;
                                             border: dashed 2px darkgray">
                                            <h4 class="text-warning"><i class="fas fa-clipboard-list mr-2"></i> Objetivo del Evento</h4>
                                            <hr>
                                            <?=ucfirst(mb_strtolower($visita->observacion))?>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-t-default m-t-30 p-r-0 p-l-0 p-t-30">
                                    <div class="row">
                                        <div class="col-4 col-md-4">
                                            <div class="founder-block">
                                                <p class="text-muted m-b-5 "><i class="fas fa-clock-o text-primary" style="font-size: 18px; "></i> Visita</p>
                                                <span class="f-18"><?= date("H:i",strtotime($visita->fechaVisita)) ?> Hrs.</span>
                                            </div>
                                        </div>
                                        <div class="col-4 col-md-4">
                                            <div class="founder-block">
                                                <p class="text-muted m-b-5 f-14"><i class="fas fa-check-double text-success" style="font-size: 18px; "></i>Estatus</p>
                                                <?php
                                                $color="";
                                                $texto="";
                                                if($visita->estatus == "P"){
                                                    $color="warning";
                                                    $texto="Pendiente";

                                                }elseif($visita->estatus == "A"){
                                                    $color="success";
                                                    $texto="Atendido";

                                                }else{
                                                    $color="danger";
                                                    $texto="Reagendado";

                                                }
                                                ?>
                                                <span class="f-16 text-<?=$color ?>">
                                                      <strong><?=$texto ?></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-4 col-md-4">
                                            <div class="founder-block">
                                                <p class="text-muted m-b-5 f-14"><i class="fas fa-user-friends text-danger" style="font-size: 18px; "></i>Integrantes</p>
                                                <span class="f-20"><?=count($invitados)+1 ?></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-success" style="max-height: 70px; min-height: 70px; height: 70px;">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="card-title text-white" style="font-size: 16px;">
                                    <i class="fas fa-comment m-r-10"></i> Seguimientos</h5>
                                <?php if ($visita->estatus == "P") { ?>
                                    <button class="btn btn-sm btn-success btn-out-dashed float-right"
                                            data-toggle="modal" data-target="#registrarSeguimiento" type="button" style="margin-top: -6px;">AGREGAR</button>

                                    <?php
                                }
                                ?>


                            </div>

                        </div>

                    </div>

                    <div class="card-content" aria-expanded="true">
                        <div class="card-body">
                            <div class="row ">
                                <div class="col-12 col-md-12">
                                    <div class="card-content">
                                        <div class="card-block mt-1" style="min-height: 350px; max-height: 350px; overflow-y: auto; overflow-x: hidden  ">

                                            <?php if(!empty($seguimientos)) {


                                                foreach ($seguimientos as $key => $value) {
                                                    ?>
                                                    <div class="card-comment ">
                                                        <div class="card-block-small">
                                                            <img class="img-radius img-50"
                                                                 src="<?php echo Yii::app()->request->baseUrl; ?>/images/Empleados/woman.jpg"
                                                                 alt="user-1">
                                                            <div class="comment-desc">
                                                                <h6><?= $value->usuario ?> <small>(<?= $value->fechaAlta ?>
                                                                        )</small></h6>
                                                                <p style="color: #0b9c8f; "><?= ucfirst(mb_strtolower($value->descripcion)) ?></p>


                                                            </div>
                                                        </div>
                                                    </div>


                                                    <?php
                                                }
                                            }else{
                                                ?>
                                                <div class="card-content" style=" overflow-y: auto">

                                                    <div class="content-box " style=" color: gray;  text-align: center; ;">
                                                        <!-- Your text -->
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/styles/img/sad.png">
                                                        <h1 class="text-muted mt-5">Opss! No hay nada que mostrar.</h1>

                                                        <p>Por favor, ingresa algún comentario para<br>visualizar su contenido.
                                                        </p>
                                                    </div>

                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <?php if ($visita->estatus == "P") { ?>
        <button class="btn btn-danger btn-lg waves-effect col-lg-12" data-toggle="modal" data-target="#cierreVisita"><i class="fas fa-check-double"></i> TERMINAR VISITA</button>

    <?php } ?>
    <!-- Page-body end -->
</div>


<div class="modal fade " id="cierreVisita" tabindex="-1" role="dialog" style="z-index: 1050;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">
                        Finalización de la Visita
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'termino-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-body ">
                <?php
                if (!empty($updateVisita->errors)) {
                    ?>
                    <div class="alert alert-danger icons-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="icofont icofont-close-line-circled"></i>
                        </button>
                        <p><strong>Atención!</strong> <?= $form->errorSummary($updateVisita) ?></p>
                    </div>

                    <?php
                }
                ?>


                <div class="row " style="padding: 13px;">
                    <div class="col-lg-12">
                        <p><strong>¿Deseas registrar la visita como finalizada el <?=date("Y-m-d") ?> a las <?=date("h:i:s") ?>?</strong><br><br> No se podrá realizar ningún cambio o agregado a la visita. El evento será considerado como completado.</p>


                        <div class="row" style="display: none">
                            <div class="col-lg-6 col-md-12 m-b-15">
                                <label class="text-muted" >Fecha de Atención</label>
                                <?php
                                if($visita->fechaAtencion==""){
                                    $visita->fechaAtencion=date("Y-m-d");
                                }
                                echo $form->dateField($updateVisita, 'fechaAtencion', array('class' => "form-control", 'placeholder' => "Fecha de Atención")); ?>
                            </div>
                            <div class="col-lg-6 col-md-12 m-b-15">
                                <label class="text-muted" style="font-size: 12px">Hora de Atención</label>
                                <?php
                               ?>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-lg-12 col-md-12 m-b-15">
                                <label class="text-muted" >Tono de finalización</label>
                                <?php
                                $tonos = CHtml::listData(Cattono::model()->findAll(), 'id', 'descripcion');
                                echo $form->dropDownList($updateVisita, 'idCatTono', $tonos, array('class' => "form-control","required" => "required", 'placeholder' => "-- Selecciona --", 'empty' => "-- Selecciona --"));
                                ?>

                            </div>

                        </div>

                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal"><i class="fas fa-times mr-2" aria-hidden="true"></i> CANCELAR</button>
                <button class="btn btn-sm btn-danger sendForm" type="submit"  ><i class="fas fa-check mr-2" aria-hidden="true"></i> ACEPTAR</button>

            </div>

        <?php $this->endWidget(); ?>

    </div>
</div>
</div>

<div class="modal fade " id="nuevoInvitado" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h4 class="modal-title">
                    Nuevo Invitado
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'invitado-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-body " style="position: static; ">
                <?php
                if (!empty($nuevoInvitado->errors)) {
                    ?>
                    <div class="alert alert-danger icons-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="icofont icofont-close-line-circled"></i>
                        </button>
                        <p><strong>Atención!</strong> <?= $form->errorSummary($nuevoInvitado) ?></p>
                    </div>

                    <?php
                }
                ?>

                <div class="row " style="padding: 13px;">
                    <div class="col-lg-12">
                        <p>Ingresa toda la información solicitada para realizar el seguimiento a la visita.</p>

                        <div class="col-lg-12 col-md-12 m-b-15">
                            <label class="text-muted" >Empleado</label>
                            <?php echo $form->dropDownList($nuevoInvitado, 'idEmpleado', $empleados, array('class' => "js-example-basic-single col-sm-12",
                                'empty' => "-- Selecciona una opción -- ")); ?>
                        </div>
                    </div>
                </div>

            </div>

            <?php if ($visita->estatus == "P") { ?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal"><i class="fas fa-times mr-2" aria-hidden="true"></i> CERRAR</button>
                    <button class="btn btn-sm btn-info waves-effect sendForm" type="submit"  ><i class="fas fa-save"></i> REGISTRAR</button>

                </div>
            <?php } ?>

            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>

<div class="modal fade " id="registrarSeguimiento">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title">
                        Nuevo Seguimiento
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="modal-body ">


                <div class="row " style="padding: 13px;">
                    <div class="col-lg-12">
                        <p>Ingresa toda la información solicitada para realizar el seguimiento a la visita.</p>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 m-b-15">
                                <label class="text-muted" >Descripción del seguimiento</label>
                                <?php echo $form->textArea($seguimiento, 'descripcion', array('class' => "form-control", "required" => "required",'placeholder' => "Ingresa la descripción completa",'rows'=>6)); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        <?php if ($visita->estatus == "P") { ?>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default waves-effect " data-dismiss="modal"><i class="fas fa-times mr-2" aria-hidden="true"></i> CERRAR</button>
                <button class="btn btn-sm btn-success waves-effect" type="submit"  ><i class="fas fa-save"></i> REGISTRAR</button>

            </div>
        <?php } ?>

        <?php $this->endWidget(); ?>

    </div>
</div>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'quitarEmpleado-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>
    <input id="empleado_id" name="empleado_id" value="23">
    <?php $this->endWidget(); ?>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'confirmar_form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>
    <input id="evento_confirma" name="evento_confirma" value="">
    <?php $this->endWidget(); ?>
</div>

<script>
    $(document).ready(function () {
        <?php
        if (!empty($nuevoInvitado->errors)) {
            ?>
        $("#nuevoInvitado").modal("show");
        <?php
        }
        ?>

        <?php
        if (!empty($updateVisita->errors)) {
        ?>
        $("#cierreVisita").modal("show");
        <?php
        }
        ?>
        // Single Search Select
        $("#Clientevisitainvitado_idEmpleado").select2({
            dropdownParent: $('#nuevoInvitado')
            }
        );


    })

    function quitarInvitado(empleado,id) {
        swal({
                title: "¿Eliminar invitado?",
                text: "Eliminaras a " + empleado + " del evento.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, adelante!",
                closeOnConfirm: false
            },
            function(){
                $("#empleado_id").val(id);
                $("#quitarEmpleado-form").submit();

            });
    }

    function setConfirmacion(title,valor) {
        swal({
                title: title,
                text: "Esta acción será definitiva y se notificará a todos los involucrados.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Si, por supuesto!",
                closeOnConfirm: false
            },
            function(){
                $("#evento_confirma").val(valor);
                $("#confirmar_form").submit();

            });
    }

</script>