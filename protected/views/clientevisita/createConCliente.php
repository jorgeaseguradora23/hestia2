<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow b {
        border-color: #fff transparent transparent transparent;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 10px;
        right: 15px;
    }
    .select2-container {
        width: 100% !important;
    }
    .select2-container--default .select2-selection--single {
        color: #fff;
        height: auto;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        background-color: white ;
        color: #535251;
        height: 36px;
        padding: 4px 30px 8px 20px;
    }
</style>
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-ui-user bg-warning"></i>

                    <div class="d-inline">
                        <h4><?= getTxtCte($cliente->id) ?> - <?= ucwords(mb_strtolower($cliente->nombre)) ?> (Nueva Visita)</h4>
                        <span>Ingrese toda la información de los campos requeridos para registrar la cotización.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Visitas</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Nueva Visita</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->


    <!-- Page-body start -->
    <div class="page-body">

        <div class="row">
            <div class="col-lg-12">


                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>
                <div class="card">
                    <div class="card-header bg-warning">
                        <h5 class="text-white">Información General</h5>
                        <div class="card-header-right" style="margin-right: 10px;">
                            <a href="<?= CController::createUrl('clientevisita/index') ?>"><button class="btn btn-sm btn-warning btn-out-dashed" type="button">SALIR</button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php
                        if (!empty($visita->errors)) {
                            ?>
                            <div class="alert alert-danger icons-alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled"></i>
                                </button>
                                <p><strong>Atención!</strong> <?= $form->errorSummary($visita) ?></p>
                            </div>

                            <?php
                        }
                        ?>
                        <div style="padding-left: 30px; padding-right: 30px;padding-top: 20px;padding-bottom: 10px;">
                            <div class="row">
                                <div class="col-lg-3 col-md-12 m-b-15" >
                                    <label class="text-muted" >Fecha de Visita</label>
                                    <?php
                                    if($visita->fechaVisita==""){
                                        $visita->fechaVisita=date("Y-m-d");
                                    }
                                    echo $form->dateField($visita, 'fechaVisita', array('class' => "form-control",
                                        'placeholder' => "Fecha Visita",
                                        'value' => $visita->fechaVisita,
                                        'title' => "Fecha Visita")); ?>
                                </div>
                                <div class="col-lg-3 col-md-12 m-b-15">
                                    <label class="text-muted" >Hora de Visita</label>
                                    <?php
                                    if($visita->_horaVisita==""){
                                        $visita->_horaVisita=date("H:i");
                                    }

                                    echo $form->timeField($visita, '_horaVisita', array('class' => "form-control", 'placeholder' => "Hora Visita", 'title' => "Hora Visita",'min'=>"8:00", 'max'=>"18:00")); ?>
                                </div>
                                <div class="col-lg-6 col-md-12 m-b-15">
                                    <label class="text-muted" >Empleado</label>
                                    <?php echo $form->dropDownList($visita, 'idEmpleado', $empleados, array('class' => "js-example-basic-single col-sm-12",
                                        'empty' => "-- Selecciona una opción -- ")); ?>
                                </div>
                                <div class="col-lg-4 col-md-12 m-b-15">
                                    <label class="text-muted" >Tipo de Visita</label>
                                    <?php
                                    $tipoVisita = CHtml::listData(Cattipovisita::model()->findAll(), 'id', 'descripcion');
                                    echo $form->dropDownList($visita, 'idCatTipoVisita', $tipoVisita, array('class' => "form-control", 'empty' => "Tipo de Visita"));
                                    ?>
                                </div>
                                <div class="col-lg-4 col-md-12 m-b-15">
                                    <label class="text-muted" >Estatus de la Visita</label>
                                    <?php
                                    echo $form->dropDownList($visita, 'confirmado', array("S" => "CONFIRMADO","N" => "POR CONFIRMAR"), array('class' => "form-control", 'empty' => "-- Selecciona una opción --"));
                                    ?>
                                </div>
                                <div class="col-lg-4 col-md-12 m-b-15">
                                    <label class="text-muted" >Tipo de Registro</label>
                                    <?php
                                    echo $form->dropDownList($visita, 'tipo', array("P" => "PROACTIVO","E" => "EN RESPUESTA"), array('class' => "form-control", 'empty' => "-- Selecciona una opción --"));
                                    ?>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-12 col-md-12 m-b-15">
                                    <label class="text-muted" >Observaciones, Comentarios o Adjuntos</label>
                                    <?php echo $form->textArea($visita, 'observacion', array('class' => "form-control", 'rows' => "3", 'placeholder' => "Observación", 'title' => "Observación")); ?>
                                </div>
                            </div>

                            <div class="row float-right mt-2">
                                <div class="col-lg-12 col-md-12 m-b-15">
                                    <button class="btn btn-sm btn-warning sendForm" type="submit"  ><i class="fas fa-save mr-2"></i> Guardar Visita</button>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>



                <?php $this->endWidget(); ?>
                <!-- tab content end -->
            </div>
        </div>

    </div>
    <!-- Page-body end -->
</div>
<script>
    $(document).ready(function(){
        // Single Search Select
        $(".js-example-basic-single").select2();
    });
</script>