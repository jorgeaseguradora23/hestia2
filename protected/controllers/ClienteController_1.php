<?php

class ClienteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'create', 'update','getCP','AgregarAgencia','GetCPFiscal','agregarFiscal','AgregarContacto','agregarProducto'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $cliente=$this->loadModel($id);
        $this->render('view', array(
            'cliente' => $cliente,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Cliente;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $colonias = array();
        $agencias= array();
        $fiscales= array();
        $contactos= array();
        $productos= array();

        if (isset($_POST['Cliente'])) {
            if(isset($_POST['agenciaNombre']) && !empty($_POST['agenciaNombre'])){
                foreach($_POST['agenciaNombre'] as $key=>$agenciaNombre){
                    $agencias[$key]['agenciaNombre']=$agenciaNombre;
                    $agencias[$key]['agenciaRFC']=$_POST['agenciaRFC'][$key];
                    $agencias[$key]['agenciaObservacion']=$_POST['agenciaObservacion'][$key];
                }
            }
            if(isset($_POST['fiscalNombre']) && !empty($_POST['fiscalNombre'])){
                foreach($_POST['fiscalNombre'] as $key=>$fiscalNombre){
                    $fiscales[$key]['fiscalNombre']=$fiscalNombre;
                    $fiscales[$key]['fiscalRFC']=$_POST['fiscalRFC'][$key];
                    $fiscales[$key]['fiscalCalle']=$_POST['fiscalCalle'][$key];
                    $fiscales[$key]['fiscalNoExterior']=$_POST['fiscalNoExterior'][$key];
                    $fiscales[$key]['fiscalNoInterior']=$_POST['fiscalNoInterior'][$key];
                    $fiscales[$key]['fiscalCp']=$_POST['fiscalCp'][$key];
                    $fiscales[$key]['fiscalColonia']=$_POST['fiscalColonia'][$key];
                    $fiscales[$key]['fiscalMunicipio']=$_POST['fiscalMunicipio'][$key];
                    $fiscales[$key]['fiscalCiudad']=$_POST['fiscalCiudad'][$key];
                    $fiscales[$key]['fiscalEstado']=$_POST['fiscalEstado'][$key];
                    $fiscales[$key]['fiscalCiudad']=$_POST['fiscalCiudad'][$key];
                    $fiscales[$key]['fiscalPais']=$_POST['fiscalPais'][$key];
                    $fiscales[$key]['fiscalUsoCfdi']=$_POST['fiscalUsoCfdi'][$key];
                    $fiscales[$key]['fiscalMetodoPago']=$_POST['fiscalMetodoPago'][$key];
                    $fiscales[$key]['fiscalFormaPago']=$_POST['fiscalUsoCfdi'][$key];
                    $fiscales[$key]['fiscalEmail']=$_POST['fiscalEmail'][$key];
                }
            }
            if(isset($_POST['contactoNombre']) && !empty($_POST['contactoNombre'])){
                foreach($_POST['contactoNombre'] as $key=>$contactoNombre){
                    $contactos[$key]['contactoNombre']=$contactoNombre;
                    $contactos[$key]['contactoEmail']=$_POST['contactoEmail'][$key];
                    $contactos[$key]['contactoTelefono']=$_POST['contactoTelefono'][$key];
                }
            }
            if(isset($_POST['productoProducto']) && !empty($_POST['productoProducto'])){
                foreach($_POST['productoProducto'] as $key=>$productoProducto){
                    $productos[$key]['productoProducto']=$productoProducto;
                    $productos[$key]['productoTipo']=$_POST['productoTipo'][$key];
                    $productos[$key]['productoCantidad']=$_POST['productoCantidad'][$key];
                    $productos[$key]['productoImporte']=$_POST['productoImporte'][$key];
                    $productos[$key]['productoFechaInicio']=$_POST['productoFechaInicio'][$key];
                    $productos[$key]['productoFechaFin']=$_POST['productoFechaFin'][$key];
                }
            }
            if(isset($_POST['terminar'])){
                $model->attributes = $_POST['Cliente'];
                if($model->isNewRecord){
                    $model->fechaAlta=date('Y-m-d H:i:s');
                    $model->usuario=Yii::app()->user->nombre_usuario;
                    $model->estatus='A';
                    $model->categoria='P';
                }
                if ($model->save()){
                    if(!empty($agencias)){
                        foreach($agencias as $agencia){
                            $agenciaGuardar= new Clienteagencia;
                            $agenciaGuardar->idCliente= $model->id;
                            $agenciaGuardar->razonSocial= $agencia['agenciaNombre'];
                            $agenciaGuardar->rfc= $agencia['agenciaRFC'];
                            $agenciaGuardar->observacion= $agencia['agenciaObservacion'];
                            $agenciaGuardar->fechaAlta=date('Y-m-d H:i:s');
                            $agenciaGuardar->usuario=Yii::app()->user->nombre_usuario;
                            $agenciaGuardar->estatus='A';
                            if($agenciaGuardar->save()){
                                /*echo 'agenciaGuardada';
                            }else{
                                var_dump($agenciaGuardar->errors);die;*/
                            }
                        }
                    }
                    if(!empty($fiscales)){
                        foreach($fiscales as $fiscal){
                            $fiscalGuardar= new Clientedatosfacturacion;
                            $fiscalGuardar->idCliente= $model->id;
                            $fiscalGuardar->razonSocial= $fiscal['fiscalNombre'];
                            $fiscalGuardar->rfc= $fiscal['fiscalRFC'];
                            $fiscalGuardar->calle= $fiscal['fiscalCalle'];
                            $fiscalGuardar->numExt= $fiscal['fiscalNoExterior'];
                            $fiscalGuardar->numInt= $fiscal['fiscalNoInterior'];
                            $fiscalGuardar->cp= $fiscal['fiscalCp'];
                            $fiscalGuardar->colonia= $fiscal['fiscalColonia'];
                            $fiscalGuardar->municipio= $fiscal['fiscalMunicipio'];
                            $fiscalGuardar->ciudad= $fiscal['fiscalCiudad'];
                            $fiscalGuardar->estado= $fiscal['fiscalEstado'];
                            $fiscalGuardar->pais= $fiscal['fiscalPais'];
                            $fiscalGuardar->idCatUsoCFDI= $fiscal['fiscalUsoCfdi'];
                            $fiscalGuardar->idCatMetodoPago= $fiscal['fiscalMetodoPago'];
                            $fiscalGuardar->idCatFormaPago= $fiscal['fiscalFormaPago'];
                            $fiscalGuardar->email= $fiscal['fiscalEmail'];
                            if($fiscalGuardar->save()){
                                /*echo 'fiscalGuardada';
                            }else{
                                var_dump($fiscalGuardar->errors);die;*/
                            }
                        }
                    }
                    if(!empty($contactos)){
                        foreach($contactos as $contacto){
                            $contactoGuardar= new Clientecontacto;
                            $contactoGuardar->idCliente= $model->id;
                            $contactoGuardar->nombreContacto= $contacto['contactoNombre'];
                            $contactoGuardar->telefono= $contacto['contactoEmail'];
                            $contactoGuardar->eMail= $contacto['contactoTelefono'];
                            if($contactoGuardar->save()){
                                /*echo 'contactoGuardada';
                            }else{
                                var_dump($contactoGuardar->errors);die;*/
                            }
                        }
                    }
                    if(!empty($productos)){
                        foreach($productos as $producto){
                            $productoGuardar= new Clienteproductoservicio;
                            $productoGuardar->idCliente= $model->id;
                            $productoGuardar->idProductoServicio= $producto['productoProducto'];
                            $productoGuardar->idCatTipoServicio= $producto['productoTipo'];
                            $productoGuardar->cantidad= $producto['productoCantidad'];
                            $productoGuardar->precio= $producto['productoImporte'];
                            $productoGuardar->fechaInicio= $producto['productoFechaInicio'];
                            $productoGuardar->fechaFinal= $producto['productoFechaFin'];
                            $productoGuardar->estatus= 'A';
                            if($productoGuardar->save()){
                                /*echo 'productoGuardada';
                            }else{
                                var_dump($productoGuardar->errors);die;*/
                            }
                        }
                    }
                    //echo 'guardado';die;
                    $this->redirect(array('view', 'id' => $model->id));
                }/*else{
                    var_dump($model->errors);die;
                }*/
            }
        }

        $this->render('create', array(
            'model' => $model,
            'colonias' => $colonias,
            'agencias' => $agencias,
            'fiscales' => $fiscales,
            'contactos' => $contactos,
            'productos' => $productos,
        ));
    }
    
    public function actionAgregarFiscal() {
        $nombre= $_POST['Cliente']['fiscalNombre'];
        $rfc= $_POST['Cliente']['fiscalRFC'];
        $calle= $_POST['Cliente']['fiscalCalle'];
        $noExterior= $_POST['Cliente']['fiscalNoExterior'];
        $noInterior= $_POST['Cliente']['fiscalNoInterior'];
        $cp= $_POST['Cliente']['fiscalCp'];
        $colonia= $_POST['Cliente']['fiscalColonia'];
        $municipio= $_POST['Cliente']['fiscalMunicipio'];
        $ciudad= $_POST['Cliente']['fiscalCiudad'];
        $estado= $_POST['Cliente']['fiscalEstado'];
        $pais= $_POST['Cliente']['fiscalPais'];
        $usoCfdi= $_POST['Cliente']['fiscalUsoCfdi'];
        $metodoPago= $_POST['Cliente']['fiscalMetodoPago'];
        $formaPago= $_POST['Cliente']['fiscalFormaPago'];
        $email= $_POST['Cliente']['fiscalEmail'];
        if(!empty($nombre) && !empty($rfc) && !empty($calle)){
            echo CJSON::encode(array('status' => 'correcto',
                "nombre"=>$nombre,
                "rfc"=>$rfc,
                "calle"=>$calle,
                "noExterior"=>$noExterior,
                "noInterior"=>$noInterior,
                "cp"=>$cp,
                "colonia"=>$colonia,
                "municipio"=>$municipio,
                "ciudad"=>$ciudad,
                "estado"=>$estado,
                "pais"=>$pais,
                "usoCfdi"=>$usoCfdi,
                "metodoPago"=>$metodoPago,
                "formaPago"=>$formaPago,
                "email"=>$email,
                    ));
        }else{
            echo CJSON::encode(array('status' => 'incorrecto'));
        }
    }
    public function actionAgregarContacto() {
        $nombre= $_POST['Cliente']['contactoNombre'];
        $email= $_POST['Cliente']['contactoEmail'];
        $telefono= $_POST['Cliente']['contactoTelefono'];
        if(!empty($nombre) && !empty($email) && !empty($telefono)){
            echo CJSON::encode(array('status' => 'correcto',
                "nombre"=>$nombre,
                "email"=>$email,
                "telefono"=>$telefono,
                    ));
        }else{
            echo CJSON::encode(array('status' => 'incorrecto'));
        }
    }
    public function actionAgregarProducto() {
        $producto= $_POST['Cliente']['productoProducto'];
        $tipo= $_POST['Cliente']['productoTipo'];
        $cantidad= $_POST['Cliente']['productoCantidad'];
        $importe= $_POST['Cliente']['productoImporte'];
        $fechaInicio= $_POST['Cliente']['productoFechaInicio'];
        $fechaFin= $_POST['Cliente']['productoFechaFin'];
        if(
                !empty($producto) &&
                !empty($tipo) &&
                !empty($cantidad) &&
                !empty($importe) &&
                !empty($fechaInicio) &&
                !empty($fechaFin)
                ){
            echo CJSON::encode(array('status' => 'correcto',
                "producto"=>$producto,
                "tipo"=>$tipo,
                "cantidad"=>$cantidad,
                "importe"=>$importe,
                "fechaInicio"=>$fechaInicio,
                "fechaFin"=>$fechaFin,
                    ));
        }else{
            echo CJSON::encode(array('status' => 'incorrecto'));
        }
    }
    public function actionAgregarAgencia() {
        $nombre= $_POST['Cliente']['agenciaNombre'];
        $rfc= $_POST['Cliente']['agenciaRFC'];
        $observacion= $_POST['Cliente']['agenciaObservacion'];
        if(!empty($nombre) && !empty($rfc) && !empty($observacion)){
            echo CJSON::encode(array('status' => 'correcto',"nombre"=>$nombre,"rfc"=>$rfc,"observacion"=>$observacion));
        }else{
            echo CJSON::encode(array('status' => 'incorrecto'));
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $colonias = array();
        $agencias=array();
        $agenciasModel= Clienteagencia::model()->findAll('idCliente=:idCliente', array(':idCliente'=>$model->id));
        if(!empty($agenciasModel)){
            foreach($agenciasModel as $key=>$agenciaModel){
                $agencias[$key]['agenciaNombre']=$agenciaModel->razonSocial;
                $agencias[$key]['agenciaRFC']=$agenciaModel->rfc;
                $agencias[$key]['agenciaObservacion']=$agenciaModel->observacion;
            }
        }
        $fiscales=array();
        $fiscalesModel= Clientedatosfacturacion::model()->findAll('idCliente=:idCliente', array(':idCliente'=>$model->id));
        if(!empty($fiscalesModel)){
            foreach($fiscalesModel as $key=>$fiscalModel){
                $fiscales[$key]['fiscalNombre']=$fiscalModel->razonSocial;
                $fiscales[$key]['fiscalRFC']=$fiscalModel->rfc;
                $fiscales[$key]['fiscalCalle']=$fiscalModel->calle;
                $fiscales[$key]['fiscalNoExterior']=$fiscalModel->numExt;
                $fiscales[$key]['fiscalNoInterior']=$fiscalModel->numInt;
                $fiscales[$key]['fiscalCp']=$fiscalModel->cp;
                $fiscales[$key]['fiscalColonia']=$fiscalModel->colonia;
                $fiscales[$key]['fiscalMunicipio']=$fiscalModel->municipio;
                $fiscales[$key]['fiscalCiudad']=$fiscalModel->ciudad;
                $fiscales[$key]['fiscalEstado']=$fiscalModel->estado;
                $fiscales[$key]['fiscalPais']=$fiscalModel->pais;
                $fiscales[$key]['fiscalUsoCfdi']=$fiscalModel->idCatUsoCFDI;
                $fiscales[$key]['fiscalMetodoPago']=$fiscalModel->idCatMetodoPago;
                $fiscales[$key]['fiscalFormaPago']=$fiscalModel->idCatFormaPago;
                $fiscales[$key]['fiscalEmail']=$fiscalModel->email;
            }
        }
        $contactos=array();
        $contactosModel= Clientecontacto::model()->findAll('idCliente=:idCliente', array(':idCliente'=>$model->id));
        if(!empty($contactosModel)){
            foreach($contactosModel as $key=>$contactoModel){
                $contactos[$key]['contactoNombre']=$contactoModel->nombreContacto;
                $contactos[$key]['contactoEmail']=$contactoModel->telefono;
                $contactos[$key]['contactoTelefono']=$contactoModel->eMail;
            }
        }
        $productos=array();
        $productosModel= Clienteproductoservicio::model()->findAll('idCliente=:idCliente', array(':idCliente'=>$model->id));
        if(!empty($productosModel)){
            foreach($productosModel as $key=>$productoModel){
                $productos[$key]['productoProducto']=$productoModel->idProductoServicio;
                $productos[$key]['productoTipo']=$productoModel->idCatTipoServicio;
                $productos[$key]['productoCantidad']=$productoModel->cantidad;
                $productos[$key]['productoImporte']=$productoModel->precio;
                $productos[$key]['productoFechaInicio']=$productoModel->fechaInicio;
                $productos[$key]['productoFechaFin']=$productoModel->fechaFinal;
            }
        }
        if (isset($_POST['Cliente'])) {
            $agencias=array();
            $fiscales=array();
            $contactos=array();
            $productos=array();
            if(isset($_POST['agenciaNombre']) && !empty($_POST['agenciaNombre'])){
                foreach($_POST['agenciaNombre'] as $key=>$agenciaNombre){
                    $agencias[$key]['agenciaNombre']=$agenciaNombre;
                    $agencias[$key]['agenciaRFC']=$_POST['agenciaRFC'][$key];
                    $agencias[$key]['agenciaObservacion']=$_POST['agenciaObservacion'][$key];
                }
            }
            if(isset($_POST['fiscalNombre']) && !empty($_POST['fiscalNombre'])){
                foreach($_POST['fiscalNombre'] as $key=>$fiscalNombre){
                    $fiscales[$key]['fiscalNombre']=$fiscalNombre;
                    $fiscales[$key]['fiscalRFC']=$_POST['fiscalRFC'][$key];
                    $fiscales[$key]['fiscalCalle']=$_POST['fiscalCalle'][$key];
                    $fiscales[$key]['fiscalNoExterior']=$_POST['fiscalNoExterior'][$key];
                    $fiscales[$key]['fiscalNoInterior']=$_POST['fiscalNoInterior'][$key];
                    $fiscales[$key]['fiscalCp']=$_POST['fiscalCp'][$key];
                    $fiscales[$key]['fiscalColonia']=$_POST['fiscalColonia'][$key];
                    $fiscales[$key]['fiscalMunicipio']=$_POST['fiscalMunicipio'][$key];
                    $fiscales[$key]['fiscalCiudad']=$_POST['fiscalCiudad'][$key];
                    $fiscales[$key]['fiscalEstado']=$_POST['fiscalEstado'][$key];
                    $fiscales[$key]['fiscalCiudad']=$_POST['fiscalCiudad'][$key];
                    $fiscales[$key]['fiscalPais']=$_POST['fiscalPais'][$key];
                    $fiscales[$key]['fiscalUsoCfdi']=$_POST['fiscalUsoCfdi'][$key];
                    $fiscales[$key]['fiscalMetodoPago']=$_POST['fiscalMetodoPago'][$key];
                    $fiscales[$key]['fiscalFormaPago']=$_POST['fiscalUsoCfdi'][$key];
                    $fiscales[$key]['fiscalEmail']=$_POST['fiscalEmail'][$key];
                }
            }
            if(isset($_POST['contactoNombre']) && !empty($_POST['contactoNombre'])){
                foreach($_POST['contactoNombre'] as $key=>$contactoNombre){
                    $contactos[$key]['contactoNombre']=$contactoNombre;
                    $contactos[$key]['contactoEmail']=$_POST['contactoEmail'][$key];
                    $contactos[$key]['contactoTelefono']=$_POST['contactoTelefono'][$key];
                }
            }
            if(isset($_POST['productoProducto']) && !empty($_POST['productoProducto'])){
                foreach($_POST['productoProducto'] as $key=>$productoProducto){
                    $productos[$key]['productoProducto']=$productoProducto;
                    $productos[$key]['productoTipo']=$_POST['productoTipo'][$key];
                    $productos[$key]['productoCantidad']=$_POST['productoCantidad'][$key];
                    $productos[$key]['productoImporte']=$_POST['productoImporte'][$key];
                    $productos[$key]['productoFechaInicio']=$_POST['productoFechaInicio'][$key];
                    $productos[$key]['productoFechaFin']=$_POST['productoFechaFin'][$key];
                }
            }
            if(isset($_POST['terminar'])){
                $model->attributes = $_POST['Cliente'];
                if($model->isNewRecord){
                    $model->fechaAlta=date('Y-m-d H:i:s');
                    $model->usuario=Yii::app()->user->nombre_usuario;
                    $model->estatus='A';
                    $model->categoria='P';
                }
                if ($model->save()){
                    Clienteagencia::model()->deleteAll('idCliente=:idCliente', array(':idCliente'=>$model->id));
                    Clientedatosfacturacion::model()->deleteAll('idCliente=:idCliente', array(':idCliente'=>$model->id));
                    Clientecontacto::model()->deleteAll('idCliente=:idCliente', array(':idCliente'=>$model->id));
                    Clienteproductoservicio::model()->deleteAll('idCliente=:idCliente', array(':idCliente'=>$model->id));
                    if(!empty($agencias)){
                        foreach($agencias as $agencia){
                            $agenciaGuardar= new Clienteagencia;
                            $agenciaGuardar->idCliente= $model->id;
                            $agenciaGuardar->razonSocial= $agencia['agenciaNombre'];
                            $agenciaGuardar->rfc= $agencia['agenciaRFC'];
                            $agenciaGuardar->observacion= $agencia['agenciaObservacion'];
                            $agenciaGuardar->fechaAlta=date('Y-m-d H:i:s');
                            $agenciaGuardar->usuario=Yii::app()->user->nombre_usuario;
                            $agenciaGuardar->estatus='A';
                            if($agenciaGuardar->save()){
                                /*echo 'agenciaGuardada';
                            }else{
                                var_dump($agenciaGuardar->errors);die;*/
                            }
                        }
                    }
                    if(!empty($fiscales)){
                        foreach($fiscales as $fiscal){
                            $fiscalGuardar= new Clientedatosfacturacion;
                            $fiscalGuardar->idCliente= $model->id;
                            $fiscalGuardar->razonSocial= $fiscal['fiscalNombre'];
                            $fiscalGuardar->rfc= $fiscal['fiscalRFC'];
                            $fiscalGuardar->calle= $fiscal['fiscalCalle'];
                            $fiscalGuardar->numExt= $fiscal['fiscalNoExterior'];
                            $fiscalGuardar->numInt= $fiscal['fiscalNoInterior'];
                            $fiscalGuardar->cp= $fiscal['fiscalCp'];
                            $fiscalGuardar->colonia= $fiscal['fiscalColonia'];
                            $fiscalGuardar->municipio= $fiscal['fiscalMunicipio'];
                            $fiscalGuardar->ciudad= $fiscal['fiscalCiudad'];
                            $fiscalGuardar->estado= $fiscal['fiscalEstado'];
                            $fiscalGuardar->pais= $fiscal['fiscalPais'];
                            $fiscalGuardar->idCatUsoCFDI= $fiscal['fiscalUsoCfdi'];
                            $fiscalGuardar->idCatMetodoPago= $fiscal['fiscalMetodoPago'];
                            $fiscalGuardar->idCatFormaPago= $fiscal['fiscalFormaPago'];
                            $fiscalGuardar->email= $fiscal['fiscalEmail'];
                            if($fiscalGuardar->save()){
                                /*echo 'fiscalGuardada';
                            }else{
                                var_dump($fiscalGuardar->errors);die;*/
                            }
                        }
                    }
                    if(!empty($contactos)){
                        foreach($contactos as $contacto){
                            $contactoGuardar= new Clientecontacto;
                            $contactoGuardar->idCliente= $model->id;
                            $contactoGuardar->nombreContacto= $contacto['contactoNombre'];
                            $contactoGuardar->telefono= $contacto['contactoEmail'];
                            $contactoGuardar->eMail= $contacto['contactoTelefono'];
                            if($contactoGuardar->save()){
                                /*echo 'contactoGuardada';
                            }else{
                                var_dump($contactoGuardar->errors);die;*/
                            }
                        }
                    }
                    if(!empty($productos)){
                        foreach($productos as $producto){
                            $productoGuardar= new Clienteproductoservicio;
                            $productoGuardar->idCliente= $model->id;
                            $productoGuardar->idProductoServicio= $producto['productoProducto'];
                            $productoGuardar->idCatTipoServicio= $producto['productoTipo'];
                            $productoGuardar->cantidad= $producto['productoCantidad'];
                            $productoGuardar->precio= $producto['productoImporte'];
                            $productoGuardar->fechaInicio= $producto['productoFechaInicio'];
                            $productoGuardar->fechaFinal= $producto['productoFechaFin'];
                            $productoGuardar->estatus= 'A';
                            if($productoGuardar->save()){
                                /*echo 'productoGuardada';
                            }else{
                                var_dump($productoGuardar->errors);die;*/
                            }
                        }
                    }
                    //echo 'guardado';die;
                    $this->redirect(array('view', 'id' => $model->id));
                }/*else{
                    var_dump($model->errors);die;
                }*/
            }
        }

        $this->render('create', array(
            'model' => $model,
            'colonias' => $colonias,
            'agencias' => $agencias,
            'fiscales' => $fiscales,
            'contactos' => $contactos,
            'productos' => $productos,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $clientes = Cliente::model()->findAll();
        $this->render('index', array(
            'clientes' => $clientes,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Cliente the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Cliente::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Cliente $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cliente-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetCP() {
        if (isset($_POST['Cliente']['cp'])) {
            $cp = $_POST['Cliente']['cp'];
            $colonias = Colonia::model()->findAll('cp=:cp', array(':cp' => $cp));
            if (!empty($colonias)) {
                $estado = $colonias[0]->estado;
                $municipio = $colonias[0]->municipio;
                $ciudad = $colonias[0]->ciudad;
                $coloniasText = '';
                foreach ($colonias as $colonia) {
                    $coloniasText .= "<option value='" . ($colonia->colonia) . "'>" . ($colonia->colonia) . "</option>";
                }
                echo CJSON::encode(array('status' => 'correcto', 'colonias' => $coloniasText, 'municipio' => $municipio, 'estado' => $estado, 'ciudad' => $ciudad));
            } else {
                echo CJSON::encode(array('status' => 'incorrecto'));
            }
        } else {
            echo CJSON::encode(array('status' => 'incorrecto'));
        }
    }
    
    public function actionGetCPFiscal() {
        if (isset($_POST['Cliente']['fiscalCp'])) {
            $cp = $_POST['Cliente']['fiscalCp'];
            $colonias = Colonia::model()->findAll('cp=:cp', array(':cp' => $cp));
            if (!empty($colonias)) {
                $estado = $colonias[0]->estado;
                $municipio = $colonias[0]->municipio;
                $ciudad = $colonias[0]->ciudad;
                $coloniasText = '';
                foreach ($colonias as $colonia) {
                    $coloniasText .= "<option value='" . ($colonia->colonia) . "'>" . ($colonia->colonia) . "</option>";
                }
                echo CJSON::encode(array('status' => 'correcto', 'colonias' => $coloniasText, 'municipio' => $municipio, 'estado' => $estado, 'ciudad' => $ciudad));
            } else {
                echo CJSON::encode(array('status' => 'incorrecto'));
            }
        } else {
            echo CJSON::encode(array('status' => 'incorrecto'));
        }
    }

}
