<?php

class ClienteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','GetCodPostal','getDatosAgencia',
                    'edit',
                    'detalle','agenda'
                    ),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


    public function actionGetCodPostal(){
        if (!isset(Yii::app()->user->nombre_usuario)) {
            $this->redirect(CController::createUrl('site/login'));
        }

        $criteria=new CDbCriteria();

        if(isset($_GET["id"])){
            $criteria->compare("cp",$_GET["id"]);
        }
        $colonias=Catcolonia::model()->findAll($criteria);

        $this->renderPartial('ajax/selectColonia',array(
            'colonias' => $colonias,
        ));
    }

    public function actiongetDatosAgencia(){
        if (!isset(Yii::app()->user->nombre_usuario)) {
            $this->redirect(CController::createUrl('site/login'));
        }

        $criteria=new CDbCriteria();

        $cliente=Cliente::model()->findByPk($_GET["id"]);

        $colonia=Catcolonia::model()->findByPk($cliente->idCatColonia);

        $this->renderPartial('ajax/selectColoniaAgencia',array(
            'colonia' => $colonia,
            'cliente' => $cliente,

        ));
    }

    public function actionEdit($id)
    {
        if (!isset(Yii::app()->user->nombre_usuario)) {
            $this->redirect(CController::createUrl('site/login'));
        }
        $contactos=new Clientecontacto();
        $model=new Cliente;
        $clienteMov= new Clientemovimiento();
        $clienteDatosFacturacion=new Clientedatosfacturacion();
        $clienteColonia=new Catcolonia();
        $dfColonia=new Catcolonia();
        $criteriaColoniaCte=new CDbCriteria();
        $criteriaColoniaDf=new CDbCriteria();
        $infoGeneralCompleto=true;
        $infoContactos=false;
        $infoFacturacion=false;
        $dfAdd=array();
        $contactoAdd=array();
        $clienteColonia=new Catcolonia();

        if(!empty($_POST['Cliente'])){
            $model=Cliente::model()->findByPk($id);
            $model->attributes=$_POST['Cliente'];
            if(!empty($_POST['Cliente']["cp"])){
                if(empty($_POST['Cliente']["colonia"])){
                    if(empty($_POST['Cliente']["idCatColonia"])){
                        $model->idCatColonia=null;
                    }
                }else{
                    $model->idCatColonia=$_POST['Cliente']["colonia"];
                }
                $criteriaColoniaCte->compare("cp",$_POST['Cliente']["cp"]);
            }else{
                $criteriaColoniaCte->compare("cp","00000");
            }

        }else{
            $model=Cliente::model()->findByPk($id);

            $datoColonia=Catcolonia::model()->findByPk($model->idCatColonia);
            $model->municipio=$datoColonia->municipio;
            $model->estado=$datoColonia->estado;
            $model->pais=$datoColonia->pais;
            $model->ciudad=$datoColonia->ciudad;
            $model->cp=$datoColonia->cp;
            $criteriaColoniaCte->compare("cp",$model->cp);


        }




        /**
         * Recarga de array si ya trae datos
         */
        if (isset($_POST["dfAdd"])){
            foreach ($_POST["dfAdd"] as $itemIn) {
                $arrayExplode=explode(",",$itemIn);
                $dfAdd[]=array(
                    "id" => $arrayExplode[0],
                    "rfc" => $arrayExplode[1],
                    "idCliente" => $arrayExplode[2],
                    "razonSocial" => $arrayExplode[3],
                    "calle" => $arrayExplode[4],
                    "numExt" => $arrayExplode[5],
                    "numInt" => $arrayExplode[6],
                    "idCatColonia" => $arrayExplode[7],
                    "idCatUsoCFDI" => $arrayExplode[8],
                    "idCatMetodoPago" => $arrayExplode[9],
                    "idCatFormaPago" => $arrayExplode[10]);
            }
        }
        /**
         * Recarga de array si ya trae datos Contacto
         */
        if (isset($_POST["contactoAdd"])) {
            foreach ($_POST["contactoAdd"] as $itemIn) {
                $arrayExplode = explode(",", $itemIn);
                $contactoAdd[] = array(
                    "id" => $arrayExplode[0],
                    "nombreContacto" => $arrayExplode[1],
                    "telefono" => $arrayExplode[2],
                    "eMail" => $arrayExplode[3]
                );
            }
        }
        /**
         * Busca el código postal del cliente
         */



        $colonias = CHtml::listData(Catcolonia::model()->findAll($criteriaColoniaCte), 'id', 'colonia');


        /**
         * Busca el código postal del datos facturación
         */
        if(isset($_POST["dfColonia"])){
            $dfColonia->attributes=$_POST["dfColonia"];
        }
        if(isset($_POST["cpBuscadoDf"])){
            if(!empty($_POST['Clientedatosfacturacion'])){
                $clienteDatosFacturacion->attributes=$_POST['Clientedatosfacturacion'];
            }
            if(!empty($dfColonia->cp)){
                $criteriaColoniaDf->compare("cp",$dfColonia->cp);
                $datosColonia=Catcolonia::model()->findAll($criteriaColoniaDf);
                foreach ($datosColonia as $datoColonia){
                    $dfColonia->municipio=$datoColonia->municipio;
                    $dfColonia->estado=$datoColonia->estado;
                    $dfColonia->pais=$datoColonia->pais;
                    $dfColonia->ciudad=$datoColonia->ciudad;
                }
            }else{
                $criteriaColoniaDf->compare("cp","00000");
            }

        }
        else{
            $criteriaColoniaDf->compare("cp","00000");
        }


        /**
         * Agregado de los datos de facturacion temporales
         */

        if(isset($_POST["btnAgregarDatosFacturacion"])){
            $clienteDatosFacturacion->attributes=$_POST['Clientedatosfacturacion'];
            $clienteDatosFacturacion->idCliente=1;
            $clienteDatosFacturacion->idCatColonia=$dfColonia->colonia;
            $infoFacturacion=true;
            $infoGeneralCompleto=false;
            $infoContactos=false;
            if($clienteDatosFacturacion->validate()){
                if ( !empty($_POST["dfAdd"]) && is_array($_POST["dfAdd"]) ) {
                    $contNew=0;
                    $bolAgregaNew=true;
                    $dfAdd=array();
                    foreach ($_POST["dfAdd"] as $itemIn) {
                        $arrayExplode=explode(",",$itemIn);
                        $dfAdd[]=array(
                            "id" => $arrayExplode[0],
                            "rfc" => $arrayExplode[1],
                            "idCliente" => $arrayExplode[2],
                            "razonSocial" => $arrayExplode[3],
                            "calle" => $arrayExplode[4],
                            "numExt" => $arrayExplode[5],
                            "numInt" => $arrayExplode[6],
                            "idCatColonia" => $arrayExplode[7],
                            "idCatUsoCFDI" => $arrayExplode[8],
                            "idCatMetodoPago" => $arrayExplode[9],
                            "idCatFormaPago" => $arrayExplode[10]
                        );
                        $contNew+=1;
                        if($arrayExplode[1]==$clienteDatosFacturacion->rfc){
                            $bolAgregaNew=false;
                        }
                    }

                    if($bolAgregaNew==true){
                        $dfAdd[]=array(
                            "id" => $contNew,
                            "rfc" => $clienteDatosFacturacion->rfc,
                            "idCliente" => $clienteDatosFacturacion->idCliente,
                            "razonSocial" => $clienteDatosFacturacion->razonSocial,
                            "calle" => $clienteDatosFacturacion->calle,
                            "numExt" => $clienteDatosFacturacion->numExt,
                            "numInt" => $clienteDatosFacturacion->numInt,
                            "idCatColonia" => $clienteDatosFacturacion->idCatColonia,
                            "idCatUsoCFDI" => $clienteDatosFacturacion->idCatUsoCFDI,
                            "idCatMetodoPago" => $clienteDatosFacturacion->idCatMetodoPago,
                            "idCatFormaPago" => $clienteDatosFacturacion->idCatFormaPago
                        );
                        $clienteDatosFacturacion=new Clientedatosfacturacion();
                        $dfColonia=new Catcolonia();
                    }

                }else{
                    $dfAdd[]=array(
                        "id" => 0,
                        "rfc" => $clienteDatosFacturacion->rfc,
                        "idCliente" => $clienteDatosFacturacion->idCliente,
                        "razonSocial" => $clienteDatosFacturacion->razonSocial,
                        "calle" => $clienteDatosFacturacion->calle,
                        "numExt" => $clienteDatosFacturacion->numExt,
                        "numInt" => $clienteDatosFacturacion->numInt,
                        "idCatColonia" => $clienteDatosFacturacion->idCatColonia,
                        "idCatUsoCFDI" => $clienteDatosFacturacion->idCatUsoCFDI,
                        "idCatMetodoPago" => $clienteDatosFacturacion->idCatMetodoPago,
                        "idCatFormaPago" => $clienteDatosFacturacion->idCatFormaPago
                    );
                    $clienteDatosFacturacion=new Clientedatosfacturacion();
                    $dfColonia=new Catcolonia();
                }
            }
        }

        /**
         * Agregado de los datos de Contacto temporales
         */
        if(isset($_POST["btnAgregarContacto"])){
            $contactos->attributes=$_POST['Clientecontacto'];
            $contactos->idCliente=1;

            $infoFacturacion=false;
            $infoGeneralCompleto=false;
            $infoContactos=true;

            if($contactos->validate()){
                if ( !empty($_POST["contactoAdd"]) && is_array($_POST["contactoAdd"]) ) {
                    $contNew=0;
                    $bolAgregaNew=true;
                    $contactoAdd=array();
                    foreach ($_POST["contactoAdd"] as $itemIn) {
                        $arrayExplode=explode(",",$itemIn);
                        $contactoAdd[]=array(
                            "id" => $arrayExplode[0],
                            "nombreContacto" => $arrayExplode[1],
                            "telefono" => $arrayExplode[2],
                            "eMail" => $arrayExplode[3]
                        );
                        $contNew+=1;
                        if($arrayExplode[1]==$contactos->nombreContacto){
                            $bolAgregaNew=false;
                        }
                    }

                    if($bolAgregaNew==true){
                        $contactoAdd[]=array(
                            "id" => $contNew,
                            "nombreContacto" => $contactos->nombreContacto,
                            "telefono" => $contactos->telefono,
                            "eMail" => $contactos->eMail
                        );
                        $contactos=new Clientecontacto();
                    }

                }else{
                    $contactoAdd[]=array(
                        "id" => 0,
                        "nombreContacto" => $contactos->nombreContacto,
                        "telefono" => $contactos->telefono,
                        "eMail" => $contactos->eMail
                    );
                    $contactos=new Clientecontacto();
                }
            }
        }


        /**
         * Eliminar datos fiscales
         */

        if(isset($_POST["btnEliminarDf"])){
            if($_POST["btnEliminarDf"]==1){
                $dfAdd=array();

                $clienteDatosFacturacion->attributes=$_POST['Clientedatosfacturacion'];
                $contNew=0;

                $infoFacturacion=true;
                $infoGeneralCompleto=false;
                $infoContactos=false;

                foreach ($_POST["dfAdd"] as $itemIn) {
                    $arrayExplode=explode(",",$itemIn);
                    if($_POST["btnEliminarDf"]!=$arrayExplode[0]){
                        $dfAdd[]=array(
                            "id" => $arrayExplode[0],
                            "rfc" => $arrayExplode[1],
                            "idCliente" => $arrayExplode[2],
                            "razonSocial" => $arrayExplode[3],
                            "calle" => $arrayExplode[4],
                            "numExt" => $arrayExplode[5],
                            "numInt" => $arrayExplode[6],
                            "idCatColonia" => $arrayExplode[7],
                            "idCatUsoCFDI" => $arrayExplode[8],
                            "idCatMetodoPago" => $arrayExplode[9],
                            "idCatFormaPago" => $arrayExplode[10]
                        );
                        $contNew+=1;
                    }

                }
            }
        }
        /**
         * Eliminar contactos
         */
        else if(isset($_POST["btnEliminarContacto"])){

            $contactoAdd=array();

            $contactos->attributes=$_POST['Clientedatosfacturacion'];
            $contNew=0;

            $infoFacturacion=false;
            $infoGeneralCompleto=false;
            $infoContactos=true;

            $contNew=0;
            foreach ($_POST["contactoAdd"] as $itemIn) {
                $arrayExplode = explode(",", $itemIn);
                if($_POST["btnEliminarContacto"]!=$arrayExplode[0]){
                    $contactoAdd[] = array(
                        "id" => $arrayExplode[0],
                        "nombreContacto" => $arrayExplode[1],
                        "telefono" => $arrayExplode[2],
                        "eMail" => $arrayExplode[3]
                    );
                    $contNew += 1;
                }
            }

        }
        /**
         * Validacion de formulario inicial
         */
        else{
            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);

            if(isset($_POST['Cliente']))
            {

                $infoGeneralCompleto=true;
                if($model->validate()){
                    if(isset($_POST["btnAgregarContacto"])){
                        $infoFacturacion=false;
                        $infoContactos=true;
                    }else{
                        $infoFacturacion=true;
                        $infoContactos=false;
                    }
                    $infoGeneralCompleto=false;
                    if($model->idCatTipoCliente==2){
                        if($model->idCliente==null || $model->idCliente==""){
                            $model->addError("idCliente","Si el tipo de cliente es CUENTA, debes seleccionar a quien pertenece");
                            $infoGeneralCompleto=true;
                            $infoFacturacion=false;
                            $infoContactos=false;
                        }
                    }
                    $this->performAjaxValidation($clienteDatosFacturacion);

                }else{
                   // var_dump("dasda");die;
                }
                //
            }
        }

        /**
         * Guardar al cliente en sistema
         */
        if(isset($_POST["btnTerminar"])){
            if($_POST["btnTerminar"]==1){
                $this->performAjaxValidation($model);

                if($model->update()){
                    $idClienteInsert=$model->id;

                    //GUARDA LOS DATOS DE FACTURACION
                    if ( !empty($_POST["dfAdd"]) && is_array($_POST["dfAdd"]) ) {
                        foreach ($_POST["dfAdd"] as $itemIn) {
                            $arrayExplode=explode(",",$itemIn);
                            $nuevoDatoFacturacion=new Clientedatosfacturacion();
                            $nuevoDatoFacturacion->rfc = $arrayExplode[1];
                            $nuevoDatoFacturacion->idCliente= $idClienteInsert;
                            $nuevoDatoFacturacion->razonSocial= $arrayExplode[3];
                            $nuevoDatoFacturacion->calle=  $arrayExplode[4];
                            $nuevoDatoFacturacion->numExt=  $arrayExplode[5];
                            $nuevoDatoFacturacion->numInt=  $arrayExplode[6];
                            $nuevoDatoFacturacion->idCatColonia= $arrayExplode[7];
                            $nuevoDatoFacturacion->idCatUsoCFDI=  $arrayExplode[8];
                            $nuevoDatoFacturacion->idCatMetodoPago=  $arrayExplode[9];
                            $nuevoDatoFacturacion->idCatFormaPago= $arrayExplode[10];
                            $nuevoDatoFacturacion->save();
                        }
                    }

                    //GUARDA LOS DATOS DE CONTACTOS

                    if ( !empty($_POST["contactoAdd"]) && is_array($_POST["contactoAdd"]) ) {
                        foreach ($_POST["contactoAdd"] as $itemIn) {
                            $arrayExplode=explode(",",$itemIn);
                            $nuevoContacto=new Clientecontacto();
                            $nuevoContacto->nombreContacto = $arrayExplode[1];
                            $nuevoContacto->idCliente= $idClienteInsert;
                            $nuevoContacto->telefono= $arrayExplode[2];
                            $nuevoContacto->eMail=  $arrayExplode[3];
                            $nuevoContacto->save();
                        }
                    }

                    //GUARDA RL REGISTRO DEL ALTA DEL CLIENTE
                    $nuevoCteMov=new Clientemovimiento();
                    $nuevoCteMov->idCliente=$idClienteInsert;
                    $nuevoCteMov->usuario=Yii::app()->user->nombre_usuario;
                    $nuevoCteMov->fechaAlta=date("Y-m-d H:i:s");
                    $nuevoCteMov->idCatMovimientoCliente=1;
                    $nuevoCteMov->save();
                    $this->redirect(array('index',"id" => $model->id));
                }
            }
        }


        $this->render('edit',array(
            'model'=>$model,
            'contactos'=>$contactos,
            'clienteDatosFacturacion' =>$clienteDatosFacturacion,
            'clienteColonia' => $clienteColonia,
            'infoGeneralCompleto' => $infoGeneralCompleto,
            'infoFacturacion' => $infoFacturacion,
            'infoContactos' => $infoContactos,
            'criteriaColoniaDf'=>$criteriaColoniaDf,
            'dfColonia' => $dfColonia,
            'criteriaColoniaCte' => $criteriaColoniaCte,
            'dfAdd' => $dfAdd,
            'contactoAdd' => $contactoAdd,
            'colonias' => $colonias,

        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $contactos=new Clientecontacto();
        $model=new Cliente;
        $clienteMov= new Clientemovimiento();
        $clienteDatosFacturacion=new Clientedatosfacturacion();
        $clienteColonia=new Catcolonia();
        $dfColonia=new Catcolonia();
        $criteriaColoniaCte=new CDbCriteria();
        $criteriaColoniaDf=new CDbCriteria();
        $infoGeneralCompleto=true;
        $infoContactos=false;
        $infoFacturacion=false;
        $dfAdd=array();
        $contactoAdd=array();

        $GUID = getGUID();
        $guidCliente=$GUID;
        $model->guid=$guidCliente;


        /**
         * Recarga de array si ya trae datos
         */
        if (isset($_POST["dfAdd"])){
            foreach ($_POST["dfAdd"] as $itemIn) {
                $arrayExplode=explode(",",$itemIn);
                $dfAdd[]=array(
                    "id" => $arrayExplode[0],
                    "rfc" => $arrayExplode[1],
                    "idCliente" => $arrayExplode[2],
                    "razonSocial" => $arrayExplode[3],
                    "calle" => $arrayExplode[4],
                    "numExt" => $arrayExplode[5],
                    "numInt" => $arrayExplode[6],
                    "idCatColonia" => $arrayExplode[7],
                    "idCatUsoCFDI" => $arrayExplode[8],
                    "idCatMetodoPago" => $arrayExplode[9],
                    "idCatFormaPago" => $arrayExplode[10]);
            }
        }
        /**
         * Recarga de array si ya trae datos Contacto
         */
        if (isset($_POST["contactoAdd"])) {
            foreach ($_POST["contactoAdd"] as $itemIn) {
                $arrayExplode = explode(",", $itemIn);
                $contactoAdd[] = array(
                    "id" => $arrayExplode[0],
                    "nombreContacto" => $arrayExplode[1],
                    "telefono" => $arrayExplode[2],
                    "eMail" => $arrayExplode[3]
                );
            }
        }
        /**
         * Busca el código postal del cliente
         */
        if(isset($_POST["Catcolonia"])){
            $clienteColonia->attributes=$_POST["Catcolonia"];
        }
        if(isset($_POST["cpBuscadoCte"])){
            if(!empty($clienteColonia->cp)){
                $criteriaColoniaCte->compare("cp",$clienteColonia->cp);
                $datosColonia=Catcolonia::model()->findAll($criteriaColoniaCte);
                foreach ($datosColonia as $datoColonia){
                    $clienteColonia->municipio=$datoColonia->municipio;
                    $clienteColonia->estado=$datoColonia->estado;
                    $clienteColonia->pais=$datoColonia->pais;
                    $clienteColonia->ciudad=$datoColonia->ciudad;
                }
            }else{
                $criteriaColoniaCte->compare("cp","00000");
            }

        }
        else{
            $criteriaColoniaCte->compare("cp","00000");
        }
        /**
         * Busca el código postal del datos facturación
         */
         if(isset($_POST["dfColonia"])){
             $dfColonia->attributes=$_POST["dfColonia"];
         }
        if(isset($_POST["cpBuscadoDf"])){
            if(!empty($_POST['Clientedatosfacturacion'])){
                $clienteDatosFacturacion->attributes=$_POST['Clientedatosfacturacion'];
            }
            if(!empty($dfColonia->cp)){
                $criteriaColoniaDf->compare("cp",$dfColonia->cp);
                $datosColonia=Catcolonia::model()->findAll($criteriaColoniaDf);
                foreach ($datosColonia as $datoColonia){
                    $dfColonia->municipio=$datoColonia->municipio;
                    $dfColonia->estado=$datoColonia->estado;
                    $dfColonia->pais=$datoColonia->pais;
                    $dfColonia->ciudad=$datoColonia->ciudad;
                }
            }else{
                $criteriaColoniaDf->compare("cp","00000");
            }

        }
        else{
            $criteriaColoniaDf->compare("cp","00000");
        }

        /**
         * Agregado de los datos de facturacion temporales
         */

        if(isset($_POST["btnAgregarDatosFacturacion"])){
            $clienteDatosFacturacion->attributes=$_POST['Clientedatosfacturacion'];
            $clienteDatosFacturacion->idCliente=1;
            $clienteDatosFacturacion->idCatColonia=$dfColonia->colonia;
            $infoFacturacion=true;
            $infoGeneralCompleto=false;
            $infoContactos=false;
            if($clienteDatosFacturacion->validate()){
                if ( !empty($_POST["dfAdd"]) && is_array($_POST["dfAdd"]) ) {
                    $contNew=0;
                    $bolAgregaNew=true;
                    $dfAdd=array();
                    foreach ($_POST["dfAdd"] as $itemIn) {
                        $arrayExplode=explode(",",$itemIn);
                        $dfAdd[]=array(
                            "id" => $arrayExplode[0],
                            "rfc" => $arrayExplode[1],
                            "idCliente" => $arrayExplode[2],
                            "razonSocial" => $arrayExplode[3],
                            "calle" => $arrayExplode[4],
                            "numExt" => $arrayExplode[5],
                            "numInt" => $arrayExplode[6],
                            "idCatColonia" => $arrayExplode[7],
                            "idCatUsoCFDI" => $arrayExplode[8],
                            "idCatMetodoPago" => $arrayExplode[9],
                            "idCatFormaPago" => $arrayExplode[10]
                        );
                        $contNew+=1;
                        if($arrayExplode[1]==$clienteDatosFacturacion->rfc){
                            $bolAgregaNew=false;
                        }
                    }

                    if($bolAgregaNew==true){
                        $dfAdd[]=array(
                            "id" => $contNew,
                            "rfc" => $clienteDatosFacturacion->rfc,
                            "idCliente" => $clienteDatosFacturacion->idCliente,
                            "razonSocial" => $clienteDatosFacturacion->razonSocial,
                            "calle" => $clienteDatosFacturacion->calle,
                            "numExt" => $clienteDatosFacturacion->numExt,
                            "numInt" => $clienteDatosFacturacion->numInt,
                            "idCatColonia" => $clienteDatosFacturacion->idCatColonia,
                            "idCatUsoCFDI" => $clienteDatosFacturacion->idCatUsoCFDI,
                            "idCatMetodoPago" => $clienteDatosFacturacion->idCatMetodoPago,
                            "idCatFormaPago" => $clienteDatosFacturacion->idCatFormaPago
                        );
                        $clienteDatosFacturacion=new Clientedatosfacturacion();
                        $dfColonia=new Catcolonia();
                    }

                }else{
                    $dfAdd[]=array(
                        "id" => 0,
                        "rfc" => $clienteDatosFacturacion->rfc,
                        "idCliente" => $clienteDatosFacturacion->idCliente,
                        "razonSocial" => $clienteDatosFacturacion->razonSocial,
                        "calle" => $clienteDatosFacturacion->calle,
                        "numExt" => $clienteDatosFacturacion->numExt,
                        "numInt" => $clienteDatosFacturacion->numInt,
                        "idCatColonia" => $clienteDatosFacturacion->idCatColonia,
                        "idCatUsoCFDI" => $clienteDatosFacturacion->idCatUsoCFDI,
                        "idCatMetodoPago" => $clienteDatosFacturacion->idCatMetodoPago,
                        "idCatFormaPago" => $clienteDatosFacturacion->idCatFormaPago
                    );
                    $clienteDatosFacturacion=new Clientedatosfacturacion();
                    $dfColonia=new Catcolonia();
                }
            }
        }

        /**
         * Agregado de los datos de Contacto temporales
         */
        if(isset($_POST["btnAgregarContacto"])){
            $contactos->attributes=$_POST['Clientecontacto'];
            $contactos->idCliente=1;

            $infoFacturacion=false;
            $infoGeneralCompleto=false;
            $infoContactos=true;

            if($contactos->validate()){
                if ( !empty($_POST["contactoAdd"]) && is_array($_POST["contactoAdd"]) ) {
                    $contNew=0;
                    $bolAgregaNew=true;
                    $contactoAdd=array();
                    foreach ($_POST["contactoAdd"] as $itemIn) {
                        $arrayExplode=explode(",",$itemIn);
                        $contactoAdd[]=array(
                            "id" => $arrayExplode[0],
                            "nombreContacto" => $arrayExplode[1],
                            "telefono" => $arrayExplode[2],
                            "eMail" => $arrayExplode[3]
                        );
                        $contNew+=1;
                        if($arrayExplode[1]==$contactos->nombreContacto){
                            $bolAgregaNew=false;
                        }
                    }

                    if($bolAgregaNew==true){
                        $contactoAdd[]=array(
                            "id" => $contNew,
                            "nombreContacto" => $contactos->nombreContacto,
                            "telefono" => $contactos->telefono,
                            "eMail" => $contactos->eMail
                        );
                        $contactos=new Clientecontacto();
                    }

                }else{
                    $contactoAdd[]=array(
                        "id" => 0,
                        "nombreContacto" => $contactos->nombreContacto,
                        "telefono" => $contactos->telefono,
                        "eMail" => $contactos->eMail
                    );
                    $contactos=new Clientecontacto();
                }
            }
        }


        /**
         * Eliminar datos fiscales
         */

        if(isset($_POST["btnEliminarDf"])){
            if($_POST["btnEliminarDf"]==1){
                $dfAdd=array();
                $model->attributes=$_POST['Cliente'];
                $clienteDatosFacturacion->attributes=$_POST['Clientedatosfacturacion'];
                $contNew=0;

                $infoFacturacion=true;
                $infoGeneralCompleto=false;
                $infoContactos=false;

                foreach ($_POST["dfAdd"] as $itemIn) {
                    $arrayExplode=explode(",",$itemIn);
                    if($_POST["btnEliminarDf"]!=$arrayExplode[0]){
                        $dfAdd[]=array(
                            "id" => $arrayExplode[0],
                            "rfc" => $arrayExplode[1],
                            "idCliente" => $arrayExplode[2],
                            "razonSocial" => $arrayExplode[3],
                            "calle" => $arrayExplode[4],
                            "numExt" => $arrayExplode[5],
                            "numInt" => $arrayExplode[6],
                            "idCatColonia" => $arrayExplode[7],
                            "idCatUsoCFDI" => $arrayExplode[8],
                            "idCatMetodoPago" => $arrayExplode[9],
                            "idCatFormaPago" => $arrayExplode[10]
                        );
                        $contNew+=1;
                    }

                }
            }
        }
        /**
         * Eliminar contactos
         */
        else if(isset($_POST["btnEliminarContacto"])){

            $contactoAdd=array();
            $model->attributes=$_POST['Cliente'];
            $contactos->attributes=$_POST['Clientedatosfacturacion'];
            $contNew=0;

            $infoFacturacion=false;
            $infoGeneralCompleto=false;
            $infoContactos=true;

            $contNew=0;
            foreach ($_POST["contactoAdd"] as $itemIn) {
                $arrayExplode = explode(",", $itemIn);
                if($_POST["btnEliminarContacto"]!=$arrayExplode[0]){
                    $contactoAdd[] = array(
                        "id" => $arrayExplode[0],
                        "nombreContacto" => $arrayExplode[1],
                        "telefono" => $arrayExplode[2],
                        "eMail" => $arrayExplode[3]
                    );
                    $contNew += 1;
                }
            }

        }
        /**
         * Validacion de formulario inicial
         */
        else{
            // Uncomment the following line if AJAX validation is needed
            $this->performAjaxValidation($model);

            if(isset($_POST['Cliente']))
            {
                $model->attributes=$_POST['Cliente'];
                $model->estatus="A";
                $model->categoria="P";
                $model->idCatColonia=$clienteColonia->colonia;
                $model->pais="-";
                $model->municipio="-";
                $model->estado="-";
                $model->ciudad="-";
                $model->cp="-";
                if($model->idCliente==""){
                    $model->idCliente=null;
                }

                if($model->validate()){
                    $infoGeneralCompleto=false;
                    if(isset($_POST["btnAgregarContacto"])){
                        $infoFacturacion=false;
                        $infoContactos=true;
                    }else{
                        $infoFacturacion=true;
                        $infoContactos=false;
                    }
                    if($model->idCatTipoCliente==2){
                        if($model->idCliente==null || $model->idCliente==""){
                            $model->addError("idCliente","Si el tipo de cliente es CUENTA, debes seleccionar a quien pertenece");
                            $infoGeneralCompleto=true;
                            $infoFacturacion=false;
                            $infoContactos=false;
                        }
                    }


                 //   $this->performAjaxValidation($clienteDatosFacturacion);

                }
                //
            }
        }

        /**
         * Guardar al cliente en sistema
         */
        if(isset($_POST["btnTerminar"])){
            if($_POST["btnTerminar"]==1){
                $this->performAjaxValidation($model);
                $model->guid=$guidCliente;
                if($model->save()){
                    $criteria=new CDbCriteria();
                    $criteria->compare("guid",$guidCliente);
                    $idClienteInsert=0;
                    $Clientes=Cliente::model()->findAll($criteria);
                    foreach ($Clientes as $cliente){
                        $idClienteInsert=$cliente->id;
                    }
                    //GUARDA LOS DATOS DE FACTURACION
                    if ( !empty($_POST["dfAdd"]) && is_array($_POST["dfAdd"]) ) {
                        foreach ($_POST["dfAdd"] as $itemIn) {
                            $arrayExplode=explode(",",$itemIn);
                            $nuevoDatoFacturacion=new Clientedatosfacturacion();
                            $nuevoDatoFacturacion->rfc = $arrayExplode[1];
                            $nuevoDatoFacturacion->idCliente= $idClienteInsert;
                            $nuevoDatoFacturacion->razonSocial= $arrayExplode[3];
                            $nuevoDatoFacturacion->calle=  $arrayExplode[4];
                            $nuevoDatoFacturacion->numExt=  $arrayExplode[5];
                            $nuevoDatoFacturacion->numInt=  $arrayExplode[6];
                            $nuevoDatoFacturacion->idCatColonia= $arrayExplode[7];
                            $nuevoDatoFacturacion->idCatUsoCFDI=  $arrayExplode[8];
                            $nuevoDatoFacturacion->idCatMetodoPago=  $arrayExplode[9];
                            $nuevoDatoFacturacion->idCatFormaPago= $arrayExplode[10];
                            $nuevoDatoFacturacion->save();
                        }
                    }

                    //GUARDA LOS DATOS DE CONTACTOS

                    if ( !empty($_POST["contactoAdd"]) && is_array($_POST["contactoAdd"]) ) {
                        foreach ($_POST["contactoAdd"] as $itemIn) {
                            $arrayExplode=explode(",",$itemIn);
                            $nuevoContacto=new Clientecontacto();
                            $nuevoContacto->nombreContacto = $arrayExplode[1];
                            $nuevoContacto->idCliente= $idClienteInsert;
                            $nuevoContacto->telefono= $arrayExplode[2];
                            $nuevoContacto->eMail=  $arrayExplode[3];
                            $nuevoContacto->save();
                        }
                    }

                    //GUARDA RL REGISTRO DEL ALTA DEL CLIENTE
                    $nuevoCteMov=new Clientemovimiento();
                    $nuevoCteMov->idCliente=$idClienteInsert;
                    $nuevoCteMov->usuario=Yii::app()->user->nombre_usuario;
                    $nuevoCteMov->fechaAlta=date("Y-m-d H:i:s");
                    $nuevoCteMov->idCatMovimientoCliente=1;
                    $nuevoCteMov->save();

                    $this->redirect(array('index'));
                }
            }
        }


        $this->render('create',array(
            'model'=>$model,
            'contactos'=>$contactos,
            'clienteDatosFacturacion' =>$clienteDatosFacturacion,
            'clienteColonia' => $clienteColonia,
            'infoGeneralCompleto' => $infoGeneralCompleto,
            'infoFacturacion' => $infoFacturacion,
            'infoContactos' => $infoContactos,
            'criteriaColoniaDf'=>$criteriaColoniaDf,
            'dfColonia' => $dfColonia,
            'criteriaColoniaCte' => $criteriaColoniaCte,
            'dfAdd' => $dfAdd,
            'contactoAdd' => $contactoAdd
        ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Cliente']))
		{
			$model->attributes=$_POST['Cliente'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

        if (!isset(Yii::app()->user->id_usuario)) {
            $this->redirect(CController::createUrl('site/login'));
        }

        $criteria=new CDbCriteria;
        $ClienteBusqueda=new Viewcliente();
        if(isset($_POST['Viewcliente'])){

            $ClienteBusqueda->attributes=$_POST['Viewcliente'];
            if(isset($ClienteBusqueda->idCatTipoCliente)){
                $criteria->compare("idCatTipoCliente",$ClienteBusqueda->idCatTipoCliente);
            }
            if(isset($ClienteBusqueda->categoria)){
                $criteria->compare("categoria",$ClienteBusqueda->categoria);
            }
            if(isset($ClienteBusqueda->idCatSector)){
                $criteria->compare("idCatSector",$ClienteBusqueda->idCatSector);
            }
            if(isset($ClienteBusqueda->idCatPonderacion)){
                $criteria->compare("idCatPonderacion",$ClienteBusqueda->idCatPonderacion);
            }
            if(isset($ClienteBusqueda->idCatFuenteContratacion)){
                $criteria->compare("idCatFuenteContratacion",$ClienteBusqueda->idCatFuenteContratacion);
            }
            if(isset($ClienteBusqueda->estatus)){
                $criteria->compare("estatus",$ClienteBusqueda->estatus);
            }

            if(isset($ClienteBusqueda->fechaAlta)){
                if($_POST['Viewunidad']['fechaAlta']!=""){
                    $dtFechaI=date_format(date_create(str_replace("/", "-", $_POST['Viewunidad']['fechaAlta'])),"Y-m-d");
                    $dtFechaF=date_format(date_create(str_replace("/", "-", $_POST['Viewunidad']['fechaAltaFin'])),"Y-m-d");

                    $criteria->addBetweenCondition("fechaAlta",$dtFechaI.' 00:00:00',$dtFechaF.' 23:59:59' );
                }

            }
            if(isset($ClienteBusqueda->nombre)){
                $criteria->addCondition("nombre LIKE '%".$ClienteBusqueda->nombre."%'");
            }

        }else{
            if(isset($_GET["id"])){
                $criteria->compare("id",$_GET["id"]);
            }else{
                $criteria->compare("estatus","W");
            }

        }




        $dataProvider=new CActiveDataProvider('Cliente');
        $Clientes=Viewcliente::model()->findAll($criteria);

        $this->render('index',array(
                'dataProvider'=>$dataProvider,
                'ClienteBusqueda' => $ClienteBusqueda,
                'Clientes' =>$Clientes
            )

        );



	}


    public function actionAgenda()
    {



        $this->render('agenda',array(

            )

        );



    }

    public function actionDetalle($id)
    {


        $criteria=new CDbCriteria;
        $criteria->compare("idCliente",$id);

        $Cliente=Viewcliente::model()->find("id=:id",array(":id"=>$id));
        $Contactos=Clientecontacto::model()->findAll($criteria);
        $datosFacturacion=Viewdatosfacturacion::model()->findAll($criteria);
        $servicios=ViewClienteServicio::model()->findAll($criteria);

        $quejas=Viewquejasugerencia::model()->findAll($criteria);
        $cotizaciones=ViewCotizacion::model()->findAll($criteria);
        $nuevoServicio=new Clientecontrato();
        $updateClietePS=new Clienteproductoservicio();
        $criteria->order="id DESC";
        $contratos=Clientecontrato::model()->findAll($criteria);

        if(isset($_POST["Clientecontrato"])){
            $nuevoServicio->attributes=$_POST["Clientecontrato"];
            $nuevoServicio->usuario=Yii::app()->user->nombre_usuario;
            $nuevoServicio->fechaAlta=date("Y-m-d H:i:s");
            $nuevoServicio->idCliente=$id;
            $nuevoServicio->estatus="A";
            if($nuevoServicio->idCatPeriodicidad=="E"){
                $nuevoServicio->facturacion="E";
            }


                if($nuevoServicio->validate()){
                    if($nuevoServicio->save()){
                        $this->redirect(CController::createUrl('cliente/detalle', array('id' => $id)));
                    }
            }
        }

        if(isset($_POST["Clienteproductoservicio"])){
            $updateClietePS=Clienteproductoservicio::model()->findByPk($_POST["Clienteproductoservicio"]["id"]);
            $updateClietePS->idClienteContrato=$_POST["Clienteproductoservicio"]["idClienteContrato"];
            if($updateClietePS->update()){
                $this->redirect(CController::createUrl('cliente/detalle', array('id' => $id)));
            }
        }

        $this->render('detalle',array(
                'Cliente' =>$Cliente,
                'Contactos' => $Contactos,
                'datosFacturacion' => $datosFacturacion,
                'servicios' => $servicios,
                'contratos' => $contratos,
                'quejas' => $quejas,
                'cotizaciones' => $cotizaciones,
                'nuevoServicio' => $nuevoServicio,
                'updateClietePS' => $updateClietePS,


            )

        );



    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Cliente the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Cliente::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Cliente $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cliente-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
