<?php

class EmpleadoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $criteria = new CDbCriteria;
        $new = new Empleado;
        $busqueda = new Empleado;



        if (isset($_POST["btnNuevo"])) {

            if (isset($_POST['Empleado'])) {
                $new->attributes = $_POST['Empleado'];
                $new->fechaAlta = date('Y-m-d H:i:s');

                if ($new->validate()) {
                    if ($new->save()) {
                        Yii::app()->user->setFlash('success', 'El Registro ha sido realizado.');
                        $this->redirect(array('index'));
                    }
                }
            }
        }


        if (isset($_POST["btnBuscar"])) {

            if (isset($_POST['Empleado'])) {
                $busqueda->attributes = $_POST['Empleado'];

                if (isset($busqueda->nombrePila) && !empty($busqueda->nombrePila)) {
                    $criteria->addCondition('nombrePila LIKE :nombrePila');
                    $criteria->params[':nombrePila'] = $busqueda->nombrePila."%";
                }
                if (isset($busqueda->apPaterno) && !empty($busqueda->apPaterno)) {
                    $criteria->addCondition('apPaterno LIKE :apPaterno');
                    $criteria->params[':apPaterno'] = $busqueda->apPaterno."%";
                }
                if (isset($busqueda->apMaterno) && !empty($busqueda->apMaterno)) {
                    $criteria->addCondition('apMaterno LIKE :apMaterno');
                    $criteria->params[':apMaterno'] = $busqueda->apMaterno."%";
                }
            }
        }
        $catalogo = Empleado::model()->findAll($criteria);
        $this->render('index', array(
            'new' => $new,
            'busqueda' => $busqueda,
            'catalogo' => $catalogo,
        ));
    }

    public function actionEliminar($id) {
        $modelDelete = Empleado::model()->findByPk($id);
        if (!empty($modelDelete)) {
            try {
                $modelDelete->delete();
                Yii::app()->user->setFlash('success', 'El Registro ha sido eliminado.');
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', 'El Registro no puede ser eliminado debido a que existe una relación con algun otro catálogo.');
            }
        } else {
            Yii::app()->user->setFlash('error', 'El Registro no puede ser eliminado debido a que no existe su ID.');
        }
        $this->redirect(array('index'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Empleado the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Empleado::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Empleado $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'empleado-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
