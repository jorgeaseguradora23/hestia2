<?php
use Dompdf\Dompdf as Dompdf;

class CotizacionController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'create', 'CreateConCliente', 'AgregarProducto', 'cierre','imprimir', 'GetPrice','ShowCotizacion'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $criteria = new CDbCriteria;
        $busqueda = new Cotizacion;
        
        if(isset($_POST['Cotizacion'])){
            $busqueda->attributes=$_POST['Cotizacion'];
            if(!empty($busqueda->_idClienteNumero)){
                $criteria->compare("idCliente",$busqueda->_idClienteNumero);
            }
            if(!empty($busqueda->idCliente)){
                $criteria->compare("idCliente",$busqueda->idCliente);
            }
            if(!empty($busqueda->tipo)){
                $criteria->compare("tipo",$busqueda->tipo);
            }
            if(!empty($busqueda->_fechaAltaInicio)){
                $criteria->addCondition("date(fechaAlta)>=:fechaAltaInicio");
                //echo $busqueda->_fechaAltaInicio;die;
                $criteria->params[":fechaAltaInicio"]=$busqueda->_fechaAltaInicio;
            }
            if(!empty($busqueda->_fechaAltaFin)){
                $criteria->addCondition("date(fechaAlta)<=:fechaAltaFin");
                $criteria->params[":fechaAltaFin"]=$busqueda->_fechaAltaFin;
            }
            if(!empty($busqueda->_fechaVencimientoInicio)){
                $criteria->addCondition("date(fechaVencimiento)>=:fechaVencimientoInicio");
                //echo $busqueda->_fechaAltaInicio;die;
                $criteria->params[":fechaVencimientoInicio"]=$busqueda->_fechaVencimientoInicio;
            }
            if(!empty($busqueda->_fechaVencimientoFin)){
                $criteria->addCondition("date(fechaVencimiento)<=:fechaVencimientoFin");
                $criteria->params[":fechaVencimientoFin"]=$busqueda->_fechaVencimientoFin;
            }
        }
        $criteria->addCondition("idCotizacion is null");

        $cotizaciones = Viewcotizacion::model()->findAll($criteria);
        $this->render('index', array(
            'busqueda' => $busqueda,
            'cotizaciones' => $cotizaciones,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Cotizacion;
        $NuevaQueja = new Cotizacion();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $criteria = new CDbCriteria;
        $ClienteBusqueda = new Viewcliente();
        if (isset($_POST['Viewcliente'])) {
            $ClienteBusqueda->attributes = $_POST['Viewcliente'];
            if (isset($ClienteBusqueda->idCatTipoCliente)) {
                $criteria->compare("idCatTipoCliente", $ClienteBusqueda->idCatTipoCliente);
            }
            if (isset($ClienteBusqueda->categoria)) {
                $criteria->compare("categoria", $ClienteBusqueda->categoria);
            }
            if (isset($ClienteBusqueda->idCatSector)) {
                $criteria->compare("idCatSector", $ClienteBusqueda->idCatSector);
            }
            if (isset($ClienteBusqueda->idCatPonderacion)) {
                $criteria->compare("idCatPonderacion", $ClienteBusqueda->idCatPonderacion);
            }
            if (isset($ClienteBusqueda->idCatFuenteContratacion)) {
                $criteria->compare("idCatFuenteContratacion", $ClienteBusqueda->idCatFuenteContratacion);
            }
            if (isset($ClienteBusqueda->estatus)) {
                $criteria->compare("estatus", $ClienteBusqueda->estatus);
            }

            if (isset($ClienteBusqueda->fechaAlta)) {
                if ($_POST['Viewunidad']['fechaAlta'] != "") {
                    $dtFechaI = date_format(date_create(str_replace("/", "-", $_POST['Viewunidad']['fechaAlta'])), "Y-m-d");
                    $dtFechaF = date_format(date_create(str_replace("/", "-", $_POST['Viewunidad']['fechaAltaFin'])), "Y-m-d");

                    $criteria->addBetweenCondition("fechaAlta", $dtFechaI . ' 00:00:00', $dtFechaF . ' 23:59:59');
                }
            }
            if(isset($ClienteBusqueda->nombre)){
                $criteria->addCondition("nombre LIKE '%".$ClienteBusqueda->nombre."%'");
            }
        }else{
            $criteria->compare("estatus","W");
        }



        $Clientes = Viewcliente::model()->findAll($criteria);

        $this->render('create', array(
            'Clientes' => $Clientes,
            'ClienteBusqueda' => $ClienteBusqueda,
        ));
    }

    public function actionCreateConCliente() {
        $cliente = Cliente::model()->findByPk($_GET['idCliente']);
        $cotizacion = new Cotizacion;
        $productosRegistrados = array();

        if (isset($_GET['idCotizacionBase']) && !empty($_GET['idCotizacionBase']) && empty($_POST)) {

            $cotizacionBase = Cotizacion::model()->findByPk($_GET['idCotizacionBase']);
            $cotizacion->attributes = CJSON::decode(CJSON::encode($cotizacionBase));
            $cotizacion->idCotizacion = $_GET['idCotizacionBase'];
            /*$cotizacion->fechaAlta = date('Y-m-d', strtotime($cotizacionBase->fechaAlta));
            $cotizacion->fechaVencimiento = date('Y-m-d', strtotime($cotizacionBase->fechaVencimiento));*/
            $cotizacion->fechaAlta = date('Y-m-d H:i:s');
            $cotizacion->fechaVencimiento = date('Y-m-d H:i:s', strtotime('+8 days'));
            $productos = viewCotizacionPS::model()->findAll('idCotizacion=:idCotizacion', array(':idCotizacion' => $cotizacionBase->id));
            foreach ($productos as $key => $producto) {

                $productosRegistrados[$key]['idProductoServicio'] = $producto->idProductoServicio;
                $productosRegistrados[$key]['cantidad'] = $producto->cantidad;
                $productosRegistrados[$key]['descuento'] = $producto->descuento;
                $productosRegistrados[$key]['precio'] = $producto->precio;
                $productosRegistrados[$key]['retencion'] = $producto->retencion;
                $productosRegistrados[$key]['tralados'] = $producto->traslados;
                $productosRegistrados[$key]['idImpuesto'] = $producto->idCatEsquemaImpuesto;
                $productosRegistrados[$key]['descuento'] = $producto->descuento;
                $productosRegistrados[$key]['subtotal'] = $producto->subtotal;
                $productosRegistrados[$key]['producto'] = $producto->descripcion;
                $productosRegistrados[$key]['observacion'] = $producto->observacion;
                $productosRegistrados[$key]['total'] = ($producto->subtotal-$producto->descuento)+$producto->traslados-$producto->retencion;


            }
        }
        $cotizacion->fechaAlta=date('Y-m-d H:i:s');

        $cotizacion->fechaVencimiento=date('Y-m-d',strtotime('+30 days'));
        if (isset($_POST["Cotizacion"])) {
            //var_dump($_POST);die;
            $cotizacion->attributes = $_POST["Cotizacion"];
            $cotizacion->fechaAlta=$cotizacion->fechaAlta." ".date('H:i:s');
            $fechaAlta=$cotizacion->fechaAlta;
            /* var_dump($cotizacion->fechaAlta);die;
              if(!empty($cotizacion->fechaAlta)){
              $fechaAlta= explode('/', $cotizacion->fechaAlta);
              $cotizacion->fechaAlta= $fechaAlta[2].'-'.$fechaAlta[1].'-'.$fechaAlta[0];
              }
              if(!empty($cotizacion->fechaVencimiento)){
              $fechaVencimiento= explode('/', $cotizacion->fechaVencimiento);
              $cotizacion->fechaVencimiento= $fechaVencimiento[2].'-'.$fechaVencimiento[1].'-'.$fechaVencimiento[0];
              } */

            if (!isset($_POST["idProductoServicio"]) || empty($_POST["idProductoServicio"])) {
                $cotizacion->addError('idCliente', 'La cotización debe tener al menos un servicio agregado.');
            }elseif (empty($cotizacion->serie)) {
                $cotizacion->addError('idCliente', 'La cotización debe tener seleccionada una serie.');
            }elseif (empty($cotizacion->idCatMoneda)) {
                $cotizacion->addError('idCatMoneda', 'La cotización debe tener una moneda seleccionada.');
            } else {
                $bolGuardaCotizacion=false;
                if($cotizacion->idCatMoneda==1){
                    $cotizacion->tipoCambio=1;
                    $bolGuardaCotizacion=true;
                }else{
                    //REVISAR SI ESTA DADO DE ALTA EL TIPO DE CAMBIO DEL DIA PARA LA MONEDA
                    $criteriaTipoCambio=new CDbCriteria();
                    $criteriaTipoCambio->compare("idCatMoneda",$cotizacion->idCatMoneda);
                    $criteriaTipoCambio->compare("fechaAlta",date("Y-m-d",strtotime($fechaAlta)));
                    $ipoCambio=Tipocambio::model()->find($criteriaTipoCambio);

                    if(!empty($ipoCambio)){
                        $cotizacion->tipoCambio=$ipoCambio->importe;
                        $bolGuardaCotizacion=true;
                    }else{
                        $cotizacion->addError('tipoCambio', 'No se ha ingresado el tipo de cambio para la fecha y moneda indicada.');
                    }
                }
                if($bolGuardaCotizacion==true){
                    $cotizacion->idCliente = $cliente->id;
                    $cotizacion->usuario = Yii::app()->user->nombre_usuario;
                    $cotizacion->estatus = "A";
                    $subtotal = 0;
                    $descuento = 0;
                    $impRetenido = 0;
                    $impTrasladado = 0;
                    foreach ($_POST["idProductoServicio"] as $key => $idProductoServicio) {

                        $subtotal += ($_POST["subtotal"][$key] );
                        $descuento += $_POST["descuento"][$key];
                        if(!empty($_POST["retencion"][$key])){
                            $iretencion=str_replace(",","",$_POST["retencion"][$key]);
                            $iretencion=$iretencion*1;
                            $impRetenido += $_POST["retencion"][$key];
                        }

                        if(!empty($_POST["tralados"][$key])){
                            $iTraslados=str_replace(",","",$_POST["tralados"][$key]);
                            $iTraslados=$iTraslados*1;
                            $impTrasladado += $iTraslados;
                        }


                    }
                    $cotizacion->subtotal = $subtotal;

                    $cotizacion->impuestoRetenido = $impRetenido;
                    $cotizacion->impuestoTrasladado = $impTrasladado;
                    $cotizacion->total = $subtotal - $descuento - $impRetenido + $impTrasladado;
                    $criteria = new CDbCriteria;
                    $criteria->addCondition('serie=' . $cotizacion->serie);
                    $criteria->order = 'folio desc';
                    $folio = Cotizacion::model()->find($criteria);
                    if (empty($folio)) {
                        $folioActual = 1;
                    } else {
                        $folioActual = $folio->folio + 1;
                    }
                    $cotizacion->folio = $folioActual;
                    if(isset($_GET["idCotizacionBase"])){
                        $cotizacion->idCotizacion=$_GET["idP"];
                        $cotizacion->estatus="S";
                        $criteria = new CDbCriteria;
                        $criteria->compare("id",$_GET["idP"]);
                        $folio = Cotizacion::model()->find($criteria);

                        $cotizacion->folio=$folio->folio;
                    }

                    if ($cotizacion->save()) {
                        foreach ($_POST["idProductoServicio"] as $key => $idProductoServicio) {
                            $productoModel = Productoservicio::model()->findByPk($idProductoServicio);
                            $producto = new Cotizacionproductoservicio;
                            $producto->idProductoServicio = $idProductoServicio;
                            $producto->idCotizacion = $cotizacion->id;
                            $producto->cantidad = $_POST["cantidad"][$key];
                            $producto->precio = $_POST["precio"][$key];
                            $producto->posicion = $key + 1;
                            $producto->idCatEsquemaImpuesto = $_POST["idImpuesto"][$key];
                            $producto->descuento = $_POST["descuento"][$key];
                            $producto->observacion = $_POST["observacion"][$key];

                            $producto->save();
                        }
                        $idRedirect="";
                        if(isset($_GET["idP"])){
                            $idRedirect=$_GET["idP"];
                        }else{
                            $idRedirect=$cotizacion->id;
                        }
                        $this->redirect(CController::createUrl('cotizacion/view', array('id' => $idRedirect)));
                    }
                }

            }
            if (isset($_POST['idProductoServicio']) && !empty($_POST['idProductoServicio'])) {
                foreach ($_POST["idProductoServicio"] as $key => $idProductoServicio) {


                    $productosRegistrados[$key]['idProductoServicio'] = $idProductoServicio;
                    $productosRegistrados[$key]['cantidad'] = $_POST['cantidad'][$key];
                    $productosRegistrados[$key]['descuento'] = $_POST['descuento'][$key];
                    $productosRegistrados[$key]['precio'] = $_POST['precio'][$key];
                    $productosRegistrados[$key]['retencion'] = $_POST['retencion'][$key];
                    $productosRegistrados[$key]['tralados'] = $_POST['tralados'][$key];
                    $productosRegistrados[$key]['descuento'] = $_POST['descuento'][$key];
                    $productosRegistrados[$key]['subtotal'] = $_POST['subtotal'][$key];
                    $productosRegistrados[$key]['producto'] = $_POST['producto'][$key];
                    $productosRegistrados[$key]['idImpuesto'] = $_POST['idImpuesto'][$key];
                    $productosRegistrados[$key]['observacion'] = $_POST['observacion'][$key];
                    $productosRegistrados[$key]['total'] = $_POST['total'][$key];




                }
            }
        }
        $this->render('createConCliente', array(
            'cliente' => $cliente,
            'cotizacion' => $cotizacion,
            'productosRegistrados' => $productosRegistrados,
        ));
    }

    public  function actionShowCotizacion(){
        if (!isset(Yii::app()->user->id_usuario)) {
            $this->redirect(CController::createUrl('site/login'));
        }
        $criteria=new CDbCriteria();
        $criteria->compare("id",$_POST["id"]);
        $datosBase=ViewCotizacion::model()->find($criteria);
        $datosCliente=Viewcliente::model()->find("id=:id",array(":id" => $datosBase->idCliente));
        $productos=ViewCotizacionPS::model()->findAll("idCotizacion=:id",array(":id" => $datosBase->id));
        $this->renderPartial('cotizacionLinea', array(
            'datosBase' => $datosBase,
            'datosCliente' => $datosCliente,
            'productos' => $productos
        ));

    }

	public function actionRechazo(){
		$rechazo = $this->loadModel($id);
		$rechazo->scenario = "rechazo";
		$rechazo->attributes = $_POST["Cotizacion"];
		$rechazo->estatus = "R";
		$rechazo->fechaCierre = date('Y-m-d H:i:s');
		if($rechazo->save()){
			$this->redirect(CController::createUrl('cotizacion/view', array('id' => $id)));
		}
	}

    public function actioncierre(){
		if (!isset(Yii::app()->user->id_usuario)) {
			$this->redirect(CController::createUrl('site/login'));
		}
		$cotizacion=new Cotizacion();
		if(isset($_POST["id"])){
			$cotizacion=Cotizacion::model()->findByPk($_POST["id"]);
		}

		if (isset($_POST["Cotizacion"])) {
			$cotizacion=Cotizacion::model()->findByPk($_POST["Cotizacion"]["id"]);
				$cotizacion->scenario = "cierre";

				$cotizacion->attributes = $_POST["Cotizacion"];

				if ($cotizacion->save()) {

					$cotizacion->estatus = "C";
					$cotizacion->idCotizacionCierre = $_POST["idCotizacionCierre"];
					$cotizacion->fechaCierre = date('Y-m-d H:i:s');
					//SE DEBE EXTRAER EL ID DE LA COTIZACION QUE ES ACEPTADA
					if($cotizacion->update()){
						$cliente=Cliente::model()->findByPk($cotizacion->idCliente);
						$cliente->categoria = 'C';
						$cliente->update();

						$clienteMovimiento = new Clientemovimiento;
						$clienteMovimiento->idCliente = $cliente->id;
						$clienteMovimiento->fechaAlta = date('Y-m-d H:i:s');
						$clienteMovimiento->usuario = Yii::app()->user->nombre_usuario;
						$clienteMovimiento->observacion = $cotizacion->contenidoCierre;
						$clienteMovimiento->idCatMovimientoCliente = 2;
						$clienteMovimiento->save();
						$productosRegistrados = Cotizacionproductoservicio::model()->findAll('idCotizacion=:idCotizacion', array(':idCotizacion' => $cotizacion->id));
						foreach ($productosRegistrados as $productoRegistrado) {
							$productoCliente = new Clienteproductoservicio;
							$productoCliente->idCliente = $cliente->id;
							$productoCliente->idProductoServicio = $productoRegistrado->idProductoServicio;
							$productoCliente->idCatTipoServicio = $cotizacion->_idCatTipoServicio;
							$productoCliente->cantidad = $productoRegistrado->cantidad;
							$productoCliente->precio = $productoRegistrado->precio;
							$productoCliente->fechaInicio = $cotizacion->_fechaInicio;
							$productoCliente->fechaFinal = $cotizacion->_fechaFinal;
							//$productoCliente->idCatTipoMonitoreo = $cotizacion->_idCatTipoMonitoreo;
							$productoCliente->idCatTipoMonitoreo = 1;
							$productoCliente->estatus = 'A';
							$productoCliente->idCatEsquemaImpuesto = $productoRegistrado->idCatEsquemaImpuesto;
							$productoCliente->idCotizacion = $cotizacion->id;
							$productoCliente->tipoRegistro = 'C';
							if(!empty($productoCliente->save())){
								$productoCliente->addError("idCatTipoMonitoreo","es por aqui");
							}

						}
						echo true;die;
					}


				}



		}
		$this->renderPartial('ajax/aceptaCotizacion', array(
			'cotizacion' => $cotizacion,

		));
	}

    public function actionView($id) {
        if (!isset(Yii::app()->user->id_usuario)) {
            $this->redirect(CController::createUrl('site/login'));
        }

        $cotizacion = $this->loadModel($id);
        $cotizacionDetalle =Viewcotizacion::model()->find("id=:id",array(":id" => $id));
        $cliente = Cliente::model()->findByPk($cotizacion->idCliente);
        $nuevoCotizacionComentario=new Cotizacionseguimiento();

        $buscarSeguimientos = Cotizacionseguimiento::model()->findAll('idCotizacion=:idCotizacion', array(':idCotizacion' => $cotizacion->id));
        $criteriaSegs=new CDbCriteria();
        $criteriaSegs->compare("idCotizacion",$cotizacion->id);
        $criteriaSegs->order="fechaAlta DESC";
        $cotSeguimientos = ViewCotizacion::model()->findAll($criteriaSegs);
        $rechazo=new Cotizacion();




        if(isset($_POST["Cotizacionseguimiento"])){
            $nuevoCotizacionComentario->attributes=$_POST["Cotizacionseguimiento"];
            $nuevoCotizacionComentario->idCotizacion=$id;
            $nuevoCotizacionComentario->usuario=Yii::app()->user->nombre_usuario;
            $nuevoCotizacionComentario->fechaAlta=date("Y-m-d H:i:s");


            if($nuevoCotizacionComentario->save()){

                $this->redirect(CController::createUrl('cotizacion/view', array('id' => $id)));
            }
        }


        $this->render('view', array(
            'cliente' => $cliente,
            'cotizacion' => $cotizacion,

            'nuevoCotizacionComentario' => $nuevoCotizacionComentario,
            'buscarSeguimientos' => $buscarSeguimientos,
            'cotizacionDetalle' => $cotizacionDetalle,
            'cotSeguimientos' => $cotSeguimientos,
            'rechazo' => $rechazo
        ));
    }

    public function actionImprimir($id) {
        $cotizacion = Viewcotizacion::model()->find("id=:id",array(":id"=> $id));

        $criteriaCliente=new CDbCriteria();

        $criteriaCliente->compare("id",$cotizacion->idCliente);
        $cliente = Viewcliente::model()->find($criteriaCliente);
        $productosRegistrados = ViewCotizacionPS::model()->findAll('idCotizacion=:idCotizacion', array(':idCotizacion' => $cotizacion->id));
        $pdf = $this->renderPartial('_imprimir', array(
            'cliente' => $cliente,
            'cotizacion' => $cotizacion,
            'productosRegistrados' => $productosRegistrados,
                ), true);
        /*Yii::import('ext.pdf.dompdf.*');
        require_once(Yii::app()->basePath . '/extensions/pdf/dompdf/dompdf_config.inc.php');
        Yii::registerAutoloader('DOMPDF_autoload');

        $dompdf = new DOMPDF();
        $dompdf->load_html($pdf);
        $dompdf->render();*/
        require_once(Yii::app()->basePath . '/extensions/dompdf/autoload.inc.php');
        require_once(Yii::app()->basePath . '/extensions/dompdf/lib/Cpdf.php');
        $dompdf = new Dompdf();
        $dompdf->loadHtml($pdf);

        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        //$dompdf->stream();
        //die;
        Yii::app()->getRequest()->sendFile('Cotizacion-' . $cotizacion->id . '.pdf', $dompdf->output());
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Cotizacion the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Cotizacion::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Cotizacion $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cotizacion-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAgregarProducto() {
        $postIdProductoServicio = $_POST['Cotizacionproductoservicio']['idProductoServicio'];
        $postCantidad = $_POST['Cotizacionproductoservicio']['cantidad'];
        $postIdCatEsquemaImpuesto = $_POST['Cotizacionproductoservicio']['idCatEsquemaImpuesto'];
        if(empty($_POST['Cotizacionproductoservicio']['descuento'])){
            $postdescuento = 0;
        }else{
            $postdescuento = $_POST['Cotizacionproductoservicio']['descuento'];
        }
        $postprecio = $_POST['Cotizacionproductoservicio']['precio'];
        if (!empty($postIdProductoServicio) && $postCantidad > 0 && $postdescuento >= 0 && $postprecio > 0 && !empty($postIdCatEsquemaImpuesto)) {
            $idProductoServicio = $postIdProductoServicio;
            $idCatEsquemaImpuesto = $postIdCatEsquemaImpuesto;
            if (empty($postdescuento) || $postdescuento < 0) {
                $descuento = 0;
            } else {
                $descuento = $postdescuento;
            }
            $cantidad = $postCantidad;
            $productoModel = Productoservicio::model()->findByPk($_POST['Cotizacionproductoservicio']['idProductoServicio']);
            $producto = $productoModel->descripcion;
            $precio = $postprecio;
            $criteriaImp=new CDbCriteria();
            $criteriaImp->compare("idCatEsquemaImpuesto",$postIdCatEsquemaImpuesto);
            $impuestos=Viewesquemaimpuesto::model()->findAll($criteriaImp);

            $impRetenido = 0.0;
            $impTrasladado = 0.0;
            foreach ($impuestos as $impuesto) {
                if ($impuesto->aplicacion == '-') {
                    $impRetenido +=  (($postprecio * $postCantidad) - $descuento)*($impuesto->tasa /100);
                } else {
                    $impTrasladado +=  (($postprecio * $postCantidad) - $descuento)*($impuesto->tasa /100);
                }
            }
            $total = $precio * $cantidad - $descuento - $impRetenido + $impTrasladado;
            echo CJSON::encode(array(
                'status' => 'correcto',
                "idProductoServicio" => $idProductoServicio,
                "descuento" => $descuento,
                "producto" => $producto,
                "cantidad" => $cantidad,
                "precio" => $precio,
                "idImpuesto" => $postIdCatEsquemaImpuesto,
                "subtotal" => $postprecio * $postCantidad,
                "tralados" => $impTrasladado,
                "retencion" => $impRetenido,
                "total" => $total,
                "observacion" => $_POST['Cotizacionproductoservicio']['observacion'],
            ));
        } else {
            echo CJSON::encode(array('status' => 'incorrecto'));
        }
    }

    public function actionGetPrice() {
        $producto = Productoservicio::model()->findByPk($_POST['Cotizacionproductoservicio']['idProductoServicio']);
        echo CJSON::encode(array('precio' => $producto->precio));
    }

}
