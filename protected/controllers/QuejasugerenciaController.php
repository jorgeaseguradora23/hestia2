<?php

class QuejasugerenciaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','Seguimiento','createConCliente'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */


    public function actioncreateConCliente()
    {
        $model=new Quejasugerencia;
        $NuevaQueja=new Quejasugerencia();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $criteria=new CDbCriteria;

        if(isset($_POST["Quejasugerencia"])){

            $NuevaQueja->attributes=$_POST["Quejasugerencia"];
            $NuevaQueja->fechaAlta=date("Y-m-d H:i:s");
            $NuevaQueja->usuario=Yii::app()->user->nombre_usuario;
            $NuevaQueja->estatus="P";
            if($NuevaQueja->validate()){
                if($NuevaQueja->save()){
                    $this->redirect(array('index'));
                }
            }
        }





        $this->render('createconcliente',array(
            'NuevaQueja' => $NuevaQueja
        ));
    }

	public function actionCreate()
	{
		$model=new Quejasugerencia;
        $NuevaQueja=new Quejasugerencia();
        // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        $criteria=new CDbCriteria;
        $ClienteBusqueda=new Viewcliente();
        if(isset($_POST['Viewcliente'])){
            $ClienteBusqueda->attributes=$_POST['Viewcliente'];
            if(isset($ClienteBusqueda->idCatTipoCliente)){
                $criteria->compare("idCatTipoCliente",$ClienteBusqueda->idCatTipoCliente);
            }
            if(isset($ClienteBusqueda->categoria)){
                $criteria->compare("categoria",$ClienteBusqueda->categoria);
            }
            if(isset($ClienteBusqueda->idCatSector)){
                $criteria->compare("idCatSector",$ClienteBusqueda->idCatSector);
            }
            if(isset($ClienteBusqueda->idCatPonderacion)){
                $criteria->compare("idCatPonderacion",$ClienteBusqueda->idCatPonderacion);
            }
            if(isset($ClienteBusqueda->idCatFuenteContratacion)){
                $criteria->compare("idCatFuenteContratacion",$ClienteBusqueda->idCatFuenteContratacion);
            }
            if(isset($ClienteBusqueda->estatus)){
                $criteria->compare("estatus",$ClienteBusqueda->estatus);
            }

            if(isset($ClienteBusqueda->fechaAlta)){
                if($_POST['Viewunidad']['fechaAlta']!=""){
                    $dtFechaI=date_format(date_create(str_replace("/", "-", $_POST['Viewunidad']['fechaAlta'])),"Y-m-d");
                    $dtFechaF=date_format(date_create(str_replace("/", "-", $_POST['Viewunidad']['fechaAltaFin'])),"Y-m-d");

                    $criteria->addBetweenCondition("fechaAlta",$dtFechaI.' 00:00:00',$dtFechaF.' 23:59:59' );
                }

            }
            if(isset($ClienteBusqueda->nombre)){
                $criteria->addCondition("nombre LIKE '%".$ClienteBusqueda->nombre."%'");
            }

        }else{
            $criteria->compare("estatus","W");
        }


        $Clientes=Viewcliente::model()->findAll($criteria);

		$this->render('create',array(
			'Clientes'=>$Clientes,
            'ClienteBusqueda' => $ClienteBusqueda,
            'NuevaQueja' => $NuevaQueja
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Quejasugerencia']))
		{
			$model->attributes=$_POST['Quejasugerencia'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Quejasugerencia');
		$model=Viewquejasugerencia::model()->findAll();
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
            'model' => $model
		));
	}

    public function actionSeguimiento()
    {
        /**
         * DEFINICION OBJETOS VACIOS
         */
        $quejaSugerenciaEdit=new Quejasugerencia();
        $quejaSugerenciaEdit=Quejasugerencia::model()->findByPk($_GET["idQS"]);
        $nuevoSeguimiento=new Quejasugerenciaseguimiento();
        $nuevoAdjunto=new QuejaSugerenciaAdjunto();
        $clienteVisita=new Clientevisita();


        /**
         * GUARDADO DE SEGUIMIENTO
         */
        if(isset($_POST["btnGuardarSeg"])){
            $nuevoSeguimiento->attributes=$_POST["Quejasugerenciaseguimiento"];
            $nuevoSeguimiento->idQuejaSugerencia=$_GET["idQS"];
            $nuevoSeguimiento->fechaAlta=date("Y-m-d H:i:s");
            $nuevoSeguimiento->usuario=Yii::app()->user->nombre_usuario;
            if($nuevoSeguimiento->save()){
                $quejaSugerenciaEdit->estatus="A";
                if($quejaSugerenciaEdit->save()){
                    $this->redirect(array('seguimiento',"idQS" => $_GET["idQS"]));
                }

            }
        }
        /**
         * GUARDADO DE ADJUNTOS
         */

        if(isset($_POST["Quejasugerenciaadjunto"])){

            $nuevoAdjunto->attributes=$_POST["Quejasugerenciaadjunto"];
            $image= CUploadedFile::getInstance($nuevoAdjunto, 'archivo');
            $nuevoAdjunto->idQuejaSugerencia=$_GET["idQS"];
            $nuevoAdjunto->fechaAlta=date("Y-m-d H:i:s");
            $nuevoAdjunto->usuario=Yii::app()->user->nombre_usuario;
            /**
             * SE DEFINE LA RUTA DE GUARDADO DE LAS IMAGES
             */
            $path=Yii::getPathOfAlias('webroot')."/library/qs/";
            $bolguarImagen=false;
            if($image!==NULL){

                $sizeFile=$_FILES["Quejasugerenciaadjunto"]["size"]["archivo"];
                if($sizeFile>0){


                    $nuevoAdjunto->archivo=$image->name;
                    $ext=explode(".",$image->name);
                    $ext=strtoupper($ext[count($ext)-1]);
                    $allowed =  array('png' ,'jpg','jpeg','PNG','JPG','JPEG','ZIP','RAR','PDF','DOCX','XLSX','XLS');

                    if(!in_array($ext,$allowed)){
                        $nuevoAdjunto->archivo=NULL;
                        $nuevoAdjunto->addError("archivo","Tipo de Archivo incorrecto solo PNG/JPG/JPEG/PDF/ZIP/DOCX");
                        //$errorString.="<br><span style='font-size: 12px;'>* El tama&ntilde;o del archivo es: [".$size." bytes".($path.$image)."]</span>";
                    }else{

                        $bolguarImagen=true;
                        $nuevoAdjunto->archivo=$image->name;
                    }
                }else{
                    $nuevoAdjunto->addError("archivo","Lo siento, exede el tamñano maximo de almacenamiento");
                    $nuevoAdjunto->archivo=NULL;
                }
            }else{

                $nuevoAdjunto->archivo=NULL;
                $nuevoAdjunto->addError("archivo","Lo siento, no se encontro ningun archivo que subir");
            }

            if($bolguarImagen==true){
                if($nuevoAdjunto->save()){
                    $quejaSugerenciaEdit->estatus="A";
                    if($quejaSugerenciaEdit->save()){
                        if($bolguarImagen==true){
                            $name_file=$nuevoAdjunto->id.".".mb_strtolower($ext);
                            if($image->saveAs($path.$name_file)){
                                /**
                                 * ACTUALIZA EL NOMBRE DEL ARCHIVO UNA VEZ QUE HA SUBIDO
                                 */
                                $nuevoAdjunto->archivo=$name_file;
                                if($nuevoAdjunto->update());
                            }
                        }
                        $this->redirect(array('seguimiento',"idQS" => $_GET["idQS"]));
                    }

                }
            }

        }
        /**
         * CIERRE DE LA QUEJA
         */


        if(isset($_POST["Quejasugerencia"])){
            $quejaSugerenciaEdit->scenario = "cierre";
            $clienteVisita->attributes=$_POST["Clientevisita"];
            $quejaSugerenciaEdit->attributes=$_POST["Quejasugerencia"];

            $clienteVisita->idCliente=$quejaSugerenciaEdit->idCliente;
            $clienteVisita->tipo="R";
            $clienteVisita->usuario=Yii::app()->user->nombre_usuario;
            $clienteVisita->estatus="P";
            $clienteVisita->confirmado="S";

            $clienteVisita->idQuejaSugerencia=$quejaSugerenciaEdit->id;

            if(isset($_POST["Quejasugerencia"]["idEmpleado"])){
                $clienteVisita->idEmpleado=$_POST["Quejasugerencia"]["idEmpleado"];
            }else{
                $clienteVisita->idEmpleado=1;
            }


            $clienteVisita->fechaVisita= "{$_POST["Quejasugerencia"]["fechaAcuerdo"]} {$clienteVisita->_horaVisita}:00";
            $clienteVisita->observacion=$_POST["Quejasugerencia"]["observacionAcuerdo"];

            if($clienteVisita->validate()){
                $guardaFinal=false;
                if(isset($_POST["Quejasugerencia"]["idCatAcuerdo"]) && ($_POST["Quejasugerencia"]["idCatAcuerdo"]!="")){
                    $quejaSugerenciaEdit->idCatAcuerdo=$_POST["Quejasugerencia"]["idCatAcuerdo"];
                    $guardaFinal=true;
                }else{
                    $guardaFinal=false;
                    $quejaSugerenciaEdit->idCatAcuerdo=null;
                    $clienteVisita->addError("idEmpleado","Selecciona un tipo de acuerdo, no puede ser nulo.");
                }
               /* if(isset($_POST["Quejasugerencia"]["idEmpleadoCierre"]) && ($_POST["Quejasugerencia"]["idEmpleadoCierre"]!="")){
                    $quejaSugerenciaEdit->idEmpleadoCierre=$_POST["Quejasugerencia"]["idEmpleadoCierre"];
                    $guardaFinal=true;
                }else{
                    $guardaFinal=false;
                    $quejaSugerenciaEdit->idEmpleadoCierre=null;
                    $clienteVisita->addError("idEmpleado","Selecciona un empleado responsable.");
                }*/

                $quejaSugerenciaEdit->fechaAcuerdo=$_POST["Quejasugerencia"]["fechaAcuerdo"];
                $quejaSugerenciaEdit->observacionAcuerdo=$_POST["Quejasugerencia"]["observacionAcuerdo"];
                $quejaSugerenciaEdit->estatus="C";
                $quejaSugerenciaEdit->estatusAcuerdo="P";
                $quejaSugerenciaEdit->fechaCierre=date("Y-m-d H:i:s");
                $quejaSugerenciaEdit->usuarioCierre=Yii::app()->user->nombre_usuario;

                if($quejaSugerenciaEdit->validate()){
                    if($clienteVisita->validate()){
                        if($guardaFinal==true){
                            if($quejaSugerenciaEdit->update()){
                              if($clienteVisita->save()){
                                  $this->redirect(array('seguimiento',"idQS" => $_GET["idQS"]));
                              }

                            }
                        }
                    }
                }



            }
        }




        $queja=Viewquejasugerencia::model()->find("id=:id",array(":id"=>$_GET["idQS"]));
        $criteria=new CDbCriteria();
        $criteria->compare("idQuejaSugerencia",$_GET["idQS"]);
        $seguimientos=ViewQuejaSugerenciaSeguimiento::model()->findAll($criteria);
        $criteria->order="fechaAlta Desc";
        $adjuntos=ViewQsAdjunto::model()->findAll($criteria);
        $dataProvider=new CActiveDataProvider('Quejasugerencia');
        $model=Viewquejasugerencia::model()->findAll();

        $this->render('seguimiento',array(
            'dataProvider'=>$dataProvider,
            'queja' => $queja,
            'nuevoSeguimiento' => $nuevoSeguimiento,
            'seguimientos' => $seguimientos,
            'nuevoAdjunto' => $nuevoAdjunto,
            'adjuntos' => $adjuntos,
            'quejaSugerenciaEdit' => $quejaSugerenciaEdit,
            'clienteVisita' => $clienteVisita
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Quejasugerencia('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Quejasugerencia']))
			$model->attributes=$_GET['Quejasugerencia'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Quejasugerencia the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Quejasugerencia::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Quejasugerencia $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='quejasugerencia-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
