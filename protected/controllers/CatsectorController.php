<?php

class CatsectorController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','eliminar'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex() {
        $criteria = new CDbCriteria;
        $new = new Catsector;
        $busqueda = new Catsector;
        if (isset($_POST["btnNuevo"])) {

            if (isset($_POST['Catsector'])) {
                $new->attributes = $_POST['Catsector'];
                $new->usuario= Yii::app()->user->nombre_usuario;
                $new->fechaAlta=date('Y-m-d H:i:s');
                
                if ($new->validate()) {
                    if ($new->save()) {
                        Yii::app()->user->setFlash('success', 'El Registro ha sido realizado.');
                        $this->redirect(array('index'));
                    }
                }
            }
        }


        if (isset($_POST["btnBuscar"])) {

            if (isset($_POST['Catsector'])){
                $busqueda->attributes = $_POST['Catsector'];

                if (isset($busqueda->usuario)) {
                    $criteria->compare("usuario", $busqueda->usuario);
                }
            }
        }
        $catalogo = Catsector::model()->findAll(); 
        $this->render('index', array(
            'new' => $new,
            'busqueda' => $busqueda,
            'catalogo' => $catalogo,
        ));
    }
    
    public function actionEliminar($id){
        $modelDelete = Catsector::model()->findByPk($id);
        if (!empty($modelDelete)) {
            try {
                $modelDelete->delete();
                Yii::app()->user->setFlash('success', 'El Registro ha sido eliminado.');
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', 'El Registro no puede ser eliminado debido a que existe una relación con algun otro catálogo.');
            }
        }else{
            Yii::app()->user->setFlash('error', 'El Registro no puede ser eliminado debido a que no existe su ID.');
        }
        $this->redirect(array('index'));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Catsector the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Catsector::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Catsector $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='catsector-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
