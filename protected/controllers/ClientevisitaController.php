<?php

class ClientevisitaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','create','createConCliente','view','CreateConIdVisita'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex() {
        $criteria = new CDbCriteria;
        $busqueda = new Clientevisita;

        /*if (isset($_POST["btnNuevo"])) {

            if (isset($_POST['Catcolonia'])) {
                $new->attributes = $_POST['Catcolonia'];
                $new->usuario = Yii::app()->user->nombre_usuario;

                if ($new->validate()) {
                    if ($new->save()) {
                        Yii::app()->user->setFlash('success', 'El Registro ha sido realizado.');
                        $this->redirect(array('index'));
                    }
                }
            }
        }*/

        if (!empty($_GET['Clientevisita'])) {
            $busqueda->attributes = $_GET['Clientevisita'];
            if(!empty($busqueda->fechaVisita)){
                $fechaInicio=$busqueda->fechaVisita;
            }
        }elseif(isset($_GET['fechaInicio']) && !empty($_GET['fechaInicio'])){
			$fechaInicio=$_GET['fechaInicio'];
        }
        

        $arrayMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        $empleados = CHtml::listData(Viewempleado::model()->findAll(),'id','nombre');

        $estaSemana=array();
        if(isset($fechaInicio)){
            if(date('N',strtotime($fechaInicio))==1){
                $estaSemana[]=$arrayMeses[date('n',strtotime($fechaInicio))-1].' '.date('j',strtotime($fechaInicio));
                $estaSemanaID[]=date('Y-m-d',strtotime($fechaInicio));
                $estaSemanaCompleto=strtotime(date('Y-m-d',strtotime($fechaInicio)));
            }else{
                $estaSemana[]=$arrayMeses[date('n',strtotime($fechaInicio))-1].' '.date('j',strtotime('last monday',strtotime($fechaInicio)));
                $estaSemanaID[]=date('Y-m-d',strtotime('last monday',strtotime($fechaInicio)));
                $estaSemanaCompleto=strtotime(date('Y-m-d',strtotime('last monday',strtotime($fechaInicio))));
            }
        }else{
            if(date('N')==1){
                $estaSemana[]=$arrayMeses[date('n')-1].' '.date('j');
                $estaSemanaID[]=date('Y-m-d');
                $estaSemanaCompleto=strtotime(date('Y-m-d'));
            }else{
                $estaSemana[]=$arrayMeses[date('n')-1].' '.date('j',strtotime('last monday'));
                $estaSemanaID[]=date('Y-m-d',strtotime('last monday'));
                $estaSemanaCompleto=strtotime(date('Y-m-d',strtotime('last monday')));
            }
        }
        for($i=1;$i<=6;$i++){
            $estaSemana[]=$arrayMeses[date('n',strtotime('+'.$i.' day',$estaSemanaCompleto))-1].' '.date('j',strtotime('+'.$i.' day',$estaSemanaCompleto));
            $estaSemanaID[]=date('Y-m-d',strtotime('+'.$i.' day',$estaSemanaCompleto));
        }

        $this->render('index', array(
            //'new' => $new,
            'busqueda' => $busqueda,
            //'catalogo' => $catalogo,
            'estaSemana' => $estaSemana,
            'estaSemanaID' => $estaSemanaID,
            'estaSemanaCompleto' => $estaSemanaCompleto,
            'arrayMeses' => $arrayMeses,
            'empleados' => $empleados,

        ));
	}
	
	public function actionCreate() {

        $criteria = new CDbCriteria;
        $ClienteBusqueda = new Viewcliente();
        if (isset($_POST['Viewcliente'])) {
            $ClienteBusqueda->attributes = $_POST['Viewcliente'];
            if (isset($ClienteBusqueda->idCatTipoCliente)) {
                $criteria->compare("idCatTipoCliente", $ClienteBusqueda->idCatTipoCliente);
            }
            if (isset($ClienteBusqueda->categoria)) {
                $criteria->compare("categoria", $ClienteBusqueda->categoria);
            }
            if (isset($ClienteBusqueda->idCatSector)) {
                $criteria->compare("idCatSector", $ClienteBusqueda->idCatSector);
            }
            if (isset($ClienteBusqueda->idCatPonderacion)) {
                $criteria->compare("idCatPonderacion", $ClienteBusqueda->idCatPonderacion);
            }
            if (isset($ClienteBusqueda->idCatFuenteContratacion)) {
                $criteria->compare("idCatFuenteContratacion", $ClienteBusqueda->idCatFuenteContratacion);
            }
            if (isset($ClienteBusqueda->estatus)) {
                $criteria->compare("estatus", $ClienteBusqueda->estatus);
            }

            if (isset($ClienteBusqueda->fechaAlta)) {
                if ($_POST['Viewunidad']['fechaAlta'] != "") {
                    $dtFechaI = date_format(date_create(str_replace("/", "-", $_POST['Viewunidad']['fechaAlta'])), "Y-m-d");
                    $dtFechaF = date_format(date_create(str_replace("/", "-", $_POST['Viewunidad']['fechaAltaFin'])), "Y-m-d");

                    $criteria->addBetweenCondition("fechaAlta", $dtFechaI . ' 00:00:00', $dtFechaF . ' 23:59:59');
                }
            }
        }



        $Clientes = Viewcliente::model()->findAll($criteria);

        $this->render('create', array(
            'Clientes' => $Clientes,
            'ClienteBusqueda' => $ClienteBusqueda,
        ));
    }

    public function actionEliminar($id) {
        $modelDelete = Catcolonia::model()->findByPk($id);
        if (!empty($modelDelete)) {
            try {
                $modelDelete->delete();
                Yii::app()->user->setFlash('success', 'El Registro ha sido eliminado.');
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', 'El Registro no puede ser eliminado debido a que existe una relación con algun otro catálogo.');
            }
        } else {
            Yii::app()->user->setFlash('error', 'El Registro no puede ser eliminado debido a que no existe su ID.');
        }
        $this->redirect(array('index'));
    }

    public function actionCreateConCliente() {
        $cliente = Cliente::model()->findByPk($_GET['idCliente']);
        $visita = new Clientevisita;
        $empleados = CHtml::listData(Viewempleado::model()->findAll(),'id','nombre');

        if (isset($_POST["Clientevisita"])) {
            //var_dump($_POST);die;
            $visita->attributes = $_POST["Clientevisita"];
            $fechaVisita=$visita->fechaVisita." ".$visita->_horaVisita.":00";
           // var_dump($fechaVisita);die;

            $visita->idCliente = $cliente->id;
            $visita->usuario = Yii::app()->user->nombre_usuario;
            $visita->estatus = "P";
            $visita->fechaVisita=$fechaVisita;
            if ($visita->save()) {
                $this->redirect(CController::createUrl('clientevisita/view', array('id' => $visita->id)));
            }
            $visita->fechaVisita=$_POST["Clientevisita"]["fechaVisita"];
        }
        $this->render('createConCliente', array(
            'cliente' => $cliente,
            'visita' => $visita,
            'empleados'=>$empleados,
        ));
    }

    public function actionCreateConIdVisita($id) {
        $visita = new Clientevisita;
        $oldVisita= CLientevisita::model()->findByPk($id);
        $cliente = Cliente::model()->findByPk($oldVisita->idCliente);
        $visita->idClienteVisita=$oldVisita->id;
        $visita->tipo = $oldVisita->tipo;
        $visita->idEmpleado = $oldVisita->idEmpleado;
        $visita->idCatTipoVisita = $oldVisita->idCatTipoVisita;
        $empleados = CHtml::listData(Viewempleado::model()->findAll(),'id','nombre');

        if (isset($_POST["Clientevisita"])) {
            //var_dump($_POST);die;
            $visita->attributes = $_POST["Clientevisita"];
            $visita->idCliente = $cliente->id;
            $visita->usuario = Yii::app()->user->nombre_usuario;
            $visita->tipo = $oldVisita->tipo;
            $visita->estatus = "P";
            $visita->fechaVisita="{$visita->fechaVisita} {$visita->_horaVisita}:00";
            if ($visita->save()) {
                $oldVisita->estatus='R';
                $oldVisita->update();
                $this->redirect(CController::createUrl('clientevisita/view', array('id' => $visita->id)));
            }
            $visita->fechaVisita=$_POST["Clientevisita"]["fechaVisita"];
        }
        $this->render('createConCliente', array(
            'cliente' => $cliente,
            'visita' => $visita,
            'empleados'=>$empleados,
        ));
    }

    public function actionView($id) {
	    $updateVisita=$this->loadModel($id);
        $visita = ViewClienteVisita::model()->find("id=:id",array(":id" => $id));
        $seguimiento= new Clientevisitaseguimiento();
        $nuevoInvitado=new Clientevisitainvitado();
        $criteria=new CDbCriteria();
        $criteria->compare("idClienteVisita",$id);

        $criteria->order="fechaAlta DESC";
        $seguimientos= Clientevisitaseguimiento::model()->findAll($criteria);
        $empleados = CHtml::listData(Viewempleado::model()->findAll(),'id','nombre');
        $criteriaVs=new CDbCriteria();
        $criteriaVs->compare("id",$id);
        $invitados=ViewClienteVisitaInvitado::model()->findAll($criteriaVs);



        if(isset($_POST["Clientevisitainvitado"])){
            $nuevoInvitado->attributes=$_POST["Clientevisitainvitado"];
            $nuevoInvitado->fechaAlta=date("Y-m-d H:i:s");
            $nuevoInvitado->usuario=Yii::app()->user->nombre_usuario;
            $nuevoInvitado->idClienteVisita=$id;
            if($nuevoInvitado->save()){
                $this->redirect(CController::createUrl('clientevisita/view', array('id' => $id)));
            }
        }

        if (isset($_POST["Clientevisita"])) {
            $updateVisita->scenario = "finaliza";
            $updateVisita->attributes = $_POST["Clientevisita"];
            $updateVisita->estatus = "A";
            $updateVisita->fechaAtencion=date("Y-m-d H:i:s");
            $updateVisita->usuarioAtencion=Yii::app()->user->nombre_usuario;
            if ($updateVisita->save()) {
                $this->redirect(CController::createUrl('clientevisita/view', array('id' => $id)));
            }else{

            }
        }
        if (isset($_POST["Clientevisitaseguimiento"])) {
            $seguimiento->attributes = $_POST["Clientevisitaseguimiento"];
            $seguimiento->fechaAlta = date('Y-m-d H:i:s');
            $seguimiento->usuario = Yii::app()->user->nombre_usuario;
            $seguimiento->idClienteVisita = $id;
            if ($seguimiento->save()) {
                $this->redirect(CController::createUrl('clientevisita/view', array('id' => $id)));
            }
        }

        if(isset($_POST["empleado_id"])){
            $invitadoElimina=Clientevisitainvitado::model()->find("id=:id",array(":id" => $_POST["empleado_id"]));
            if($invitadoElimina->delete()){
                $this->redirect(CController::createUrl('clientevisita/view', array('id' => $id)));
            }
        }

        if(isset($_POST["evento_confirma"])){

            $update=Clientevisita::model()->find("id=:id",array(":id" => $id));
            $update->confirmado=$_POST["evento_confirma"];
            if($update->update()){
                $this->redirect(CController::createUrl('clientevisita/view', array('id' => $id)));
            }
        }


        $this->render('view', array(
            'visita' => $visita,
            'empleados' => $empleados,
            'seguimiento' => $seguimiento,
            'seguimientos' => $seguimientos,
            'nuevoInvitado' => $nuevoInvitado,
            'updateVisita' => $updateVisita,
            'invitados' => $invitados,
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Clientevisita the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Clientevisita::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Clientevisita $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='clientevisita-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
