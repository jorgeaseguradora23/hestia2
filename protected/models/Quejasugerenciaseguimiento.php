<?php

/**
 * This is the model class for table "quejasugerenciaseguimiento".
 *
 * The followings are the available columns in table 'quejasugerenciaseguimiento':
 * @property string $id
 * @property string $idQuejaSugerencia
 * @property string $comentario
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $idCatTipoSeguimiento
 * @property string $participacionTercero
 *
 * The followings are the available model relations:
 * @property Quejasugerencia $idQuejaSugerencia0
 * @property Cattiposeguimiento $idCatTipoSeguimiento0
 */
class Quejasugerenciaseguimiento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quejasugerenciaseguimiento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idQuejaSugerencia, comentario, fechaAlta, usuario, idCatTipoSeguimiento, participacionTercero', 'required'),
			array('idQuejaSugerencia, idCatTipoSeguimiento', 'length', 'max'=>10),
			array('usuario', 'length', 'max'=>45),
			array('participacionTercero', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idQuejaSugerencia, comentario, fechaAlta, usuario, idCatTipoSeguimiento, participacionTercero', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idQuejaSugerencia0' => array(self::BELONGS_TO, 'Quejasugerencia', 'idQuejaSugerencia'),
			'idCatTipoSeguimiento0' => array(self::BELONGS_TO, 'Cattiposeguimiento', 'idCatTipoSeguimiento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idQuejaSugerencia' => 'Id Queja Sugerencia',
			'comentario' => 'Comentario',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'idCatTipoSeguimiento' => 'Id Cat Tipo Seguimiento',
			'participacionTercero' => 'Participacion Tercero',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idQuejaSugerencia',$this->idQuejaSugerencia,true);
		$criteria->compare('comentario',$this->comentario,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('idCatTipoSeguimiento',$this->idCatTipoSeguimiento,true);
		$criteria->compare('participacionTercero',$this->participacionTercero,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Quejasugerenciaseguimiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
