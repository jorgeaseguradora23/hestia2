<?php

/**
 * This is the model class for table "clienteproductoservicio".
 *
 * The followings are the available columns in table 'clienteproductoservicio':
 * @property string $id
 * @property string $idCliente
 * @property string $idProductoServicio
 * @property string $idCatTipoServicio
 * @property string $cantidad
 * @property string $precio
 * @property string $fechaInicio
 * @property string $fechaFinal
 * @property string $idCatTipoMonitoreo
 * @property string $estatus
 * @property string $idCatEsquemaImpuesto
 * @property string $idCotizacion
 * @property string $tipoRegistro
 * @property string $idClienteContrato
 *
 * The followings are the available model relations:
 * @property Clientecontrato $idClienteContrato0
 * @property Cliente $idCliente0
 * @property Productoservicio $idProductoServicio0
 * @property Cattiposervicio $idCatTipoServicio0
 * @property Cattipomonitoreo $idCatTipoMonitoreo0
 * @property Catesquemaimpuesto $idCatEsquemaImpuesto0
 * @property Cotizacion $idCotizacion0
 */
class Clienteproductoservicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clienteproductoservicio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, idProductoServicio, idCatTipoServicio, cantidad, precio, fechaInicio, fechaFinal, estatus, tipoRegistro', 'required'),
			array('idCliente, idProductoServicio, idCatTipoServicio, idCatTipoMonitoreo, idCatEsquemaImpuesto, idCotizacion, idClienteContrato', 'length', 'max'=>10),
			array('cantidad, precio', 'length', 'max'=>22),
			array('estatus, tipoRegistro', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCliente, idProductoServicio, idCatTipoServicio, cantidad, precio, fechaInicio, fechaFinal, idCatTipoMonitoreo, estatus, idCatEsquemaImpuesto, idCotizacion, tipoRegistro, idClienteContrato', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idClienteContrato0' => array(self::BELONGS_TO, 'Clientecontrato', 'idClienteContrato'),
			'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
			'idProductoServicio0' => array(self::BELONGS_TO, 'Productoservicio', 'idProductoServicio'),
			'idCatTipoServicio0' => array(self::BELONGS_TO, 'Cattiposervicio', 'idCatTipoServicio'),
			'idCatTipoMonitoreo0' => array(self::BELONGS_TO, 'Cattipomonitoreo', 'idCatTipoMonitoreo'),
			'idCatEsquemaImpuesto0' => array(self::BELONGS_TO, 'Catesquemaimpuesto', 'idCatEsquemaImpuesto'),
			'idCotizacion0' => array(self::BELONGS_TO, 'Cotizacion', 'idCotizacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'idProductoServicio' => 'Id Producto Servicio',
			'idCatTipoServicio' => 'Id Cat Tipo Servicio',
			'cantidad' => 'Cantidad',
			'precio' => 'Precio',
			'fechaInicio' => 'Fecha Inicio',
			'fechaFinal' => 'Fecha Final',
			'idCatTipoMonitoreo' => 'Id Cat Tipo Monitoreo',
			'estatus' => 'Estatus',
			'idCatEsquemaImpuesto' => 'Id Cat Esquema Impuesto',
			'idCotizacion' => 'Id Cotizacion',
			'tipoRegistro' => 'Tipo Registro',
			'idClienteContrato' => 'Id Cliente Contrato',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('idProductoServicio',$this->idProductoServicio,true);
		$criteria->compare('idCatTipoServicio',$this->idCatTipoServicio,true);
		$criteria->compare('cantidad',$this->cantidad,true);
		$criteria->compare('precio',$this->precio,true);
		$criteria->compare('fechaInicio',$this->fechaInicio,true);
		$criteria->compare('fechaFinal',$this->fechaFinal,true);
		$criteria->compare('idCatTipoMonitoreo',$this->idCatTipoMonitoreo,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('idCatEsquemaImpuesto',$this->idCatEsquemaImpuesto,true);
		$criteria->compare('idCotizacion',$this->idCotizacion,true);
		$criteria->compare('tipoRegistro',$this->tipoRegistro,true);
		$criteria->compare('idClienteContrato',$this->idClienteContrato,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clienteproductoservicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
