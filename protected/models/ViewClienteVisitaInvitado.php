<?php

/**
 * This is the model class for table "viewClienteVisitaInvitado".
 *
 * The followings are the available columns in table 'viewClienteVisitaInvitado':
 * @property string $id
 * @property string $idInvitado
 * @property string $idEmpleado
 * @property string $foto
 * @property string $nombrePila
 * @property string $apPaterno
 * @property string $apMaterno
 * @property string $nombre
 */
class ViewClienteVisitaInvitado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewClienteVisitaInvitado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEmpleado, foto, nombrePila, apPaterno, apMaterno', 'required'),
			array('id, idInvitado, idEmpleado', 'length', 'max'=>10),
			array('foto', 'length', 'max'=>120),
			array('nombrePila, apPaterno, apMaterno', 'length', 'max'=>80),
			array('nombre', 'length', 'max'=>242),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idInvitado, idEmpleado, foto, nombrePila, apPaterno, apMaterno, nombre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idInvitado' => 'Id Invitado',
			'idEmpleado' => 'Id Empleado',
			'foto' => 'Foto',
			'nombrePila' => 'Nombre Pila',
			'apPaterno' => 'Ap Paterno',
			'apMaterno' => 'Ap Materno',
			'nombre' => 'Nombre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idInvitado',$this->idInvitado,true);
		$criteria->compare('idEmpleado',$this->idEmpleado,true);
		$criteria->compare('foto',$this->foto,true);
		$criteria->compare('nombrePila',$this->nombrePila,true);
		$criteria->compare('apPaterno',$this->apPaterno,true);
		$criteria->compare('apMaterno',$this->apMaterno,true);
		$criteria->compare('nombre',$this->nombre,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ViewClienteVisitaInvitado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
