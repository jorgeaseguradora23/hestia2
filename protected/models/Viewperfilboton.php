<?php

/**
 * This is the model class for table "viewperfilboton".
 *
 * The followings are the available columns in table 'viewperfilboton':
 * @property string $idCatPerfil
 * @property string $id
 * @property string $idCatMenu
 * @property string $menu
 * @property string $icon
 * @property string $color
 * @property string $seccion
 * @property string $boton
 * @property string $link
 * @property string $idPadre
 * @property string $hijos
 */
class Viewperfilboton extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewperfilboton';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCatPerfil, idCatMenu, menu, icon, color, seccion, boton, link', 'required'),
			array('idCatPerfil, id, idCatMenu, seccion, idPadre', 'length', 'max'=>10),
			array('menu, boton', 'length', 'max'=>60),
			array('icon', 'length', 'max'=>45),
			array('color', 'length', 'max'=>30),
			array('link', 'length', 'max'=>240),
			array('hijos', 'length', 'max'=>21),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idCatPerfil, id, idCatMenu, menu, icon, color, seccion, boton, link, idPadre, hijos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCatPerfil' => 'Id Cat Perfil',
			'id' => 'ID',
			'idCatMenu' => 'Id Cat Menu',
			'menu' => 'Menu',
			'icon' => 'Icon',
			'color' => 'Color',
			'seccion' => 'Seccion',
			'boton' => 'Boton',
			'link' => 'Link',
			'idPadre' => 'Id Padre',
			'hijos' => 'Hijos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCatPerfil',$this->idCatPerfil,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCatMenu',$this->idCatMenu,true);
		$criteria->compare('menu',$this->menu,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('seccion',$this->seccion,true);
		$criteria->compare('boton',$this->boton,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('idPadre',$this->idPadre,true);
		$criteria->compare('hijos',$this->hijos,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Viewperfilboton the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
