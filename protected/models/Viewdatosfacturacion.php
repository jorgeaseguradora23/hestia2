<?php

/**
 * This is the model class for table "viewdatosfacturacion".
 *
 * The followings are the available columns in table 'viewdatosfacturacion':
 * @property string $id
 * @property string $idCliente
 * @property string $rfc
 * @property string $razonSocial
 * @property string $direccion
 * @property string $usoCFDI
 * @property string $metodoPago
 * @property string $formaPago
 */
class Viewdatosfacturacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewdatosfacturacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, rfc, razonSocial, usoCFDI, metodoPago, formaPago', 'required'),
			array('id, idCliente', 'length', 'max'=>10),
			array('rfc', 'length', 'max'=>13),
			array('razonSocial, usoCFDI', 'length', 'max'=>360),
			array('metodoPago', 'length', 'max'=>60),
			array('formaPago', 'length', 'max'=>45),
			array('direccion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCliente, rfc, razonSocial, direccion, usoCFDI, metodoPago, formaPago', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'rfc' => 'Rfc',
			'razonSocial' => 'Razon Social',
			'direccion' => 'Direccion',
			'usoCFDI' => 'Uso Cfdi',
			'metodoPago' => 'Metodo Pago',
			'formaPago' => 'Forma Pago',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('rfc',$this->rfc,true);
		$criteria->compare('razonSocial',$this->razonSocial,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('usoCFDI',$this->usoCFDI,true);
		$criteria->compare('metodoPago',$this->metodoPago,true);
		$criteria->compare('formaPago',$this->formaPago,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Viewdatosfacturacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
