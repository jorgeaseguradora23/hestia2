<?php

/**
 * This is the model class for table "viewcliente".
 *
 * The followings are the available columns in table 'viewcliente':
 * @property string $id
 * @property string $idCatTipoCliente
 * @property string $textTipoCliente
 * @property string $nombre
 * @property string $categoria
 * @property string $categoriaCliente
 * @property string $estatus
 * @property string $textEstatus
 * @property string $colorEstatus
 * @property string $idCatSector
 * @property string $sector
 * @property string $calle
 * @property string $numExt
 * @property string $numInt
 * @property string $colonia
 * @property string $tipoColonia
 * @property string $municipio
 * @property string $estado
 * @property string $ciudad
 * @property string $pais
 * @property string $cp
 * @property integer $idCatcolonia
 * @property string $telefono
 * @property string $movil
 * @property string $idCatPonderacion
 * @property string $ponderacion
 * @property string $eMail
 * @property string $comentario
 * @property string $idCatFuenteContratacion
 * @property string $eMailFacturacion
 * @property string $fuenteContratacion
 * @property string $fechaProspeccion
 * @property string $fechaAlta
 * @property string $fechaBaja
 * @property string $motivoProspeccion
 * @property string $motivoAlta
 * @property string $motivoBaja
 * @property string $usuarioProspeccion
 * @property string $usuarioAlta
 * @property string $usuarioBaja
 * @property string $observacionProspeccion
 * @property string $observacionAlta
 * @property string $observacionBaja
 * @property string $idCliente
 */
class Viewcliente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewcliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCatTipoCliente, nombre, categoria, estatus, idCatSector, sector, calle, numExt, colonia, tipoColonia, municipio, estado, pais, cp, idCatcolonia, telefono, idCatPonderacion, ponderacion, idCatFuenteContratacion, fuenteContratacion', 'required'),
			array('idCatcolonia', 'numerical', 'integerOnly'=>true),
			array('id, idCatTipoCliente, textEstatus, idCatSector, cp, idCatPonderacion, idCatFuenteContratacion, idCliente', 'length', 'max'=>10),
			array('textTipoCliente, pais, ponderacion, fuenteContratacion, usuarioProspeccion, usuarioAlta, usuarioBaja', 'length', 'max'=>45),
			array('nombre', 'length', 'max'=>1024),
			array('categoria, estatus', 'length', 'max'=>1),
			array('categoriaCliente', 'length', 'max'=>9),
			array('colorEstatus', 'length', 'max'=>7),
			array('sector', 'length', 'max'=>360),
			array('calle', 'length', 'max'=>240),
			array('numExt, numInt, colonia, tipoColonia, municipio, estado, ciudad', 'length', 'max'=>120),
			array('telefono, movil, motivoProspeccion, motivoAlta, motivoBaja', 'length', 'max'=>80),
			array('eMail, comentario, eMailFacturacion, fechaProspeccion, fechaAlta, fechaBaja, observacionProspeccion, observacionAlta, observacionBaja', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCatTipoCliente, textTipoCliente, nombre, categoria, categoriaCliente, estatus, textEstatus, colorEstatus, idCatSector, sector, calle, numExt, numInt, colonia, tipoColonia, municipio, estado, ciudad, pais, cp, idCatcolonia, telefono, movil, idCatPonderacion, ponderacion, eMail, comentario, idCatFuenteContratacion, eMailFacturacion, fuenteContratacion, fechaProspeccion, fechaAlta, fechaBaja, motivoProspeccion, motivoAlta, motivoBaja, usuarioProspeccion, usuarioAlta, usuarioBaja, observacionProspeccion, observacionAlta, observacionBaja, idCliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCatTipoCliente' => 'Id Cat Tipo Cliente',
			'textTipoCliente' => 'Text Tipo Cliente',
			'nombre' => 'Nombre',
			'categoria' => 'Categoria',
			'categoriaCliente' => 'Categoria Cliente',
			'estatus' => 'Estatus',
			'textEstatus' => 'Text Estatus',
			'colorEstatus' => 'Color Estatus',
			'idCatSector' => 'Id Cat Sector',
			'sector' => 'Sector',
			'calle' => 'Calle',
			'numExt' => 'Num Ext',
			'numInt' => 'Num Int',
			'colonia' => 'Colonia',
			'tipoColonia' => 'Tipo Colonia',
			'municipio' => 'Municipio',
			'estado' => 'Estado',
			'ciudad' => 'Ciudad',
			'pais' => 'Pais',
			'cp' => 'Cp',
			'idCatcolonia' => 'Id Catcolonia',
			'telefono' => 'Telefono',
			'movil' => 'Movil',
			'idCatPonderacion' => 'Id Cat Ponderacion',
			'ponderacion' => 'Ponderacion',
			'eMail' => 'E Mail',
			'comentario' => 'Comentario',
			'idCatFuenteContratacion' => 'Id Cat Fuente Contratacion',
			'eMailFacturacion' => 'E Mail Facturacion',
			'fuenteContratacion' => 'Fuente Contratacion',
			'fechaProspeccion' => 'Fecha Prospeccion',
			'fechaAlta' => 'Fecha Alta',
			'fechaBaja' => 'Fecha Baja',
			'motivoProspeccion' => 'Motivo Prospeccion',
			'motivoAlta' => 'Motivo Alta',
			'motivoBaja' => 'Motivo Baja',
			'usuarioProspeccion' => 'Usuario Prospeccion',
			'usuarioAlta' => 'Usuario Alta',
			'usuarioBaja' => 'Usuario Baja',
			'observacionProspeccion' => 'Observacion Prospeccion',
			'observacionAlta' => 'Observacion Alta',
			'observacionBaja' => 'Observacion Baja',
			'idCliente' => 'Id Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCatTipoCliente',$this->idCatTipoCliente,true);
		$criteria->compare('textTipoCliente',$this->textTipoCliente,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('categoria',$this->categoria,true);
		$criteria->compare('categoriaCliente',$this->categoriaCliente,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('textEstatus',$this->textEstatus,true);
		$criteria->compare('colorEstatus',$this->colorEstatus,true);
		$criteria->compare('idCatSector',$this->idCatSector,true);
		$criteria->compare('sector',$this->sector,true);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('numExt',$this->numExt,true);
		$criteria->compare('numInt',$this->numInt,true);
		$criteria->compare('colonia',$this->colonia,true);
		$criteria->compare('tipoColonia',$this->tipoColonia,true);
		$criteria->compare('municipio',$this->municipio,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('ciudad',$this->ciudad,true);
		$criteria->compare('pais',$this->pais,true);
		$criteria->compare('cp',$this->cp,true);
		$criteria->compare('idCatcolonia',$this->idCatcolonia);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('movil',$this->movil,true);
		$criteria->compare('idCatPonderacion',$this->idCatPonderacion,true);
		$criteria->compare('ponderacion',$this->ponderacion,true);
		$criteria->compare('eMail',$this->eMail,true);
		$criteria->compare('comentario',$this->comentario,true);
		$criteria->compare('idCatFuenteContratacion',$this->idCatFuenteContratacion,true);
		$criteria->compare('eMailFacturacion',$this->eMailFacturacion,true);
		$criteria->compare('fuenteContratacion',$this->fuenteContratacion,true);
		$criteria->compare('fechaProspeccion',$this->fechaProspeccion,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('fechaBaja',$this->fechaBaja,true);
		$criteria->compare('motivoProspeccion',$this->motivoProspeccion,true);
		$criteria->compare('motivoAlta',$this->motivoAlta,true);
		$criteria->compare('motivoBaja',$this->motivoBaja,true);
		$criteria->compare('usuarioProspeccion',$this->usuarioProspeccion,true);
		$criteria->compare('usuarioAlta',$this->usuarioAlta,true);
		$criteria->compare('usuarioBaja',$this->usuarioBaja,true);
		$criteria->compare('observacionProspeccion',$this->observacionProspeccion,true);
		$criteria->compare('observacionAlta',$this->observacionAlta,true);
		$criteria->compare('observacionBaja',$this->observacionBaja,true);
		$criteria->compare('idCliente',$this->idCliente,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Viewcliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
