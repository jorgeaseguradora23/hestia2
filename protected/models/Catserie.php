<?php

/**
 * This is the model class for table "catserie".
 *
 * The followings are the available columns in table 'catserie':
 * @property string $id
 * @property string $serie
 * @property string $estatus
 * @property string $tipo
 * @property string $usuario
 * @property string $fechaAlta
 */
class Catserie extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catserie';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('serie, estatus, tipo, usuario, fechaAlta', 'required'),
			array('serie', 'length', 'max'=>3),
			array('estatus, tipo', 'length', 'max'=>1),
			array('usuario', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, serie, estatus, tipo, usuario, fechaAlta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'serie' => 'Serie',
			'estatus' => 'Estatus',
			'tipo' => 'Tipo',
			'usuario' => 'Usuario',
			'fechaAlta' => 'Fecha Alta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('serie',$this->serie,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Catserie the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
