<?php

/**
 * This is the model class for table "cotizacion".
 *
 * The followings are the available columns in table 'cotizacion':
 * @property string $id
 * @property string $idCliente
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $estatus
 * @property string $subtotal
 * @property string $impuestoRetenido
 * @property string $impuestoTrasladado
 * @property string $total
 * @property string $fechaVencimiento
 * @property string $serie
 * @property string $folio
 * @property string $tipo
 * @property string $observacion
 * @property string $idCotizacion
 * @property string idCatMoneda
 * @property string $tipoCambio
 * @property string idCotizacionCierre
 * * @property string $cargoProveedor
 *
 * The followings are the available model relations:
 * @property Cliente $idCliente0
 * @property Cotizacion $idCotizacion0
 * @property Cotizacion[] $cotizacions
 * @property Cotizacionadjunto[] $cotizacionadjuntos
 * @property Cotizacionproductoservicio[] $cotizacionproductoservicios
 */
class Cotizacion extends CActiveRecord
{
    public $_fechaInicio;
    public $_fechaFinal;
    public $_fechaAltaInicio;
    public $_fechaAltaFin;
    public $_fechaVencimientoInicio;
    public $_fechaVencimientoFin;
    public $_idCatTipoMonitoreo;
    public $_idCatTipoServicio;
    public $_idClienteNumero;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cotizacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, fechaAlta, usuario, estatus, subtotal,  impuestoRetenido, impuestoTrasladado, total, fechaVencimiento, serie, folio, tipo, idCatMoneda, tipoCambio',
                'required',
                'message'=>'{attribute}  no puede estar vacío.'),
			array('idCliente, folio, idCotizacion, idCatMoneda, idCotizacionCierre', 'length', 'max'=>10),
			array('usuario', 'length', 'max'=>45),
			array('estatus, tipo, cargoProveedor', 'length', 'max'=>1),
			array('subtotal, impuestoRetenido, impuestoTrasladado, total, costoProveedor, tipoCambio', 'length', 'max'=>22),
			array('serie', 'length', 'max'=>3),
			array('observacion,_idClienteNumero,_fechaAltaInicio,_fechaAltaFin,_fechaVencimientoInicio,_fechaVencimientoFin', 'safe'),
			array('personaCierre,emailCierre,contenidoCierre,_fechaInicio,_fechaFinal,_idCatTipoServicio, cargoProveedor, costoProveedor, idCatMoneda', 'required','on'=>'cierre'),
            array('personaCierre,emailCierre,contenidoCierre, idCatMoneda', 'required','on'=>'rechazo'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCliente, fechaAlta, usuario, estatus, subtotal, impuestoRetenido, impuestoTrasladado, total, fechaVencimiento, serie, folio, tipo, idCatMoneda, observacion, idCotizacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
			'idCotizacion0' => array(self::BELONGS_TO, 'Cotizacion', 'idCotizacion'),
			'cotizacions' => array(self::HAS_MANY, 'Cotizacion', 'idCotizacion'),
			'cotizacionadjuntos' => array(self::HAS_MANY, 'Cotizacionadjunto', 'idCotizacion'),
			'cotizacionproductoservicios' => array(self::HAS_MANY, 'Cotizacionproductoservicio', 'idCotizacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'estatus' => 'Estatus',
			'subtotal' => 'Subtotal',
			'impuestoRetenido' => 'Impuesto Retenido',
			'impuestoTrasladado' => 'Impuesto Trasladado',
			'total' => 'Total',
			'fechaVencimiento' => 'Fecha Vencimiento',
			'serie' => 'Serie',
			'folio' => 'Folio',
			'tipo' => 'Tipo',
			'observacion' => 'Observacion',
			'idCotizacion' => 'Id Cotizacion',
            'cargoProveedor' => 'Tipo de Cargo',
            'personaCierre' => 'Autorización de cierre',
            'costoProveedor' => 'Costo del Proveedor',
            'idCatMoneda' => 'Selecciona una moneda',
            'tipoCambio' => 'Tipo de Cambio'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('subtotal',$this->subtotal,true);
		$criteria->compare('impuestoRetenido',$this->impuestoRetenido,true);
		$criteria->compare('impuestoTrasladado',$this->impuestoTrasladado,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('fechaVencimiento',$this->fechaVencimiento,true);
		$criteria->compare('serie',$this->serie,true);
		$criteria->compare('folio',$this->folio,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('idCotizacion',$this->idCotizacion,true);
        $criteria->compare('cargoProveedor',$this->cargoProveedor,true);
        $criteria->compare('idCatMoneda',$this->idCatMoneda,true);
        $criteria->compare('tipoCambio',$this->tipoCambio,true);
        $criteria->compare('idCotizacionCierre',$this->idCotizacionCierre,true);



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cotizacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
