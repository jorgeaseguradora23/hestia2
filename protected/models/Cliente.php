<?php

/**
 * This is the model class for table "cliente".
 *
 * The followings are the available columns in table 'cliente':
 * @property string $id
 * @property string $idCatTipoCliente
 * @property string $nombre
 * @property string $categoria
 * @property string $estatus
 * @property string $idCatCelula
 * @property string $idCatSector
 * @property string $calle
 * @property string $numExt
 * @property string $numInt
 * @property integer $idCatColonia
 * @property string $telefono
 * @property string $movil
 * @property string $idCatPonderacion
 * @property string $eMail
 * @property string $comentario
 * @property string $idCatFuenteContratacion
 * @property string $idCliente
 * @property string $eMailFacturacion
 * @property string $guid
 * @property string $idCteRelacionado
 * @property string $cp
 * @property string $municipio
 * @property string $estado
 * @property string $ciudad
 * @property string $pais
 *
 * The followings are the available model relations:
 * @property Auditcliente[] $auditclientes
 * @property Cfd[] $cfds
 * @property Catsector $idCatSector0
 * @property Catcelula $idCatCelula0
 * @property Catponderacion $idCatPonderacion0
 * @property Cattipocliente $idCatTipoCliente0
 * @property Catfuentecontratacion $idCatFuenteContratacion0
 * @property Catcolonia $idCatColonia0
 * @property Clienteacceso[] $clienteaccesos
 * @property Clientecontacto[] $clientecontactos
 * @property Clientecontrato[] $clientecontratos
 * @property Clientedatosfacturacion[] $clientedatosfacturacions
 * @property Clientemovimiento[] $clientemovimientos
 * @property Clienteproductoservicio[] $clienteproductoservicios
 * @property Clientevisita[] $clientevisitas
 * @property Cotizacion[] $cotizacions
 * @property Quejasugerencia[] $quejasugerencias
 */
class Cliente extends CActiveRecord
{
    public $cp;
    public $municipio;
    public $estado;
    public $ciudad;
    public $pais;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCatTipoCliente, nombre, categoria, estatus, idCatCelula, idCatSector, calle, numExt, idCatColonia, telefono, 
			idCatPonderacion, municipio,estado, ciudad, pais,
			idCatFuenteContratacion, guid, cp', 'required'),
			array('idCatColonia', 'numerical', 'integerOnly'=>true),
			array('idCatTipoCliente, idCatCelula, idCatSector, idCatPonderacion, idCatFuenteContratacion, idCliente, idCteRelacionado', 'length', 'max'=>10),
			array('nombre', 'length', 'max'=>1024),
			array('categoria, estatus', 'length', 'max'=>1),
			array('calle', 'length', 'max'=>240),
			array('numExt, numInt', 'length', 'max'=>120),
			array('telefono, movil', 'length', 'max'=>80),
			array('guid', 'length', 'max'=>360),
			array('eMail, comentario, eMailFacturacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCatTipoCliente, nombre, categoria, estatus, idCatCelula, idCatSector, calle, numExt, numInt, idCatColonia, telefono, movil, idCatPonderacion, 
			eMail, pais,
			comentario, idCatFuenteContratacion, idCliente, eMailFacturacion,  guid, idCteRelacionado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'auditclientes' => array(self::HAS_MANY, 'Auditcliente', 'idCliente'),
			'cfds' => array(self::HAS_MANY, 'Cfd', 'idCliente'),
			'idCatSector0' => array(self::BELONGS_TO, 'Catsector', 'idCatSector'),
			'idCatCelula0' => array(self::BELONGS_TO, 'Catcelula', 'idCatCelula'),
			'idCatPonderacion0' => array(self::BELONGS_TO, 'Catponderacion', 'idCatPonderacion'),
			'idCatTipoCliente0' => array(self::BELONGS_TO, 'Cattipocliente', 'idCatTipoCliente'),
			'idCatFuenteContratacion0' => array(self::BELONGS_TO, 'Catfuentecontratacion', 'idCatFuenteContratacion'),
			'idCatColonia0' => array(self::BELONGS_TO, 'Catcolonia', 'idCatColonia'),
			'clienteaccesos' => array(self::HAS_MANY, 'Clienteacceso', 'idCliente'),
			'clientecontactos' => array(self::HAS_MANY, 'Clientecontacto', 'idCliente'),
			'clientecontratos' => array(self::HAS_MANY, 'Clientecontrato', 'idCliente'),
			'clientedatosfacturacions' => array(self::HAS_MANY, 'Clientedatosfacturacion', 'idCliente'),
			'clientemovimientos' => array(self::HAS_MANY, 'Clientemovimiento', 'idCliente'),
			'clienteproductoservicios' => array(self::HAS_MANY, 'Clienteproductoservicio', 'idCliente'),
			'clientevisitas' => array(self::HAS_MANY, 'Clientevisita', 'idCliente'),
			'cotizacions' => array(self::HAS_MANY, 'Cotizacion', 'idCliente'),
			'quejasugerencias' => array(self::HAS_MANY, 'Quejasugerencia', 'idCliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCatTipoCliente' => 'Id Cat Tipo Cliente',
			'nombre' => 'Nombre',
			'categoria' => 'Categoria',
			'estatus' => 'Estatus',
			'idCatCelula' => 'Selecciona una celula',
			'idCatSector' => 'Selecciona un sector',
			'calle' => 'Calle',
			'numExt' => 'Num Ext',
			'numInt' => 'Num Int',
			'idCatColonia' => 'Id Cat Colonia',
			'telefono' => 'Telefono',
			'movil' => 'Movil',
			'idCatPonderacion' => 'Selecciona una ponderación',
			'eMail' => 'E Mail',
			'comentario' => 'Comentario',
			'idCatFuenteContratacion' => 'Selecciona una Fuente Contratacion',
			'idCliente' => 'Id Cliente',
			'eMailFacturacion' => 'E Mail Facturacion',
			'guid' => 'Guid',
			'idCteRelacionado' => 'Id Cte Relacionado',
            'cp' => "Código Postal"
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCatTipoCliente',$this->idCatTipoCliente,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('categoria',$this->categoria,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('idCatCelula',$this->idCatCelula,true);
		$criteria->compare('idCatSector',$this->idCatSector,true);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('numExt',$this->numExt,true);
		$criteria->compare('numInt',$this->numInt,true);
		$criteria->compare('idCatColonia',$this->idCatColonia);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('movil',$this->movil,true);
		$criteria->compare('idCatPonderacion',$this->idCatPonderacion,true);
		$criteria->compare('eMail',$this->eMail,true);
		$criteria->compare('comentario',$this->comentario,true);
		$criteria->compare('idCatFuenteContratacion',$this->idCatFuenteContratacion,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('eMailFacturacion',$this->eMailFacturacion,true);
		$criteria->compare('guid',$this->guid,true);
		$criteria->compare('idCteRelacionado',$this->idCteRelacionado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
