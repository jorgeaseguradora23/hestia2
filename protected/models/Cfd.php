<?php

/**
 * This is the model class for table "cfd".
 *
 * The followings are the available columns in table 'cfd':
 * @property string $id
 * @property string $fechaAlta
 * @property string $estatus
 * @property string $idCliente
 * @property string $serie
 * @property string $folio
 * @property string $usuario
 * @property string $subtotal
 * @property string $total
 * @property string $xml
 * @property string $timbre
 * @property string $folioFiscal
 * @property string $pagado
 * @property string $tipo
 * @property string $idCatFormaPago
 * @property string $idCatMetodoPago
 *
 * The followings are the available model relations:
 * @property Cliente $idCliente0
 * @property Catformapago $idCatFormaPago0
 * @property Catmetodopago $idCatMetodoPago0
 * @property Cfdproductoservicio[] $cfdproductoservicios
 */
class Cfd extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cfd';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fechaAlta, estatus, idCliente, serie, folio, usuario, subtotal, total, xml, timbre, folioFiscal, pagado, tipo, idCatFormaPago, idCatMetodoPago', 'required'),
			array('estatus, serie, timbre, pagado, tipo', 'length', 'max'=>1),
			array('idCliente, folio, idCatFormaPago, idCatMetodoPago', 'length', 'max'=>10),
			array('usuario', 'length', 'max'=>45),
			array('subtotal, total', 'length', 'max'=>22),
			array('folioFiscal', 'length', 'max'=>360),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fechaAlta, estatus, idCliente, serie, folio, usuario, subtotal, total, xml, timbre, folioFiscal, pagado, tipo, idCatFormaPago, idCatMetodoPago', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
			'idCatFormaPago0' => array(self::BELONGS_TO, 'Catformapago', 'idCatFormaPago'),
			'idCatMetodoPago0' => array(self::BELONGS_TO, 'Catmetodopago', 'idCatMetodoPago'),
			'cfdproductoservicios' => array(self::HAS_MANY, 'Cfdproductoservicio', 'idCfd'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fechaAlta' => 'Fecha Alta',
			'estatus' => 'Estatus',
			'idCliente' => 'Id Cliente',
			'serie' => 'Serie',
			'folio' => 'Folio',
			'usuario' => 'Usuario',
			'subtotal' => 'Subtotal',
			'total' => 'Total',
			'xml' => 'Xml',
			'timbre' => 'Timbre',
			'folioFiscal' => 'Folio Fiscal',
			'pagado' => 'Pagado',
			'tipo' => 'Tipo',
			'idCatFormaPago' => 'Id Cat Forma Pago',
			'idCatMetodoPago' => 'Id Cat Metodo Pago',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('serie',$this->serie,true);
		$criteria->compare('folio',$this->folio,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('subtotal',$this->subtotal,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('xml',$this->xml,true);
		$criteria->compare('timbre',$this->timbre,true);
		$criteria->compare('folioFiscal',$this->folioFiscal,true);
		$criteria->compare('pagado',$this->pagado,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('idCatFormaPago',$this->idCatFormaPago,true);
		$criteria->compare('idCatMetodoPago',$this->idCatMetodoPago,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cfd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
