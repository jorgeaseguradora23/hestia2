<?php

/**
 * This is the model class for table "unidad".
 *
 * The followings are the available columns in table 'unidad':
 * @property string $idunidad
 * @property string $placa
 * @property string $noEconomico
 * @property string $idCatEngomado
 * @property string $idCatMarca
 * @property string $idCatModelo
 * @property string $idCatTipo
 * @property string $noSerie
 * @property string $noMotor
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $fechaCompra
 * @property string $kilometrajeInicial
 * @property string $guid
 * @property string $idCatestado
 * @property string $idCatUsoUnidad
 *
 * The followings are the available model relations:
 * @property Catengomado $idCatEngomado0
 * @property Catmarca $idCatMarca0
 * @property Catmodelo $idCatModelo0
 * @property Cattipo $idCatTipo0
 * @property Catestado $idCatestado0
 * @property Catusounidad $idCatUsoUnidad0
 * @property Unidadbitacora[] $unidadbitacoras
 * @property Catobligacion[] $catobligacions
 * @property Unidadseguro[] $unidadseguros
 * @property Catservicio[] $catservicios
 */
class Unidad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unidad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('placa, noEconomico, idCatEngomado, idCatMarca, idCatModelo, idCatTipo, noSerie, noMotor, fechaAlta, usuario, fechaCompra, kilometrajeInicial, guid', 'required'),
			array('placa', 'length', 'max'=>12),
			array('noEconomico, idCatEngomado, idCatMarca, idCatModelo, idCatTipo, idCatestado, idCatUsoUnidad', 'length', 'max'=>10),
			array('noSerie, noMotor', 'length', 'max'=>80),
			array('usuario, guid', 'length', 'max'=>45),
			array('kilometrajeInicial', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idunidad, placa, noEconomico, idCatEngomado, idCatMarca, idCatModelo, idCatTipo, noSerie, noMotor, fechaAlta, usuario, fechaCompra, kilometrajeInicial, guid, idCatestado, idCatUsoUnidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCatEngomado0' => array(self::BELONGS_TO, 'Catengomado', 'idCatEngomado'),
			'idCatMarca0' => array(self::BELONGS_TO, 'Catmarca', 'idCatMarca'),
			'idCatModelo0' => array(self::BELONGS_TO, 'Catmodelo', 'idCatModelo'),
			'idCatTipo0' => array(self::BELONGS_TO, 'Cattipo', 'idCatTipo'),
			'idCatestado0' => array(self::BELONGS_TO, 'Catestado', 'idCatestado'),
			'idCatUsoUnidad0' => array(self::BELONGS_TO, 'Catusounidad', 'idCatUsoUnidad'),
			'unidadbitacoras' => array(self::HAS_MANY, 'Unidadbitacora', 'idUnidad'),
			'catobligacions' => array(self::MANY_MANY, 'Catobligacion', 'unidadobligacion(idunidad, idCatObligacion)'),
			'unidadseguros' => array(self::HAS_MANY, 'Unidadseguro', 'idunidad'),
			'catservicios' => array(self::MANY_MANY, 'Catservicio', 'unidadservicio(idunidad, idCatServicio)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idunidad' => 'Idunidad',
			'placa' => 'Placa',
			'noEconomico' => 'No Economico',
			'idCatEngomado' => 'Id Cat Engomado',
			'idCatMarca' => 'Id Cat Marca',
			'idCatModelo' => 'Id Cat Modelo',
			'idCatTipo' => 'Id Cat Tipo',
			'noSerie' => 'No Serie',
			'noMotor' => 'No Motor',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'fechaCompra' => 'Fecha Compra',
			'kilometrajeInicial' => 'Kilometraje Inicial',
			'guid' => 'Guid',
			'idCatestado' => 'Id Catestado',
			'idCatUsoUnidad' => 'Id Cat Uso Unidad',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idunidad',$this->idunidad,true);
		$criteria->compare('placa',$this->placa,true);
		$criteria->compare('noEconomico',$this->noEconomico,true);
		$criteria->compare('idCatEngomado',$this->idCatEngomado,true);
		$criteria->compare('idCatMarca',$this->idCatMarca,true);
		$criteria->compare('idCatModelo',$this->idCatModelo,true);
		$criteria->compare('idCatTipo',$this->idCatTipo,true);
		$criteria->compare('noSerie',$this->noSerie,true);
		$criteria->compare('noMotor',$this->noMotor,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('fechaCompra',$this->fechaCompra,true);
		$criteria->compare('kilometrajeInicial',$this->kilometrajeInicial,true);
		$criteria->compare('guid',$this->guid,true);
		$criteria->compare('idCatestado',$this->idCatestado,true);
		$criteria->compare('idCatUsoUnidad',$this->idCatUsoUnidad,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Unidad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
