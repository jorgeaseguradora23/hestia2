<?php

/**
 * This is the model class for table "cotizacionproductoservicio".
 *
 * The followings are the available columns in table 'cotizacionproductoservicio':
 * @property string $id
 * @property string $idProductoServicio
 * @property string $idCotizacion
 * @property string $cantidad
 * @property string $precio
 * @property string $posicion
 * @property string $idCatEsquemaImpuesto
 * @property string $descuento
 * @property string $observacion
 *
 * The followings are the available model relations:
 * @property Cotizacion $idCotizacion0
 * @property Productoservicio $idProductoServicio0
 * @property Catesquemaimpuesto $idCatEsquemaImpuesto0
 */
class Cotizacionproductoservicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cotizacionproductoservicio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProductoServicio, idCotizacion, cantidad, precio, posicion, idCatEsquemaImpuesto, descuento', 'required'),
			array('idProductoServicio, idCotizacion, posicion, idCatEsquemaImpuesto', 'length', 'max'=>10),
			array('cantidad, precio, descuento', 'length', 'max'=>22),
			array('observacion', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idProductoServicio, idCotizacion, cantidad, precio, posicion, idCatEsquemaImpuesto, descuento, observacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCotizacion0' => array(self::BELONGS_TO, 'Cotizacion', 'idCotizacion'),
			'idProductoServicio0' => array(self::BELONGS_TO, 'Productoservicio', 'idProductoServicio'),
			'idCatEsquemaImpuesto0' => array(self::BELONGS_TO, 'Catesquemaimpuesto', 'idCatEsquemaImpuesto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idProductoServicio' => 'Id Producto Servicio',
			'idCotizacion' => 'Id Cotizacion',
			'cantidad' => 'Cantidad',
			'precio' => 'Precio',
			'posicion' => 'Posicion',
			'idCatEsquemaImpuesto' => 'Id Cat Esquema Impuesto',
			'descuento' => 'Descuento',
			'observacion' => 'Observacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idProductoServicio',$this->idProductoServicio,true);
		$criteria->compare('idCotizacion',$this->idCotizacion,true);
		$criteria->compare('cantidad',$this->cantidad,true);
		$criteria->compare('precio',$this->precio,true);
		$criteria->compare('posicion',$this->posicion,true);
		$criteria->compare('idCatEsquemaImpuesto',$this->idCatEsquemaImpuesto,true);
		$criteria->compare('descuento',$this->descuento,true);
		$criteria->compare('observacion',$this->observacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cotizacionproductoservicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
