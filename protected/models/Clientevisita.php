<?php

/**
 * This is the model class for table "clientevisita".
 *
 * The followings are the available columns in table 'clientevisita':
 * @property string $id
 * @property string $idCliente
 * @property string $fechaVisita
 * @property string $confirmado
 * @property string $usuario
 * @property string $observacion
 * @property string $idEmpleado
 * @property string $idQuejaSugerencia
 * @property string $tipo
 * @property string $idCatTipoVisita
 * @property string $estatus
 * @property string $idClienteVisita
 * @property string $fechaAtencion
 * @property string $usuarioAtencion
 * @property string $idCatTono
 *
 * The followings are the available model relations:
 * @property Cliente $idCliente0
 * @property Empleado $idEmpleado0
 * @property Quejasugerencia $idQuejaSugerencia0
 * @property Cattipovisita $idCatTipoVisita0
 * @property Clientevisita $idClienteVisita0
 * @property Clientevisita[] $clientevisitas
 * @property Cattono $idCatTono0
 * @property Clientevisitainvitado[] $clientevisitainvitados
 * @property Clientevisitaseguimiento[] $clientevisitaseguimientos
 */
class Clientevisita extends CActiveRecord
{
    public $_horaVisita;
    public $_horaAtencion;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clientevisita';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, fechaVisita,  confirmado, usuario, observacion, idEmpleado, tipo, idCatTipoVisita, estatus', 'required'),
			array('idCliente, idEmpleado, idQuejaSugerencia, idCatTipoVisita, idClienteVisita, idCatTono', 'length', 'max'=>10),
			array('confirmado, tipo, estatus', 'length', 'max'=>1),
			array('usuario, usuarioAtencion', 'length', 'max'=>45),
			array('fechaAtencion,_horaVisita, _horaAtencion,', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
            array('fechaAtencion, idCatTono, usuarioAtencion', 'required','on'=>'finaliza'),
			array('id, idCliente, fechaVisita, confirmado, usuario, observacion, idEmpleado, idQuejaSugerencia, tipo, idCatTipoVisita, estatus, idClienteVisita, fechaAtencion, usuarioAtencion, idCatTono', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
			'idEmpleado0' => array(self::BELONGS_TO, 'Empleado', 'idEmpleado'),
			'idQuejaSugerencia0' => array(self::BELONGS_TO, 'Quejasugerencia', 'idQuejaSugerencia'),
			'idCatTipoVisita0' => array(self::BELONGS_TO, 'Cattipovisita', 'idCatTipoVisita'),
			'idClienteVisita0' => array(self::BELONGS_TO, 'Clientevisita', 'idClienteVisita'),
			'clientevisitas' => array(self::HAS_MANY, 'Clientevisita', 'idClienteVisita'),
			'idCatTono0' => array(self::BELONGS_TO, 'Cattono', 'idCatTono'),
			'clientevisitainvitados' => array(self::HAS_MANY, 'Clientevisitainvitado', 'idClienteVisita'),
			'clientevisitaseguimientos' => array(self::HAS_MANY, 'Clientevisitaseguimiento', 'idClienteVisita'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'fechaVisita' => 'Fecha Visita',
			'confirmado' => 'Estatus de la Visita, ',
			'usuario' => 'Usuario',
			'observacion' => 'Observacion',
			'idEmpleado' => 'Id Empleado',
			'idQuejaSugerencia' => 'Id Queja Sugerencia',
			'tipo' => 'Selecciona un tipo de Registro, ',
			'idCatTipoVisita' => 'Selecciona Tipo Visita',
			'estatus' => 'Estatus',
			'idClienteVisita' => 'Id Cliente Visita',
			'fechaAtencion' => 'Fecha Atencion',
			'usuarioAtencion' => 'Usuario Atencion',
			'idCatTono' => 'Selecciona un Tono, ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('fechaVisita',$this->fechaVisita,true);
		$criteria->compare('confirmado',$this->confirmado,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('idEmpleado',$this->idEmpleado,true);
		$criteria->compare('idQuejaSugerencia',$this->idQuejaSugerencia,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('idCatTipoVisita',$this->idCatTipoVisita,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('idClienteVisita',$this->idClienteVisita,true);
		$criteria->compare('fechaAtencion',$this->fechaAtencion,true);
		$criteria->compare('usuarioAtencion',$this->usuarioAtencion,true);
		$criteria->compare('idCatTono',$this->idCatTono,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clientevisita the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
