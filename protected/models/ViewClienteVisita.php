<?php

/**
 * This is the model class for table "viewclientevisita".
 *
 * The followings are the available columns in table 'viewclientevisita':
 * @property string $id
 * @property string $idCliente
 * @property string $nombre
 * @property string $calle
 * @property string $numExt
 * @property string $numInt
 * @property string $telefono
 * @property string $eMail
 * @property string $fechaVisita
 * @property string $horaVisita
 * @property string $usuario
 * @property string $observacion
 * @property string $idEmpleado
 * @property string $empleado
 * @property string $foto
 * @property string $idQuejaSugerencia
 * @property string $tipo
 * @property string $idCatTipoVisita
 * @property string $tipoVisita
 * @property string $estatus
 * @property string $idClienteVisita
 * @property string $fechaAtencion
 * @property string $usuarioAtencion
 * @property string $idCatTono
 * @property string $tono
 * @property string $confirmado
 * @property string $estatusEvento
 * @property string $estatusColor
 */
class Viewclientevisita extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewclientevisita';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, nombre, calle, numExt, telefono, fechaVisita, usuario, observacion, idEmpleado, foto, tipo, idCatTipoVisita, tipoVisita, estatus, confirmado', 'required'),
			array('id, idCliente, idEmpleado, idQuejaSugerencia, idCatTipoVisita, idClienteVisita, idCatTono', 'length', 'max'=>10),
			array('nombre', 'length', 'max'=>1024),
			array('calle', 'length', 'max'=>240),
			array('numExt, numInt, foto', 'length', 'max'=>120),
			array('telefono', 'length', 'max'=>80),
			array('usuario, usuarioAtencion, tono', 'length', 'max'=>45),
			array('empleado', 'length', 'max'=>242),
			array('tipo, estatus, confirmado', 'length', 'max'=>1),
			array('tipoVisita', 'length', 'max'=>360),
			array('estatusEvento', 'length', 'max'=>13),
			array('estatusColor', 'length', 'max'=>7),
			array('eMail, horaVisita, fechaAtencion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCliente, nombre, calle, numExt, numInt, telefono, eMail, fechaVisita, horaVisita, usuario, observacion, idEmpleado, empleado, foto, idQuejaSugerencia, tipo, idCatTipoVisita, tipoVisita, estatus, idClienteVisita, fechaAtencion, usuarioAtencion, idCatTono, tono, confirmado, estatusEvento, estatusColor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'nombre' => 'Nombre',
			'calle' => 'Calle',
			'numExt' => 'Num Ext',
			'numInt' => 'Num Int',
			'telefono' => 'Telefono',
			'eMail' => 'E Mail',
			'fechaVisita' => 'Fecha Visita',
			'horaVisita' => 'Hora Visita',
			'usuario' => 'Usuario',
			'observacion' => 'Observacion',
			'idEmpleado' => 'Id Empleado',
			'empleado' => 'Empleado',
			'foto' => 'Foto',
			'idQuejaSugerencia' => 'Id Queja Sugerencia',
			'tipo' => 'Tipo',
			'idCatTipoVisita' => 'Id Cat Tipo Visita',
			'tipoVisita' => 'Tipo Visita',
			'estatus' => 'Estatus',
			'idClienteVisita' => 'Id Cliente Visita',
			'fechaAtencion' => 'Fecha Atencion',
			'usuarioAtencion' => 'Usuario Atencion',
			'idCatTono' => 'Id Cat Tono',
			'tono' => 'Tono',
			'confirmado' => 'Confirmado',
			'estatusEvento' => 'Estatus Evento',
			'estatusColor' => 'Estatus Color',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('numExt',$this->numExt,true);
		$criteria->compare('numInt',$this->numInt,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('eMail',$this->eMail,true);
		$criteria->compare('fechaVisita',$this->fechaVisita,true);
		$criteria->compare('horaVisita',$this->horaVisita,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('idEmpleado',$this->idEmpleado,true);
		$criteria->compare('empleado',$this->empleado,true);
		$criteria->compare('foto',$this->foto,true);
		$criteria->compare('idQuejaSugerencia',$this->idQuejaSugerencia,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('idCatTipoVisita',$this->idCatTipoVisita,true);
		$criteria->compare('tipoVisita',$this->tipoVisita,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('idClienteVisita',$this->idClienteVisita,true);
		$criteria->compare('fechaAtencion',$this->fechaAtencion,true);
		$criteria->compare('usuarioAtencion',$this->usuarioAtencion,true);
		$criteria->compare('idCatTono',$this->idCatTono,true);
		$criteria->compare('tono',$this->tono,true);
		$criteria->compare('confirmado',$this->confirmado,true);
		$criteria->compare('estatusEvento',$this->estatusEvento,true);
		$criteria->compare('estatusColor',$this->estatusColor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Viewclientevisita the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
