<?php

/**
 * This is the model class for table "catproductoservicio".
 *
 * The followings are the available columns in table 'catproductoservicio':
 * @property string $id
 * @property string $descripcion
 * @property string $cveProductoServicio
 * @property string $fechaInicio
 * @property string $fechaFin
 * @property string $ivaTrasladado
 * @property string $iepsTrasladado
 * @property string $complemento
 *
 * The followings are the available model relations:
 * @property Productoservicio[] $productoservicios
 */
class Catproductoservicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catproductoservicio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, cveProductoServicio, fechaInicio, ivaTrasladado, iepsTrasladado, complemento', 'required'),
			array('descripcion', 'length', 'max'=>260),
			array('cveProductoServicio', 'length', 'max'=>8),
			array('ivaTrasladado, iepsTrasladado', 'length', 'max'=>1),
			array('complemento', 'length', 'max'=>45),
			array('fechaFin', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descripcion, cveProductoServicio, fechaInicio, fechaFin, ivaTrasladado, iepsTrasladado, complemento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productoservicios' => array(self::HAS_MANY, 'Productoservicio', 'idCatProductoServicio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'cveProductoServicio' => 'Cve Producto Servicio',
			'fechaInicio' => 'Fecha Inicio',
			'fechaFin' => 'Fecha Fin',
			'ivaTrasladado' => 'Iva Trasladado',
			'iepsTrasladado' => 'Ieps Trasladado',
			'complemento' => 'Complemento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('cveProductoServicio',$this->cveProductoServicio,true);
		$criteria->compare('fechaInicio',$this->fechaInicio,true);
		$criteria->compare('fechaFin',$this->fechaFin,true);
		$criteria->compare('ivaTrasladado',$this->ivaTrasladado,true);
		$criteria->compare('iepsTrasladado',$this->iepsTrasladado,true);
		$criteria->compare('complemento',$this->complemento,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Catproductoservicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
