<?php

/**
 * This is the model class for table "viewCotizacionPS".
 *
 * The followings are the available columns in table 'viewCotizacionPS':
 * @property string $idProductoServicio
 * @property string $idCotizacion
 * @property string $cantidad
 * @property string $precio
 * @property string $subtotal
 * @property string $posicion
 * @property string $idCatEsquemaImpuesto
 * @property string $traslados
 * @property string $retencion
 * @property string $descuento
 * @property string $observacion
 * @property string $descripcion
 */
class ViewCotizacionPS extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewCotizacionPS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProductoServicio, idCotizacion, cantidad, precio, posicion, idCatEsquemaImpuesto, descuento, descripcion', 'required'),
			array('idProductoServicio, idCotizacion, posicion, idCatEsquemaImpuesto', 'length', 'max'=>10),
			array('cantidad, precio, descuento', 'length', 'max'=>22),
			array('subtotal', 'length', 'max'=>44),
			array('traslados, retencion', 'length', 'max'=>65),
			array('descripcion', 'length', 'max'=>360),
			array('observacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idProductoServicio, idCotizacion, cantidad, precio, subtotal, posicion, idCatEsquemaImpuesto, traslados, retencion, descuento, observacion, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idProductoServicio' => 'Id Producto Servicio',
			'idCotizacion' => 'Id Cotizacion',
			'cantidad' => 'Cantidad',
			'precio' => 'Precio',
			'subtotal' => 'Subtotal',
			'posicion' => 'Posicion',
			'idCatEsquemaImpuesto' => 'Id Cat Esquema Impuesto',
			'traslados' => 'Traslados',
			'retencion' => 'Retencion',
			'descuento' => 'Descuento',
			'observacion' => 'Observacion',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idProductoServicio',$this->idProductoServicio,true);
		$criteria->compare('idCotizacion',$this->idCotizacion,true);
		$criteria->compare('cantidad',$this->cantidad,true);
		$criteria->compare('precio',$this->precio,true);
		$criteria->compare('subtotal',$this->subtotal,true);
		$criteria->compare('posicion',$this->posicion,true);
		$criteria->compare('idCatEsquemaImpuesto',$this->idCatEsquemaImpuesto,true);
		$criteria->compare('traslados',$this->traslados,true);
		$criteria->compare('retencion',$this->retencion,true);
		$criteria->compare('descuento',$this->descuento,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ViewCotizacionPS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
