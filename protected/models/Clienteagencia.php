<?php

/**
 * This is the model class for table "clienteagencia".
 *
 * The followings are the available columns in table 'clienteagencia':
 * @property string $id
 * @property string $idCliente
 * @property string $razonSocial
 * @property string $rfc
 * @property string $observacion
 * @property string $estatus
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $fechaBaja
 * @property string $motivoBaja
 * @property string $usuarioBaja
 *
 * The followings are the available model relations:
 * @property Cliente $idCliente0
 */
class Clienteagencia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clienteagencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, razonSocial, rfc, observacion, estatus, fechaAlta, usuario', 'required'),
			array('idCliente', 'length', 'max'=>10),
			array('razonSocial', 'length', 'max'=>360),
			array('rfc', 'length', 'max'=>13),
			array('observacion, usuario, usuarioBaja', 'length', 'max'=>300),
			array('estatus', 'length', 'max'=>1),
			array('fechaBaja, motivoBaja, usuarioBaja', 'safe',),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCliente, razonSocial, rfc, observacion, estatus, fechaAlta, usuario, fechaBaja, motivoBaja, usuarioBaja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'razonSocial' => 'Razon Social',
			'rfc' => 'Rfc',
			'observacion' => 'Observacion',
			'estatus' => 'Estatus',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'fechaBaja' => 'Fecha Baja',
			'motivoBaja' => 'Motivo Baja',
			'usuarioBaja' => 'Usuario Baja',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('razonSocial',$this->razonSocial,true);
		$criteria->compare('rfc',$this->rfc,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('fechaBaja',$this->fechaBaja,true);
		$criteria->compare('motivoBaja',$this->motivoBaja,true);
		$criteria->compare('usuarioBaja',$this->usuarioBaja,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clienteagencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
