<?php

/**
 * This is the model class for table "cfdproductoservicio".
 *
 * The followings are the available columns in table 'cfdproductoservicio':
 * @property string $id
 * @property string $idProductoServicio
 * @property string $idCfd
 * @property string $cantidad
 * @property string $precio
 * @property string $idCatEsquemaImpuesto
 * @property string $posicion
 * @property string $descuento
 *
 * The followings are the available model relations:
 * @property Productoservicio $idProductoServicio0
 * @property Cfd $idCfd0
 * @property Catesquemaimpuesto $idCatEsquemaImpuesto0
 */
class Cfdproductoservicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cfdproductoservicio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProductoServicio, idCfd, cantidad, precio, idCatEsquemaImpuesto, posicion, descuento', 'required'),
			array('idProductoServicio, idCfd, idCatEsquemaImpuesto, posicion', 'length', 'max'=>10),
			array('cantidad, precio, descuento', 'length', 'max'=>22),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idProductoServicio, idCfd, cantidad, precio, idCatEsquemaImpuesto, posicion, descuento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProductoServicio0' => array(self::BELONGS_TO, 'Productoservicio', 'idProductoServicio'),
			'idCfd0' => array(self::BELONGS_TO, 'Cfd', 'idCfd'),
			'idCatEsquemaImpuesto0' => array(self::BELONGS_TO, 'Catesquemaimpuesto', 'idCatEsquemaImpuesto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idProductoServicio' => 'Id Producto Servicio',
			'idCfd' => 'Id Cfd',
			'cantidad' => 'Cantidad',
			'precio' => 'Precio',
			'idCatEsquemaImpuesto' => 'Id Cat Esquema Impuesto',
			'posicion' => 'Posicion',
			'descuento' => 'Descuento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idProductoServicio',$this->idProductoServicio,true);
		$criteria->compare('idCfd',$this->idCfd,true);
		$criteria->compare('cantidad',$this->cantidad,true);
		$criteria->compare('precio',$this->precio,true);
		$criteria->compare('idCatEsquemaImpuesto',$this->idCatEsquemaImpuesto,true);
		$criteria->compare('posicion',$this->posicion,true);
		$criteria->compare('descuento',$this->descuento,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cfdproductoservicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
