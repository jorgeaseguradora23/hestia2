<?php

/**
 * This is the model class for table "quejasugerencia".
 *
 * The followings are the available columns in table 'quejasugerencia':
 * @property string $id
 * @property string $idCliente
 * @property string $idEmpleado
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $descripcion
 * @property string $idCatTono
 * @property string $estatus
 * @property string $idCatCelula
 * @property string $idCatMetodoContacto
 * @property string $tipo
 * @property string $idCatTipoIncidencia
 * @property string $fechaCierre
 * @property string $usuarioCierre
 * @property string $idCatAcuerdo
 * @property string $fechaAcuerdo
 * @property string $observacionAcuerdo
 * @property string $estatusAcuerdo
 * @property string $idEmpleadoCierre
 *
 * The followings are the available model relations:
 * @property Clientevisita[] $clientevisitas
 * @property Empleado $idEmpleadoCierre0
 * @property Cliente $idCliente0
 * @property Empleado $idEmpleado0
 * @property Catmetodocontacto $idCatMetodoContacto0
 * @property Cattono $idCatTono0
 * @property Cattipoincidencia $idCatTipoIncidencia0
 * @property Catacuerdo $idCatAcuerdo0
 * @property Catcelula $idCatCelula0
 * @property Quejasugerenciaadjunto[] $quejasugerenciaadjuntos
 * @property Quejasugerenciaseguimiento[] $quejasugerenciaseguimientos
 */
class Quejasugerencia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quejasugerencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, idEmpleado, fechaAlta, usuario, descripcion, idCatTono, estatus, idCatCelula, idCatMetodoContacto, tipo', 'required'),
			array('idCliente, idEmpleado, idCatTono, idCatCelula, idCatMetodoContacto, idCatTipoIncidencia, idCatAcuerdo', 'length', 'max'=>10),
			array('usuario, usuarioCierre', 'length', 'max'=>45),
			array('estatus, tipo, estatusAcuerdo', 'length', 'max'=>1),
			array('observacionAcuerdo', 'length', 'max'=>1024),
			array('fechaCierre, fechaAcuerdo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
            array('idEmpleadoCierre', 'required','on'=>'cierre'),
            array('id, idCliente, idEmpleado, fechaAlta, usuario, descripcion, idCatTono, estatus, idCatCelula, idCatMetodoContacto, tipo, idCatTipoIncidencia, fechaCierre, usuarioCierre, idCatAcuerdo, fechaAcuerdo, observacionAcuerdo, estatusAcuerdo, idEmpleadoCierre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clientevisitas' => array(self::HAS_MANY, 'Clientevisita', 'idQuejaSugerencia'),
			'idEmpleadoCierre0' => array(self::BELONGS_TO, 'Empleado', 'idEmpleadoCierre'),
			'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
			'idEmpleado0' => array(self::BELONGS_TO, 'Empleado', 'idEmpleado'),
			'idCatMetodoContacto0' => array(self::BELONGS_TO, 'Catmetodocontacto', 'idCatMetodoContacto'),
			'idCatTono0' => array(self::BELONGS_TO, 'Cattono', 'idCatTono'),
			'idCatTipoIncidencia0' => array(self::BELONGS_TO, 'Cattipoincidencia', 'idCatTipoIncidencia'),
			'idCatAcuerdo0' => array(self::BELONGS_TO, 'Catacuerdo', 'idCatAcuerdo'),
			'idCatCelula0' => array(self::BELONGS_TO, 'Catcelula', 'idCatCelula'),
			'quejasugerenciaadjuntos' => array(self::HAS_MANY, 'Quejasugerenciaadjunto', 'idQuejaSugerencia'),
			'quejasugerenciaseguimientos' => array(self::HAS_MANY, 'Quejasugerenciaseguimiento', 'idQuejaSugerencia'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'idEmpleado' => 'Id Empleado',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'descripcion' => 'Descripcion',
			'idCatTono' => 'Selecciona un TOno',
			'estatus' => 'Estatus',
			'idCatCelula' => 'Selecciona una Celula',
			'idCatMetodoContacto' => 'Id Cat Metodo Contacto',
			'tipo' => 'Tipo',
			'idCatTipoIncidencia' => 'Id Cat Tipo Incidencia',
			'fechaCierre' => 'Fecha Cierre',
			'usuarioCierre' => 'Usuario Cierre',
			'idCatAcuerdo' => 'Selecciona el tipo de acuerdo',
			'fechaAcuerdo' => 'Fecha Acuerdo',
			'observacionAcuerdo' => 'Observacion Acuerdo',
			'estatusAcuerdo' => 'Estatus Acuerdo',
			'idEmpleadoCierre' => 'Selecciona el Responsable Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('idEmpleado',$this->idEmpleado,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('idCatTono',$this->idCatTono,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('idCatCelula',$this->idCatCelula,true);
		$criteria->compare('idCatMetodoContacto',$this->idCatMetodoContacto,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('idCatTipoIncidencia',$this->idCatTipoIncidencia,true);
		$criteria->compare('fechaCierre',$this->fechaCierre,true);
		$criteria->compare('usuarioCierre',$this->usuarioCierre,true);
		$criteria->compare('idCatAcuerdo',$this->idCatAcuerdo,true);
		$criteria->compare('fechaAcuerdo',$this->fechaAcuerdo,true);
		$criteria->compare('observacionAcuerdo',$this->observacionAcuerdo,true);
		$criteria->compare('estatusAcuerdo',$this->estatusAcuerdo,true);
		$criteria->compare('idEmpleadoCierre',$this->idEmpleadoCierre,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Quejasugerencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
