<?php

/**
 * This is the model class for table "viewcotizacion".
 *
 * The followings are the available columns in table 'viewcotizacion':
 * @property string $id
 * @property string $idCotizacion
 * @property string $idCliente
 * @property string $idCotizacionCierre
 * @property string $nombre
 * @property string $categoriaCliente
 * @property string $textTipoCliente
 * @property string $idPadre
 * @property string $padre
 * @property string $folio
 * @property string $estatus
 * @property string $estatusText
 * @property string $personaCierre
 * @property string $eMailCierre
 * @property string $contenidoCierre
 * @property string $fechaCierre
 * @property string $cargoProveedor
 * @property string $costoProveedor
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $total
 * @property string $fechaVencimiento
 * @property string $tipo
 * @property string $textTipo
 * @property string $observacion
 * @property string $moneda
 * @property string $idCatMoneda
 * @property string $tipoCambio
 */
class Viewcotizacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewcotizacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, nombre, idPadre, estatus, fechaAlta, usuario, total, fechaVencimiento, tipo, idCatMoneda, tipoCambio', 'required'),
			array('id, idCotizacion, idCliente, idCotizacionCierre, idPadre, textTipo, idCatMoneda', 'length', 'max'=>10),
			array('nombre', 'length', 'max'=>1024),
			array('categoriaCliente, estatusText', 'length', 'max'=>9),
			array('textTipoCliente, usuario', 'length', 'max'=>45),
			array('folio', 'length', 'max'=>13),
			array('estatus, cargoProveedor, tipo', 'length', 'max'=>1),
			array('personaCierre, eMailCierre', 'length', 'max'=>120),
			array('costoProveedor, total, tipoCambio', 'length', 'max'=>22),
			array('moneda', 'length', 'max'=>4),
			array('padre, contenidoCierre, fechaCierre, observacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCotizacion, idCliente, idCotizacionCierre, nombre, categoriaCliente, textTipoCliente, idPadre, padre, folio, estatus, estatusText, personaCierre, eMailCierre, contenidoCierre, fechaCierre, cargoProveedor, costoProveedor, fechaAlta, usuario, total, fechaVencimiento, tipo, textTipo, observacion, moneda, idCatMoneda, tipoCambio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCotizacion' => 'Id Cotizacion',
			'idCliente' => 'Id Cliente',
			'idCotizacionCierre' => 'Id Cotizacion Cierre',
			'nombre' => 'Nombre',
			'categoriaCliente' => 'Categoria Cliente',
			'textTipoCliente' => 'Text Tipo Cliente',
			'idPadre' => 'Id Padre',
			'padre' => 'Padre',
			'folio' => 'Folio',
			'estatus' => 'Estatus',
			'estatusText' => 'Estatus Text',
			'personaCierre' => 'Persona Cierre',
			'eMailCierre' => 'E Mail Cierre',
			'contenidoCierre' => 'Contenido Cierre',
			'fechaCierre' => 'Fecha Cierre',
			'cargoProveedor' => 'Cargo Proveedor',
			'costoProveedor' => 'Costo Proveedor',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'total' => 'Total',
			'fechaVencimiento' => 'Fecha Vencimiento',
			'tipo' => 'Tipo',
			'textTipo' => 'Text Tipo',
			'observacion' => 'Observacion',
			'moneda' => 'Moneda',
			'idCatMoneda' => 'Id Cat Moneda',
			'tipoCambio' => 'Tipo Cambio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCotizacion',$this->idCotizacion,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('idCotizacionCierre',$this->idCotizacionCierre,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('categoriaCliente',$this->categoriaCliente,true);
		$criteria->compare('textTipoCliente',$this->textTipoCliente,true);
		$criteria->compare('idPadre',$this->idPadre,true);
		$criteria->compare('padre',$this->padre,true);
		$criteria->compare('folio',$this->folio,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('estatusText',$this->estatusText,true);
		$criteria->compare('personaCierre',$this->personaCierre,true);
		$criteria->compare('eMailCierre',$this->eMailCierre,true);
		$criteria->compare('contenidoCierre',$this->contenidoCierre,true);
		$criteria->compare('fechaCierre',$this->fechaCierre,true);
		$criteria->compare('cargoProveedor',$this->cargoProveedor,true);
		$criteria->compare('costoProveedor',$this->costoProveedor,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('fechaVencimiento',$this->fechaVencimiento,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('textTipo',$this->textTipo,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('moneda',$this->moneda,true);
		$criteria->compare('idCatMoneda',$this->idCatMoneda,true);
		$criteria->compare('tipoCambio',$this->tipoCambio,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Viewcotizacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
