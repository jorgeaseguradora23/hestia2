<?php

/**
 * This is the model class for table "clientevisitainvitado".
 *
 * The followings are the available columns in table 'clientevisitainvitado':
 * @property string $id
 * @property string $idClienteVisita
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $idEmpleado
 *
 * The followings are the available model relations:
 * @property Clientevisita $idClienteVisita0
 * @property Empleado $idEmpleado0
 */
class Clientevisitainvitado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clientevisitainvitado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idClienteVisita, fechaAlta, usuario, idEmpleado', 'required'),
			array('idClienteVisita, idEmpleado', 'length', 'max'=>10),
			array('usuario', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idClienteVisita, fechaAlta, usuario, idEmpleado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idClienteVisita0' => array(self::BELONGS_TO, 'Clientevisita', 'idClienteVisita'),
			'idEmpleado0' => array(self::BELONGS_TO, 'Empleado', 'idEmpleado'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idClienteVisita' => 'Id Cliente Visita',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'idEmpleado' => 'Id Empleado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idClienteVisita',$this->idClienteVisita,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('idEmpleado',$this->idEmpleado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clientevisitainvitado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
