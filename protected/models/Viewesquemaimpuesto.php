<?php

/**
 * This is the model class for table "viewesquemaimpuesto".
 *
 * The followings are the available columns in table 'viewesquemaimpuesto':
 * @property string $idCatEsquemaImpuesto
 * @property string $idCatImpuesto
 * @property string $descripcion
 * @property string $tasa
 * @property string $aplicacion
 */
class Viewesquemaimpuesto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewesquemaimpuesto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCatEsquemaImpuesto, idCatImpuesto, descripcion, tasa, aplicacion', 'required'),
			array('idCatEsquemaImpuesto, idCatImpuesto', 'length', 'max'=>10),
			array('descripcion', 'length', 'max'=>45),
			array('tasa', 'length', 'max'=>22),
			array('aplicacion', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idCatEsquemaImpuesto, idCatImpuesto, descripcion, tasa, aplicacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCatEsquemaImpuesto' => 'Id Cat Esquema Impuesto',
			'idCatImpuesto' => 'Id Cat Impuesto',
			'descripcion' => 'Descripcion',
			'tasa' => 'Tasa',
			'aplicacion' => 'Aplicacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCatEsquemaImpuesto',$this->idCatEsquemaImpuesto,true);
		$criteria->compare('idCatImpuesto',$this->idCatImpuesto,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('tasa',$this->tasa,true);
		$criteria->compare('aplicacion',$this->aplicacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Viewesquemaimpuesto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
