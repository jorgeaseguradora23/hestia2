<?php

/**
 * This is the model class for table "clientedatosfacturacion".
 *
 * The followings are the available columns in table 'clientedatosfacturacion':
 * @property string $id
 * @property string $idCliente
 * @property string $rfc
 * @property string $razonSocial
 * @property string $calle
 * @property string $numExt
 * @property string $numInt
 * @property integer $idCatColonia
 * @property string $idCatUsoCFDI
 * @property string $idCatMetodoPago
 * @property string $idCatFormaPago
 *
 * The followings are the available model relations:
 * @property Catusocfdi $idCatUsoCFDI0
 * @property Catmetodopago $idCatMetodoPago0
 * @property Catformapago $idCatFormaPago0
 * @property Catcolonia $idCatColonia0
 * @property Cliente $idCliente0
 */
class Clientedatosfacturacion extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clientedatosfacturacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idCliente, rfc, razonSocial, calle, numExt, idCatColonia, idCatUsoCFDI, idCatMetodoPago, idCatFormaPago', 'required'),
            array('idCatColonia', 'numerical', 'integerOnly'=>true),
            array('idCliente, idCatUsoCFDI, idCatMetodoPago, idCatFormaPago', 'length', 'max'=>10),
            array('rfc', 'length', 'max'=>13),
            array('rfc', 'length', 'min'=>12),
            array('razonSocial', 'length', 'max'=>360),
            array('calle', 'length', 'max'=>240),
            array('numExt, numInt', 'length', 'max'=>45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idCliente, rfc, razonSocial, calle, numExt, numInt, idCatColonia, idCatUsoCFDI, idCatMetodoPago, idCatFormaPago', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idCatUsoCFDI0' => array(self::BELONGS_TO, 'Catusocfdi', 'idCatUsoCFDI'),
            'idCatMetodoPago0' => array(self::BELONGS_TO, 'Catmetodopago', 'idCatMetodoPago'),
            'idCatFormaPago0' => array(self::BELONGS_TO, 'Catformapago', 'idCatFormaPago'),
            'idCatColonia0' => array(self::BELONGS_TO, 'Catcolonia', 'idCatColonia'),
            'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'idCliente' => 'Id Cliente',
            'rfc' => 'RFC',
            'razonSocial' => 'Razon Social',
            'calle' => 'Calle',
            'numExt' => 'Num Exterior',
            'numInt' => 'Num Interior',
            'idCatColonia' => 'Selecciona una Colonia, ',
            'idCatUsoCFDI' => 'Selecciona un Uso de CFDI',
            'idCatMetodoPago' => 'Selecciona un Método de Pago',
            'idCatFormaPago' => 'Selecciona una Forma de Pago',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('idCliente',$this->idCliente,true);
        $criteria->compare('rfc',$this->rfc,true);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('calle',$this->calle,true);
        $criteria->compare('numExt',$this->numExt,true);
        $criteria->compare('numInt',$this->numInt,true);
        $criteria->compare('idCatColonia',$this->idCatColonia);
        $criteria->compare('idCatUsoCFDI',$this->idCatUsoCFDI,true);
        $criteria->compare('idCatMetodoPago',$this->idCatMetodoPago,true);
        $criteria->compare('idCatFormaPago',$this->idCatFormaPago,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Clientedatosfacturacion the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
