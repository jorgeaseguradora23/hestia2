<?php

/**
 * This is the model class for table "catcolonia".
 *
 * The followings are the available columns in table 'catcolonia':
 * @property integer $id
 * @property string $cp
 * @property string $colonia
 * @property string $tipo
 * @property string $municipio
 * @property string $estado
 * @property string $ciudad
 * @property string $pais
 *
 * The followings are the available model relations:
 * @property Cliente[] $clientes
 * @property Clientedatosfacturacion[] $clientedatosfacturacions
 */
class Catcolonia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catcolonia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cp, colonia, tipo, municipio, estado, pais', 'required'),
			array('cp', 'length', 'max'=>10),
			array('colonia, tipo, municipio, estado, ciudad', 'length', 'max'=>120),
			array('pais', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cp, colonia, tipo, municipio, estado, ciudad, pais', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clientes' => array(self::HAS_MANY, 'Cliente', 'idCatColonia'),
			'clientedatosfacturacions' => array(self::HAS_MANY, 'Clientedatosfacturacion', 'idCatColonia'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cp' => 'Cp',
			'colonia' => 'Colonia',
			'tipo' => 'Tipo',
			'municipio' => 'Municipio',
			'estado' => 'Estado',
			'ciudad' => 'Ciudad',
			'pais' => 'Pais',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cp',$this->cp,true);
		$criteria->compare('colonia',$this->colonia,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('municipio',$this->municipio,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('ciudad',$this->ciudad,true);
		$criteria->compare('pais',$this->pais,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Catcolonia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
