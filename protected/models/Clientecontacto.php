<?php

/**
 * This is the model class for table "clientecontacto".
 *
 * The followings are the available columns in table 'clientecontacto':
 * @property string $id
 * @property string $idCliente
 * @property string $nombreContacto
 * @property string $telefono
 * @property string $eMail
 *
 * The followings are the available model relations:
 * @property Cliente $idCliente0
 */
class Clientecontacto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clientecontacto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, nombreContacto, telefono, eMail', 'required'),
			array('idCliente', 'length', 'max'=>10),
			array('nombreContacto', 'length', 'max'=>80),
			array('telefono', 'length', 'max'=>45),
			array('eMail', 'length', 'max'=>160),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCliente, nombreContacto, telefono, eMail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'nombreContacto' => 'Nombre Contacto',
			'telefono' => 'Telefono',
			'eMail' => 'E Mail',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('nombreContacto',$this->nombreContacto,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('eMail',$this->eMail,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clientecontacto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
