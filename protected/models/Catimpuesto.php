<?php

/**
 * This is the model class for table "catimpuesto".
 *
 * The followings are the available columns in table 'catimpuesto':
 * @property string $id
 * @property string $descripcion
 * @property string $tasa
 * @property string $aplicacion
 * @property string $cveImpuesto
 *
 * The followings are the available model relations:
 * @property Catdesgloceimpuesto[] $catdesgloceimpuestos
 */
class Catimpuesto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catimpuesto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, tasa, aplicacion, cveImpuesto', 'required'),
			array('descripcion', 'length', 'max'=>45),
			array('tasa', 'length', 'max'=>22),
			array('aplicacion', 'length', 'max'=>1),
			array('cveImpuesto', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descripcion, tasa, aplicacion, cveImpuesto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catdesgloceimpuestos' => array(self::HAS_MANY, 'Catdesgloceimpuesto', 'idCatImpuesto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'tasa' => 'Tasa',
			'aplicacion' => 'Aplicacion',
			'cveImpuesto' => 'Cve Impuesto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('tasa',$this->tasa,true);
		$criteria->compare('aplicacion',$this->aplicacion,true);
		$criteria->compare('cveImpuesto',$this->cveImpuesto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Catimpuesto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
