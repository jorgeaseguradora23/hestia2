<?php

/**
 * This is the model class for table "viewClienteServicio".
 *
 * The followings are the available columns in table 'viewClienteServicio':
 * @property string $id
 * @property string $idCliente
 * @property string $idClienteContrato
 * @property string $idProductoServicio
 * @property string $producto
 * @property string $idCatTipoServicio
 * @property integer $diasServicio
 * @property integer $diasTrancurridos
 * @property string $tipoServicio
 * @property string $cantidad
 * @property string $precio
 * @property string $fechaInicio
 * @property string $fechaFinal
 * @property string $idCatTipoMonitoreo
 * @property string $monitoreo
 * @property string $estatus
 */
class ViewClienteServicio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewClienteServicio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCliente, idProductoServicio, producto, idCatTipoServicio, tipoServicio, cantidad, precio, fechaInicio, fechaFinal, monitoreo, estatus', 'required'),
			array('diasServicio, diasTrancurridos', 'numerical', 'integerOnly'=>true),
			array('id, idCliente, idClienteContrato, idProductoServicio, idCatTipoServicio, idCatTipoMonitoreo', 'length', 'max'=>10),
			array('producto', 'length', 'max'=>360),
			array('tipoServicio', 'length', 'max'=>45),
			array('cantidad, precio', 'length', 'max'=>22),
			array('monitoreo', 'length', 'max'=>160),
			array('estatus', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCliente, idClienteContrato, idProductoServicio, producto, idCatTipoServicio, diasServicio, diasTrancurridos, tipoServicio, cantidad, precio, fechaInicio, fechaFinal, idCatTipoMonitoreo, monitoreo, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'idClienteContrato' => 'Id Cliente Contrato',
			'idProductoServicio' => 'Id Producto Servicio',
			'producto' => 'Producto',
			'idCatTipoServicio' => 'Id Cat Tipo Servicio',
			'diasServicio' => 'Dias Servicio',
			'diasTrancurridos' => 'Dias Trancurridos',
			'tipoServicio' => 'Tipo Servicio',
			'cantidad' => 'Cantidad',
			'precio' => 'Precio',
			'fechaInicio' => 'Fecha Inicio',
			'fechaFinal' => 'Fecha Final',
			'idCatTipoMonitoreo' => 'Id Cat Tipo Monitoreo',
			'monitoreo' => 'Monitoreo',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('idClienteContrato',$this->idClienteContrato,true);
		$criteria->compare('idProductoServicio',$this->idProductoServicio,true);
		$criteria->compare('producto',$this->producto,true);
		$criteria->compare('idCatTipoServicio',$this->idCatTipoServicio,true);
		$criteria->compare('diasServicio',$this->diasServicio);
		$criteria->compare('diasTrancurridos',$this->diasTrancurridos);
		$criteria->compare('tipoServicio',$this->tipoServicio,true);
		$criteria->compare('cantidad',$this->cantidad,true);
		$criteria->compare('precio',$this->precio,true);
		$criteria->compare('fechaInicio',$this->fechaInicio,true);
		$criteria->compare('fechaFinal',$this->fechaFinal,true);
		$criteria->compare('idCatTipoMonitoreo',$this->idCatTipoMonitoreo,true);
		$criteria->compare('monitoreo',$this->monitoreo,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ViewClienteServicio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
