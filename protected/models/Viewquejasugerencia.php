<?php

/**
 * This is the model class for table "viewquejasugerencia".
 *
 * The followings are the available columns in table 'viewquejasugerencia':
 * @property string $tipo
 * @property string $textTipo
 * @property string $idCatTipoIncidencia
 * @property string $tipoIncidencia
 * @property string $usuarioCierre
 * @property string $fechaCierre
 * @property string $fechaAcuerdo
 * @property string $observacionAcuerdo
 * @property string $estatusAcuerdo
 * @property string $textEstatusAcuerdo
 * @property string $acuerdo
 * @property string $idCatAcuerdo
 * @property string $id
 * @property string $idCliente
 * @property string $nombre
 * @property string $idEmpleado
 * @property string $empleado
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $descripcion
 * @property string $idCatTono
 * @property string $tono
 * @property string $color
 * @property string $estatus
 * @property string $txtEstatus
 * @property string $idCatCelula
 * @property string $celula
 * @property string $idCatMetodoContacto
 * @property string $metodoContacto
 * @property string $idEmpleadoCierre
 * @property string $empleadoCierre
 */
class Viewquejasugerencia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewquejasugerencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo, tipoIncidencia, idCliente, nombre, idEmpleado, fechaAlta, usuario, descripcion, idCatTono, tono, color, estatus, idCatCelula, celula, idCatMetodoContacto, metodoContacto', 'required'),
			array('tipo, estatusAcuerdo, estatus', 'length', 'max'=>1),
			array('textTipo, idCatTipoIncidencia, textEstatusAcuerdo, idCatAcuerdo, id, idCliente, idEmpleado, idCatTono, idCatCelula, idCatMetodoContacto, idEmpleadoCierre', 'length', 'max'=>10),
			array('tipoIncidencia, usuarioCierre, usuario, tono, color', 'length', 'max'=>45),
			array('observacionAcuerdo, nombre', 'length', 'max'=>1024),
			array('acuerdo, celula', 'length', 'max'=>360),
			array('empleado, empleadoCierre', 'length', 'max'=>242),
			array('txtEstatus', 'length', 'max'=>11),
			array('metodoContacto', 'length', 'max'=>120),
			array('fechaCierre, fechaAcuerdo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tipo, textTipo, idCatTipoIncidencia, tipoIncidencia, usuarioCierre, fechaCierre, fechaAcuerdo, observacionAcuerdo, estatusAcuerdo, textEstatusAcuerdo, acuerdo, idCatAcuerdo, id, idCliente, nombre, idEmpleado, empleado, fechaAlta, usuario, descripcion, idCatTono, tono, color, estatus, txtEstatus, idCatCelula, celula, idCatMetodoContacto, metodoContacto, idEmpleadoCierre, empleadoCierre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tipo' => 'Tipo',
			'textTipo' => 'Text Tipo',
			'idCatTipoIncidencia' => 'Id Cat Tipo Incidencia',
			'tipoIncidencia' => 'Tipo Incidencia',
			'usuarioCierre' => 'Usuario Cierre',
			'fechaCierre' => 'Fecha Cierre',
			'fechaAcuerdo' => 'Fecha Acuerdo',
			'observacionAcuerdo' => 'Observacion Acuerdo',
			'estatusAcuerdo' => 'Estatus Acuerdo',
			'textEstatusAcuerdo' => 'Text Estatus Acuerdo',
			'acuerdo' => 'Acuerdo',
			'idCatAcuerdo' => 'Id Cat Acuerdo',
			'id' => 'ID',
			'idCliente' => 'Id Cliente',
			'nombre' => 'Nombre',
			'idEmpleado' => 'Id Empleado',
			'empleado' => 'Empleado',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'descripcion' => 'Descripcion',
			'idCatTono' => 'Id Cat Tono',
			'tono' => 'Tono',
			'color' => 'Color',
			'estatus' => 'Estatus',
			'txtEstatus' => 'Txt Estatus',
			'idCatCelula' => 'Id Cat Celula',
			'celula' => 'Celula',
			'idCatMetodoContacto' => 'Id Cat Metodo Contacto',
			'metodoContacto' => 'Metodo Contacto',
			'idEmpleadoCierre' => 'Id Empleado Cierre',
			'empleadoCierre' => 'Empleado Cierre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('textTipo',$this->textTipo,true);
		$criteria->compare('idCatTipoIncidencia',$this->idCatTipoIncidencia,true);
		$criteria->compare('tipoIncidencia',$this->tipoIncidencia,true);
		$criteria->compare('usuarioCierre',$this->usuarioCierre,true);
		$criteria->compare('fechaCierre',$this->fechaCierre,true);
		$criteria->compare('fechaAcuerdo',$this->fechaAcuerdo,true);
		$criteria->compare('observacionAcuerdo',$this->observacionAcuerdo,true);
		$criteria->compare('estatusAcuerdo',$this->estatusAcuerdo,true);
		$criteria->compare('textEstatusAcuerdo',$this->textEstatusAcuerdo,true);
		$criteria->compare('acuerdo',$this->acuerdo,true);
		$criteria->compare('idCatAcuerdo',$this->idCatAcuerdo,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('idEmpleado',$this->idEmpleado,true);
		$criteria->compare('empleado',$this->empleado,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('idCatTono',$this->idCatTono,true);
		$criteria->compare('tono',$this->tono,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('txtEstatus',$this->txtEstatus,true);
		$criteria->compare('idCatCelula',$this->idCatCelula,true);
		$criteria->compare('celula',$this->celula,true);
		$criteria->compare('idCatMetodoContacto',$this->idCatMetodoContacto,true);
		$criteria->compare('metodoContacto',$this->metodoContacto,true);
		$criteria->compare('idEmpleadoCierre',$this->idEmpleadoCierre,true);
		$criteria->compare('empleadoCierre',$this->empleadoCierre,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Viewquejasugerencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
