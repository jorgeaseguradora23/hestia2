<?php

/**
 * This is the model class for table "clientecontrato".
 *
 * The followings are the available columns in table 'clientecontrato':
 * @property string $id
 * @property string $fechaInicio
 * @property string $fechaFinal
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $idCliente
 * @property string $urlArchivo
 * @property string $tipoContrato
 * @property string $idCatPeriodicidad
 * @property string $tipoFacturacion
 * @property string $estatus
 * @property string $fechaBaja
 * @property string $usuarioBaja
 * @property string $motivoBaja
 * @property string $facturacion
 *
 * The followings are the available model relations:
 * @property Cliente $idCliente0
 * @property Catperiodicidad $idCatPeriodicidad0
 */
class Clientecontrato extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clientecontrato';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fechaInicio, fechaFinal, fechaAlta, usuario, idCliente, tipoContrato, idCatPeriodicidad, tipoFacturacion, estatus, facturacion', 'required'),
			array('usuario, usuarioBaja', 'length', 'max'=>45),
			array('idCliente, idCatPeriodicidad', 'length', 'max'=>10),
			array('urlArchivo', 'length', 'max'=>1024),
			array('tipoContrato, tipoFacturacion, estatus, facturacion', 'length', 'max'=>1),
			array('fechaBaja, motivoBaja', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fechaInicio, fechaFinal, fechaAlta, usuario, idCliente, urlArchivo, tipoContrato, idCatPeriodicidad, tipoFacturacion, estatus, fechaBaja, usuarioBaja, motivoBaja, facturacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente0' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
			'idCatPeriodicidad0' => array(self::BELONGS_TO, 'Catperiodicidad', 'idCatPeriodicidad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fechaInicio' => 'Fecha Inicio',
			'fechaFinal' => 'Fecha Final',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'idCliente' => 'Id Cliente',
			'urlArchivo' => 'Url Archivo',
			'tipoContrato' => 'Tipo Contrato',
			'idCatPeriodicidad' => 'Id Cat Periodicidad',
			'tipoFacturacion' => 'Tipo Facturacion',
			'estatus' => 'Estatus',
			'fechaBaja' => 'Fecha Baja',
			'usuarioBaja' => 'Usuario Baja',
			'motivoBaja' => 'Motivo Baja',
			'facturacion' => 'Facturacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('fechaInicio',$this->fechaInicio,true);
		$criteria->compare('fechaFinal',$this->fechaFinal,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('idCliente',$this->idCliente,true);
		$criteria->compare('urlArchivo',$this->urlArchivo,true);
		$criteria->compare('tipoContrato',$this->tipoContrato,true);
		$criteria->compare('idCatPeriodicidad',$this->idCatPeriodicidad,true);
		$criteria->compare('tipoFacturacion',$this->tipoFacturacion,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('fechaBaja',$this->fechaBaja,true);
		$criteria->compare('usuarioBaja',$this->usuarioBaja,true);
		$criteria->compare('motivoBaja',$this->motivoBaja,true);
		$criteria->compare('facturacion',$this->facturacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clientecontrato the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
