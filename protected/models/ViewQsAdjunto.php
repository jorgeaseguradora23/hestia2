<?php

/**
 * This is the model class for table "viewQsAdjunto".
 *
 * The followings are the available columns in table 'viewQsAdjunto':
 * @property string $id
 * @property string $idQuejaSugerencia
 * @property string $adjunto
 * @property string $fechaAlta
 * @property string $usuario
 * @property string $archivo
 * @property string $nombre
 */
class ViewQsAdjunto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewQsAdjunto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idQuejaSugerencia, adjunto, fechaAlta, usuario', 'required'),
			array('id, idQuejaSugerencia', 'length', 'max'=>10),
			array('usuario', 'length', 'max'=>45),
			array('archivo', 'length', 'max'=>120),
			array('nombre', 'length', 'max'=>242),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idQuejaSugerencia, adjunto, fechaAlta, usuario, archivo, nombre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idQuejaSugerencia' => 'Id Queja Sugerencia',
			'adjunto' => 'Adjunto',
			'fechaAlta' => 'Fecha Alta',
			'usuario' => 'Usuario',
			'archivo' => 'Archivo',
			'nombre' => 'Nombre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idQuejaSugerencia',$this->idQuejaSugerencia,true);
		$criteria->compare('adjunto',$this->adjunto,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('archivo',$this->archivo,true);
		$criteria->compare('nombre',$this->nombre,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ViewQsAdjunto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
