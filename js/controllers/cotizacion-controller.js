function cierreCotizacionSet(save = false,id){

    if (save == false) {
        $("#nuevo-model").modal("show");
        getDatosAjax("cotizacion/cierre", "#divCreateModal", id, function () {

        });
    } else {
        setDatosAjax("cotizacion/cierre", "#divCreateModal", "#nuevo-form", function (response) {
            if (response == true) {
                $("#cierreCotizacion").modal("hide");
                location.reload();
            }
        });
    }
}