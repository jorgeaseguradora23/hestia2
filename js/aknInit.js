$(".fileSelectBoton").hide();
$('.fileDownBoton').prop('readOnly', true);
$(".sendForm").one('click', function (event) {
    event.preventDefault();
    //do something
    // $('form#gasto-form').submit();
    var padreSuperior=$(this).closest('form');
    $(this).prop('disabled', true);
    $(this).html("Por favor, espere.");
    $(padreSuperior).submit();
});

$(".fileUpBoton").on('click', function (event) {
    event.preventDefault();
    $('.fileSelectBoton').click();
});
$(".fileSelectBoton").on('change', function (event) {
    event.preventDefault();
    $(".fileDownBoton").val($(this).val());
});