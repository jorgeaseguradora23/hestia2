/**
 *
 * @param idTable
 * @param ordering
 * @param length
 * @param aaSorting array [[3, "asc"], [2, "asc"], [4, "asc"], [5, "asc"]]
 */
var errorRecusosInsuficientes="";

function setLoad(div){
    $(div).html("<table id='loadDiv' width=\"100%\" >\n" +
        "                        <tr>\n" +
        "                            <td valign='middle' align=\"center\">\n" +
        "                                <img src=\"" + pathUrl + "/styles/assets/img/loader.svg\" width='44' height='44' alt=\"Loader\"><br>\n" +
        "                                <small class=\"text-primary\">\n" +
        "                                    Cargando\n" +
        "                                </small>\n" +
        "                            </td>\n" +
        "                        </tr>\n" +
        "                    </table>");


    if($( "#loadDiv" ).parent().height()<100){
        $("#loadDiv").height(250);
    }else{
        $("#loadDiv").height($( "#loadDiv" ).parent().height());
    }

}

function setDatatable(idTable, ordering = true, length = [10, 25, 50, 100], aaSorting = [],
                      searching = true, paging = true, info = true, color="danger") {
    if (length.length == 0) {
        length = [10, 25, 50, 100];
    }

    $(idTable).DataTable(
            {
                "ordering": ordering,
                "searching": searching,
                "paging": paging,
                "info": info,
                "lengthMenu": length,
                "pageLength": length[0],

                oLanguage: {
                    oAria: {
                        sSortAscending: ": activate to sort column ascending",
                        sSortDescending: ": activate to sort column descending"
                    },
                    oPaginate: {
                        sFirst: "Primero",
                        sLast: "Último",
                        sNext: "Siguiente",
                        sPrevious: "Anterior"
                    },
                    sEmptyTable: "No data available in table",
                    sInfo: "Mostrando del _START_ al _END_ de _TOTAL_ registros totales",
                    sInfoEmpty: "Mostrando del 0 al 0 de 0 registros",
                    sInfoFiltered: "(filtrado sobre _MAX_ registros totales)",
                    sLengthMenu: "Mostrar  _MENU_  registros por página",
                    sLoadingRecords: "Loading...",
                    sProcessing: "Processing...",
                    sSearch: "Filtrar por:",
                    sSearchPlaceholder: "Indicio de busqueda",
                    sZeroRecords: "No se encontraron coincidencias"
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                    if (iDisplayIndex % 2) {
                        $('td', nRow).addClass('bg-row-white');
                    } else {
                        $('td', nRow).addClass('bg-row-' + color);

                    }
                    $('th').addClass("th-header bg-th-" + color);

                },

            }
    );
    $(idTable).removeClass("dataTable");
    $(idTable).removeClass("akDataTable");

}

function setModel(objectIn, actionCompleteIn = function () {}, id = "", type = false, urlReturn = "") {
    if (!objectIn) {
        alert("Fallo envio de objeto / parametro!");
    }
    var data = "";
    data = "id=" + id + "&";
    var div = $(objectIn).data("div");
    var action = $(objectIn).data("action");
    var bolValida = true;
    var url = $(objectIn).data("url");


    if (action) {

        if (type == false) {//type== TRUE para formularios con archivo

            if ($(action).prop("tagName") === 'FORM') {
                data += $(action).closest('form').serialize();
            }
        } else {
            var form = $(action)[0];
            var data = new FormData(form);
        }

        bolValida = false;
        swal({
            title: "¿Estas Seguro?",
            text: "La información se registrará de manera permanente sin marcha atrás!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: 'Si, Adelante!',
            cancelButtonText: 'Cancelar',
            closeOnConfirm: true //closeOnCancel: false

        }, function () {
            if (type == false) {
                getAjaxData(objectIn, actionCompleteIn);
            } else {
                executeAjaxMulti(url, data, div, objectIn, actionCompleteIn, urlReturn);
            }
        });

       
    }
    if (bolValida === true) {
        if (type == false) {
            getAjaxData(objectIn, actionCompleteIn);
        } else {
            executeAjaxMulti(url, data, div, objectIn, actionCompleteIn);
        }

    }
}

function getAjaxData(objectIn,actionSuccessComplete = function (){}, lenght = [10, 20, 50, 100], language = "es",loadingShow=true, actionBeforeSend = function (){},ordening=true) {
    if (!objectIn) {
        alert("Fallo envio de objeto / parametro!");
    }

    var div = $(objectIn).data("div");
    var url = $(objectIn).data("url");
    var action = $(objectIn).data("action");
    var data = "";
    if (action) {
        if($(action).prop("tagName") === 'FORM'){
            data = $(action).serialize();
        }else{
            data = "id=" + action;
        }
    }

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function () {
            actionBeforeSend();
            if (loadingShow == true) {
                setLoad(div);
            }
        },
        success: function (response) {
            $(div).html(response);
            actionSuccessComplete(response);
            setDatatable(".akDataTable", ordening, lenght);
            $(".SlectBox").SumoSelect();
            initDropify(".dropify");
        }
    });
}



function setModelfilesUp(objectIn, actionSuccess = function (){},id = "", beforeAction = function (){}) {
    if (!objectIn) {
        alert("Fallo envio de objeto / parametro!");
    }
    var data = "";
    data = "id=" + id + "&";

    var div = $(objectIn).data("div");
    var action = $(objectIn).data("action");
    var bolValida = true;
    var url = $(objectIn).data("url");
    if (action) {
        var form = $(action)[0];
        data = new FormData(form);

        bolValida = false;
        swal({
            title: "¿Estas Seguro?",
            text: "La información se registrará de manera permanente sin marcha atrás!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: 'Si, Adelante!',
            cancelButtonText: 'Cancelar',
            closeOnConfirm: true //closeOnCancel: false

        }, function () {
            executeAjaxMulti(url, data, div, objectIn,actionSuccess);
        });


        if (bolValida === true) {
            executeAjaxMulti(url, data, div, objectIn,actionSuccess);
        }
    }else{
        executeAjaxMulti(url, data, div, objectIn,actionSuccess);
    }



}
function executeAjaxMulti(url, data, div, objectIn, fnaccion = function () {},beforeAction = function (){}) {

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        enctype: 'multipart/form-data',
        processData: false,  // Important!
        contentType: false,
        cache: false,
        beforeSend: function () {
            setLoad(div);
            beforeAction();
        },
        success: function (response) {

            if (response == true) {
                fnaccion(response);

            } else {
                $(div).html(response);
                $(".SlectBox").SumoSelect();
                initDropify(".dropify");
            }
        }
    });
}
function msgCompleto() {

}


function alertas(type, title, message, duracion = 3000) {
    // alert-danger -> blue PROCESO DE INSERT O UPDATE COMPLETO
    // success -> green PROCESO DE BUSQUEDA COMPLETO COMPLETO
    // alert -> red // ERROR
    $(".notification-popup").removeClass("hide").removeClass("alert-danger").removeClass("alert").removeClass("success");
    $(".notification-popup").addClass(type);
    $(".notification-popup p .task").empty().append(title);
    $(".notification-popup p .notification-text").empty().append(message);
    setTimeout(function () {
        $(".notification-popup").addClass("hide");
    }, duracion);
}
function showToastNotification(response) {
    if (response != false && response != undefined) {
        alertas("alert-danger",
            "<i class=\"fas fa-exclamation-circle \" style='margin-right: 10px;'></i> Atención",
            "Tienes nuevas notificaciones que debes atender a la brevedad. Presiona en la campana para revisarlas!",
            2000);
    }
}
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
var navNotification = function (setExtraParameters = {}, onClickFunction = function(){}, onCloseFunction = function(){}) {
    if (Notification) {
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }
        var defaultValues = {
            title: 'Junghanns',
            icon: "https://junghanns.app/system/template/assets/img/favicon.png",
            body: "Notificación de prueba en Xitrus"
        };

        var extra = Object.assign(defaultValues, setExtraParameters);
        var noti = new Notification(extra.title, {icon: extra.icon, body: extra.body});

        noti.onclick = onClickFunction;
        noti.onclose = onCloseFunction;

        setTimeout(function () {
            noti.close();
        }, 10000);
}
}

function getAjaxExcel(data, url, title, color) {

    $.ajax({
        type: "POST",
        url: url,
        data: {
            color: 'info',
            title: title,
            data: JSON.stringify(data)
        },
        beforeSend: function () {
//            $(div).html("<table height='500px;' width=\"100%\">\n" +
//                    "    <tr align=\"center\">\n" +
//                    "     <td><div class='row' style='text-align: center'><img class='center' width='136px' style='display: block; margin-left: auto; margin-right: auto;' src=\" "+baseUrlAx+"../img/load.gif\"></div> " +
//                    "<span class=\"text-success\" style=\"font-size: 14px;  \"><strong>PROCESANDO</strong></span></td>\n" +
//                    "    </tr>\n" +
//                    "</table>");
        },
        success: function (data) {

            if (IsJsonString(data)) {
                var response = $.parseJSON(data);
                if (response.hasOwnProperty('status') && response.status === 1) {
                    window.open(response.file, '_blank');
                } else {
                    alert("Error al bajar el archivo");
                }
            }
        }
    });

}
/*
LIMITADOR DE CARACTERES EN CAMPOS INPUT
 */
$(document).ready(function (){

    $("input").keypress(function (e){
        var objetoInput=$(this);
        if(objetoInput.data("format")){
            if(objetoInput.data("format")=="tel"){
                if(objetoInput.val().length>=14){
                    return false;
                }else{
                    var respuesta=maskTel(objetoInput,e);

                    return respuesta;
                }

            }
        }
        if(objetoInput.data("max")){
            if(objetoInput.val().length>=objetoInput.data("max")){
                return false;
            }
        }

    });
});

function currencyFormat(num) {
    return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
function numberFormar(num) {
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

$(document).ready(function(){
    $("input").keydown(function (e){
        // Capturamos qué telca ha sido
        var keyCode= e.which;
        // Si la tecla es el Intro/Enter
        if (keyCode == 13){
            // Evitamos que se ejecute eventos
            event.preventDefault();
            // Devolvemos falso
            return false;
        }
    });
})

function initMap(latitud,longitud,idMapa) {

    // Creamos un objeto mapa y especificamos el elemento DOM donde se va a mostrar.
    var map = new google.maps.Map(document.getElementById(idMapa), {
        center: {lat: latitud , lng: longitud},
        scrollwheel: false,
        zoom: 18,
        zoomControl: true,
        rotateControl : false,
        mapTypeControl: false,
        streetViewControl: false,
    });


    // Creamos el marcador
    var marker = new google.maps.Marker({
        position: {lat: latitud, lng: longitud},
        draggable: false
    });

    // Le asignamos el mapa a los marcadores.
    marker.setMap(map);


}

/*Se puede generar de la siguiente forma tambien*/
//var $a = $("<a>");
//var response = JSON.parse(data);
//$a.attr("href", response.file);
//$("body").append($a);
//$a.attr("download", "file.xls");
//$a[0].click();
//$a.remove();

function initDropify(object){
    $(object).dropify({
        tpl: {
            wrap:            '<div class="dropify-wrapper"></div>',
            loader:          '<div class="dropify-loader"></div>',
            message:         '<div class="dropify-message"><span class="file-icon" /> <p>Arrastra un archivo aquí o presiona click</p></div>',
            preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">Arrastra o presiona click para reemplazar</p></div></div></div>',
            filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
            clearButton:     '<button type="button" class="dropify-clear">REMOVER</button>',
            errorLine:       '<p class="dropify-error">{{ error }}</p>',
            errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
        }
    });
}

function openModal(objeto){
    $(objeto).removeAttr('class');
    $(objeto).attr('class', 'modal');
    var efecto=Math.floor((Math.random() * 11) + 1);
    var efectos=[
        "effect-flip-horizontal",
        "effect-newspaper",
        "effect-scale",
        "effect-slide-in-right",
        "effect-slide-in-bottom",
        "effect-fall",
        "effect-flip-vertical",
        "effect-super-scaled",
        "effect-sign",
        "effect-rotate-bottom",
        "effect-rotate-left",
    ]
    $(objeto).addClass(efectos[efecto-1]);
    $(objeto).modal("show");
}

function getDatosAjax(url,divResultado,formulario,funcionSuccess){
    var objetoMov=$("<object></object>")
    objetoMov.data("url", baseUrlAx +url);
    objetoMov.data("div", divResultado);
    objetoMov.data("action", formulario);
    getAjaxData(objetoMov,function (){
        funcionSuccess();
    });
}

function setDatosAjax(url,divResultado,formulario,funcionSuccess,id=""){
    var objetoMov=$("<object></object>")
    objetoMov.data("url", baseUrlAx +url);
    objetoMov.data("div", divResultado);
    objetoMov.data("action", formulario);
    setModel(objetoMov,
        funcionSuccess
        , id);
}